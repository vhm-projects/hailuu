function isNumberKey(evt){
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(document).ready(function () {

	const swiper = new Swiper(".slide-banner", {
        pagination: {
			el: ".swiper-pagination",
			clickable: true,
        },
	});

	const swiperCustomer = new Swiper(".swiper-customer", {
        slidesPerView: 2,
        spaceBetween: 30,
        // slidesPerColumn: 1,
        // autoHeight: true,
        effect: 'slide',
        pagination: {
			el: ".swiper-pagination",
			clickable: true,
        },

   //      breakpoints: {
			// 375: {
			// 	slidesPerColumn: 2,
			// 	slidesPerView: 2,
		 //        spaceBetween: 30,
			// },
   //      }
	});

	$('.wrapper-flash-sale, .wrapper-new-product').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  prevArrow: `<button class="btn-slick-previous"><img class='slick-next' src='./assets/images/chevron-thin-left.png' /></button>`,
      nextArrow:`<button class="btn-slick-next"><img class='slick-next' src='./assets/images/chevron-thin-right.png' /></button>`,
	  // mobileFirst: true,
	  adaptiveHeight: true,
	  responsive: [
	  	{
	      breakpoint: 1270,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4
	      }
	    },
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 850,
	      settings: {
	        slidesToShow: 2.5,
	        slidesToScroll: 2.5
	      }
	    },
	    {
	      breakpoint: 800,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 660,
	      settings: {
	        slidesToShow: 1.5,
	        slidesToScroll: 1.5
	      }
	    },
	    // {
	    //   breakpoint: 375,
	    //   settings: {
	    //     slidesToShow: 1,
	    //     slidesToScroll: 1
	    //   }
	    // }
	  ]
	});
 
 	$('.toggle-contact').on('click', function() {
 		const iconToggle = $(this).find('img');
 		const currentSrc = iconToggle.attr('src');
 		const closeSrc 	 = './assets/images/close.png';
 		const gridSrc	 = './assets/images/grid.png';

 		if(currentSrc === closeSrc){
 			$('.menu-contact').hide();
	 		iconToggle.attr('src', gridSrc);
 		} else{
 			$('.menu-contact').show();
	 		iconToggle.attr('src', closeSrc);
 		}
 	})

 	$('.container-list-service-v2 .list-service-item-v2').on('click', function(){
 		const popupDetailProject 	= $('.popup-detail-project');
 		const imgSrc				= $(this).find('img').attr('src');
 		const title 				= $(this).data('title');
 		const description 			= $(this).data('description');
 		const price 				= $(this).data('price');

 		popupDetailProject.find('img').attr('src', imgSrc);
 		popupDetailProject.find('.popup-content b').text(title);
 		popupDetailProject.find('.popup-content p').text(description);
 		popupDetailProject.find('.popup-content span').text(price);

 		popupDetailProject.addClass('show');
 	})

 	// ================= SCRIPT MENU HEADER ===================

 	$('.btn-show-menu-category').on('click', function(){
 		$('.header-3 .wrapper-column-menu-service').toggleClass('d-none');
 		$(this).toggleClass('active');
 	})

 	$('.header-sub-category a').on('click', function(){
 		$('.header-3 .wrapper-column-menu-child-service').toggleClass('d-none')
 		$(this).toggleClass('active');
 	})

 	$('.wrapper-cart a, .btn-popup-cart').on('click', function(){
 		$(this).toggleClass('active');
 		$('.wrapper-menu-popup-cart').toggleClass('d-none');
 	})

 	$('.btn-cancel-update-my-cart').on('click', function(){
 		$(this).closest('.wrapper-menu-popup-edit-cart').addClass('d-none');
 		$(this)
 			.closest('.wrapper-menu-popup-cart__product')
 			.find('.wrapper-menu-popup-cart__product-detail a')
 			.text('Chỉnh sửa')
 			.removeClass('active');
 	})

 	$('.wrapper-menu-popup-cart__product .wrapper-menu-popup-cart__product-detail a').on('click', function(){
 		const isActive = $(this).hasClass('active');

 		$(this).text(isActive ? 'Chỉnh sửa' : 'Đang chỉnh sửa').toggleClass('active');
 		$(this).closest('.wrapper-menu-popup-cart__product').find('.wrapper-menu-popup-edit-cart').toggleClass('d-none');
 	})

 	$('.popup').on('click', function(){
 		$(this).removeClass('show');
 	})

 	$('.input-search').on('focus', function(){
 		$('.wrapper-list-result-search').toggleClass('show');
 	})


 	// ================= SCRIPT MODAL ===================

	$('.btn-signin').on('click', function() {
		$('.modal-signin').addClass('show');
	})

 	$('.btn-signup').on('click', function() {
 		$('.modal-signup').addClass('show');
 	})

	$('.btn-forgot-password').on('click', function() {
		$('.modal-forgot-password').addClass('show');
	})

	$('.btn-create-new-password').on('click', function() {
		$('.modal-create-new-password').addClass('show');
	})

	$('.btn-send-otp').on('click', function() {
		$('.modal-send-otp').addClass('show');
	})

 	$('.btn-close-modal').on('click', function() {
 		$(this).closest('.modal-custom').removeClass('show');
 	})

 	$('.input-otp').on('keypress', function(e){
 		return isNumberKey(e) && $(this).val().length < 1;
 	})

});
