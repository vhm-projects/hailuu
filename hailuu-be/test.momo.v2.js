const uuidv1 = require('uuid').v1;
const https = require('https');
const crypto = require('crypto');
const request = require('request');
const { randomStringUpperCaseAndNumber } = require('./www/utils/string_utils');

//parameters send to MoMo get get payUrl

var partnerCode = "MOMO"
var accessKey = "F8BBA842ECF85"
var serectkey = "K951B6PE1waDMi640xX08PD3vg6EkVlz"

// var partnerCode = "MOMOIQA420180417"
// var accessKey = "Q8gbQHeDesB2Xs0t"
// var serectkey = "PPuDXq1KowPT1ftR8DvlQTHhC03aul17"

var orderInfo = "pay with MoMo vhm-102"
var redirectUrl = "http://localhost:5001/url-return"
var ipnUrl = "http://localhost:5001/ipn-return"
var amount = "1000"
var orderId = randomStringUpperCaseAndNumber(20)
var requestId = randomStringUpperCaseAndNumber(20)
var requestType = "captureWallet"
var extraData = 'email=abc@gmail.com' || "eyJ1c2VybmFtZSI6ICJtb21vIn0="
// var extraData = "merchantName=;merchantId=" //pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store

//before sign HMAC SHA256 with format

// accessKey=$accessKey&amount=$amount&extraData=
// $extraData&ipnUrl=$ipnUrl&orderId=$orderId&orderInfo=
// $orderInfo&partnerCode=$partnerCode&redirectUrl=$redirectUrl
// &requestId=$requestId&requestType=$requestType

var rawSignature = `accessKey=${accessKey}&amount=${amount}&extraData=${extraData}&ipnUrl=${ipnUrl}&orderId=${orderId}&orderInfo=${orderInfo}&partnerCode=${partnerCode}&redirectUrl=${redirectUrl}&requestId=${requestId}&requestType=${requestType}`

//puts raw signature
console.log("--------------------RAW SIGNATURE----------------")
console.log(rawSignature)
//signature

var signature = crypto.createHmac('sha256', serectkey)
                   .update(rawSignature)
                   .digest('hex');
console.log("--------------------SIGNATURE----------------")
console.log(signature)

//json object send to MoMo endpoint
var body = JSON.stringify({
    "partnerCode": partnerCode,
    "accessKey": accessKey,
    "requestId": requestId,
    "amount": amount,
    "orderId": orderId,
    "orderInfo": orderInfo,
    "ipnUrl": ipnUrl,
    "redirectUrl": redirectUrl,
    "extraData": extraData,
    "requestType": requestType,
    "signature": signature,
	"lang": "vi"
})

//Create the HTTPS objects
var options = {
  hostname: 'test-payment.momo.vn',
  port: 443,
  path: '/v2/gateway/api/create',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(body)
 }
};

//Send the request and get the response
console.log("Sending....")
var req = https.request(options, (res) => {
  console.log(`Status: ${res.statusCode}`);
  console.log(`Headers: ${JSON.stringify(res.headers)}`);
  res.setEncoding('utf8');

  let body = '';
  res.on('data', (chunk) => {
    try {
		body += chunk;
		
		// const responseData = JSON.parse(body);

		// console.log({ __RESEPONSE_DATA: responseData });
		// console.log({ __PAY_URL: responseData.payUrl });
	} catch (error) {
		console.log(error);
	}
  });
  res.on('end', () => {
	console.log({ __BODY: body });
	const responseData = JSON.parse(body);

	console.log({ __RESEPONSE_DATA: responseData });
	console.log({ __PAY_URL: responseData.payUrl });
    console.log('No more data in response.');
  });
});

req.on('error', (e) => {
  console.log(`problem with request: ${e.message}`);
});

// write data to request body
req.write(body);
req.end();
