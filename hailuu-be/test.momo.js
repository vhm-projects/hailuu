const uuidv1 = require('uuid').v1;
const https = require('https');
const crypto = require('crypto');
const { randomStringUpperCaseAndNumber } = require('./www/utils/string_utils');

//parameters send to MoMo get get payUrl
var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
var hostname = "https://test-payment.momo.vn"
var path = "/gw_payment/transactionProcessor"

var partnerCode = "MOMO"
var accessKey = "F8BBA842ECF85"
var serectkey = "K951B6PE1waDMi640xX08PD3vg6EkVlz"

// var partnerCode = "MOMOIQA420180417"
// var accessKey = "Q8gbQHeDesB2Xs0t"
// var serectkey = "PPuDXq1KowPT1ftR8DvlQTHhC03aul17"

var orderInfo = "pay with MoMo vhm-102"
var returnUrl = "http://localhost:5001/url-return"
var notifyurl = "https://store.vtctelecom.com.vn/beta/api/store/postipn?SecretKey=31e31575-1a02-4826-9d6a-dd4dbec85f96&ApiKey=16f71ead-e510-4092-aa47-c9e0d24b89fb"
var amount = "1000"
var orderId = randomStringUpperCaseAndNumber(20)
var requestId = randomStringUpperCaseAndNumber(20)
var requestType = "captureMoMoWallet"
var extraData = "eyJ1c2VybmFtZSI6ICJtb21vIn0="
// var extraData = "merchantName=;merchantId=" //pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store

//before sign HMAC SHA256 with format
//partnerCode=$partnerCode&accessKey=$accessKey&requestId=$requestId&amount=$amount&orderId=$oderId&orderInfo=$orderInfo&returnUrl=$returnUrl&notifyUrl=$notifyUrl&extraData=$extraData

var rawSignature = `partnerCode=${partnerCode}&accessKey=${accessKey}&requestId=${requestId}&amount=${amount}&orderId=${orderId}&orderInfo=${orderInfo}&returnUrl=${returnUrl}&notifyUrl=${notifyurl}&extraData=${extraData}`


//puts raw signature
console.log("--------------------RAW SIGNATURE----------------")
console.log(rawSignature)
//signature

var signature = crypto.createHmac('sha256', serectkey)
                   .update(rawSignature)
                   .digest('hex');
console.log("--------------------SIGNATURE----------------")
console.log(signature)

//json object send to MoMo endpoint
var body = JSON.stringify({
    partnerCode : partnerCode,
    accessKey : accessKey,
    requestId : requestId,
    amount : amount,
    orderId : orderId,
    orderInfo : orderInfo,
    returnUrl : returnUrl,
    notifyUrl : notifyurl,
    extraData : extraData,
    requestType : requestType,
    signature : signature,
})
//Create the HTTPS objects
var options = {
  hostname: 'test-payment.momo.vn',
  port: 443,
  path: '/gw_payment/transactionProcessor',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(body)
 }
};

//Send the request and get the response
console.log("Sending....")
var req = https.request(options, (res) => {
  console.log(`Status: ${res.statusCode}`);
  console.log(`Headers: ${JSON.stringify(res.headers)}`);
  res.setEncoding('utf8');
  res.on('data', (body) => {
    try {
		console.log({ __BODY: body });
		const responseData = JSON.parse(body);

		console.log({ __RESEPONSE_DATA: responseData });
		console.log({ __PAY_URL: responseData.payUrl });
	} catch (error) {
		console.log(error);
	}
  });
  res.on('end', () => {
    console.log('No more data in response.');
  });
});

req.on('error', (e) => {
  console.log(`problem with request: ${e.message}`);
});

// write data to request body
req.write(body);
req.end();