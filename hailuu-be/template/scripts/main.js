// let mediaQueryPc = window.matchMedia("screen and (min-width: 1200px)");
// console.log({ mediaQueryPc })

const languageDataTable = {
	"language": {
		"lengthMenu": "Hiển thị _MENU_ kết quả",
		"search": "Tìm kiếm",
		"zeroRecords": "Không tìm thấy kết quả trùng khớp",
		"info": "Trang _PAGE_ tổng _PAGES_ kết quả",
		"infoEmpty": "Không tìm thấy kết quả",
		"infoFiltered": "(lọc từ _MAX_ kết quả hiện có)",
		"paginate": {
			"first":      "Trang đầu tiên",
			"last":       "Trang cuối cùng",
			"next":       "Trang tiếp",
			"previous":   "Trang trước"
		},
	},
}

async function addFileToUpload(multiInput, cbDone = null, cbProgress = null, elementContainer = '') {
	isDoneUpload = false;
	let arrUrlPromise 	= [];
	let totalFile 		= 0;
	console.log({ multiInput })

	// Thêm url vào mảng arrUrlPromise
	if(multiInput.length === 1){
		const url = generateLinkS3({ file: multiInput[0] });

        arrUrlPromise = [url];
		totalFile 	  = 1;
	}else if(multiInput.length == undefined){
		const url = generateLinkS3({ file: multiInput });

        arrUrlPromise = [url];
		totalFile 	  = 1;
	} else {
		for (const input of multiInput) {
            if(input.files && input.files.length){
                for (const file of input.files) {
                    const url = generateLinkS3({ file, type: input.type });
                    arrUrlPromise = [...arrUrlPromise, url];
                    totalFile++;
                }
            } else{
                let url = generateLinkS3({ file: input, type: input.type });
                arrUrlPromise = [...arrUrlPromise, url];
                totalFile++;
            }
		}
	}

	// Generate link S3 -> get list link upload -> upload S3 async
	const listUrl = await Promise.all(arrUrlPromise);
	listUrl.length && listUrl.map(link => {
		console.log({ link });
		const { file, uri, signedUrl, type } = link;
		uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress });
	})

	return () => listUrl;
}

function initEditor(selector, options = {}, cb = null) {
    tinyMCE.init({
        selector,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor codesample',
            'searchreplace visualblocks code fullscreen quickbars hr nonbreaking pagebreak',
            'insertdatetime media table paste imagetools wordcount emoticons' // formatpainter powerpaste
        ],
        toolbar: 'undo redo | styleselect | bold italic | forecolor backcolor |' +
                'alignleft aligncenter alignright alignjustify |' +
                'outdent indent | numlist bullist | emoticons nonbreaking | fullscreen',
        menubar: 'file edit insert view format table tools custom',
        advlist_bullet_styles: 'default,square,circle,disc',
        quickbars_selection_toolbar: 'bold italic | forecolor backcolor | formatselect | quicklink blockquote',
        quickbars_insert_toolbar: 'nonbreaking | quicktable | numlist bullist | outdent indent | hr pagebreak | emoticons', // quickimage
        menu: {
            custom: { title: 'Lists', items: 'customBulletLine customBulletPlus' }
        },
        nonbreaking_force_tab: true,
        lists_indent_on_tab: true,
        statusbar: true,
        draggable_modal: true,
        branding: false,
        fullscreen_native: true,
        height: 250,
        toolbar_mode: 'floating',
        placeholder: 'Type here...',
        default_link_target: '_blank',
        paste_word_valid_elements: "b,strong,i,em,h1,h2",
        paste_enable_default_filters: false,
        mobile: {
            resize: false
        },
        image: null,
        // paste_preprocess: function(plugin, args) {
        // 	args.content += ' ';
        // },
        // indent_use_margin: true,
        // image_title: true,
        // file_picker_types: 'file image',
        // images_file_types: 'jpeg,jpg,png,bmp,webp',
        // paste_as_text: true,
        paste_data_images: false,
        file_picker_callback: function(cb, value, meta){
            let input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            let that = this;

            input.onchange = function () {
                let file = this.files[0];
                that.image = file;

                let reader = new FileReader();
                reader.onload = function (e) {
                    let id = 'blobid' + (new Date()).getTime();
                    let blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    let base64 = reader.result.split(',')[1];
                    let blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    // console.log({ file, blobCache, base64, blobInfo, uri: blobInfo.blobUri() });

                    /* call the callback and populate the Title field with the file name */
                    cb(blobInfo.blobUri(), { title: file.name, alt: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
            input.remove();
        },
        setup: function(editor){
            editor.on('OpenWindow', e => {
                $('.tox-button[title="Save"]').off('click').on('click', async function(){
					if(e.target.image){
						$(this).prop('disabled', true);
						$(this).addClass('d-flex');
						$(this).html(`
							<img src="../../../../template/images/icon-loading.gif" style="width:20px;height:20px;margin-right:5px;margin-top:2px" /> Save
						`);

						const image = e.target.image;
						await addFileToUpload([image], links => {
							$(this).prop('disabled', false);
							e.target.windowManager.close();

							if(links && links.length){
								const uri = links[0].uri;

								tinyMCE.activeEditor.execCommand('mceInsertContent', false, `
									<img src="${uri}" width="300" height="239" alt=${uri} />
								`);
							}

						})
				
					}
				})
            })

            editor.on('KeyDown', e => {
                // console.log({ __code: e.keyCode, char, currentChar, __key: e.key });

                if ((e.keyCode == 8 || e.keyCode == 46) && editor.selection) { // delete & backspace keys
                    const selectedNode = editor.selection.getNode(); // get the selected node (element) in the editor

                    if (selectedNode && selectedNode.nodeName == 'IMG') {
                        const { src, title, alt } = selectedNode;
						console.log({ src, title, alt });
                    }
                }

                if (e.keyCode === 9) { // tab pressed
                    if (e.shiftKey) {
                        editor.execCommand('Outdent');
                    }else {
                        editor.execCommand('Indent');
                    }

                    e.preventDefault();
                    return false;
                }
            });

            editor.ui.registry.addMenuItem('customBulletLine', {
                text: 'Bullet line',
                icon: 'horizontal-rule',
                tooltip: 'Insert Bullet Line',
                onAction: function (_) {
                    editor.execCommand('mceInsertContent', false, `<li style="list-style-type: '- '"> </li>`);
                }
            });

            editor.ui.registry.addMenuItem('customBulletPlus', {
                text: 'Bullet plus',
                icon: 'plus',
                tooltip: 'Insert Bullet Plus',
                onAction: function (_) {
                    editor.execCommand('mceInsertContent', false, `<li style="list-style-type: '+ '"> </li>`);
                }
            });

            // editor.on('Paste', e => {
            // 	console.log({ __pasteEvent: e });
            // })

            // editor.on('Undo', e => {
            // 	console.log({ e });
            // 	const selectedNode = editor.selection.getNode();
            // 	if (selectedNode && selectedNode.nodeName == 'IMG') {
            // 		console.log('Undo: ', { __node: selectedNode.nodeName });
            // 		e.target.undoManager.clear();
            // 	}
            // })

            if(cb && {}.toString.call(cb) === '[object Function]'){
                cb(editor);
            }
        },
        ...options,
    });
}

function change_alias(alias) {
	let str = alias;
	str = str.toLowerCase();
	str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
	str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
	str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
	str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
	str = str.replace(/"/g, '')
	str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
	str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
	str = str.replace(/đ/g,"d");
	str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
	str = str.replace(/ + /g," ");
	str = str.trim(); 
	return str;
}

let convertToSlug = plainText => {
	let text_converted_alias = change_alias(plainText);
	let text_split_with_space = text_converted_alias.split(' ');
	let text_joined = text_split_with_space.join('-');
	return text_joined;
}

function readURL() {
	if (this.files && this.files[0]) {
		let reader = new FileReader();
		reader.onload = function(e) {
			console.log(e.target.result);
			$('#imagePreview').css('background-image', `url('${e.target.result}')`);
			$('#imagePreview').hide();
			$('#imagePreview').fadeIn(650);
		}
		reader.readAsDataURL(this.files[0]);
	}
}
function validatePhone(phone) {
	let phone_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
	return phone_regex.test(phone);
}
function readURLAvatar(input) {
	if ( input.files && input.files[0] ) {
		for (let  i = 0; i < input.files.length; i++){
			let src = URL.createObjectURL(input.files[i]);
			$("#imagePreview").attr("style", `background-image: url(${src});`)
            $("#imagePreview").attr("data-src", `${src}`);
			$('#imagePreview').fadeIn(650);
			return src;
		}
	}
}

function checkValidate(params) {
	let { title, slug, category, content } = params;

	if(!title) return { error: true, msg: 'Bạn cần nhập tiêu đề cho bài viết' };
	if(!slug) return { error: true, msg: 'Bạn cần nhập slug cho bài viết' };
	if(!category) return { error: true, msg: 'Bạn cần nhập danh mục cho bài viết' };
	if(!content) return { error: true, msg: 'Bạn cần nhập nội dung cho bài viết' };

	return { error: false, msg: 'not_problem' };
}



// =================== UPLOAD S3 ======================= 👀
// ======================= UPLOAD S3 =========================
let arrLinksUploaded = [];
let isDoneUpload 	 = true;
let countDone 		 = 0;

window.onbeforeunload = function() {
	if(!isDoneUpload){
		return isDoneUpload;
	} else return;
};


function generateLinkS3({ file, type = '' }) {
	return new Promise(resolve => {
		const { type: contentType, name: fileName } = file;
		$.get(
			`${location.origin}/generate-link-s3?fileName=${fileName}&type=${contentType}`, 
			signedUrl => resolve({ 
				signedUrl: signedUrl.linkUpload.data,
				uri: `https://s3-ap-southeast-1.amazonaws.com/ldk-software.hailuu/root/${signedUrl.fileName}`,
				file,
				type
			})
		);
	})
}

function uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress }) {
	$.ajax({
		url: signedUrl.url,
		type: 'PUT',
		dataType: 'html',
		processData: false,
		headers: { 'Content-Type': file.type },
		crossDomain: true,
		data: file,
		xhr: function() {
			let myXhr = $.ajaxSettings.xhr();

			myXhr.upload.onprogress = function(e) {
				console.log(Math.floor(e.loaded / e.total * 100) + '%');
			};

			if(myXhr.upload){
				if({}.toString.call(cbProgress) === '[object Function]'){
					myXhr.upload.addEventListener('progress', e => {
						if(e.lengthComputable){
							let max = e.total;
							let current = e.loaded;
							let percentage = (current * 100)/max;
							cbProgress(percentage, type);
						}
					}, false);
				} else{
					myXhr.upload.addEventListener('progress', progress, false);
				}
			}

			return myXhr;
		},
	}).done(function(){
		previewUpload({ elementContainer, uri });
		countDone++;
		arrLinksUploaded = [...arrLinksUploaded, {
			uri,
			type
		}]

		if(countDone === totalFile){
			// Check is function
			({}.toString.call(cbDone) === '[object Function]') && cbDone(arrLinksUploaded);

			arrLinksUploaded = [];
			isDoneUpload = true;
			countDone = 0;
		}
	}).fail(function (error) {
		console.error(error);
	});
}

function progress(e){
	if(e.lengthComputable){
		let max = e.total;
		let current = e.loaded;
		let percentage = (current * 100)/max;
		$('.progress').css({ width: parseInt(percentage) + '%' });
	}
}

function previewUpload({ uri, elementContainer }) {
	const container = $(elementContainer);
	if(container.length){
		const img = `<img src="${uri}" alt="Img Preview" />`;
		container.append(img);
	}
}





$(document).ready(function() {

	$('.fancybox').fancybox(); 

	if($(window).width() < 1025){
	  $(".admin-back-icon").css({
		display: "none"
	  })
	}else{
	   $(".admin-back-icon").css({
		display: "block"
	  })
	}
	$("#hidden-menu").on("click", (e)=> {
	  e.preventDefault();
	  $("#hidden-menu").hide();
	  $("body").addClass("enlarged")
	})

	$("#show-menu").on("click", (e)=> {
	  e.preventDefault();
	  $("#hidden-menu").show();
	   $("body").removeClass("enlarged")
	})

	$(".updatePointField").editable({
		type:"text",
		pk:1,
		name:"username",
		title:"Enter username",
		mode:"inline",
		inputclass:"form-control-sm",
		success: function(response, newVal) {
			if (Number.isNaN(Number(newVal))) 
			{
				alert('Vui Lòng Nhập Số');
				return '';
			}
			let userID = $(this).attr('__userID');
			let isFeature = Number(newVal);
			
			$.ajax({
				url: `/admin/update-feature-user/${userID}`,
				method: 'POST',
				data: { amountFeature: isFeature },
				success: function(resp) {
					if (!resp.error) return toastr.info("Cập Nhật Thành Công");
					return alert('Lỗi Cập Nhật, vui lòng thử lại sau!');
				},
				error: function(err){
					console.log(err.message)
					return alert('Lỗi Cập Nhật, vui lòng thử lại sau!');
				}
			});
			return Number(newVal);
		}
	});

	$(".updatePercentCommissionField").editable({
		type:"text",
		pk:1,
		name:"username",
		title:"Nhập Hoa Hồng",
		mode:"inline",
		inputclass:"form-control-sm",
		success: function(response, newVal) {
			if (Number.isNaN(Number(newVal))) 
			{
				alert('Vui Lòng Nhập Số');
				return '';
			}

			if (Number(newVal) > 1) {
				alert('Vui lòng nhập % từ 0.1->1.0');
				return '';
			}

			let userID = $(this).attr('__userID');
			let percentCommission = Number(newVal);
			
			$.ajax({
				url: `/admin/update-percentcommission-user/${userID}`,
				method: 'POST',
				data: { percentCommission },
				success: function(resp) {
					if (!resp.error) return toastr.info("Cập Nhật Thành Công");
					return alert('Lỗi Cập Nhật, vui lòng thử lại sau!');
				},
				error: function(err){
					console.log(err.message)
					return alert('Lỗi Cập Nhật, vui lòng thử lại sau!');
				}
			});
			return Number(newVal);
		}
	});

	

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
});
// GET LINK FROM SERVER 
function getLinkS3(file) {
	return new Promise(async resolve => {
		try {
			let link;
			await addFileToUpload(file, links => {
				link = links[0].uri;
				if(!link)
					return resolve({ error: true, message: 'fail' });
				return resolve({ error: false, data: link });
			});
			
		} catch (error) {
			return resolve({ error: true, message: error.message });
		}
	})
}




