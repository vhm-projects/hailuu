let formatter = new Intl.NumberFormat('en-US');
let phone_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;

function change_alias(alias) {
	let str = alias;
	str = str.toLowerCase();
	str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
	str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
	str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
	str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
	str = str.replace(/"/g, '')
	str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
	str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
	str = str.replace(/đ/g,"d");
	str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
	str = str.replace(/ + /g," ");
	str = str.trim(); 
	return str;
}

function convertToSlug(plainText){
	let text_converted_alias = change_alias(plainText);
	let text_split_with_space = text_converted_alias.split(' ');
	let text_joined = text_split_with_space.join('-');

	return text_joined;
}

function validEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function debounce(fn, delay, immediate) {
	return function(args){
		clearTimeout(fn.id);
		fn.id = setTimeout(fn.bind(this, args), delay);

		if(immediate){
			clearTimeout(fn.id);
			fn.call(this, args);
		}
	}
}


function isNumberKey(evt, that){
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
	// that(9)
	// new KeyboardEvent('keypress',{'key':'Tab'});
    return true;
}

function enableError(inputs, blockInvalid = [], text) {
	inputs.length && inputs.map(input => {
		$(input).addClass('is-invalid');
	})

	if(blockInvalid.length && blockInvalid.hasClass('d-none')){
		blockInvalid.removeClass('d-none').text(text);
	}
}

function disableError(inputs, blockInvalid = []) {
	inputs.length && inputs.map(input => {
		$(input).removeClass('is-invalid');
	})

	if(blockInvalid.length && !blockInvalid.hasClass('d-none')){
		blockInvalid.addClass('d-none').text('');
	}
}

function checkEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function checkPhone(phone){
	var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
	if (phone !== '') {
		if (vnf_regex.test(phone) == false)
			return false;
		return true;
	} else {
		return false;
	}
}

function percentDiscount(priceBefore, priceAfter){
	let ratio = priceAfter/priceBefore*100;
	ratio = (100-ratio);
	return ratio.toFixed()
}

// =================== UPLOAD S3 ======================= 👀
// ======================= UPLOAD S3 =========================
let arrLinksUploaded = [];
let isDoneUpload 	 = true;
let countDone 		 = 0;

window.onbeforeunload = function() {
	if(!isDoneUpload){
		return isDoneUpload;
	} else return;
};

async function addFileToUpload(multiInput, cbDone = null, cbProgress = null, elementContainer = '') {
	isDoneUpload = false;
	let arrUrlPromise 	= [];
	let totalFile 		= 0;
	console.log({ multiInput })

	// Thêm url vào mảng arrUrlPromise
	if(multiInput.length === 1){
		const url = generateLinkS3({ file: multiInput[0] });

        arrUrlPromise = [url];
		totalFile 	  = 1;
	}else if(multiInput.length == undefined){
		const url = generateLinkS3({ file: multiInput });

        arrUrlPromise = [url];
		totalFile 	  = 1;
	} else {
		for (const input of multiInput) {
            if(input.files && input.files.length){
                for (const file of input.files) {
                    const url = generateLinkS3({ file, type: input.type });
                    arrUrlPromise = [...arrUrlPromise, url];
                    totalFile++;
                }
            } else{
                let url = generateLinkS3({ file: input, type: input.type });
                arrUrlPromise = [...arrUrlPromise, url];
                totalFile++;
            }
		}
	}

	// Generate link S3 -> get list link upload -> upload S3 async
	const listUrl = await Promise.all(arrUrlPromise);
	listUrl.length && listUrl.map(link => {
		console.log({ link });
		const { file, uri, signedUrl, type } = link;
		uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress });
	})

	return () => listUrl;
}

function generateLinkS3({ file, type = '' }) {
	return new Promise(resolve => {
		const { type: contentType, name: fileName } = file;
		$.get(
			`${location.origin}/generate-link-s3?fileName=${fileName}&type=${contentType}`, 
			signedUrl => resolve({ 
				signedUrl: signedUrl.linkUpload.data,
				uri: `https://s3-ap-southeast-1.amazonaws.com/ldk-software.hailuu/root/${signedUrl.fileName}`,
				file,
				type
			})
		);
	})
}

function uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress }) {
	$.ajax({
		url: signedUrl.url,
		type: 'PUT',
		dataType: 'html',
		processData: false,
		headers: { 'Content-Type': file.type },
		crossDomain: true,
		data: file,
		xhr: function() {
			let myXhr = $.ajaxSettings.xhr();

			myXhr.upload.onprogress = function(e) {
				console.log(Math.floor(e.loaded / e.total * 100) + '%');
			};

			if(myXhr.upload){
				if({}.toString.call(cbProgress) === '[object Function]'){
					myXhr.upload.addEventListener('progress', e => {
						if(e.lengthComputable){
							let max = e.total;
							let current = e.loaded;
							let percentage = (current * 100)/max;
							cbProgress(percentage, type);
						}
					}, false);
				} else{
					myXhr.upload.addEventListener('progress', progress, false);
				}
			}

			return myXhr;
		},
	}).done(function(){
		previewUpload({ elementContainer, uri });
		countDone++;
		arrLinksUploaded = [...arrLinksUploaded, {
			uri,
			type
		}]

		if(countDone === totalFile){
			// Check is function
			({}.toString.call(cbDone) === '[object Function]') && cbDone(arrLinksUploaded);

			arrLinksUploaded = [];
			isDoneUpload = true;
			countDone = 0;
		}
	}).fail(function (error) {
		console.error(error);
	});
}

function progress(e){
	if(e.lengthComputable){
		let max = e.total;
		let current = e.loaded;
		let percentage = (current * 100)/max;
		$('.progress').css({ width: parseInt(percentage) + '%' });
	}
}

function previewUpload({ uri, elementContainer }) {
	const container = $(elementContainer);
	if(container.length){
		const img = `<img src="${uri}" alt="Img Preview" />`;
		container.append(img);
	}
}

// GET LINK FROM SERVER 
function getLinkS3(file) {
	return new Promise(async resolve => {
		try {
			await addFileToUpload(file, links => {
				let link = links[0].uri;
				if(!link)
					return resolve({ error: true, message: 'fail' });

				return resolve({ error: false, data: link });
			});
		} catch (error) {
			return resolve({ error: true, message: error.message });
		}
	})
}



$(document).ready(function () {

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	new StarRating('.star-rating', {
		tooltip: false,
	});

	new Swiper(".slide-banner", {
        pagination: {
			el: ".swiper-pagination",
			clickable: true,
        },
	});

	new Swiper(".swiper-customer", {
        slidesPerView: 2,
        spaceBetween: 30,
        // slidesPerColumn: 1,
        // autoHeight: true,
        effect: 'slide',
        pagination: {
			el: ".swiper-pagination",
			clickable: true,
        },
        // breakpoints: {
		// 	375: {
		// 		slidesPerColumn: 2,
		// 		slidesPerView: 2,
		//         spaceBetween: 30,
		// 	},
        // }
	});

	$('.wrapper-flash-sale, .wrapper-new-product').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  prevArrow: `<button class="btn-slick-previous"><img class='slick-next' src='/template/hailuu-fe/assets/images/chevron-thin-left.png' /></button>`,
      nextArrow:`<button class="btn-slick-next"><img class='slick-next' src='/template/hailuu-fe/assets/images/chevron-thin-right.png' /></button>`,
	  // mobileFirst: true,
	  adaptiveHeight: true,
	  responsive: [
	  	{
	    //   breakpoint: 1470,
	      breakpoint: 1500,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4
	      }
	    },
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 850,
	      settings: {
	        slidesToShow: 2.5,
	        slidesToScroll: 2.5
	      }
	    },
	    {
	      breakpoint: 800,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 660,
	      settings: {
	        slidesToShow: 1.5,
	        slidesToScroll: 1.5
	      }
	    }
	  ]
	});

	$('.wrapper-order-service__temp').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: `<button class="btn-slick-previous"><img class='slick-next' src='/template/hailuu-fe/assets/images/chevron-thin-left.png' /></button>`,
		nextArrow:`<button class="btn-slick-next"><img class='slick-next' src='/template/hailuu-fe/assets/images/chevron-thin-right.png' /></button>`,
		// mobileFirst: true,
		adaptiveHeight: true,
		responsive: [
		  {
			breakpoint: 1500,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		  },
		  {
			breakpoint: 1270,
			settings: {
			  slidesToShow: 3,
			  slidesToScroll: 3
			}
		  },
		  {
			breakpoint: 1024,
			settings: {
			  slidesToShow: 2.5,
			  slidesToScroll: 2.5
			}
		  },
		  {
			breakpoint: 850,
			settings: {
			  slidesToShow: 2.5,
			  slidesToScroll: 2.5
			}
		  },
		  {
			breakpoint: 800,
			settings: {
			  slidesToShow: 2,
			  slidesToScroll: 2
			}
		  },
		  {
			breakpoint: 660,
			settings: {
			  slidesToShow: 2,
			  slidesToScroll: 2
			}
		  }
		]
	});
	
 	$('.toggle-contact').on('click', function() {
		$('.wrapper-menu-popup-cart').addClass('d-none')
 		const iconToggle = $(this).find('img');
 		const currentSrc = iconToggle.attr('src');
 		const closeSrc 	 = '/template/hailuu-fe/assets/images/close.png';
 		const gridSrc	 = '/template/hailuu-fe/assets/images/grid.png';

 		if(currentSrc === closeSrc){
 			$('.menu-contact').hide();
	 		iconToggle.attr('src', gridSrc);
 		} else{
 			$('.menu-contact').show();
	 		iconToggle.attr('src', closeSrc);
 		}
 	})

 	$('.container-list-service-v2 .list-service-item-v2').on('click', function(){
 		// const popupDetailProject 	= $('.popup-detail-project');
 		// const imgSrc				= $(this).find('img').attr('src');
 		// const title 				= $(this).data('title');
 		// const description 			= $(this).data('description');
 		// const price 				= $(this).data('price');

 		// popupDetailProject.find('img').attr('src', imgSrc);
 		// popupDetailProject.find('.popup-content b').text(title);
 		// popupDetailProject.find('.popup-content p').text(description);
 		// popupDetailProject.find('.popup-content span').text(price);

 		// popupDetailProject.addClass('show');
 	})

 	// ================= SCRIPT MENU HEADER ===================

 	// $('.btn-popup-cart').on('click', function(){
 	// 	$(this).toggleClass('active');
 	// 	$('.wrapper-menu-popup-cart').toggleClass('d-none');
 	// })

 	$(document).on('click', '.btn-cancel-update-my-cart', function(){
 		$(this).closest('.wrapper-menu-popup-edit-cart').addClass('d-none');

		 // start close popup cart
		 $(this).toggleClass('active');
		 $('.wrapper-menu-popup-cart').toggleClass('d-none');
		 $('.menu-home-mobile').removeClass('active')
		 $('.menu-category-mobile').removeClass('active')
		 $('.menu-account-mobile').removeClass('active')
		 // end close popup cart

 		$(this)
 			.closest('.wrapper-menu-popup-cart__product')
 			.find('.wrapper-menu-popup-cart__product-detail a')
 			.text('Chỉnh sửa')
 			.removeClass('active');
 	})

 	$(document).on('click', '.wrapper-menu-popup-cart__product .wrapper-menu-popup-cart__product-detail a', function(){
 		const isActive = $(this).hasClass('active');

 		$(this).text(isActive ? 'Chỉnh sửa' : 'Đang chỉnh sửa').toggleClass('active');
 		$(this).closest('.wrapper-menu-popup-cart__product').find('.wrapper-menu-popup-edit-cart').toggleClass('d-none');
 	})

 	$('.popup').on('click', function(){
 		$(this).removeClass('show');
 	})

	$('.modal-custom.modal-child-service .backdrop').on('click', function () {

		var scrollPosition = [
			self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
			self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
		  ];

		$(this).parent().removeClass('show');
		var html = $('html');
		var scrollPosition = html.data('scroll-position');
		html.css('overflow', html.data('previous-overflow'));
		window.scrollTo(scrollPosition[0], scrollPosition[1])
	});

	$('#dropdownMenuLink').on('click', () => {
		$('.wrapper-menu-popup-cart').addClass('d-none')
	})

	// $('.is-logged').on({
	// 	'mouseover': function(){
	// 		$('.dropdown-menu-account').show();
	// 	},
	// 	'mouseleave': function(){
	// 		const dropDownMenuAccount = $('.dropdown-menu-account');

	// 		console.log({
	// 			dropDownMenuAccount,
	// 			isHover: dropDownMenuAccount.is(':hover')
	// 		});

	// 		if(!dropDownMenuAccount.is(":hover")) {
	// 			dropDownMenuAccount.hide();
	// 		}
	// 	}
	// })

	$('.container-header-bottom .btn-show-menu-category').on('click', function(){
		$(this).toggleClass('active');
		$('.container-backdrop.backdrop-2').toggleClass('active');
 		$('.header-3 .wrapper-column-menu-service').toggleClass('d-none');
 	})

	$('.btn-show-menu-category').on({
		'mouseover': function(){
			$(this).addClass('active');
			// $('.container-backdrop.backdrop-2').toggleClass('active');
			$('.header-3 .wrapper-column-menu-service').removeClass('d-none');
		},
		'click': function(){
			$(this).addClass('active');
			// $('.container-backdrop.backdrop-2').toggleClass('active');
			$('.header-3 .wrapper-column-menu-service').removeClass('d-none');
			$('.menu-home-mobile').removeClass('active')
			$('.menu-cart-mobile').removeClass('active')
			$('.menu-account-mobile').removeClass('active')
		},
		'mouseleave': function(){
			const wrapperColumn = $('.header-3 .wrapper-column-menu-service');

			if(!wrapperColumn.is(":hover")) {
				wrapperColumn.addClass('d-none');
				$(this).removeClass('active');
			}
		}
	})

	$('.header-3 .wrapper-column-menu-service').on('mouseleave', function() {
		$(this).slideUp(200).addClass('d-none');
		$('.btn-show-menu-category').removeClass('active');
	})

	$('.menu-header-service-sub').each(function (index, elem) { 
		$(elem).on({
			'mouseover': function(){
				$(`.wrapper-column-menu-child__service-${index + 1}`).slideDown(200);
				$(this).addClass('active');
			},
			'mouseleave': function(){
				const wrapperColumn = $(`.wrapper-column-menu-child__service-${index + 1}`);
				if(!wrapperColumn.is(":hover")) {
					wrapperColumn.fadeOut(200);
					$(this).removeClass('active');
				}
			}
		})
	
		$(`.wrapper-column-menu-child__service-${index + 1}`).on('mouseleave', function() {
			$(this).fadeOut(200);
			$(elem).removeClass('active');
		})
	});

	// $('.menu-header-service-1').on({
	// 	'mouseover': function(){
	// 		$('.wrapper-column-menu-child__service-1').slideDown(200); //.removeClass('d-none')
	// 		$(this).addClass('active');
	// 	},
	// 	'mouseleave': function(){
	// 		const wrapperColumn = $('.wrapper-column-menu-child__service-1');
	// 		if(!wrapperColumn.is(":hover")) {
	// 			wrapperColumn.fadeOut(200);
	// 			$('.menu-header-service-1').removeClass('active');
	// 		}
	// 	}
	// })

	// $('.wrapper-column-menu-child__service-1').on('mouseleave', function() {
	// 	$(this).fadeOut(200);
	// 	$('.menu-header-service-1').removeClass('active');
	// })

	$('.input-search').on({
		'focus': function(){
			$('.input-search').addClass('active');
			$('.container-backdrop.backdrop-1').addClass('active');
			$('.wrapper-list-result-search').addClass('active');
		},
		'focusout': function(){
			if(!$('.wrapper-list-result-search').is(":hover")) {
				$('.input-search').removeClass('active');
				$('.container-backdrop.backdrop-1').removeClass('active');
				$('.wrapper-list-result-search').removeClass('active');
			}
		}
	})

	$('.container-backdrop.backdrop-1').on('click', function(){
		$(this).removeClass('active');
		// Input search
		$('.input-search').removeClass('active');
		$('.wrapper-list-result-search').removeClass('active');
 	})

	 $('.container-backdrop.backdrop-2').on('click', function(){
		$(this).removeClass('active');
		// Menu category
		$('.header-3 .wrapper-column-menu-service').addClass('d-none');
		$('.btn-show-menu-category').removeClass('active')
 	})


 	// ================= SCRIPT MODAL ===================

	$('.btn-signin').on('click', function() {
		$('.modal-custom').removeClass('show');
		$('.modal-signin').addClass('show');
	})

 	$('.btn-signup').on('click', function() {
		$('.modal-custom').removeClass('show');
 		$('.modal-signup').addClass('show');
 	})

	$('.btn-forgot-password').on('click', function() {
		$('.modal-custom').removeClass('show');
		$('.modal-forgot-password').addClass('show');
	})

	$('.btn-create-new-password').on('click', function() {
		$('.modal-custom').removeClass('show');
		$('.modal-create-new-password').addClass('show');
	})

	$('.btn-send-otp').on('click', function() {
		$('.modal-send-otp').addClass('show');
	})

 	$('.btn-close-modal').on('click', function() {
 		$(this).closest('.modal-custom').removeClass('show');
 	})

 	$('.input-otp').on('keyup', function(e){
		if (e.keyCode != 8 && (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 95 || e.keyCode > 106)) {
			return true;
		}
		if (e.keyCode === 8 || e.keyCode === 37) {
			$(this).prev('.input-otp').focus();
		} else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
			$(this).next('.input-otp').focus();
		}
 	})
	// toastr.options = {
	// 	"closeButton": false,
	// 	"debug": false,
	// 	"positionClass": "toast-top-right",
	// 	"onclick": null,
	// 	"showDuration": "300",
	// 	"hideDuration": "1000",
	// 	"timeOut": "5000",
	// 	"extendedTimeOut": "1000",
	// 	"showEasing": "swing",
	// 	"hideEasing": "linear",
	// 	"showMethod": "fadeIn",
	// 	"hideMethod": "fadeOut"
	// };

});
