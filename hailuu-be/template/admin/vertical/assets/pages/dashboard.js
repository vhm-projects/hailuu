!async function(e) {
    "use strict";
    let a = function() {};

    let STATUS_ORDER = [
        { value: 0, text: 'Đang xử lý'},
        { value: 1, text: 'Đã nhận'},
        { value: 2, text: 'Đang phê duyệt'},
        { value: 3, text: 'Đang in ấn'},
        { value: 4, text: 'Đóng gói và vận chuyển'},
        { value: 5, text: 'Hoàn thành giao hàng'},
    ]
    
    //Check input require
    function returnDataChart() {
        return new Promise(async resolve => {
            try {
                $.ajax({
                    url: `/order/report-order`,
                    method: 'GET',
                    success: function(resp){
                        if(!resp.error){
                            let listDataChart = resp.data;
                            let listDataResult = [];
                            listDataChart && listDataChart.length && listDataChart.forEach(item => {
                                STATUS_ORDER.forEach(status => {
                                    if(item._id.toString() == status.value.toString()){
                                        listDataResult = [...listDataResult, { label: status.text, value: item.count }]
                                    }   
                                })
                            })
                            console.log({ listDataResult });
                            return resolve({ error: false, data: listDataResult });
                        }else{
                            return resolve({ error: true });
                        }
                    },
                    error: function(){
                        return resolve({ error: true });
                    }
                })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    
    //let listDataChart = await returnDataChart();
    
    a.prototype.createDonutChart = function(e, a, r) {
        Morris.Donut({
            element: e,
            data: a,
            resize: !0,
            colors: r
        })
    }, 
    
    a.prototype.init = function() {

        this.createDonutChart("morris-donut-example", [
            {
                label: "Download Sales",
                value: 12
            }, 
            {
                label: "Đóng gói và vận chuyển",
                value: 30
            }, 
            {
                label: "Mail-Order Sales",
                value: 20
            }
        ], ["#33CC33", "#28bbe3", "#FF6633", "#9966FF", "#339999" ]);
        
    }, e.Dashboard = new a, e.Dashboard.Constructor = a
}(window.jQuery),
function(e) {
    "use strict";
    window.jQuery.Dashboard.init()
}();