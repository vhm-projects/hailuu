"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const AMOUNT_COLL = require('../database/amount-coll.js');

class Model extends BaseModel {
    constructor() {
        super(AMOUNT_COLL);
    }

    // Tạo mới
    insert({ name, amount }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name || !amount) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                let dataInsert = {
                    name,
                    amount,
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Lấy danh sách
    getList({ status }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                
                let STATUS_ACCEPT = [0,1]
                if(status){
                    conditionObj.status = status
                }else{
                    conditionObj.status = { $in: STATUS_ACCEPT }
                }
                
                let listData = await AMOUNT_COLL.find(conditionObj);
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Lấy thông tin chi tiết
    getInfo({ amountID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(amountID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoData = await AMOUNT_COLL.findById(amountID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_info' });
           
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Xoá
    remove({ amountID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(amountID))
                    return resolve({ error: true, message: 'params_not_valid' });
            
                let LOCK_STATUS = -1;
                
                let infoRemove = await AMOUNT_COLL.findByIdAndUpdate(amountID, {status: LOCK_STATUS}, {new: true});
                if(!infoRemove)
                    return resolve({ error: true, message: 'cannot_remove' });
           
                return resolve({ error: false, data: infoRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Sửa
    update({ amountID, name, amount, status }) {
        return new Promise(async resolve => {
            try {
                if(!name || !amount || !ObjectID.isValid(amountID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let dataUpdate = { 
                    name, 
                    amount,
                    status
                }
                
                let infoUpdate = await AMOUNT_COLL.findByIdAndUpdate(amountID, dataUpdate, { new: true });
                if(!infoUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

           
                return resolve({ error: false, data: infoUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

}

exports.MODEL = new Model;