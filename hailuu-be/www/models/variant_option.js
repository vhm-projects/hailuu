"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const VARIANT_OPTION_COLL = require('../database/variant_option-coll');

class Model extends BaseModel {
    constructor() {
        super(VARIANT_OPTION_COLL);
    }

    // Tạo mới
    insert({ name, parentID }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                let dataInsert = { name }

                if(parentID) {
                    let infoParent = await VARIANT_OPTION_COLL.findById(parentID);
                    let { level } = infoParent;
                    dataInsert.parent = parentID;
                    dataInsert.level = level + 1;
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                if(parentID){
                    await VARIANT_OPTION_COLL.findByIdAndUpdate(parentID, { $addToSet: { childs : resultInsert._id }});
                }

                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }


    // Tạo mới
    update({ variantOptionID, name }) { 
        return new Promise(async (resolve) => {
            try {
                let infoVariantOption = await VARIANT_OPTION_COLL.findByIdAndUpdate(variantOptionID, { name });
                if(!infoVariantOption)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false, data: infoVariantOption });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ variantOptionID, status }) { 
        return new Promise(async (resolve) => {
            try {
                let infoVariantOption = await VARIANT_OPTION_COLL.findByIdAndUpdate(variantOptionID, { status });
                if(!infoVariantOption)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false, data: infoVariantOption });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    
    // Danh sách biến thể
    getList() { 
        return new Promise(async (resolve) => {
            try {
                let listData = await VARIANT_OPTION_COLL.find({ level: 1, status: 1 }).populate("childs");
                return resolve({ error: false, data: listData });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ variantOptionID }) { 
        return new Promise(async (resolve) => {
            try {
                let infoVariantOption = await VARIANT_OPTION_COLL.findById(variantOptionID);
                if(!infoVariantOption)
                    return resolve({ error: true, message: "cannot_get_variant_option"});
                return resolve({ error: false, data: infoVariantOption });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;