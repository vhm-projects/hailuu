"use strict";

const promise                   = require('bluebird');
const ObjectID                  = require('mongoose').Types.ObjectId;
const { checkObjectIDs }        = require('../utils/utils');
const GALLERY_PRODUCT_COLL      = require('../database/gallery_product-coll');
const IMAGE_MODEL               = require('../models/image').MODEL;
const BaseModel                 = require('./intalize/base_model');

class Model extends BaseModel {

    constructor() {
        super(require('../database/gallery_product-coll'))
    }

    /**
     * Create gallery
     * Dattv
     */
    insert({ name, url, image, description, userID }) {
        return new promise(async resolve => {
            try {
                if( !checkObjectIDs(userID) || !name)
                return resolve({ error: true, message: "params_invalid" });
                
                let dataInsert = { name, description, userCreate: userID };

                let { name: nameImage, size, path } = image;

                if(url){
                    dataInsert.url = url;
                }

                let infoImage = await IMAGE_MODEL.insert({ name: nameImage, size, path, userCreate: userID });
                if(infoImage){
                    dataInsert.image = infoImage.data._id;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

     /**
     * Get infomation gallery
     * Dattv
     */
    getInfo({ galleryID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(galleryID))
                return resolve({ error: true, message: "params_invalid" });

                let infoGalleryProduct = await GALLERY_PRODUCT_COLL.findById(galleryID)
                .populate({
                    path: "image",
                    select: "name path"
                });

                if (!infoGalleryProduct) 
                    return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoGalleryProduct })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Get list gallery
     * Dattv
     */
    getList({ status }) {
        return new promise(async resolve => {
            try {

                let STATUS_BLOCK = -1
                let conditionObj = {};
                if(status){
                    conditionObj.status = status
                }else{
                    conditionObj.status = {
                        $ne: STATUS_BLOCK
                    }
                }

                let listGalleruProduct = await GALLERY_PRODUCT_COLL.find(conditionObj)
                .select("menu name description avatar priceAfter priceBefore flashSale slug")
                .populate({
                    path: "image",
                    select: "name path"
                }).sort({ order: 1 })

                if (!listGalleruProduct) 
                    return resolve({ error: true, message: 'cannot_get_list' });
                return resolve({ error: false, data: listGalleruProduct })

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListForEnduser() {
        return new promise(async resolve => {
            try {
             
                let listGalleruProduct = await GALLERY_PRODUCT_COLL.find({ status: 1})
                .select("menu name description avatar priceAfter priceBefore flashSale slug")
                .populate({
                    path: "image",
                    select: "name path"
                }).limit(3).sort({ order: 1 })

                if (!listGalleruProduct) 
                    return resolve({ error: true, message: 'cannot_get_list' });
                return resolve({ error: false, data: listGalleruProduct })

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //update gallery
    update({ galleryID, name, url, image, description, status, userID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(galleryID, userID))
                return resolve({ error: true, message: "params_invalid" });

                let dataUpdate = { userUpdate: userID };

                if(name){
                    dataUpdate.name = name;
                }
                
                if(description){
                    dataUpdate.description = description;
                }

                if(url){
                    dataUpdate.url = url;
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                    if(infoImage){
                        dataUpdate.image = infoImage.data._id;
                    }
                }

                let statusActive = [0, 1];

                if(status && statusActive.includes(Number(status))){
                    dataUpdate.status = status;
                }
                
                let infoAfterUpdate = await GALLERY_PRODUCT_COLL.findByIdAndUpdate(galleryID, dataUpdate, { new: true });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Remove gallery
     * Dattv
     */
     remove({ galleryID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(galleryID))
                return resolve({ error: true, message: "params_invalid" });
            
                let infoAfterRemove = await GALLERY_PRODUCT_COLL.findByIdAndUpdate(galleryID, { status: -1 }, {new: true})
                if (!infoAfterRemove) 
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterRemove })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update order gallery product
     * Dattv
     */
     updateOrder({ arrayObject }) {
        return new Promise(async resolve => {
            try {
                
                if(!arrayObject.length)
                    return resolve({ error: true, message: 'params_invalid' });
                
                for (let item of arrayObject) {
                    let updateOrder = await GALLERY_PRODUCT_COLL.findByIdAndUpdate(item._id, {
                        order: item.index
                    }, { new: true })

                    if(!updateOrder)
                        return resolve({ error: true, message: 'cannot_update_order' });
                }

                return resolve({ error: false, message: "update success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   

}

exports.MODEL = new Model;