"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      	= require('mongoose').Types.ObjectId;
const uuidv4							= require('uuid').v4;
const https								= require('https');
const crypto							= require('crypto');
const moment                            = require("moment");

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 			= require('../utils/string_utils');
const { checkObjectIDs } 				= require('../utils/utils');
const { sendMailReportStatusOrder } 	= require('../mailer/module/mail_user');

/**
 * BASE
 */
const BaseModel 		= require('./intalize/base_model');
var request 			= require('request');

/**
 * COLLECTIONS
 */
const ORDER_COLL 			= require('../database/order-coll');
const ORDER_LINE_COLL 		= require('../database/order_line-coll');
const CUSTOMER_ADDRESS_COLL = require('../package/customer/database/customer_address-coll');
const CUSTOMER_COLL 		= require('../database/customer-coll');
const COMMON_MODEL			= require('../config/constants/constant/common').MODEL;


class Model extends BaseModel {
    constructor() {
        super(ORDER_COLL);
    }

	checkOrderIDExists(orderID){
		return new Promise(resolve => {
			(async function recursiveCheckOrderID(orderID){
				let checkExists = await ORDER_COLL.findOne({ orderID });
				if(checkExists){
					orderID = randomStringNumber(10);
					recursiveCheckOrderID(orderID);
				} else{
					resolve(orderID);
				}
			})(orderID)
		})
	}

	sendZaloZNSAndEmail({ templateID, orderID, statusText }){
		return new Promise( async resolve => {
			if(!process.env.URL_ZALO_ZNS)
				return resolve({ error: true, message: "url_zalo_zns_faild" })
			const checkExists = await ORDER_COLL
			.findById(orderID)
			.populate({
				path: 'orderLine customer address',
				populate: {
					path: 'product files productProperty',
					populate: {
						path: 'avatar variant_values',
						populate: 'parent variant'
					}
				}
			}).lean();

			let infoAddress = null;
			if(checkExists && checkExists.address){
				infoAddress = await CUSTOMER_ADDRESS_COLL.findById(checkExists.address._id);

				let infoProvince   = await COMMON_MODEL.getInfoProvince({ 
					provinceCode: infoAddress.city 
				});
				let infoDistrict   = await COMMON_MODEL.getInfoDistrict({ 
					districtCode: infoAddress.district 
				});
				let infoWard       = await COMMON_MODEL.getInfoWard({ 
					district: infoAddress.district, 
					wardCode: infoAddress.ward 
				});

				infoAddress = {
					info: infoAddress,
					infoProvince: infoProvince.data, 
					infoDistrict: infoDistrict.data, 
					infoWard: infoWard.data
				}
			}

			// Send Zalo ZNS cập nhật trạng thái đơn hàng
			let { orderLine, amount, customer, orderID: codeOrderID  } = checkExists;
			let { phone } = customer;
			if(phone){
				// Địa chỉ
				let fullAddress =""
				if (infoAddress) {
					fullAddress = `${infoAddress.info.address}, ${infoAddress.infoWard[1].name_with_type}, ${infoAddress.infoDistrict[1].name_with_type}, ${infoAddress.infoProvince[1].name_with_type}`
				} else{
					fullAddress = "Nhận sản phẩm in tại cửa hàng của Hailuu.vn";
				}

				// Sản phẩm
				let fullProduct = ""
				orderLine.forEach((item, index) => {
					let { product } = item;
					let chamPhay = ""
					if(index < orderLine.length -1){
						chamPhay = ", "
					}
					fullProduct += product.name + chamPhay; 
				});

				let phone84 = `84${phone.substring(1)}`;

				var options = {
					'method': 'POST',
					'url': process.env.URL_ZALO_ZNS,
					'headers': {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						"phone": phone84,
						"template_id": templateID,
						"template_data": {
							"order_code": codeOrderID,
							"cost": amount,
							"address": fullAddress.substring(0, 30),
							"payment_status": statusText,
							"customer_name": customer.fullname && customer.fullname || customer.email,
							"product_name": fullProduct.substring(0, 30)
						},
						"tracking_id": "123"
					})
				};

				request(options, function (error, response) {
					if (error) throw new Error(error);
						console.log(response.body);
				});
			}
			return resolve({ error: false, message: "Send zalo ZNS success"});
		})
	}

    insert({ 
		orderLine, customerID, note, weight, amount, 
		deliveryPrice, payment, addressID, exportVAT 
	}) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs([...orderLine, customerID]) || !payment)
                    return resolve({ error: true, message: 'params_invalid' });

				if(addressID && !ObjectID.isValid(addressID))
					return resolve({ error: true, message: 'params_invalid' });

				const orderID = await this.checkOrderIDExists(randomStringNumber(10));
                const dataInsert = {
					orderID,
                    orderLine, 
                    customer: customerID,
                    note,
                    weight, 
                    amount,
                    deliveryPrice,
                    payment,
					exportVAT,
                    userCreate: customerID
                }
				addressID && (dataInsert.address = addressID);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_order_failed' });

				// Xoá giỏ hàng
				await ORDER_LINE_COLL.updateMany({ _id: { $in: orderLine } }, { $set: { status: -1 } });

				// Gửi zalo zns đặt hàng thành công
				await this.sendZaloZNSAndEmail({ templateID: "212140", orderID: infoAfterInsert._id, statusText: "Đặt hàng thành công" });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
		orderID, orderLine, note, weight, amount, deliveryPrice, deliveryDate, 
		payment, address, status, statusPayment, userUpdate 
	}) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs([orderID, userUpdate]))
                    return resolve({ error: true, message: 'params_invalid' });

                const checkExists = await ORDER_COLL
					.findById(orderID)
					.populate({
						path: 'orderLine customer address',
						populate: {
							path: 'product files productProperty',
							populate: {
								path: 'avatar variant_values',
								populate: 'parent variant'
							}
						}
					})
					.lean();

                if(!checkExists)
                    return resolve({ error: true, message: "order_is_not_exists" });

                const dataUpdate = { userUpdate };
                note        	&& (dataUpdate.note         	= note);
                weight     		&& (dataUpdate.weight      		= weight);
                amount       	&& (dataUpdate.amount        	= amount);
                deliveryPrice   && (dataUpdate.deliveryPrice    = deliveryPrice);
                deliveryDate    && (dataUpdate.deliveryDate     = deliveryDate);
                payment     	&& (dataUpdate.payment      	= payment);
                address     	&& (dataUpdate.address      	= address);
                status      	&& (dataUpdate.status       	= status);

				statusPayment = +statusPayment;
				if([0,1].includes(statusPayment)){
					switch (checkExists.payment) {
						case 0: // Thanh toán khi nhận hàng
							dataUpdate.statusPayment = statusPayment;
							break;
						case 1: // Chuyển khoản
							dataUpdate.statusPayment = statusPayment === 1 ? 2 : 0;
							break;
						default:
							break;
					}
				}

				// Đang in ấn
				if(+status === 2 && checkExists.status !== 2){
					let infoAddress = null;
					if(checkExists && checkExists.address){
						infoAddress = await CUSTOMER_ADDRESS_COLL.findById(checkExists.address._id);

						let infoProvince   = await COMMON_MODEL.getInfoProvince({ 
							provinceCode: infoAddress.city 
						});
						let infoDistrict   = await COMMON_MODEL.getInfoDistrict({ 
							districtCode: infoAddress.district 
						});
						let infoWard       = await COMMON_MODEL.getInfoWard({ 
							district: infoAddress.district, 
							wardCode: infoAddress.ward 
						});

						infoAddress = {
							info: infoAddress,
							infoProvince: infoProvince.data, 
							infoDistrict: infoDistrict.data, 
							infoWard: infoWard.data
						}
					}

					// Send mail cập nhật trạng thái đơn hàng
					// sendMailReportStatusOrder(checkExists.customer.email, checkExists, infoAddress);

					// Gửi zalo zns đặt cập nhật trạng thái đóng gói và vận chuyển
					await this.sendZaloZNSAndEmail({ templateID: "212545", orderID, statusText: "Đang in ấn" });
				}

				// Đóng gói và vận chuyển
				if(+status === 3 && checkExists.status !== 3){
					let infoAddress = null;
					if(checkExists && checkExists.address){
						infoAddress = await CUSTOMER_ADDRESS_COLL.findById(checkExists.address._id);

						let infoProvince   = await COMMON_MODEL.getInfoProvince({ 
							provinceCode: infoAddress.city 
						});
						let infoDistrict   = await COMMON_MODEL.getInfoDistrict({ 
							districtCode: infoAddress.district 
						});
						let infoWard       = await COMMON_MODEL.getInfoWard({ 
							district: infoAddress.district, 
							wardCode: infoAddress.ward 
						});
						
						infoAddress = {
							info: infoAddress,
							infoProvince: infoProvince.data,
							infoDistrict: infoDistrict.data,
							infoWard: infoWard.data
						}
					}	

					// Send mail cập nhật trạng thái đơn hàng
					sendMailReportStatusOrder(checkExists.customer.email, checkExists, infoAddress);

					// Gửi zalo zns đặt cập nhật trạng thái đóng gói và vận chuyển
					await this.sendZaloZNSAndEmail({ templateID: "212545", orderID, statusText: "Đóng gói và vận chuyển" });
				}
				
				if(orderLine && orderLine.length){
					if(!checkObjectIDs(orderLine))
						return resolve({ error: true, message: 'params_invalid' });
					dataUpdate.orderLine = orderLine;
				}

                await this.updateWhereClause({ _id: orderID }, dataUpdate);

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ orderID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderID))
                    return resolve({ error: true, message: "params_invalid" });

                const infoAfterDelete = await ORDER_COLL.findByIdAndDelete(orderID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: "cannot_delete_order" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ orderID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderID))
                    return resolve({ error: true, message: "params_invalid" });

				const infoOrder = await ORDER_COLL
					.findById(orderID)
					.populate({
						path: 'orderLine customer address',
						populate: {
							path: 'product files productProperty',
							populate: {
								path: 'avatar variant_values',
								populate: 'parent variant'
							}
						}
					})
					.lean();

                if(!infoOrder)
                    return resolve({ error: true, message: "order_is_not_exists" });

                return resolve({ error: false, data: infoOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfoByOrderID({ orderID, customerID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(customerID))
					return resolve({ error: true, message: "params_invalid" });

				const infoOrder = await ORDER_COLL
					.findOne({ orderID, customer: customerID })
					.populate({
						path: 'orderLine customer address',
						populate: {
							path: 'product files productProperty',
							populate: {
								path: 'avatar variantOptions',
								populate: 'parent'
							}
						}
					})
					.lean();

                if(!infoOrder)
                    return resolve({ error: true, message: "order_is_not_exists" });

                return resolve({ error: false, data: infoOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, status, customer }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.note = new RegExp(key, 'i');
                }

				status 	 && (condition.status 	= status);
				customer && (condition.customer = customer);

                const listOrder = await ORDER_COLL
					.find(condition)
					.populate({
						path: 'orderLine customer address',
						populate: {
							path: 'product files productProperty',
							populate: 'avatar'
						}
					})
					.sort({ modifyAt: -1 })
					.lean();

                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

                return resolve({ error: false, data: listOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilter({ keyword, status, statusPayment }){
        return new Promise(async resolve => {
            try {
                const conditionObj = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

					conditionObj.$or = [
						{ orderID: new RegExp(key, 'i') },
						{ 'customer.fullname': new RegExp(key, 'i') },
						{ 'customer.email': new RegExp(key, 'i') },
						{ 'customer.phone': new RegExp(key, 'i') }
					];
                }

				status 			&& (conditionObj.status 		= +status);
				statusPayment 	&& (conditionObj.statusPayment 	= +statusPayment);

				let listOrder = await ORDER_COLL.aggregate([
					{
						$lookup: {
						   from: 'customers',
						   localField: 'customer',
						   foreignField: '_id',
						   as: 'customer'
						}
					},
					{
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					},
					{
						$lookup: {
							from: 'order_lines',
							localField: 'orderLine',
							foreignField: '_id',
							as: 'orderLine'
						}
					},
					// {
					// 	$lookup: {
					// 	   from: 'variant_v2',
					// 	   localField: 'order_lines.productProperty',
					// 	   foreignField: '_id',
					// 	   as: 'productProperty'
					// 	}
					// },
					// {
					// 	$unwind: {
					// 		path: '$productProperty',
					// 		preserveNullAndEmptyArrays: true
					// 	}
					// },
					{
						$match: conditionObj
					},
					{
						$lookup: {
						   from: 'customer_addresses',
						   localField: 'address',
						   foreignField: '_id',
						   as: 'address'
						}
					},
					{
						$unwind: {
							path: '$address',
							preserveNullAndEmptyArrays: true
						}
					},
					{ $sort: { modifyAt: -1 } }
				])

				// listOrder = await ORDER_COLL.populate(listOrder, {  path: 'order_lines.productProperty' })

				// listOrder.map(order => {
				// 	console.log({ orderLine: order.orderLine })
				// })
                // const listOrder = await ORDER_COLL
				// 	.find(conditionObj)
				// 	.populate('orderLine account address')
				// 	.lean();

                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

                return resolve({ error: false, data: listOrder });
            } catch (error) {
				console.error(error);
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFromTo({ from, to }){
        return new Promise(async resolve => {
            try {
                
                let conditionObj = {};
                if(from && to){
                    conditionObj.createAt = { $gte: moment(from).startOf("day")._d, $lte: moment(to).startOf("day")._d }
                }

                const listOrder = await ORDER_COLL
					.find(conditionObj)
					.populate('orderLine account address')
					.lean();

                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

                return resolve({ error: false, data: listOrder });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * DANH SÁCH KHÁCH HÀNG ORDER NHIỀU NHẤT
     * DEPV
     */
    listCustomerOrderMany({ from, to }){
        return new Promise(async resolve => {
            try {
                let conditionSearch = {};
                if(from && to){
                    // conditionSearch.createAt = { $gte: new Date(from), $lte: new Date(to) };
					conditionSearch.createAt = { $gte: moment(from).startOf("day")._d, $lte: moment(to).startOf("day")._d }
                }
                let listCustomer = await ORDER_COLL.aggregate([
                    {
                        $match: conditionSearch
                    },
                    {
                        $group: { 
                            _id: "$customer",
                            count: { $sum: 1 },
                            totalMoney: { $sum: "$amount" },
                        },
                    },
                    {
                        $lookup: {
                           from: 'customers',
                           localField: '_id',
                           foreignField: '_id',
                           as: 'customer'
                        }
                    },
                    {
                        $unwind: '$customer',
                        
                    },
                ]).sort({ count: -1 });
                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * COUNT SỐ LƯỢNG GIAO DỊCH VÀ TỔNG TIỀN CỦA KHÁCH HÀNG VÀ DANH SÁCH GIAO DỊCH CỦA KHÁCH HÀNG
     */
	getListCustomer({ customer }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(!checkObjectIDs(customer))
                    return resolve({ error: true, message: "params_invalid" });

				condition.customer = customer;
                const listOrder = await ORDER_COLL
					.find(condition)
					.populate('orderLine customer address')
					.sort({ createAt: -1 })
					.lean();
                const countOrder = await ORDER_COLL.count(condition);

                const STATUS_PAY     = 1;
                const STATUS_BANKING = 2;
                const STATUS_MOMO    = 3;
                const STATUS_VALID = [STATUS_PAY, STATUS_BANKING, STATUS_MOMO];
                const pricePayOrder = await ORDER_COLL.aggregate([
                    {
                        $match: {
                            customer: ObjectID(customer),
                            statusPayment: {
                                $in: STATUS_VALID
                            },
                            // amount: { $sum: "$amount" },
                        }
                    },{
                        $group: {
                            _id: null,
                            total: { $sum: "$amount" }
                        }
                    }
                ])

                let data = {
                    listOrder,
                    countOrder,
                    pricePayOrder: pricePayOrder[0] && pricePayOrder[0].total || 0
                }
                if(!listOrder)
                    return resolve({ error: true, message: "cannot_get_list_order" });

                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * THỐNG KÊ SỐ LƯỢNG ORDER DỰA VÀO STATUS || VIEW CHART
     * DATTV
     */
	listDataOrderReport({ from, to }){
        return new Promise(async resolve => {
            try {
                let conditionObj = {};
                if(from && to){
                    let _fromDate   = moment(from).startOf('day').format();
                    let _toDate     = moment(to).endOf('day').format();

                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
					// conditionObj.createAt = { $gte: moment(from).startOf("day")._d, $lte: moment(to).startOf("day")._d }

                }

                let listDataOfStatus = await ORDER_COLL.aggregate([
                    {
                        $match: { ...conditionObj }
                    },
                    {
                        $group: { 
                            _id: "$status",
                            count: { $sum: 1 },
                        },
                    },
                ]).sort({ count: -1 });
                return resolve({ error: false, data: listDataOfStatus });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * THỐNG KÊ SỐ LƯỢNG ORDER VÀ TỔNG TIỀN
     * DATTV
     */
	listDataOrder({ start, end }){
        return new Promise(async resolve => {
            try {
                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
					// conditionObj.createAt = { $gte: moment(from).startOf("day")._d, $lte: moment(to).startOf("day")._d }

                }

                let listDataOrder = await ORDER_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                    {
                        $group: { 
                            _id: null,
                            count: { $sum: 1 },
                            totalMoney: { $sum: "$amount" }
                        }
                    },
                ]).sort({ count: -1 });
                return resolve({ error: false, data: listDataOrder[0] });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	// "merchantName=;merchantId=" => bWVyY2hhbnROYW1lPTttZXJjaGFudElkPQo=
	createRequestPayment({ orderId, orderInfo, amount, extraData = 'bWVyY2hhbnROYW1lPTttZXJjaGFudElkPQo=' }) {
		return new Promise(async resolve => {
			try {
				const { MOMO_PARTNER_CODE, MOMO_ACCESS_KEY, MOMO_SECRET_KEY, URL_RETURN, URL_IPN, MOMO_HOSTNAME } = process.env;
				const requestId 	= uuidv4();
				const requestType	= 'captureWallet';

				const rawSignature = `accessKey=${MOMO_ACCESS_KEY}&amount=${amount}&extraData=${extraData}&ipnUrl=${URL_IPN}&orderId=${orderId}&orderInfo=${orderInfo}&partnerCode=${MOMO_PARTNER_CODE}&redirectUrl=${URL_RETURN}&requestId=${requestId}&requestType=${requestType}`

				//puts raw signature
				// console.log("--------------------RAW SIGNATURE----------------")
				// console.log(rawSignature)

				//signature
				const signature = crypto.createHmac('sha256', MOMO_SECRET_KEY)
								.update(rawSignature)
								.digest('hex');
				// console.log("--------------------SIGNATURE----------------")
				// console.log(signature);

				//json object send to MoMo endpoint
				const body = JSON.stringify({
					partnerCode: MOMO_PARTNER_CODE,
					accessKey: MOMO_ACCESS_KEY,
					requestId: requestId,
					amount: amount,
					orderId: orderId,
					orderInfo: orderInfo,
					ipnUrl: URL_IPN,
					redirectUrl: URL_RETURN,
					extraData: extraData,
					requestType: requestType,
					signature: signature,
					lang: "vi"
				})

				//Create the HTTPS objects
				const options = {
					hostname: MOMO_HOSTNAME,
					port: 443,
					path: '/v2/gateway/api/create',
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Content-Length': Buffer.byteLength(body)
					}
				};

				const request = https.request(options, (res) => {
					res.setEncoding('utf8');

					let body = '';
					res.on('data', (chunk) => {
						body += chunk;
					});

					res.on('end', () => {
						try {
							// console.log({ __BODY: body });
							const responseData = JSON.parse(body);

							// console.log({ __RESEPONSE_DATA: responsspameData, __PAY_URL: responseData.payUrl });
							// console.log('No more data in response.');
							return resolve({ error: false, data: { PAY_URL: responseData.payUrl, responseData } });
						} catch (error) {
							return resolve({ error: true, message: error.message });
						}
					});
				});

				request.on('error', (e) => {
					console.log(`problem with request: ${e.message}`);
					return resolve({ error: true, message: e.message });
				});

				// write data to request body
				request.write(body);
				request.end();
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

	checkStatusTransactionMomo({ requestId, orderId }) {
		return new Promise(async resolve => {
			try {
				const { MOMO_SECRET_KEY, MOMO_ACCESS_KEY, MOMO_PARTNER_CODE, MOMO_HOSTNAME } = process.env;

				const rawSignature = `accessKey=${MOMO_ACCESS_KEY}&orderId=${orderId}&partnerCode=${MOMO_PARTNER_CODE}&requestId=${requestId}`;

				//puts raw signature
				// console.log("--------------------RAW SIGNATURE----------------")
				// console.log(rawSignature)

				//signature
				const signature = crypto.createHmac('sha256', MOMO_SECRET_KEY)
								.update(rawSignature)
								.digest('hex');
				// console.log("--------------------SIGNATURE----------------")
				// console.log(signature);

				//json object send to MoMo endpoint
				const body = JSON.stringify({
					partnerCode: MOMO_PARTNER_CODE,
					requestId,
					orderId,
					signature,
					lang: "vi",
				})

				//Create the HTTPS objects
				const options = {
					hostname: MOMO_HOSTNAME,
					port: 443,
					path: '/v2/gateway/api/query',
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Content-Length': Buffer.byteLength(body)
					}
				};

				let request = https.request(options, (res) => {
					res.setEncoding('utf8');

					let body = '';
					res.on('data', (chunk) => {
						body += chunk;
					});

					res.on('end', () => {
						try {
							// console.log({ __BODY: body });
							const responseData = JSON.parse(body);

							// console.log({ __RESEPONSE_DATA: responseData });
							// console.log('No more data in response.');
							return resolve({ error: false, data: responseData });
						} catch (error) {
							return resolve({ error: true, message: error.message });
						}
					});
				});

				request.on('error', (e) => {
					console.log(`problem with request: ${e.message}`);
					return resolve({ error: true, message: e.message });
				});

				// write data to request body
				request.write(body);
				request.end();
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
