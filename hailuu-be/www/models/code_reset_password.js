"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');
const { sendMailNewAccount }        = require('../mailer/module/mail_user');


/**
 * INTERNAL PAKCAGE
 */
const cfJWS                         = require('../config/cf_jws');
const { randomStringFixLengthCode } = require('../utils/string_utils');

/**
 * BASE
 */
const BaseModel = require('../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CODE_RESET_PASSWORD_COLL  		= require('../database/code_reset_password-coll');

class Model extends BaseModel {
    constructor() {
        super(CODE_RESET_PASSWORD_COLL);
    }

	insert({ email, code }) {
        return new Promise(async resolve => {
            try {
                if(!email || !code)
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let nowAddWith15Minute = (minutes) => {
                    let minutesForAdd = minutes * 60 * 1000;
                    let dateNow = new Date();
                    return dateNow.setTime(dateNow.getTime() + minutesForAdd);
                }
                let timeLife = nowAddWith15Minute(1);

                let dataInsert = {
                    email, code, timeLife: new Date(timeLife)
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if (!infoAfterInsert) {
                    return resolve({ error: true, message: "cannot_insert" });
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkCode({ email, code }) {
        return new Promise(async resolve => {
            try {
                if(!email || !code)
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoCode = await CODE_RESET_PASSWORD_COLL.findOne({ email, code, timeLife: { $gte: new Date() }})
                
                if (!infoCode) {
                    return resolve({ error: true, message: "cannot_find_code" });
                }

                return resolve({ error: false, data: infoCode });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
