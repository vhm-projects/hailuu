"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const VARIANT_V2_COLL = require('../database/variant_v2-coll');
const PRODUCT_COLL = require('../database/product-coll.js');

class Model extends BaseModel {
    constructor() {
        super(VARIANT_V2_COLL);
    }

    // Tạo mới
    insert({ variantOptions, weight, priceBefore, priceAfter, productID }) { 
        return new Promise(async (resolve) => {
            try {

                if(!ObjectID.isValid(productID) || !weight || !priceBefore)
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let dataInsert = {
                    product: productID,
                    variantOptions,
                    weigth: Number(weight),
                    priceBefore: Number(priceBefore),
                }
                if(priceAfter)
                    dataInsert.priceAfter = Number(priceAfter)

                //Check đã có variant trong product chưa?
                let variantExisted = await VARIANT_V2_COLL.findOne({ product: productID, variantOptions });
                if(variantExisted)
                    return resolve({ error: true, message: 'variant_is_existed_at_product' });

                console.log({ dataInsert });
                
                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ variantID, weight, priceBefore, priceAfter }) { 
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(variantID) || !weight || !priceBefore)
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let dataInsert = {
                    weigth: Number(weight),
                    priceBefore: Number(priceBefore),
                }
                if(priceAfter)
                    dataInsert.priceAfter = Number(priceAfter)
                
                let resultUpdate = await VARIANT_V2_COLL.findByIdAndUpdate(variantID, dataInsert, { new: true });

                if(!resultUpdate)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ variantID, status }) { 
        return new Promise(async (resolve) => {
            try {
              
                let statusActive = [ -1, 0, 1];
                if(!ObjectID.isValid(variantID) || !statusActive.includes(Number(status)))
                    return resolve({ error: true, message: 'params_not_valid' });
              
                
                let resultUpdate = await VARIANT_V2_COLL.findByIdAndUpdate(variantID, { status }, { new: true });

                if(!resultUpdate)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByProduct({ productID }) { 
        return new Promise(async (resolve) => {
            try {

                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                //Check đã có variant trong product chưa?
                let listData = await VARIANT_V2_COLL.find({ product: productID, status: 1 }).populate("variantOptions");
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get' });

                return resolve({ error: false, data: listData });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getPriceProduct({ productID, variantOptions }) { 
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let infoPrice = await VARIANT_V2_COLL.findOne({ product: productID, variantOptions, status: 1 });
                if(!infoPrice)
                    return resolve({ error: true, message: 'cannot_get' });

                return resolve({ error: false, data: infoPrice });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ variantID }) { 
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(variantID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let infoVariant = await VARIANT_V2_COLL.findById(variantID).populate("variantOptions");
                if(!infoVariant)
                    return resolve({ error: true, message: 'cannot_get' });

                return resolve({ error: false, data: infoVariant });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;