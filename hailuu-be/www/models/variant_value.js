"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const VARIANT_VALUE_COLL = require('../database/variant_value-coll');
const PRODUCT_VARIANT_VALUE_COLL = require('../database/product_variant_value-coll');
const PRODUCT_COLL = require('../database/product-coll.js');

class Model extends BaseModel {
    constructor() {
        super(VARIANT_VALUE_COLL);
    }

    // Tạo mới
    insert({ parentID, name, variant, product }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name)
                    return resolve({ error: true, message: "params_invalide" });

                let dataInsert = {
                    name, variant, product
                }

                if(parentID){
                    dataInsert.parent = parentID;
                }
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: "cannot_insert" });
                return resolve({ error: false, data : infoAfterInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ variantValueID, name }) { 
        return new Promise(async (resolve) => {
            try {
                console.log({ variantValueID, name });
                if(!name)
                    return resolve({ error: true, message: "params_invalide" });

                let dataUpdate = {
                    name
                }
                let infoAfterUpdate = await VARIANT_VALUE_COLL.findByIdAndUpdate(variantValueID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_insert" });

                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remvove({ variantValueID }) { 
        return new Promise(async (resolve) => {
            try {
                let infoAfterUpdate = await VARIANT_VALUE_COLL.findByIdAndUpdate(variantValueID, { status: 2 }, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_insert" });
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    listVariantValueChilds({ variantValueID }) { 
        return new Promise(async (resolve) => {
            try {
                let listData = await VARIANT_VALUE_COLL.find({ parent: variantValueID, status: 1 });
                if(!listData)
                    return resolve({ error: true, message: "cannot_getlist" });
                let listDataAfterFormat = [];
                for (const item of listData) {
                    let { _id } = item;
                    let infoProductVariantValue =  await PRODUCT_VARIANT_VALUE_COLL.findOne({ variant_values: [variantValueID, _id] , status: 1});
                    listDataAfterFormat.push({ variantValue: item, productVariantValue: infoProductVariantValue });
                }
                return resolve({ error: false, data: listDataAfterFormat });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;