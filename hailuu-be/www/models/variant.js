"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const VARIANT_COLL = require('../database/variant-coll.js');
const PRODUCT_COLL = require('../database/product-coll.js');

class Model extends BaseModel {
    constructor() {
        super(VARIANT_COLL);
    }

    // Tạo mới
    insert({ name, parent, product, typeShow }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name || !product)
                    return resolve({ error: true, message: "params_invalide" });

                let dataInsert = {
                    name,
                    parent,
                    product,
                    typeShow,
                }
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: "cannot_insert" });
                return resolve({ error: false, data : infoAfterInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ variantID, name, typeShow }) { 
        return new Promise(async (resolve) => {
            try {
                let dataUpdate = {
                }
                if(name){
                    dataUpdate.name = name;
                }

                if(typeShow){
                    dataUpdate.typeShow = typeShow;
                }
                let infoAfterUpdate = await VARIANT_COLL.findByIdAndUpdate(variantID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_Update" });
                return resolve({ error: false, data : infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;