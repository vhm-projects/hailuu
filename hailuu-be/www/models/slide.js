"use strict";
let fs                                    = require("fs");
let path                                  = require('path');
const promise                             = require('bluebird');
const BaseModel                           = require('./intalize/base_model');
const ObjectID                            = require('mongoose').Types.ObjectId;
const SLIDE_COLL                          = require('../database/slide-coll.js');
const ADMIN_COLL                          = require('../database/user-coll.js');
const IMAGE_COLL                          = require('../database/image-coll');
const IMAGE_MODEL                         = require('../models/image').MODEL;
const { log } = require("console");



class Model extends BaseModel {
    constructor() {
        super(require('../database/slide-coll.js'));
    }

    // Thêm slide
    insert({ title, description, link, userID, image }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!image)
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = {
                    title, description, link, userCreate: userID
                }
                
                let { name, size, path } = image;
                let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                if(infoImage){
                    dataInsert.image = infoImage.data._id;
                }

                let resultInsert = await that.insertData(dataInsert);
                if(!resultInsert){
                    return resolve({ error: true, message: 'cannot_insert' });
                }
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // update mẫu tin  (cập nhật trạng thái ngưng hoạt động,...)
    update({ slideID, image, title, description, link, userID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {

                if(!ObjectID.isValid(slideID))
                    return resolve({ error: true, message: 'params_invald' });
                
                let dataUpdate = { title, description, link, userUpdate: userID };
                if(image){
                    let { name, size, path } = image;
                    let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                    if(infoImage){
                        dataUpdate.image = infoImage.data._id;
                    }
                }

                let infoAfterUpdate = await SLIDE_COLL.findByIdAndUpdate(slideID, dataUpdate);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // update status
    updateStatus({ slideID, status }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                let STATUS_OF_SYSTEM = [ -1, 0 , 1];
                if(!ObjectID.isValid(slideID) || !STATUS_OF_SYSTEM.includes())
                    return resolve({ error: true, message: 'params_invald' });

                let infoAfterUpdate = await IMAGE_COLL.findByIdAndUpdate(slideID, { status });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    remove({ slideID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(slideID))
                    return resolve({ error: true, message: 'params_invald' });

                let infoAfterUpdate = await SLIDE_COLL.findByIdAndDelete(slideID);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ slideID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid(slideID))
                    return resolve({ error: true, message: 'params_invald' });
            
                let resultFind =  await SLIDE_COLL.findById(slideID).populate({
                    path : 'image'
                })

                if(!resultFind)
                    return resolve({ error: true, message: 'invalid_object_id2' });
                
                return resolve({ error: false, data: resultFind });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAdmin(){
        return new Promise(async (resolve) => {
            try {
                let statusAccess = [ 0, 1 ];
                let resultSearch = await SLIDE_COLL.find({ status: { $in: statusAccess }})
                .populate({
                    path : 'image'
                }).sort({ order: 1 })
                if(!resultSearch)
                    return resolve({ error: true, message: 'cant_get' });
                return resolve({ error: false, data: resultSearch });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListClient(){
        return new Promise(async (resolve) => {
            try {
                let statusActive = 1;
                let resultSearch = await SLIDE_COLL.find({ status: statusActive })
                .populate({
                    path : 'image'
                }).sort({ order: 1 })
                if(!resultSearch)
                    return resolve({ error: true, message: 'cant_get' });
                return resolve({ error: false, data: resultSearch });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update order gallery product
     * Dattv
     */
     updateOrder({ arrayObject }) {
        return new Promise(async resolve => {
            try {
                
                if(!arrayObject.length)
                    return resolve({ error: true, message: 'params_invalid' });
                
                for (let item of arrayObject) {
                    let updateOrder = await SLIDE_COLL.findByIdAndUpdate(item._id, {
                        order: item.index
                    }, { new: true })

                    if(!updateOrder)
                        return resolve({ error: true, message: 'cannot_update_order' });
                }

                return resolve({ error: false, message: "update success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   
}
exports.MODEL = new Model;
