"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_VARIANT_VALUE_COLL = require('../database/product_variant_value-coll');
const PRODUCT_COLL = require('../database/product-coll.js');

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_VARIANT_VALUE_COLL);
    }
    // Tạo mới
    insert({ variant_values, product, price, price_at_compare }) { 
        return new Promise(async (resolve) => {
            try {

                let dataInsert = {
                    variant_values, product, price_at_compare
                }
                if(price){
                    dataInsert.price = price;
                }
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: "cannot_insert" });
                return resolve({ error: false, data : infoAfterInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // update
    update({ productVariantValueID, price, price_at_compare, weight }) { 
        return new Promise(async (resolve) => {
            try {
                console.log({ productVariantValueID, price, price_at_compare });
                let dataUpdate = {
                   
                }
                if(!isNaN(+price)){
                    dataUpdate.price = price;
                }

                if(!isNaN(+price_at_compare)){
                    dataUpdate.price_at_compare = price_at_compare;
                }

                if(!isNaN(+weight)){
                    dataUpdate.weight = weight;
                }
                let infoAfterUpdate = await PRODUCT_VARIANT_VALUE_COLL.findByIdAndUpdate(productVariantValueID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false, data : infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ productVariantValueID }) { 
        return new Promise(async (resolve) => {
            try {
                let listData = await PRODUCT_VARIANT_VALUE_COLL.find({ variant_values: [productVariantValueID], status: 1 });
                if(!listData)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false, data : listData });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByVariant_values({ variant_values }) { 
        return new Promise(async (resolve) => {
            try {
				console.log({ variant_values })
                let infoData = await PRODUCT_VARIANT_VALUE_COLL.findOne({ variant_values, status: 1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get" });
                return resolve({ error: false, data : infoData });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;