"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID 	= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
// const { randomStringUpperCaseAndNumber } 	= require('../utils/string_utils');

/**
 * BASE
 */
const BaseModel 							= require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const TRANSACTION_COLL                		= require('../database/transaction-coll');


class Model extends BaseModel {
    constructor() {
        super(TRANSACTION_COLL);
    }

	insert({ transactionID, orderID, orderInfo, amount, bankCode, ipAddr, customerID, status, message }){
		return new Promise(async resolve => {
			try {
				if (!ObjectID.isValid(orderID) || !ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				if(!transactionID || !amount)
					return resolve({ error: true, message: 'transactionID_and_amount_is_required' });

				let infoAfterAdd = await this.insertData({
					transactionID,
					order: orderID, 
					customer: customerID, 
					orderInfo, 
					amount, 
					bankCode,
					ipAddr,
					status,
					message
				});

				if(!infoAfterAdd)
                    return resolve({ error: true, message: "cannot_add_transaction" });

                return resolve({ error: false, data: infoAfterAdd });
			} catch (error) {
				return resolve({ error: true, messsage: error.message });
			}
		})
	}

    updateStatus({ transactionID, status }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.updateOne({ _id: transactionID }, {
					$set: { status }
				});

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_update_status_transaction" });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoTransaction = await TRANSACTION_COLL
					.findById(transactionID)
					.populate('order customer')
					.lean();

                if(!infoTransaction)
                    return resolve({ error: true, message: "cannot_get_info_transaction" });

                return resolve({ error: false, data: infoTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByCustomer({ customerID, status, page = 1, limit = 30 }) {
        return new Promise(async resolve => {
            try {
				if (!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

				let conditionObj = { customer: customerID };
				if(!isNaN(status)){
					conditionObj.status = status;
				}

				limit = +limit;
				page  = +page;

				let listTransactionOfCustomer = await TRANSACTION_COLL
					.find(conditionObj)
					.populate('order customer')
					.sort({ modifyAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit)
					.lean();

                if(!listTransactionOfCustomer) 
                    return resolve({ error: true, message: "cannot_get_list_transaction_of_customer" });

				let totalTransaction = await TRANSACTION_COLL.countDocuments(conditionObj);

                return resolve({ 
					error: false, 
					data: {
						listTransactionOfCustomer,
						currentPage: page,
						perPage: limit,
						total: totalTransaction
					} 
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilterWithPaging({ 
		 keyword, status, customerID, bankCode, fromDate, toDate, fromPrice, toPrice, page = 1, limit = 30 
	}) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {};
				limit = +limit;
				page  = +page;

                if(keyword){
					let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
					key = new RegExp(key, 'i');

					conditionObj.$or = [
						{ 'order.orderID': key },
						{ 'customer.customerID': key },
						{ 'customer.fullname': key },
						{ 'transactionID': key },
						{ 'bankCode': key },
					]
				}

				if(fromPrice){
					conditionObj['order.amount'] = {
						$gte: +fromPrice
					}
				}

				if(toPrice){
					conditionObj['order.amount'] = {
						$lt: +toPrice
					}
				}

				if(fromPrice && toPrice){
					conditionObj['order.amount'] = { 
						$gte: +fromPrice, 
						$lt: +toPrice
					}
				}

				if(fromDate && toDate){
					conditionObj.createAt = {
                        $gte: new Date(fromDate),
                        $lt: new Date(toDate)
                    }
				}

				customerID 	&& (conditionObj['customer._id'] = ObjectID(customerID));
				bankCode 	&& (conditionObj.bankCode 		 = bankCode);
				status 		&& (conditionObj.status 		 = +status);

				let pipeline = [
					{
						$lookup: {
						   from: 'orders',
						   localField: 'order',
						   foreignField: '_id',
						   as: 'order'
						 }
					},
					{
						$lookup: {
						   from: 'customers',
						   localField: 'customer',
						   foreignField: '_id',
						   as: 'customer'
						 }
					},
					{
						$match: conditionObj
					},
					{ 
						$unwind: {
							path: '$order',
							preserveNullAndEmptyArrays: true
						}
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					}
				];

				let listTransaction = await TRANSACTION_COLL.aggregate([
					...pipeline,
					{ $limit: limit },
					{ $skip: (page - 1) * limit },
					{ $sort: { modifyAt: -1 } }
				])

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

				let totalTransaction = await TRANSACTION_COLL.aggregate([
					...pipeline,
					{ $count: 'total' },
				]);

                return resolve({ 
					error: false, 
					data: {
						listTransaction,
						currentPage: page,
						perPage: limit,
						total: totalTransaction.length && totalTransaction[0].total
					}
				});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByFilter({ keyword, status, customerID, bankCode, fromDate, toDate, fromPrice, toPrice }) {
	   return new Promise(async resolve => {
		   try {
			   let conditionObj = {};

			   if(keyword){
				   let key = keyword.split(" ");
				   key = '.*' + key.join(".*") + '.*';
				   key = new RegExp(key, 'i');

				   conditionObj.$or = [
					   { 'order.orderID': key },
					   { 'customer.customerID': key },
					   { 'customer.fullname': key },
					   { 'transactionID': key },
					   { 'bankCode': key },
				   ]
			   }

			   if(fromPrice){
				   conditionObj['order.amount'] = {
					   $gte: +fromPrice
				   }
			   }

			   if(toPrice){
				   conditionObj['order.amount'] = {
					   $lt: +toPrice
				   }
			   }

			   if(fromPrice && toPrice){
				   conditionObj['order.amount'] = { 
					   $gte: +fromPrice, 
					   $lt: +toPrice
				   }
			   }

			   if(fromDate && toDate){
				   conditionObj.createAt = {
					   $gte: new Date(fromDate),
					   $lt: new Date(toDate)
				   }
			   }

			   customerID 	&& (conditionObj['customer._id'] = ObjectID(customerID));
			   bankCode 	&& (conditionObj.bankCode 		 = bankCode);
			   status 		&& (conditionObj.status 		 = +status);

			   let listTransaction = await TRANSACTION_COLL.aggregate([
					{
						$lookup: {
							from: 'orders',
							localField: 'order',
							foreignField: '_id',
							as: 'order'
						}
					},
					{
						$lookup: {
							from: 'customers',
							localField: 'customer',
							foreignField: '_id',
							as: 'customer'
						}
					},
					{
						$match: conditionObj
					},
					{ 
						$unwind: {
							path: '$order',
							preserveNullAndEmptyArrays: true
						}
					},
					{ 
						$unwind: {
							path: '$customer',
							preserveNullAndEmptyArrays: true
						}
					},
					{ $sort: { modifyAt: -1 } }
			   ])

			   if(!listTransaction)
				   return resolve({ error: true, message: "cannot_get_list_transaction" });

			   return resolve({ error: false, data: listTransaction });
		   } catch (error) {
			   return resolve({ error: true, message: error.message });
		   }
	   })
   }


}

exports.MODEL = new Model;
