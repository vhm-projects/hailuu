"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_VARIANT_COLL = require('../database/product_variant-coll');
const PRODUCT_COLL = require('../database/product-coll.js');

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_VARIANT_COLL);
    }

    // Tạo mới
    insert({ name, productID, parentID, parentPreviousID, price, level }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name || !productID)
                    return resolve({ error: true, message: 'params_not_valid' });

                let dataInsert = { name };
                if(productID)
                    dataInsert.product = productID;

                if(parentID)
                    dataInsert.parent = parentID;

                if(parentPreviousID)
                    dataInsert.parentPrevious = parentPreviousID;

                if(price)
                    dataInsert.price = price;

                if(level)
                    dataInsert.level = level;
                
                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;