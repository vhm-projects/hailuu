"use strict";
let fs                                    = require("fs");
let path                                  = require('path');
const promise                             = require('bluebird');
const BaseModel                           = require('./intalize/base_model');
const ObjectID                            = require('mongoose').Types.ObjectId;
const GOOD_PROJECT_COLL                   = require('../database/good_project-coll');
const ADMIN_COLL                          = require('../database/user-coll.js');
const IMAGE_COLL                          = require('../database/image-coll');
const IMAGE_MODEL                         = require('../models/image').MODEL;

class Model extends BaseModel {
    constructor() {
        super(require('../database/good_project-coll.js'));
    }

    // Thêm slide
    insert({ title, description, price, userID, image, type }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!image)
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = {
                    title, description, price, userCreate: userID, type
                }
                
                let { name, size, path } = image;
                let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                if(infoImage){
                    dataInsert.image = infoImage.data._id;
                }

                let resultInsert = await that.insertData(dataInsert);
                if(!resultInsert){
                    return resolve({ error: true, message: 'cannot_insert' });
                }
                return resolve({ error: false, data: resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // update mẫu tin  (cập nhật trạng thái ngưng hoạt động,...)
    update({  goodProjectID, image, title, description, price, userID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {

                if(!ObjectID.isValid( goodProjectID))
                    return resolve({ error: true, message: 'params_invald' });
                
                let dataUpdate = { title, description, price, userUpdate: userID };
                if(image){
                    let { name, size, path } = image;
                    let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                    if(infoImage){
                        dataUpdate.image = infoImage.data._id;
                    }
                }

                let infoAfterUpdate = await GOOD_PROJECT_COLL.findByIdAndUpdate( goodProjectID, dataUpdate);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // update status
    updateStatus({  goodProjectID, status }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                let STATUS_OF_SYSTEM = [ -1, 0 , 1];
                if(!ObjectID.isValid( goodProjectID) || !STATUS_OF_SYSTEM.includes())
                    return resolve({ error: true, message: 'params_invald' });

                let infoAfterUpdate = await IMAGE_COLL.findByIdAndUpdate( goodProjectID, { status });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    remove({  goodProjectID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid( goodProjectID))
                    return resolve({ error: true, message: 'params_invald' });

                let infoAfterUpdate = await GOOD_PROJECT_COLL.findByIdAndDelete( goodProjectID);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "update_faild" });
            
                return resolve({ error: false, data: infoAfterUpdate });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({  goodProjectID }) { 
        const that = this;
        return new Promise(async (resolve) => {
            try {
                if(!ObjectID.isValid( goodProjectID))
                    return resolve({ error: true, message: 'params_invald' });
            
                let resultFind =  await GOOD_PROJECT_COLL.findById( goodProjectID).populate({
                    path : 'image'
                })

                if(!resultFind)
                    return resolve({ error: true, message: 'invalid_object_id2' });
                
                return resolve({ error: false, data: resultFind });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAdmin(){
        return new Promise(async (resolve) => {
            try {
                let statusAccess = [ 0, 1 ];
                
                let resultSearch = await GOOD_PROJECT_COLL.find({ status: { $in: statusAccess }})
                .populate({
                    path : 'image'
                }).sort({ order: 1 })

                if(!resultSearch)
                    return resolve({ error: true, message: 'cant_get' });
                return resolve({ error: false, data: resultSearch });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListEndUserByType({ type }){
        return new Promise(async (resolve) => {
            try {
                let statusAccess = 1
                let resultSearch = await GOOD_PROJECT_COLL.find({ status: statusAccess, type })
                .populate({
                    path: 'image'
                }).
                sort({ order: 1 });
                if(!resultSearch)
                    return resolve({ error: true, message: 'cant_get' });
                return resolve({ error: false, data: resultSearch });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListClient(){
        return new Promise(async (resolve) => {
            try {
                let statusActive = 1;
                let resultSearch = await GOOD_PROJECT_COLL.find({ status: statusActive })
                .populate({
                    path : 'image'
                }).sort({ order: 1 })
                
                if(!resultSearch)
                    return resolve({ error: true, message: 'cant_get' });
                return resolve({ error: false, data: resultSearch });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update order
     * Dattv
     */
     updateOrder({ arrayObject }) {
        return new Promise(async resolve => {
            try {
                
                if(!arrayObject.length)
                    return resolve({ error: true, message: 'params_invalid' });
                
                for (let item of arrayObject) {
                    let updateOrder = await GOOD_PROJECT_COLL.findByIdAndUpdate(item._id, {
                        order: item.index
                    }, { new: true })

                    if(!updateOrder)
                        return resolve({ error: true, message: 'cannot_update_order' });
                }

                return resolve({ error: false, message: "update success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   
}
exports.MODEL = new Model;
