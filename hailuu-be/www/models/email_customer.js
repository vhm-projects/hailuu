"use strict";
const Object = require('mongoose').Types.ObjectId;
const { validEmail } = require('../utils/string_utils');
const EMAIL_CUSTOMER_COLL = require('../database/email_customer-coll.js');

const BaseModel = require('./intalize/base_model');

class Model extends BaseModel {

    constructor() {
        super(EMAIL_CUSTOMER_COLL)
    }

    insert({ email }) {
        return new Promise(async resolve => {
            try {
                if(!validEmail(email))
                    return resolve({ error: true, message: 'email_not_valid' });

                let isExistEmail = await EMAIL_CUSTOMER_COLL.findOne({ email });
                if(isExistEmail)
                    return resolve({ error: true, message: 'email_exist' });

                let infoEmailCustomer = await this.insertData({ email });
                
                if(!infoEmailCustomer)
                    return resolve({ error: true, message: 'cannot_insert_email_customer' });

                return resolve({ error: false, data: infoEmailCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListEmailCustomer() {
        return new Promise(async resolve => {
            try {
                let listEmailCustomer = await EMAIL_CUSTOMER_COLL.find({});
                if(!listEmailCustomer)
                    return resolve({ error: true, message: 'cannot_get_list_email_customer' });

                return resolve({ error: false, data: listEmailCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ emailCustomerID }) {
        return new Promise(async resolve => {
            try {
                if(!Object.isValid(emailCustomerID)) {
                    return resolve({ error: true, message: 'params_Invalid' });
                }

                let infoEmailCustomerDelete = await EMAIL_CUSTOMER_COLL.findByIdAndRemove({ _id: emailCustomerID });
                if(!infoEmailCustomerDelete)
                    return resolve({ error: true, message: 'cannot_delete_email_customer' });
                
                return resolve({ error: false, data: infoEmailCustomerDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;