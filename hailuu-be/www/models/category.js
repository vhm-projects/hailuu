"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { convertToSlug } = require('../utils/utils');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const CATEGORY_COLL = require('../database/category-coll');
const BLOG_COLL = require('../database/blog-coll');


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_COLL);
    }

    insert({ name, slug, description, status, userCreate }) {
        return new Promise(async resolve => {
            try {
                if(!name || !slug)
                    return resolve({ error: true, message: 'params_not_valid' });

				name = name.trim();
				slug = convertToSlug(slug);

                const checkExists = await CATEGORY_COLL.findOne({
					$or: [
						{ name },
						{ slug }
					]
				});
                if(checkExists)
                    return resolve({ error: true, message: 'name_or_slug_existed' });
    
                const dataInsert = {
                    name,
                    slug,
					description,
                    status,
					userCreate
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_category_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoCategory = await CATEGORY_COLL.findOne({ _id: categoryID, status: { $in: [0,1] } });
                if(!infoCategory)
                    return resolve({ error: true, message: 'category_is_not_exists' });

                return resolve({ error: false, data: infoCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ categoryID, name, slug, description, status, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await CATEGORY_COLL.findById(categoryID);
                if(!checkExists)
                    return resolve({ error: true, message: 'category_is_not_exists' });

                const dataUpdateCategory = { userUpdate };
                name    		&& (dataUpdateCategory.name 		= name.trim());
                slug 			&& (dataUpdateCategory.slug 		= convertToSlug(slug));
                description   	&& (dataUpdateCategory.description 	= description);

				if([0,1].includes(+status)){
					dataUpdateCategory.status = status;
				}

                await this.updateWhereClause({ _id: categoryID }, {
                    ...dataUpdateCategory
                });

                return resolve({ error: false, data: dataUpdateCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await CATEGORY_COLL.findByIdAndUpdate(categoryID, { status: -1 });
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'category_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, status = { $in: [0,1] } }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { name: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                    ]
                }

				condition.status = status;
                const listCategories = await CATEGORY_COLL.find(condition).lean().sort({ order: 1 });
                if(!listCategories)
                    return resolve({ error: true, message: 'not_found_categories_list' });
                let listDataAfterAddAmountBlog = []
                for (let category of listCategories) {
                    let amountBlog = await BLOG_COLL.count({ category: category._id });
                    category = { ...category, amountBlog }
                    listDataAfterAddAmountBlog.push(category);
                }

                return resolve({ error: false, data: listDataAfterAddAmountBlog });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update order
     * Dattv
     */
     updateOrder({ arrayObject }) {
        return new Promise(async resolve => {
            try {
                
                if(!arrayObject.length)
                    return resolve({ error: true, message: 'params_invalid' });
                
                for (let item of arrayObject) {
                    let updateOrder = await CATEGORY_COLL.findByIdAndUpdate(item._id, {
                        order: item.index
                    }, { new: true })

                    if(!updateOrder)
                        return resolve({ error: true, message: 'cannot_update_order' });
                }

                return resolve({ error: false, message: "update success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
