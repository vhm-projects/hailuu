"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const BaseModel                     = require('./intalize/base_model');
const MENU_COLL                     = require('../database/menu-coll');
const PRODUCT_COLL                  = require('../database/product-coll');

const COMMENT_COLL                  = require('../database/comment-coll');
const ORDER_LINE_COLL               = require('../database/order_line-coll');
const VARIANT_COLL                  = require('../database/variant-coll');
const IMAGE_COLL                    = require('../database/image-coll');
const { convertToSlug, unique, checkObjectIDs, generate }             = require('../utils/utils')
const fs                            = require('fs');
const Path                          = require('path');
const moment                        = require('moment');
moment.locale('vi')

const VARIANT_MODEL                 = require('../models/variant').MODEL;
const VARIANT_VALUE_MODEL           = require('../models/variant_value').MODEL;
const PRODUCT_VARIANT_VALUE_MODEL   = require('../models/product_variant_value').MODEL;

class Model extends BaseModel {
    constructor() {
        super(require('../database/product-coll'))
    }

    /**
     * Add PRODUCT
     * Dattv
     */
    insert({ name, description, menu, userID, avatar, excelFile, illustratorFile, vectorFile, pdfFile, images,
            introProduct, noteOrder, priceList, priceBefore, priceAfter, ratioSale, flashSale, flashPrint, basicParameters, variant }) {
        return new Promise(async resolve => {
            try {
                if(!name || !description || !ObjectID.isValid(menu) || !priceBefore )
                    return resolve({ error: true, message: 'params_invalid' });

                // return console.log({  name, description, menu, userID, avatar, images, arrayFile,
                //     introProduct, noteOrder, priceList, priceBefore, priceAfter, ratioSale, flashSale, flashPrint, basicParameters, variant  });

                let checkExisted = await PRODUCT_COLL.findOne({ name });
                if(checkExisted)
                    return resolve({ error: true, message: 'name_is_existed' });
                let slug = `${convertToSlug(name)}-${generate(4)}`

                let dataInsert = {
                    name,
                    description,
                    menu,
                    slug,
                    userCreate: userID,
                    introProduct, 
                    noteOrder, 
                    priceList,
                    priceBefore,
                    priceAfter,
                    avatar,
                    fileExcel: excelFile,
                    fileIllustrator: illustratorFile,
                    fileVectorEPS: vectorFile,
                    filePDF: pdfFile,
                    ratioSale,
                    flashSale,
                    flashPrint,
                    basicParameters
                }

                // console.log({dataInsert});

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert_product' });

                //========================Start Xử lý variant
                let productID = infoAfterInsert._id;
                
                let { typeShowParent, typeShowChild, nameVariantParent, nameVariantChild, propertyVariantParent, propertyVariantChild } = variant;
                // let arrVariantProduct = [];
                let infoVariantParent = await VARIANT_MODEL.insert({ name: nameVariantParent, product: productID, typeShow: typeShowParent });
                let infoVariantChild = await VARIANT_MODEL.insert({ name: nameVariantChild, product: productID, typeShow: typeShowChild });
                
                for (const item of propertyVariantParent) {
                    let { id, name, price, priceAfter } = item;
                    let infoVariantValueParent = await VARIANT_VALUE_MODEL.insert({ name, variant: infoVariantParent.data._id, product: productID  });
                    let infoProductVariantValue = await PRODUCT_VARIANT_VALUE_MODEL.insert({ variant_values: [infoVariantValueParent.data._id], product: productID, price: priceAfter, price_at_compare: price });
                    if( propertyVariantChild ){
                        for (const item1 of propertyVariantChild) {
                            let { parentID, name, price, priceAfter } = item1;
                            if(id == parentID){
                                let infoVariantValueChild = await VARIANT_VALUE_MODEL.insert({ parentID: infoVariantValueParent.data._id, name, variant: infoVariantChild.data._id, product: productID  });
                                let infoProductVariantValueChild = await PRODUCT_VARIANT_VALUE_MODEL.insert({ variant_values: [infoVariantValueParent.data._id, infoVariantValueChild.data._id], product: productID, price: priceAfter, price_at_compare: price });
                            }
                        }
                    }
                }
                //========================End Xử lý variant

                //Thêm sản phẩm vào danh sách sản phẩm của menu
                await MENU_COLL.findByIdAndUpdate(menu, {
                    $addToSet: { products: infoAfterInsert._id }
                }, { new: true });

                //Thêm danh sách images product
                if(images && images.length){
                    infoAfterInsert = await PRODUCT_COLL.findByIdAndUpdate(infoAfterInsert._id, {
                        $addToSet: { images: { $each: images } }
                    }, { new: true })
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin sản phẩm
     * Dattv
     */
    getInfo({ productID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoProduct = await PRODUCT_COLL.findById(productID)
                .populate("menu avatar images fileExcel fileIllustrator fileVectorEPS filePDF comments")
                .populate({
                    path: 'menu',
                    select: '_id name parent',
                    populate: {
                        path: 'parent',
                        select: '_id name'
                    }
                })
                .populate({
                    path: 'variants',
                    populate: {
                        path: 'childs',
                    }
                })

                if(!infoProduct)
                    return resolve({ error: true, message: 'cannot_get_product' });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin sản phẩm theo slug
     * Dattv
     */
    getInfoBySlug({ slug }) {
        return new Promise(async resolve => {
            try {
                if(!slug)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoProduct = await PRODUCT_COLL.findOne({ slug })
                .populate("menu avatar images comments fileExcel fileIllustrator fileVectorEPS filePDF")
                .populate({
                    path: 'menu',
                    select: '_id name parent ',
                    populate: {
                        path: 'parent',
                        select: '_id name slug'
                    }
                })
                .populate({
                    path: 'variants',
                    populate: {
                        path: 'faceNumber printSpecification amount',
                    }
                })
                .populate({
                    path: 'variantOption',
                    populate: {
                        path: 'childs',
                    }
                })

                if(!infoProduct)
                    return resolve({ error: true, message: 'cannot_get_product' });

                return resolve({ error: false, data: infoProduct  });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin sản phẩm theo slug
     * Dattv
     */
    getInfoWithSlug({ slug }) {
        return new Promise(async resolve => {
            try {
                if(!slug)
                    return resolve({ error: true, message: 'params_invalid' });

                let infoProduct = await PRODUCT_COLL.findOne({ slug });

                if(!infoProduct)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                return resolve({ error: false, data: infoProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy danh sách sản phẩm theo menu
     * Dattv
     */
    getListProductOfMenu({ menuID, limit = 100, productID }) {
        return new Promise(async resolve => {
            try {
                let NO_ACCEPT_STATUS = -1;
                if(!ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFind = { menu: menuID, status: { $ne: NO_ACCEPT_STATUS } }
                if(productID){
                    dataFind._id = { $ne: productID }
                }
                let listProductOfMenu = await PRODUCT_COLL.find(dataFind)
                .select("menu name description avatar priceAfter priceBefore flashSale slug")
                .populate("menu avatar").limit(limit).sort({createAt: -1});

                if(!listProductOfMenu)
                    return resolve({ error: true, message: 'cannot_get_product' });

                return resolve({ error: false, data: listProductOfMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy danh sách sản phẩm
     * Dattv
     */
    getList({ status, menuID }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                let STATUS_REMOVE = -1;

                if(status){
                    conditionObj.status = status;
                }else{
                    conditionObj.status = {
                        $ne: STATUS_REMOVE
                    }
                }

                if(menuID && ObjectID.isValid(menuID)){
                    conditionObj.menu = menuID;
                }

                let listProduct = await PRODUCT_COLL.find(conditionObj)
                .select("menu name description avatar priceAfter priceBefore flashSale slug")
                .populate("avatar")
                .populate({
                    path: "menu",
                    populate: {
                        path: "parent"
                    }
                })
                .sort({createAt: -1});

                if(!listProduct)
                    return resolve({ error: true, message: 'cannot_get_product' });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy số lượng đơn hàng sản phẩm
     * Dattv
     */
    getCountOrderOfProduct({ status, menuID }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                let STATUS_REMOVE = -1;

                if(status){
                    conditionObj.status = status;
                }else{
                    conditionObj.status = {
                        $ne: STATUS_REMOVE
                    }
                }

                if(menuID && ObjectID.isValid(menuID)){
                    conditionObj.menu = menuID;
                }

                let listProduct = await ORDER_LINE_COLL.aggregate([
                    {
                        $project: {
                            _id: 1,
                            status: 1,
                            menu: 1, 
                            product: 1
                        }
                    },
                    {
                        $match: conditionObj
                    },
                    {
                        $group: {
                            _id: '$product',
                            count: { $sum: 1 }
                        }
                    },
                ])

                if(!listProduct)
                    return resolve({ error: true, message: 'cannot_get_product' });

                return resolve({ error: false, data: listProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Xóa sản phẩm
     * Dattv
     */
    remove({ productID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await PRODUCT_COLL.findByIdAndDelete(productID).populate("image");

                //Xóa khỏi products trong collection menu
                let { menu } = infoAfterDelete;
                await MENU_COLL.findByIdAndUpdate(menu, {
                    $pull: { products: infoAfterDelete._id }
                }, {new: true })

                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_remove_product' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Khoá sản phẩm
     * Dattv
     */
    removeProductNewVer({ productID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await PRODUCT_COLL.findByIdAndUpdate(productID, {status: -1 }, {new: true});

                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_remove_product' });

                let { menu } = infoAfterDelete;
                await MENU_COLL.findByIdAndUpdate(menu, {
                    $pull: { products: infoAfterDelete._id }
                }, {new: true });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật sản phẩm
     * Dattv
     */
    update({ productID, name, description, url, menu, introProduct, noteOrder, priceList, status, color, 
        priceBefore, priceAfter, ratioSale, flashSale, flashPrint, basicParameters, avatar, excelFile, illustratorFile, vectorFile, pdfFile, 
        imageIDRemmove, images, userID }) {
        return new Promise(async resolve => {
            try {

                if(!name || !description || !priceBefore || !ObjectID.isValid(productID) || !ObjectID.isValid(userID) || !ObjectID.isValid(menu) )
                    return resolve({ error: true, message: 'params_invalid' });

                let infoProduct = await PRODUCT_COLL.findById(productID);
                let { menu: menuOld } = infoProduct;

                if(menuOld.toString() != menu.toString()){
                    await MENU_COLL.findByIdAndUpdate(menuOld, { 
                        $pull: { products: productID }
                    }, {new: true})
                }

                let dataUpdate = {
                    name, 
                    description, 
                    slug: url,
                    menu, 
                    introProduct, 
                    noteOrder, 
                    priceList, 
                    status: Number(status), 
                    color, 
                    priceBefore,
                    priceAfter,
                    ratioSale: Number(ratioSale), 
                    flashSale, 
                    flashPrint,
                    basicParameters,
                    userID
                }

                if(avatar && ObjectID.isValid(avatar)){
                    dataUpdate.avatar = avatar;
                }

                if(excelFile && ObjectID.isValid(excelFile)){
                    dataUpdate.fileExcel = excelFile;
                }

                if(illustratorFile && ObjectID.isValid(illustratorFile)){
                    dataUpdate.fileIllustrator = illustratorFile;
                }

                if(vectorFile && ObjectID.isValid(vectorFile)){
                    dataUpdate.fileVectorEPS = vectorFile;
                }

                if(pdfFile && ObjectID.isValid(pdfFile)){
                    dataUpdate.filePDF = pdfFile;
                }

                let infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, dataUpdate, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_product' });

                //Xoá khỏi danh sách images product
                if(imageIDRemmove && imageIDRemmove.length){
                    infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(infoAfterUpdate._id, {
                        $pullAll: { images: imageIDRemmove }
                    }, { new: true });

                    for (const imageID of imageIDRemmove) {
                        await IMAGE_COLL.findByIdAndDelete(imageID);
                    }
                }

                //Thêm danh sách images product
                if(images && images.length){
                    infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(infoAfterUpdate._id, {
                        $addToSet: { images: { $each: images } }
                    }, { new: true });
                }

                // //Xoá khỏi danh sách file product
                // if(fileIDRemove && fileIDRemove.length){
                //     infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(infoAfterUpdate._id, {
                //         $pullAll: { arrayFile: fileIDRemove }
                //     }, { new: true });

                //     for (const imageID of fileIDRemove) {
                //         await IMAGE_COLL.findByIdAndDelete(imageID);
                //     }
                // }

                // //Thêm danh sách file product
                // if(files && files.length){
                //     infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(infoAfterUpdate._id, {
                //         $addToSet: { arrayFile: { $each: files } }
                //     }, { new: true });
                // }

                if(menuOld.toString() != menu.toString()){
                    await MENU_COLL.findByIdAndUpdate(menu, { 
                        $addToSet: { products: productID }
                    }, {new: true})
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật hình ảnh sản phẩm
     * Dattv
     */
    updateImageProduct({ productID, imageIDRemove, filesArrayInsert }) {
        return new Promise(async resolve => {
            try {

                if(!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let action = false;
                //Nếu có xóa ảnh
                if(ObjectID.isValid(imageIDRemove)){

                    //Xóa ảnh
                    let infoImageDelete = await IMAGE_COLL.findByIdAndDelete(imageIDRemove);
                    await fs.unlinkSync(infoImageDelete.path);

                    //Xóa trong product
                    await PRODUCT_COLL.findByIdAndUpdate(productID, {
                        $pull: { image: imageIDRemove }
                    })

                    if(infoImageDelete.main == 1){
                        //Nếu ảnh là main thì update ảnh đầu tiên của product là 1
                        let infoProduct = await PRODUCT_COLL.findById(productID);
                        let firstImageProductID = infoProduct.image[0];
                        await IMAGE_COLL.findByIdAndUpdate(firstImageProductID, {
                            main: 1
                        }, { new: true })
                    }

                    action = true;
                }

                //Nếu có image thì insert image
                if(filesArrayInsert){
                    for (let image of filesArrayInsert) {
                        let { filename, path } = image;

                        let infoFileInsert = new IMAGE_COLL({ name: filename, path, product: productID });
                        await infoFileInsert.save();
                        
                        //Đồng thời đẩy vào image product
                        await PRODUCT_COLL.findByIdAndUpdate(productID, {
                            $addToSet: { image: infoFileInsert._id }
                        }, { new: true });
                    }
                    action = true;
                }
                
                if(action == false)
                    return resolve({ error: true, message: 'cannot_update_product' });

                return resolve({ error: false, data: 'update_success' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật trạng thái sản phẩm
     * Dattv
     */
    updateStatus({ productID, status, userID }) {
        return new Promise(async resolve => {
            try {
                if(!status || !ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, {
                    status, userUpdate: userID
                }, {new: true});

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_product' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật hình ảnh chính của sản phẩm
     * Dattv
     */
    updateMainImageProduct({ productID, imageID, userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(productID, imageID))
                    return resolve({ error: true, message: 'params_invalid' });

                //B1. Update isMain tất cả hình ảnh của sản phẩm => 0 (phụ)
                let infoAllImageHaveIsMain = await IMAGE_COLL.findOne({ product: productID, main: 1 });
                if (infoAllImageHaveIsMain) {
                    let infoImageIsMainOld = await IMAGE_COLL.findOneAndUpdate({
                        main: 1, product: productID
                    }, { main: 0 }, { new: true }); //Result here
                    if (!infoImageIsMainOld) return resolve({ error: true, message: 'cannot_update_old_image' });
                }
                //B2. Update isMain hình ảnh => 1 (chính)
                let infoImageProductAfterUpdate = await IMAGE_COLL.findByIdAndUpdate(imageID, {
                    main: 1, userUpdate: userID
                }, { new: true });

                if (!infoImageProductAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_image_product' });

                return resolve({ error: false, data: infoImageProductAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Tìm kiếm sản phẩm theo tên
     * Dattv
     */
    searchWithKey({ key, limit, sort }) {
        return new Promise(async resolve => {
            try {

                let conditionObj = {};

                //Tìm theo key
                if(key && key.length > 0){
                    let keyword = key.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionObj.name = new RegExp(keyword, 'i');
                }
                const STATUS_ACTIVE = 1;
                conditionObj.status = STATUS_ACTIVE;

                /**
                 *  Điều kiện sắp xếp
                 *  1: Giá từ cao đến thấp
                 *  2: Giá từ thấp đến cao
                 *  3: Giời gian mới nhất
                 *  4: Thời gian cũ nhât
                 */
                let dataSort = {};
                if(sort == 1){
                    dataSort.priceBefore = -1;
                }
                if(sort == 2){
                    dataSort.priceBefore = 1;
                }

                if(sort == 3){
                    dataSort.createAt = -1;
                }
                if(sort == 4){
                    dataSort.createAt = 1;
                }

                let listData = await PRODUCT_COLL.find(conditionObj)
                .select("menu name description avatar priceAfter priceBefore flashSale slug")
                .populate("avatar").limit(limit).sort(dataSort);

                if(!listData)
                    return resolve({ error: true, message: 'cannot_find_data' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách flashsale
     * Depv
     */
    listFlashsale({ limit, sort }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                 /**
                 *  Điều kiện sắp xếp
                 *  1: Giá từ cao đến thấp
                 *  2: Giá từ thấp đến cao
                 *  3: Giời gian mới nhất
                 *  4: Thời gian cũ nhât
                 */
                let dataSort = {};
                if(sort == 1){
                    dataSort.priceBefore = -1;
                }
                if(sort == 2){
                    dataSort.priceBefore = 1;
                }

                if(sort == 3){
                    dataSort.createAt = -1;
                }
                if(sort == 4){
                    dataSort.createAt = 1;
                }

                let listData = await PRODUCT_COLL.find({ status: STATUS_ACTIVE, priceAfter: { $ne: null } })
                                    .select("menu name description avatar priceAfter priceBefore flashSale slug")
                                    .populate({ path: "avatar" })
                                    .limit(limit)
                                    .sort(dataSort);
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Danh sách sản phẩm được yêu thích
     * BA: Dựa vào lượt đánh giá Sao khi bình luận, và số lượt order
     * Depv
     */
    listProductFavorite({ page = 1, limit = 10, type = 1 }) {
        return new Promise(async resolve => {
            try {
                /**
                 * BA 
                 * 1 Dựa vào việc đánh giá trong bình luận.
                 * 2 Dựa vào lượt order trên sản phẩm.
                 */
                /**
                 * Type
                 *  1: Theo tuần
                 *  2: Theo Tháng
                 *  3: Theo Quý
                 */
                let objQuery = {};
                // Lấy data theo tuần
                if(type == 1){
                    var firstday = new moment().startOf('isoWeek').format("YYYY-MM-DD");
                    var lastday = new moment().endOf("isoWeek").format("YYYY-MM-DD");
                    // console.log({ firstday, lastday });
                    objQuery = {
                        product: { $exists: true },
                        createAt: {
                            $gte: moment(firstday).startOf('day')._d,
                            $lte: moment(lastday).endOf('day')._d,
                            // $gte: new Date(firstday),
                            // $lte: new Date(lastday),
                        }
                    }
                }
                // console.log({ objQuery });
                // Lấy data theo tháng
                if(type == 2){
                    var firstday = new moment().startOf('month').format("YYYY-MM-DD");
                    var lastday = new moment().endOf("month").format("YYYY-MM-DD");
                    objQuery = {
                        product: { $exists: true },
                        createAt: {
                            $gte: moment(firstday).startOf('day')._d,
                            $lte: moment(lastday).endOf('day')._d,
                            // $gte: firstday,
                            // $lte: lastday
                        }
                    }
                }

                // Lấy data theo quý
                if(type == 3){
                    var firstday = new moment().startOf('quarter').format("YYYY-MM-DD");
                    var lastday = new moment().endOf("quarter").format("YYYY-MM-DD");
                    objQuery = {
                        product: { $exists: true },
                        createAt: {
                            $gte: moment(firstday).startOf('day')._d,
                            $lte: moment(lastday).endOf('day')._d,
                            // $gte: firstday,
                            // $lte: lastday
                        }
                    }
                }
                // console.log({ objQuery });
                // Danh sách comment của sản phẩm
                let listCommentProduct = await COMMENT_COLL.find(objQuery).select("product");
                // Danh sách order của sản phẩm
                let listOderProduct = await ORDER_LINE_COLL.find(objQuery).select("product");
                
                let arrIdProductFavorite = []
                listCommentProduct.forEach(element => {
                    arrIdProductFavorite.push(ObjectID(element.product));
                });

                listOderProduct.forEach(element => {
                    arrIdProductFavorite.push(ObjectID(element.product));
                });

                const STATUS_ACTIVE = 1;
                let listData = await PRODUCT_COLL.find({ _id: { $in: unique(arrIdProductFavorite) }, status: STATUS_ACTIVE })
                                    .select("name description avatar priceAfter priceBefore flashSale slug")
                                    .populate({ path: "avatar" })
                                    .skip((page * limit) - limit)
                                    .limit(limit);
                let totalItem = await PRODUCT_COLL.count({ _id: { $in: unique(arrIdProductFavorite) }, status: STATUS_ACTIVE });
                let pages = Math.ceil(totalItem/limit);

                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                
                return resolve({ error: false, data: { listData, page, pages } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Danh sách sản phẩm mới nhất
     * Depv
     */
    listProductNews({ limit, sort = 3 }) {
        return new Promise(async resolve => {
            try {

                const STATUS_ACTIVE = 1;
                 /**
                 *  Điều kiện sắp xếp
                 *  1: Giá từ cao đến thấp
                 *  2: Giá từ thấp đến cao
                 *  3: Giời gian mới nhất
                 *  4: Thời gian cũ nhât
                 */
                let dataSort = {};
                if(sort == 1){
                    dataSort.priceBefore = -1;
                }
                if(sort == 2){
                    dataSort.priceBefore = 1;
                }

                if(sort == 3){
                    dataSort.createAt = -1;
                }
                if(sort == 4){
                    dataSort.createAt = 1;
                }
                let listData = await PRODUCT_COLL.find({ status: STATUS_ACTIVE })
                                    .select("menu name description avatar priceAfter priceBefore flashSale slug")
                                    .populate({ path: "avatar" })
                                    .limit(limit)
                                    .sort(dataSort);

                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Danh sach sản phẩm dịch vụ
     * Depv
     */
    listAllProduct({ limit }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                let listData = await PRODUCT_COLL.find({ status: STATUS_ACTIVE })
                                    .select("name description avatar priceAfter priceBefore flashSale slug")
                                    .populate({ path: "avatar" })
                                    .limit(limit)
                                    .sort({ createAt: -1 });
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Danh sach sản phẩm dịch vụ theo menu cha
     * Depv
     */
    listProductByMenuParent({ limit, menuID, sort }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                /**
                 *  Điều kiện sắp xếp
                 *  1: Giá từ cao đến thấp
                 *  2: Giá từ thấp đến cao
                 *  3: Giời gian mới nhất
                 *  4: Thời gian cũ nhât
                 */
                let dataSort = {};
                if(sort == 1){
                    dataSort.priceBefore = -1;
                }
                if(sort == 2){
                    dataSort.priceBefore = 1;
                }

                if(sort == 3){
                    dataSort.createAt = -1;
                }
                if(sort == 4){
                    dataSort.createAt = 1;
                }

                let listData = []
                let slugMenu;
                if(ObjectID.isValid(menuID)){
                    // Lấy thông tin menu
                    let infoMenu = await MENU_COLL.findById(menuID).select("level childs slug");
                    slugMenu = infoMenu.slug;
                    if(infoMenu.level === 1){
                        let listMenuIdChilds = infoMenu.childs.map(item => ObjectID(item));
                        listData = await PRODUCT_COLL.find({ menu: { $in: listMenuIdChilds }, status: STATUS_ACTIVE })
                                        .select("name description avatar priceAfter priceBefore flashSale slug")
                                        .populate({ path: "avatar" })
                                        .limit(limit)
                                        .sort(dataSort);
                    }else{
                        listData = await PRODUCT_COLL.find({ menu: menuID, status: STATUS_ACTIVE })
                            .select("menu name description avatar priceAfter priceBefore flashSale slug")
                            .populate({ path: "avatar" })
                            .limit(limit)
                            .sort(dataSort);
                    }
                }else{
                    slugMenu = "tat-ca"
                    listData = await PRODUCT_COLL.find({ status: STATUS_ACTIVE })
                                        .select("name description avatar priceAfter priceBefore flashSale slug")
                                        .populate({ path: "avatar" })
                                        .limit(limit)
                                        .sort(dataSort);                    
                  
                }

                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                return resolve({ error: false, data: listData, slugMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Lấy thông tin sản phẩm con
     * Depv
     */
    getInfoProductChild({ productID, faceNumberID, printSpecificationID, amountID  }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(productID, faceNumberID, printSpecificationID, amountID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await VARIANT_COLL.findOne({ product: productID, faceNumber: faceNumberID, printSpecification: printSpecificationID, amount: amountID })
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_data' });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;