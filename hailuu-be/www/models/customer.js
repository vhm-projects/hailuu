"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');
let moment                        	= require('moment');

/**
 * INTERNAL PAKCAGE
 */
const cfJWS                         = require('../config/cf_jws');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const CUSTOMER_COLL = require('../database/customer-coll');
const { checkEmail, checkObjectIDs } = require("../utils/utils");
const CODE_RESET_PASSWORD_MODEL = require('../models/code_reset_password').MODEL;
const CODE_RESET_PASSWORD_COLL 	= require('../database/code_reset_password-coll');

class Model extends BaseModel {
    constructor() {
        super(require('../database/customer-coll'))
    }

    insert({ fullname, email, phone, password, birthday, gender }) {
        return new Promise(async resolve => {
            try {
                if(!email || !password || !fullname)
                    return resolve({ error: true, message: 'email_password_phone_name_not_valid' });

                if (!checkEmail(email)) {
                    return resolve({ error: true, message: 'email_not_valid' });
                }
                const emailValid = email.toLowerCase().trim();

                const STATUS_VALID = [0, 1];
                const checkExists = await CUSTOMER_COLL.findOne({
                    $or: [
                        { email: emailValid },
                    ],
                    status: {
                        $in: STATUS_VALID
                    }
                });

                if(checkExists){
                    return resolve({ error: true, message: "email_existed" });
                }

                const dataInsert = {
                    fullname, email
                };
                
                const hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                dataInsert.password = hashPassword;
                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_account_failed' });

                dataInsert && delete dataInsert.password;
                return resolve({ error: false, data: dataInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    register({ fullname, email, password }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email || !password)
                    return resolve({ error: true, message: 'params_not_valid' });

                const checkExists = await CUSTOMER_COLL.findOne({ email });

                if(checkExists){
                    return resolve({ error: true, message: "email_existed" });
                }
                const dataInsert = { fullname, email };

                const hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                dataInsert.password = hashPassword;
                const infoAfterInsert = await this.insertData(dataInsert);
                delete dataInsert.password;

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_account_failed' });

                return resolve({ error: false, data: dataInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ accountID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(accountID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoAccount = await CUSTOMER_COLL.findById(accountID).populate("address");
                if(!infoAccount)
                    return resolve({ error: true, message: "account_is_not_exists" });

                return resolve({ error: false, data: infoAccount });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ accountID, fullname, email, phone, password, birthday, gender }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(accountID))
                    return resolve({ error: true, message: "param_not_valid" });

                const checkExists = await CUSTOMER_COLL.findById(accountID);
                if(!checkExists)
                    return resolve({ error: true, message: "account_is_not_exists" });

				// if(oldPassword){
				// 	const isMatchPass = await compare(oldPassword, checkExists.password);
				// 	if (!isMatchPass) 
				// 		return resolve({ error: true, message: 'old_password_wrong' });
				// }

                const dataUpdateUser = {};

                /**
                 * Kiểm tra tồn tại email
                 */
                const checkExistsEmail = await CUSTOMER_COLL.findOne({email});
                
                if( email && !checkExistsEmail || accountID == checkExistsEmail._id ){
                    email     && (dataUpdateUser.email     = email.toLowerCase().trim());
                }else{
                    return resolve({ error: true, message: "email_wrong" });
                }

                /**
                 * Kiểm tra tồn tại firstName vs lastName
                 */
                if ( !firstName || !lastName ){
                    return resolve({ error: true, message: 'firstName_or_lastName_not_valid' });
                }else{
                    dataUpdateUser.firstName             = firstName;
                    dataUpdateUser.lastName              = lastName;
                }

                /**
                 * Kiểm tra phone có 10 ký tự -> 11 ký tự
                 */
                if ( phone ){
                    if ( phone.length == 11 || phone.length == 10 ){
                        dataUpdateUser.phone = phone;
                    }else{
                        return resolve({ error: true, message: "phone_invalid" });
                    }
                }

                password  && (dataUpdateUser.password  = hashSync(password, 8));
                company   && (dataUpdateUser.company   = company);
                vatNumber && (dataUpdateUser.vatNumber = vatNumber);
                address   && (dataUpdateUser.address   = address  );

                await this.updateWhereClause({ _id: accountID }, {
                    ...dataUpdateUser
                });

                password && delete dataUpdateUser.password;
                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateUser({ accountID, fullname, email, phone, birthday, gender, password, oldPassword }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(accountID))
                    return resolve({ error: true, message: "param_not_valid" });
                
                const checkExists = await CUSTOMER_COLL.findById(accountID);
                if(!checkExists)
                    return resolve({ error: true, message: "account_is_not_exists" });
				
                if (password && !oldPassword ){
                    return resolve({ error: true, message: "oldPassword_invalid" });
                }
                
                if (!password && oldPassword ){
                    return resolve({ error: true, message: "password_invalid" });
                }

                if( oldPassword ){
					const isMatchPass = await compare(oldPassword, checkExists.password);
					if (!isMatchPass) 
						return resolve({ error: true, message: 'old_password_wrong' });
				}
                const dataUpdateUser = {};

                /**
                 * Kiểm tra tồn tại email
                 */

                const checkExistsEmail = await CUSTOMER_COLL.findOne({email});
                
                if(email){
                    if (!checkEmail(email)) {
                        return resolve({ error: true, message: 'email_not_valid' });
                    }
                    if ( !checkExistsEmail || accountID == checkExistsEmail._id ){
                        dataUpdateUser.email     = email.toLowerCase().trim();
                    }
                }

                /**
                 * Kiểm tra tồn tại fullname
                 */
                if(fullname){
                    dataUpdateUser.fullname             = fullname;
                }

                if(phone){
                    if (!Number(phone) || Number(phone) < 0)
                        return resolve({ error: true, message: 'phone_not_valid' });
                    
                    /**
                     * Kiểm tra phone có 10 ký tự aaaaaaaa
                     */
                    if(phone.length != 10)
                        return resolve({ error: true, message: "phone_must_10_character" });
                    
                    dataUpdateUser.phone = phone;
                }

                password  && (dataUpdateUser.password  = hashSync(password, 8));
                gender    && (dataUpdateUser.gender    = gender);
                birthday  && (dataUpdateUser.birthday  = new Date(birthday));
                // console.log({ dataUpdateUser });
                await this.updateWhereClause({ _id: accountID }, {
                    ...dataUpdateUser
                });

                password && delete dataUpdateUser.password;
                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateNewSletter({ status, accountID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(accountID))
                    return resolve({ error: true, message: "param_not_valid" });

				// if(oldPassword){
				// 	const isMatchPass = await compare(oldPassword, checkExists.password);
				// 	if (!isMatchPass) 
				// 		return resolve({ error: true, message: 'old_password_wrong' });
				// }

                const dataUpdateUser = {};
                status   && (dataUpdateUser.status   = status  );

                await this.updateWhereClause({ _id: accountID }, {
                    ...dataUpdateUser
                });
                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    delete({ accountID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(accountID))
                    return resolve({ error: true, message: "param_not_valid" });

                const STATUS_DELETE = -1;
                const infoAfterDelete = await CUSTOMER_COLL.findByIdAndUpdate(accountID, {
                    status: STATUS_DELETE
                });
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "account_is_not_exists" }); 

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ }){
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE   = 1;
                const STATUS_INACTIVE = 0;
                const STATUS_VALID = [STATUS_ACTIVE, STATUS_INACTIVE];
                let dataFind = {
                    status: {
                        $in: STATUS_VALID
                    }
                }
                const listAccount = await CUSTOMER_COLL.find({ ...dataFind }).sort({ createAt: -1 });
                if(!listAccount)
                    return resolve({ error: true, message: "not_found_accounts_list" });
                
                return resolve({ error: false, data: listAccount });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getWeekOfMonth({date}) {
        const startWeekDayIndex = 1; // 1 MonthDay 0 Sundays
        const firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
        const firstDay = firstDate.getDay();
      
        let weekNumber = Math.ceil((date.getDate() + firstDay) / 7);
        if (startWeekDayIndex === 1) {
          if (date.getDay() === 0 && date.getDate() > 1) {
            weekNumber -= 1;
          }
      
          if (firstDate.getDate() === 1 && firstDay === 0 && date.getDate() > 1) {
            weekNumber += 1;
          }
        }
        return weekNumber;
    }
    
    getListAccMonthBest({ month }){
        return new Promise(async resolve => {
            try {
                const listAccount = await CUSTOMER_COLL.find({});
                if(!listAccount)
                    return resolve({ error: true, message: "not_found_accounts_list" });
                

                let dataMonth = [
                    {id: "01", month: "Tháng 1", total: 0},
                    {id: "02", month: "Tháng 2", total: 0},
                    {id: "03", month: "Tháng 3", total: 0},
                    {id: "04", month: "Tháng 4", total: 0},
                    {id: "05", month: "Tháng 5", total: 0},
                    {id: "06", month: "Tháng 6", total: 0},
                    {id: "07", month: "Tháng 7", total: 0},
                    {id: "08", month: "Tháng 8", total: 0},
                    {id: "09", month: "Tháng 9", total: 0},
                    {id: "10", month: "Tháng 10", total: 0},
                    {id: "11", month: "Tháng 11", total: 0},
                    {id: "12", month: "Tháng 12", total: 0},
                ];
                if( month ){
                    let dataWeeks = [
                        {id: "1", month: "Tuần 1", total: 0},
                        {id: "2", month: "Tuần 2", total: 0},
                        {id: "3", month: "Tuần 3", total: 0},
                        {id: "4", month: "Tuần 4", total: 0},
                        {id: "5", month: "Tuần 5", total: 0},
                    ];

                    listAccount.forEach(( account ) => {
                        if( moment(account.createAt).format('YYYY-MM-DD').substring(5, 7) == month ){
                            dataWeeks.forEach( date => {
                                if ( this.getWeekOfMonth({date: account.createAt}) == Number(date.id) ){
                                    date.total = date.total + 1;
                                }
                            })
                        }
                    });
                    return resolve({ error: false, data: dataWeeks });
                }else{
                    listAccount.forEach(( account ) => {
                        dataMonth.forEach( date => {
                            if ( moment(account.createAt).format('YYYY-MM-DD').substring(5, 7) == date.id ){
                                date.total = date.total + 1;
                                // date.total = date.total + 1;
                            }
                        })
                    });
                    return resolve({ error: false, data: dataMonth });
                }
                // console.log({ createAt : moment(Date.now()).format('YYYY-MM-DD').substring(5, 7)});
                // console.log({ createAt : moment(listAccount[0].createAt).format('YYYY-MM-DD')});
                // console.log({ createAt : moment(Date.now()).format('YYYY-MM-DD')});
               
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Tìm kiếm theo tên, email
     * Date: 06/04/2021
     * Dev: SonLP
     */
    searchNameEmail({ keyword, limit = 10 }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { firstName: new RegExp(key, 'i') },
                        { lastName: new RegExp(key, 'i') }
                    ]
                }

                const listAccount = await CUSTOMER_COLL.find(condition).limit(limit);
                if( !listAccount )
                    return resolve({ error: true, message: "not_found_accounts_list" });

                return resolve({ error: false, data: listAccount });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await CUSTOMER_COLL.findOne({ email });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

				if (checkExists.status == -1) 
                    return resolve({ error: true, message: 'user_deleted' });

                const infoUser = {
                    _id: checkExists._id,
                    fullname: checkExists.fullname,
                    status: checkExists.status,
                    email: checkExists.email,
                    level: 3,
                }

                if (checkExists.phone) {
					infoUser.phone = checkExists.phone;
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateAddressID({ accountID, addressID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(accountID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }
                
                let dataUpdate = {
                    $addToSet: { 
                        address:addressID
                    }
                }
                let infoCustomer = await CUSTOMER_COLL.findById(accountID);
                if (!infoCustomer.addressDefault) {
                    dataUpdate = {
                        ...dataUpdate,
                        addressDefault: addressID
                    }
                }
                let infoCustomerAfterUpdate = await CUSTOMER_COLL.findByIdAndUpdate(accountID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if (!infoCustomerAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_customer' });

                return resolve({
                    error: false,
                    data: infoCustomerAfterUpdate
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    updateChangeAddressDefault({ accountID, addressID, status }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(accountID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }

                let infoCustomer = await CUSTOMER_COLL.findById(accountID);
                if (infoCustomer.address.length > 1) {
                    const STATUS_ACTIVE   = 1;
                    const STATUS_INACTIVE = 0;
                    const STATUS_VALID    = [STATUS_ACTIVE, STATUS_INACTIVE];
                    if (Number.isNaN(Number(status)) || !STATUS_VALID.includes(Number(status))) {
                        return resolve({ error: true, message: 'status_invalid' }); 
                    }

                    let infoCustomerAfterUpdate;
                    if (infoCustomer.addressDefault.toString() != addressID.toString()) {
                        infoCustomerAfterUpdate = await CUSTOMER_COLL.findByIdAndUpdate(accountID, {
                            addressDefault: addressID
                        }, {
                            new: true
                        });
                    }
                    if (!infoCustomerAfterUpdate)
                        return resolve({ error: true, message: 'cannot_update_customer' });
    
                    return resolve({
                        error: false,
                        data: infoCustomerAfterUpdate
                    });
                } else {
                    return resolve({
                        error: false,
                        data: {},
                    });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkCodeForgetPassword({ email, code }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await CUSTOMER_COLL.findOne({ email: email.toLowerCase().trim(), status: 1 });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                let infoCode =  await CODE_RESET_PASSWORD_MODEL.checkCode({ email, code });

                if (infoCode.error) {
                    return resolve(infoCode);
                }

                return resolve(infoCode);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateForgetPassword({ email, code, password }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await CUSTOMER_COLL.findOne({ email: email.toLowerCase().trim(), status: 1 });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                if (!password) {
                    return resolve({ error: true, message: 'password_invalid' });
                }
                let infoCode = await CODE_RESET_PASSWORD_COLL.findOne({ email, code })
                if(!infoCode) 
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                
                let passwordNew = hashSync(password, 8);

                let infoUserAfterUpdate = await CUSTOMER_COLL.findOneAndUpdate({ email }, {
                    password: passwordNew
                });

                if (!infoUserAfterUpdate) {
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                }

                return resolve({ error: false, data: email });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
