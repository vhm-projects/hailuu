"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { convertToSlug } = require('../utils/utils');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const POST_COLL = require('../database/blog-coll');


class Model extends BaseModel {
    constructor() {
        super(POST_COLL);
    }

    insert({ title, categoryID, slug, description, content, image, seo, status, type, userCreate }) {
        return new Promise(async resolve => {
            try {
				if((categoryID && !ObjectID.isValid(categoryID)) || !ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: 'param_not_valid' });

                if(!title || !slug || !content)
                    return resolve({ error: true, message: 'params_not_valid' });

				slug = convertToSlug(slug);

                const checkExists = await POST_COLL.findOne({
					$or: [
						{ title },
						{ slug }
					]
				});
                if(checkExists)
                    return resolve({ error: true, message: 'title_or_slug_existed' });

                const dataInsert = {
                    title,
                    slug,
					content,
					image,
					description,
                    status,
					seo: seo.split(','),
					type,
					userCreate,
					category: categoryID,
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_post_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ postID, title, categoryID, slug, description, content, image, seo, status, type, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || 
					!ObjectID.isValid(postID) || 
					!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await POST_COLL.findById(postID);
                if(!checkExists)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                const dataUpdatePost = {};
                title    		&& (dataUpdatePost.title 		= title);
                slug 			&& (dataUpdatePost.slug 		= convertToSlug(slug));
                description   	&& (dataUpdatePost.description 	= description);
                content    		&& (dataUpdatePost.content 		= content);
                image    		&& (dataUpdatePost.image 		= image);
                categoryID    	&& (dataUpdatePost.category 	= categoryID);
                type    		&& (dataUpdatePost.type 		= type);

				dataUpdatePost.seo 		    = seo.split(',');
				dataUpdatePost.status 		= status;
                dataUpdatePost.userUpdate 	= userUpdate;

                await this.updateWhereClause({ _id: postID }, {
                    ...dataUpdatePost
                });

                return resolve({ error: false, data: dataUpdatePost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await POST_COLL.findByIdAndRemove(postID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoPost = await POST_COLL
					.findById(postID)
					.populate('category comments image');

                if(!infoPost)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getPostBySlug({ slug }) {
        return new Promise(async resolve => {
            try {
                const infoPost = await POST_COLL
					.findOne({ slug })                            
					.populate({
						path: 'category comments image',
						options: {
							sort: { createAt: -1 }
						}
					});

                if(!infoPost)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, slug, type, category, status }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                        { content: new RegExp(key, 'i') },
                    ]
                }

				slug 	&& (condition.slug 	 = slug);
				type 	&& (condition.type 	 = type);
				status 	&& (condition.status = status);

                if(category && ObjectID(category)){
                    condition.category = category
                }

                const listPosts = await POST_COLL
					.find(condition)
					.populate('category image')
					.sort({ modifyAt: -1 })
					.lean();

                if(!listPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getRelatedPost({ postID, category }){
        return new Promise(async resolve => {
            try {
                const listRelatedPosts = await POST_COLL.find({
                    _id: { $nin: [postID] },
					category: category._id
                }).populate('category image').limit(3).sort('modifyAt').lean();

                if(!listRelatedPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listRelatedPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListMostViewed(){
		return new Promise(async resolve => {
			try {
				const listMostViewedPost = await BLOG_COLL.find({})
					.populate('category image')
					.sort({ views: -1 })
					.limit(5)
					.lean();

				if(!listMostViewedPost)
                    return resolve({ error: true, message: 'not_found_posts_list' });
				
				return resolve({ error: false, data: listMostViewedPost });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
