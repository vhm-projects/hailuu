"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const IMAGE_COLL = require('../database/image-coll');

class Model extends BaseModel {
    constructor() {
        super(IMAGE_COLL);
    }

    insert({ product, blog, name, path, size, type, main, status, userCreate }) {
        return new Promise(async resolve => {
            try {
				if((product && !ObjectID.isValid(product)))
                    return resolve({ error: true, message: 'param_not_valid' });

                if((blog && !ObjectID.isValid(blog)))
                    return resolve({ error: true, message: 'param_not_valid' });

                if(!name || !path)
                    return resolve({ error: true, message: 'params_not_valid' });

                // const checkExists = await IMAGE_COLL.findOne({ name });
                // if(checkExists)
                //     return resolve({ error: true, message: 'image_existed' });
                const dataInsert = {
                    product,
                    blog,
                    name,
					path,
					size,
					type,
                    main,
                    status,
					userCreate,
                }

                const infoImageAfterInsert = await this.insertData(dataInsert);
                if(!infoImageAfterInsert)
                    return resolve({ error: true, message: 'add_image_failed' });

                return resolve({ error: false, data: infoImageAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;