"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const CONTACT_MESSAGE_COLL = require('../database/contact_message-coll.js');

const { validEmail } = require('../utils/string_utils');

class Model extends BaseModel {
    constructor() {
        super(CONTACT_MESSAGE_COLL);
    }

    // Thêm tin nhắn liên hệ mới
    insert({ fullname, email, nickname, content }) { 
        return new Promise(async (resolve) => {
            try {
                if(!fullname || !email || !nickname || !content) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                if(!validEmail(email)) {
                    return resolve({ error: true, message: 'email_invalid' });
                }

                let infoContactMessageInsert = {
                    fullname,
                    email,
                    nickname,
                    content
                }

                let resultInsert = await this.insertData(infoContactMessageInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert_contact_message' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListContactMessage() {
        return new Promise(async resolve => {
            try {
                let listContactMessage = await CONTACT_MESSAGE_COLL.find({});
                if(!listContactMessage)
                    return resolve({ error: true, message: 'cannot_get_list_contact_message' });

                return resolve({ error: false, data: listContactMessage });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    getDetailContactMessage({ contactMessageID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactMessageID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoContactMessage = await CONTACT_MESSAGE_COLL.findById({ _id: contactMessageID });
                if(!infoContactMessage)
                    return resolve({ error: true, message: 'cannot_get_list_contact_message' });

                // bấm vào xem chi tiết tự động cập nhật trạng thái thành đã xem
                await CONTACT_MESSAGE_COLL.findByIdAndUpdate(contactMessageID, { status: 1 });

                return resolve({ error: false, data: infoContactMessage });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

}

exports.MODEL = new Model;