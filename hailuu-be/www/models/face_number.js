"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const FACE_NUMBER_COLL = require('../database/face_number-coll.js');

class Model extends BaseModel {
    constructor() {
        super(FACE_NUMBER_COLL);
    }

    // Tạo mới
    insert({ name, amount }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name || !amount) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                let dataInsert = {
                    name,
                    amount,
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Lấy danh sách
    getList({ status }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { };

                let STATUS_ACCEPT = [0,1]
                if(status){
                    conditionObj.status = status
                }else{
                    conditionObj.status = { $in: STATUS_ACCEPT }
                }
                
                let listFaceNumber = await FACE_NUMBER_COLL.find(conditionObj);
                if(!listFaceNumber)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listFaceNumber });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Lấy thông tin chi tiết
    getInfo({ faceNumberID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(faceNumberID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoFaceNumber = await FACE_NUMBER_COLL.findById(faceNumberID);
                if(!infoFaceNumber)
                    return resolve({ error: true, message: 'cannot_get_info' });
           
                return resolve({ error: false, data: infoFaceNumber });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Xoá
    remove({ faceNumberID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(faceNumberID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let LOCK_STATUS = -1;
                
                let infoFaceNumberRemove = await FACE_NUMBER_COLL.findByIdAndUpdate(faceNumberID, { status: LOCK_STATUS }, {new: true});
                if(!infoFaceNumberRemove)
                    return resolve({ error: true, message: 'cannot_remove' });
           
                return resolve({ error: false, data: infoFaceNumberRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Sửa
    update({ faceNumberID, name, amount, status }) {
        return new Promise(async resolve => {
            try {
                if(!name || !amount || !ObjectID.isValid(faceNumberID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let dataUpdate = { 
                    name, 
                    amount,
                    status
                }
                
                let infoFaceNumberAfterUpdate = await FACE_NUMBER_COLL.findByIdAndUpdate(faceNumberID, dataUpdate, { new: true });
                if(!infoFaceNumberAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

           
                return resolve({ error: false, data: infoFaceNumberAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

}

exports.MODEL = new Model;