"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const PRINT_SPECIFICATION_COLL = require('../database/print_specification-coll.js');

class Model extends BaseModel {
    constructor() {
        super(PRINT_SPECIFICATION_COLL);
    }

    // Tạo mới
    insert({ name, description }) { 
        return new Promise(async (resolve) => {
            try {
                if(!name || !description) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                let dataInsert = {
                    name,
                    description,
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Lấy danh sách
    getList({ status }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                
                let STATUS_ACCEPT = [0,1]
                if(status){
                    conditionObj.status = status
                }else{
                    conditionObj.status = { $in: STATUS_ACCEPT }
                }

                let listData = await PRINT_SPECIFICATION_COLL.find(conditionObj);
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Lấy thông tin chi tiết
    getInfo({ printSpecificationID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(printSpecificationID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoData = await PRINT_SPECIFICATION_COLL.findById(printSpecificationID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_info' });
           
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Xoá
    remove({ printSpecificationID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(printSpecificationID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let LOCK_STATUS = -1;
                
                let infoRemove = await PRINT_SPECIFICATION_COLL.findByIdAndUpdate(printSpecificationID, { status: LOCK_STATUS}, {new: true});
                if(!infoRemove)
                    return resolve({ error: true, message: 'cannot_remove' });
           
                return resolve({ error: false, data: infoRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Sửa
    update({ printSpecificationID, name, description, status }) {
        return new Promise(async resolve => {
            try {
                if(!name || !description || !ObjectID.isValid(printSpecificationID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let dataUpdate = { 
                    name, 
                    description ,
                    status
                }
                
                let infoUpdate = await PRINT_SPECIFICATION_COLL.findByIdAndUpdate(printSpecificationID, dataUpdate, { new: true });
                if(!infoUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

           
                return resolve({ error: false, data: infoUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

}

exports.MODEL = new Model;