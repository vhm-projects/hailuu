"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const USER_CONTACT_COLL = require('../database/user_contact-coll.js');

const { validEmail } = require('../utils/string_utils');

class Model extends BaseModel {
    constructor() {
        super(USER_CONTACT_COLL);
    }

    // Tạo mới
    insert({ fullname, email, phone }) { 
        return new Promise(async (resolve) => {
            try {
                if(!fullname || !email || !phone ) {
                    return resolve({ error: true, message: 'params_not_valid' });
                }

                if(!validEmail(email)) {
                    return resolve({ error: true, message: 'email_invalid' });
                }

                let dataInsert = {
                    fullname,
                    email,
                    phone,
                }

                let resultInsert = await this.insertData(dataInsert);
                if(!resultInsert)
                    return resolve({ error: true, message: 'cannot_insert_contact_message' });

                return resolve({ error: false, data : resultInsert });
            }catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Lấy danh sách
    getList() {
        return new Promise(async resolve => {
            try {
                let listUserContact = await USER_CONTACT_COLL.find({});
                if(!listUserContact)
                    return resolve({ error: true, message: 'cannot_get_list_contact' });

                return resolve({ error: false, data: listUserContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Lấy thông tin chi tiết
    getInfo({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoUserContact = await USER_CONTACT_COLL.findById(contactID);
                if(!infoUserContact)
                    return resolve({ error: true, message: 'cannot_get_info' });

                // bấm vào xem chi tiết tự động cập nhật trạng thái thành đã xem
                await USER_CONTACT_COLL.findByIdAndUpdate(contactID, { status: 1 }, { new: true });

                return resolve({ error: false, data: infoUserContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Lấy thông tin chi tiết
    remove({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoUserContactAfterRemove = await USER_CONTACT_COLL.findByIdAndDelete(contactID);
                if(!infoUserContactAfterRemove)
                    return resolve({ error: true, message: 'cannot_get_info' });
              
                return resolve({ error: false, data: infoUserContactAfterRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Cập nhật trạng thái liên hệ
    updateStatus({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'params_not_valid' });
                
                let infoUserContactAfterUpdate = await USER_CONTACT_COLL.findByIdAndUpdate(contactID, { status: 1 }, {new: true});
                if(!infoUserContactAfterUpdate)
                    return resolve({ error: true, message: 'cannot_get_info' });
              
                return resolve({ error: false, data: infoUserContactAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

}

exports.MODEL = new Model;