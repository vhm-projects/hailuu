"use strict";

const promise = require('bluebird');
const ObjectID           = require('mongoose').Types.ObjectId;
const { checkObjectIDs } = require('../utils/utils');
const CUSTOMER_REVIEW_COLL = require('../database/customer_review-coll');

const BaseModel = require('./intalize/base_model');

class Model extends BaseModel {

    constructor() {
        super(require('../database/customer_review-coll'))
    }

    /**
     * Thêm khách hàng review
     * Depv247
     */
    insert({ fullname, image, position, company, content, userID }) {
        return new promise(async resolve => {
            try {
                if( !checkObjectIDs(userID) ||!fullname || !position || !company || !content)
                return resolve({ error: true, message: "params_invalid" });
                
                let dataInsert = { fullname, position, company, content, userCreate: userID };

                if(image){
                    dataInsert.image = image;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    update({ customerReviewID, fullname, image, position, company, content, status, userID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerReviewID, userID))
                return resolve({ error: true, message: "params_invalid" });

                let dataUpdate = { userUpdate: userID };
                if(fullname){
                    dataUpdate.fullname = fullname;
                }

                if(position){
                    dataUpdate.position = position;
                }

                if(company){
                    dataUpdate.company = company;
                }

                if(content){
                    dataUpdate.content = content;
                }

                if(image){
                    dataUpdate.image = image;
                }

                let statusActive = [0, 1];
                if(statusActive.includes(Number(status))){
                    dataUpdate.status = status;
                }
                
                let infoAfterUpdate = await CUSTOMER_REVIEW_COLL.findByIdAndUpdate(customerReviewID, dataUpdate, { new: true });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ customerReviewID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerReviewID))
                return resolve({ error: true, message: "params_invalid" });
            
                let dataSignal = await CUSTOMER_REVIEW_COLL.findById(customerReviewID).populate({
                    path: "image",
                    select: "name path"
                });;
                if (!dataSignal) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: dataSignal })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByStatus({ status }) {
        return new promise(async resolve => {
            try {
                let infoAfterUpdate = await CUSTOMER_REVIEW_COLL.find({ status: 1 }).populate({
                    path: "image",
                    select: "name path"
                });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getAll() {
        return new promise(async resolve => {
            try {
                let infoAfterUpdate = await CUSTOMER_REVIEW_COLL.find().populate({
                    path: "image",
                    select: "name path"
                });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByProduct({ productID }) {
        return new promise(async resolve => {
            try {
                let listReview = await CUSTOMER_REVIEW_COLL.find({ productID }).populate({
                    path: "image",
                    select: "name path"
                });
                if (!listReview) 
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: listReview })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;