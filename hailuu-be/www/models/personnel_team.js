"use strict";

const promise                   = require('bluebird');
const ObjectID                  = require('mongoose').Types.ObjectId;
const { checkObjectIDs }        = require('../utils/utils');
const PERSONNEL_TEAM_COLL       = require('../database/personnel_team-coll');
const IMAGE_MODEL               = require('./image').MODEL;
const BaseModel                 = require('./intalize/base_model');

class Model extends BaseModel {

    constructor() {
        super(require('../database/personnel_team-coll'))
    }

    /**
     * Create personnel
     * Dattv
     */
    insert({ fullname, avatar, position, description, userID, email, phone }) {
        return new promise(async resolve => {
            try {
                if( !checkObjectIDs(userID) || !fullname)
                return resolve({ error: true, message: "params_invalid" });
                
                let dataInsert = { fullname, description, userCreate: userID };

                if(description){
                    dataInsert.description = description;
                }

                if(email){
                    dataInsert.email = email;
                }

                if(phone){
                    dataInsert.phone = phone;
                }

                if(position){
                    dataInsert.type = 1;
                    dataInsert.position = position;
                }else{
                    dataInsert.type = 2;
                }

                let { name, size, path } = avatar;
                let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                if(infoImage){
                    dataInsert.avatar = infoImage.data._id;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

     /**
     * Get infomation
     * Dattv
     */
    getInfo({ personnelID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(personnelID))
                return resolve({ error: true, message: "params_invalid" });
            
                let infoPesonnel = await PERSONNEL_TEAM_COLL.findById(personnelID)
                .populate({
                    path: "avatar",
                    select: "name path"
                });

                if (!infoPesonnel) 
                    return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoPesonnel })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

     /**
     * Get team personnel
     * Dattv
     */
    getList({ status, type }) {
        return new promise(async resolve => {
            try {

                let STATUS_BLOCK = -1
                let conditionObj = {};
                if(status){
                    conditionObj.status = status
                }else{
                    conditionObj.status = {
                        $ne: STATUS_BLOCK
                    }
                }

                if(type){
                    conditionObj.type = type
                }

                let listPersonnel = await PERSONNEL_TEAM_COLL.find(conditionObj).populate({
                    path: "avatar",
                    select: "name path"
                });

                if (!listPersonnel) 
                    return resolve({ error: true, message: 'cannot_get_list' });
                return resolve({ error: false, data: listPersonnel })

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ personnelID, fullname, avatar, position, description, status, email, phone, userID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(personnelID, userID))
                return resolve({ error: true, message: "params_invalid" });

                let dataUpdate = { userUpdate: userID };

                if(email){
                    dataUpdate.email = email;
                }

                if(phone){
                    dataUpdate.phone = phone;
                }

                if(fullname){
                    dataUpdate.fullname = fullname;
                }

                if(position){
                    dataUpdate.position = position;
                }

                if(description){
                    dataUpdate.description = description;
                }

                if(avatar){
                    let { name, size, path } = avatar;
                    let infoImage = await IMAGE_MODEL.insert({ name, size, path, userCreate: userID });
                    if(infoImage){
                        dataUpdate.avatar = infoImage.data._id;
                    }
                }

                let statusActive = [0, 1];

                if(status && statusActive.includes(Number(status))){
                    dataUpdate.status = status;
                }
                
                let infoAfterUpdate = await PERSONNEL_TEAM_COLL.findByIdAndUpdate(personnelID, dataUpdate, { new: true });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Remove personnel
     * Dattv
     */
     remove({ personnelID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(personnelID))
                return resolve({ error: true, message: "params_invalid" });
            
                let infoAfterRemove = await PERSONNEL_TEAM_COLL.findByIdAndUpdate(personnelID, { status: -1 }, {new: true})
                if (!infoAfterRemove) 
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoAfterRemove })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   

}

exports.MODEL = new Model;