"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const BaseModel                     = require('./intalize/base_model');
const MENU_COLL                     = require('../database/menu-coll');
const PRODUCT_COLL                  = require('../database/product-coll');
const { convertToSlug, generate }   = require('../utils/utils')
const IMAGE_MODEL 				    = require('../models/image').MODEL;
const PRODUCT_MODEL 			    = require('../models/product').MODEL;


class Model extends BaseModel {
    constructor() {
        super(require('../database/menu-coll'))
    }

    /**
     * Thêm menu
     * Dattv
     */
    insert({ name, description, parent, userID, image }) {
        return new Promise(async resolve => {
            try {
                if(!name || !ObjectID.isValid(userID))
                    return resolve({ error: true, message: 'params_invalid' });

                let slug = convertToSlug(name);

                let checkExisted = await MENU_COLL.findOne({ slug, status: 1 });
                if(checkExisted)
                    return resolve({ error: true, message: 'url_is_existed' });
                
                let dataInsert = {
                    name,
                    description,
                    userCreate: userID,
                    slug
                }

                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInser = await IMAGE_MODEL.insert({ name, path, size });
                    dataInsert.image = infoImageAfterInser.data._id;
                }

                let infoAfterInsert;

                //Nếu là menu con
                if(ObjectID.isValid(parent)){

                    //Up level
                    let infoParent = await MENU_COLL.findById(parent);
                    let { level } = infoParent;
                    dataInsert.level = Number(++level);
                    dataInsert.parent = parent;
                    infoAfterInsert = await this.insertData(dataInsert);

                    //Đẩy vào danh sách menu con của menu cha
                    await MENU_COLL.findByIdAndUpdate(parent, {
                        $addToSet: { childs: infoAfterInsert._id }
                    }, {new: true })
                }else{
                    infoAfterInsert = await this.insertData(dataInsert);
                }

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert_menu' });
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin menu
     * Dattv
     */
    getInfo({ menuID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoMenu = await MENU_COLL.findById(menuID);

                if(!infoMenu)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                return resolve({ error: false, data: infoMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Lấy thông tin menu
     * Dattv
     */
    getInfoMenuChild({ menuID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoMenu = await MENU_COLL.findById(menuID)
                            .populate("image")
                            .populate({
                                path: "products",
                                populate: {
                                    path: "avatar"
                                }
                            })
                if(!infoMenu)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                return resolve({ error: false, data: infoMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Lấy tất cả danh sách menu
     * Dattv
     */
    getlistAllMenu() {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                const LEVEL_PARENT = 1;

                let condition = { status: STATUS_ACTIVE, level: LEVEL_PARENT };
                let listMenu = await MENU_COLL.find(condition).populate({
                    path: "childs",
                    match: {
                        status: 1
                    },
                    options: { sort: { order: 1 }},
                    populate: {
                        path: "image products",
                    }
                }).sort({ order: 1 })

                if(!listMenu)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                let listMenuAndProduct = [];
                for (const menu of listMenuAndProduct) {
                    
                }


                return resolve({ error: false, data: listMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy danh sách menu cấp 1
     * Dattv
     */
     getlistMenuLevelHighest({ status }) {
        return new Promise(async resolve => {
            try {
                let statusShow = [0, 1];
                let levelParent = 1;

                let conditionObj = {};
                if(status){
                    conditionObj.status = status;
                }else{
                    conditionObj.status = {
                        $in: statusShow
                    }
                }
                let listMenuLevelHighest = await MENU_COLL.find({ level: levelParent, ...conditionObj })
                .populate("image")
                .populate({
                    path: "childs",
                    match: {
                        status: { $in: [0,1]  }
                    }
                })
                .sort({ order: 1 });
                if(!listMenuLevelHighest)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                return resolve({ error: false, data: listMenuLevelHighest });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy danh sách menu cấp 2
     * Dattv
     */
     getlistMenuOfParent({ menuID, status }) {
        return new Promise(async resolve => {
            try {
                let statusShow = [0, 1];
                let confition = {};
                if(status){
                    confition.status = status;
                }else{
                    confition.status = {
                        $in: statusShow
                    }
                }
                let listMenuChildOfParent = await MENU_COLL.find({ parent: menuID, ...confition }).populate("image").sort({ order : 1 }).lean();

                if(!listMenuChildOfParent)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                let listMenuChildOfParentAndAmountProduct = []
                for (const element of listMenuChildOfParent) {
                    let { _id } = element;
                    let amountProduct = await PRODUCT_COLL.count({ menu: _id, status: 1 });
                    listMenuChildOfParentAndAmountProduct.push({  ...element, amountProduct })
                }
               
                return resolve({ error: false, data: listMenuChildOfParentAndAmountProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin menu theo slug
     * Dattv
     */
    getInfoWithSlug({ slug }) {
        return new Promise(async resolve => {
            try {
                if(!slug)
                    return resolve({ error: true, message: 'params_invalid' });
                let infoMenu = await MENU_COLL.findOne({ slug, status: { $ne: -1 } });
                if(!infoMenu)
                    return resolve({ error: true, message: 'cannot_get_menu' });

                return resolve({ error: false, data: infoMenu });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Xóa menu
     * Dattv
     */
    remove({ menuID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });
                let statusRemove = -1;
                let infoAfterDelete = await MENU_COLL.findByIdAndUpdate(menuID, { status: statusRemove, slug: generate(10), name: generate(10) });

                //Xóa khỏi child của menu parent nếu có
                let { parent } = infoAfterDelete;
                if(parent){
                    await MENU_COLL.findByIdAndUpdate(parent, {
                        $pull: { childs: infoAfterDelete._id }
                    }, {new: true })
                }

                //Xóa tất cả menu con
                await MENU_COLL.updateMany({ parent: menuID }, { status: statusRemove, slug: generate(10), name: generate(10) });

                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_remove_menu' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật menu
     * Dattv
     */
    update({ menuID, name, description, userID, image }) {
        return new Promise(async resolve => {
            try {
                if(!name || !ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });

                let slug = convertToSlug(name);
                let checkSlug = await MENU_COLL.findOne({ slug, _id: { $ne: menuID }  });
                if(checkSlug)
                    return resolve({ error: true, message: "url_is_existed" })
                let dataUpdate = {
                    name, description, slug, userUpdate: userID
                }
                if(image){
                    let { name, size, path } = image;
                    let infoImageAfterInser = await IMAGE_MODEL.insert({ name, path, size });
                    dataUpdate.image = infoImageAfterInser.data._id;
                }
            
                let infoAfterUpdate = await MENU_COLL.findByIdAndUpdate(menuID, dataUpdate, {new: true});

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_menu' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật trạng thái menu
     * Dattv
     */
    updateStatus({ menuID, status, userID }) {
        return new Promise(async resolve => {
            try {
                if(!status || !ObjectID.isValid(menuID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await MENU_COLL.findByIdAndUpdate(menuID, {
                    status, userUpdate: userID
                }, {new: true});

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_menu' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update order menu
     * Dattv
     * Solution: Nhận vào một mảng object các menu và index của menu đó (dùng hàm foreach jquery)
     */
    updateOrderMenu({ arrayObjectMenu }) {
        return new Promise(async resolve => {
            try {
                
                if(!arrayObjectMenu.length)
                    return resolve({ error: true, message: 'params_invalid' });
                
                //Update order menu
                for (let menu of arrayObjectMenu) {
                    let updateOrderMenu = await MENU_COLL.findByIdAndUpdate(menu._id, {
                        order: menu.index
                    }, { new: true })

                    if(!updateOrderMenu)
                        return resolve({ error: true, message: 'cannot_update_order_menu' });
                }

                return resolve({ error: false, message: "Update success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Tìm kiếm theo tên
     * Dattv
     */
    searchWithKey(key) {
        return new Promise(async resolve => {
            try {

                let conditionObj = {};
                /**
                 * SEARCH WITH KEY
                 */
                 if(key && key.length > 0){
                    let keyword = key.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionObj.name = new RegExp(keyword, 'i');
                }


                let listData = await MENU_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
                ]);

                if(!listData)
                    return resolve({ error: true, message: 'cannot_find_data' });
                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;