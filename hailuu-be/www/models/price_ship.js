"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID 			= require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel 		= require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const PRICE_SHIP_COLL 	= require('../database/price_ship-coll');


class Model extends BaseModel {
    constructor() {
        super(PRICE_SHIP_COLL);
    }

    insert({ name, price, from, to, userCreate }) {
        return new Promise(async resolve => {
            try {
                if(!name || !price || !from || !to)
                    return resolve({ error: true, message: 'param_invalid' });

                const checkExists = await PRICE_SHIP_COLL.findOne({ name });
                if(checkExists)
                    return resolve({ error: true, message: 'name_existed' });
    
                const dataInsert = {
                    name,
                    price,
					from,
					to,
					userCreate
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_price_ship_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ priceShipID, name, price, from = 0, to = 0, status, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || !ObjectID.isValid(priceShipID))
                    return resolve({ error: true, message: 'param_invalid' });

                const checkExists = await PRICE_SHIP_COLL.findById(priceShipID);
                if(!checkExists)
                    return resolve({ error: true, message: 'price_ship_is_not_exists' });

                const dataUpdatePriceShip = { 
					from,
					to,
					userUpdate 
				};
                name   	&& (dataUpdatePriceShip.name 	= name);
                price 	&& (dataUpdatePriceShip.price 	= price);

				if([0,1].includes(+status)){
					dataUpdatePriceShip.status = status;
				}

                await this.updateWhereClause({ _id: priceShipID }, dataUpdatePriceShip);

                return resolve({ error: false, data: dataUpdatePriceShip });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ priceShipID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(priceShipID))
                    return resolve({ error: true, message: 'param_invalid' });

                const infoAfterDelete = await PRICE_SHIP_COLL.findByIdAndUpdate(priceShipID, { status: -1 });
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'price_ship_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ priceShipID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(priceShipID))
                    return resolve({ error: true, message: 'param_invalid' });

                const infoPriceShip = await PRICE_SHIP_COLL.findOne({ _id: priceShipID, status: { $in: [0,1] } });
                if(!infoPriceShip)
                    return resolve({ error: true, message: 'price_ship_is_not_exists' });

                return resolve({ error: false, data: infoPriceShip });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ status = { $in: [0,1] } }){
        return new Promise(async resolve => {
            try {
                const listPriceShip = await PRICE_SHIP_COLL
					.find({ status } )
					.sort({ modifyAt: -1 })
					.lean();

                if(!listPriceShip)
                    return resolve({ error: true, message: 'cannot_get_list_price_ship' });

                return resolve({ error: false, data: listPriceShip });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
