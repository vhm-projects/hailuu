"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const COMMENT_COLL = require('../database/comment-coll');
const BLOG_COLL = require('../database/blog-coll');
const PRODUCT_COLL = require('../database/product-coll');


class Model extends BaseModel {
    constructor() {
        super(COMMENT_COLL);
    }

    insert({ fullname, email, content, star, blogID, productID }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email || !content)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    fullname,
                    email,
					content
                }

                if(ObjectID.isValid(blogID)) {
                    dataInsert.blog = blogID;
                }

                if(ObjectID.isValid(productID)) {
                    dataInsert.product = productID;
                }

                if(star) {
                    dataInsert.star = star;
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_faild' });

                let infoBlogReceivceComment = null;
                if(ObjectID.isValid(blogID)) {
                    infoBlogReceivceComment = await BLOG_COLL.findByIdAndUpdate({ _id: blogID }, {
                        $addToSet: { comments: infoAfterInsert._id }
                    }, { new: true });
                }

                let infoProductReceivceComment = null;
                if(ObjectID.isValid(productID)) {
                    infoProductReceivceComment = await PRODUCT_COLL.findByIdAndUpdate({ _id: productID }, {
                        $addToSet: { comments: infoAfterInsert._id }
                    }, { new: true });
                }

                if(!infoProductReceivceComment && !infoBlogReceivceComment)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByBlogOrProduct({ blogID, productID, status, page=1, limit= 10 }) {
        return new Promise(async resolve => {
            try {
                // type 1 đang trả về danh sách comment của blog 0 thì của product
                let conditionFind = {}; 
				let type;

                if(ObjectID.isValid(blogID)) {
                    conditionFind.blog = blogID;
                    type = 1;
                }

                if(ObjectID.isValid(productID)) {
                    conditionFind.product = productID;
                    type = 0;
                }

                let statusAccess = [0, 1];
                if(statusAccess.includes(+status)){
                    conditionFind.status = status;
                }

                if(isNaN(page)){
					page = 1;
				}

                const listData = await COMMENT_COLL
					.find(conditionFind)
                    .populate('blog product')
                    .skip((page * limit) - limit)
                    .limit(limit)
					.sort({ createAt: -1 })
                if(!listData)
                    return resolve({ error: true, message: 'get_data_faild' });
                let totalItem = await COMMENT_COLL.count(conditionFind);

                //let pages = Math.ceil(totalItem/limit);
                return resolve({ 
                    error: false, 
                    data: { 
                        type, 
                        listData, 
                        blogID, 
                        productID, 
                        currentPage: page, 
                        perPage: limit, 
                        total: totalItem 
                    }});

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByBlogOrProductRating({ blogID, productID, status }) {
        return new Promise(async resolve => {
            try {
                // type 1 đang trả về danh sách comment của blog 0 thì của product
                let conditionFind = {}; 
				let type;

                if(ObjectID.isValid(blogID)) {
                    conditionFind.blog = blogID;
                    type = 1;
                }

                if(ObjectID.isValid(productID)) {
                    conditionFind.product = productID;
                    type = 0;
                }

                let statusAccess = [1];
                if(statusAccess.includes(+status)){
                    conditionFind.status = status;
                }

                const listData = await COMMENT_COLL
					.find(conditionFind)
                    .select("star")
					.sort({ createAt: -1 })
                if(!listData)
                    return resolve({ error: true, message: 'get_data_faild' });

                //let pages = Math.ceil(totalItem/limit);
                return resolve({ 
                    error: false, 
                    data: listData,
                })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByPostWithPage({ postID, page = 1, limit = 8 }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_invalid' });

				if(isNaN(page)){
					page = 1;
				}

				const listCommentByPost = await COMMENT_COLL
					.find({ blog: postID })
                    .skip((page * limit) - limit)
					.limit(limit)
					.sort({ createAt: -1 })
					.lean();

				if(!listCommentByPost)
					return resolve({ error: true, message: 'cannot_get_list_comment' });

				const totalComment = await COMMENT_COLL.countDocuments({ blog: postID });

				return resolve({ 
					error: false, 
					data: {
						listCommentByPost,
						currentPage: page,
						perPage: limit,
						total: totalComment
					}
				});
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

    getListCommentApproved({ page }) {
        return new Promise(async resolve => {
            try {
                let perPage = 8;

                // đã duyệt: 1 ---- không duyệt: 0
                let listCommentApproved = await COMMENT_COLL.find({ status: 1 })
                    .skip((perPage * page) - perPage)
                    .limit(perPage)
                    .exec((err, comments) => {
                        if(err)
                            return resolve({ error: true, message: 'cannot_get_list_comment' });
                        return resolve({ error: false, data: comments });
                    });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ commentID, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(commentID))
                    return resolve({ error: true, message: 'param_not_valid' });
                let dataUpdate = { }

                let statusAccess = [0, 1];

                if(statusAccess.includes(Number(status))){
                    dataUpdate.status = status;
                }

                const infoAfterUpdate = await COMMENT_COLL.findByIdAndUpdate(commentID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ commentID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(commentID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await COMMENT_COLL.findByIdAndDelete(commentID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
