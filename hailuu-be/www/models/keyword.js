"use strict";

const promise = require('bluebird');
const ObjectID           = require('mongoose').Types.ObjectId;
const { checkObjectIDs } = require('../utils/utils');
const KEYWORD_COLL = require('../database/keyword-coll');

const BaseModel = require('./intalize/base_model');

class Model extends BaseModel {

    constructor() {
        super(require('../database/keyword-coll'))
    }

    /**
     * Thêm khách hàng review
     * Depv247
     */
    insert({ name, customerID }) {
        return new promise(async resolve => {
            try {
                if(!name)
                return resolve({ error: true, message: "params_invalid" });
                
                let dataInsert = { name, customer: customerID };
                let infoAfterInsert = await this.insertData(dataInsert);

                if (!infoAfterInsert) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    update({ keywordID, name }) {
        return new promise(async resolve => {
            try {
                
                let infoAfterUpdate = await KEYWORD_COLL.findByIdAndUpdate(keywordID, { name }, { new: true });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ keywordID, status }) {
        return new promise(async resolve => {
            try {
                
                let infoAfterUpdate = await KEYWORD_COLL.findByIdAndUpdate(keywordID, { status }, { new: true });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ keywordID }) {
        return new promise(async resolve => {
            try {
                if(!ObjectID.isValid(keywordID))
                return resolve({ error: true, message: "params_invalid" });
            
                let dataSignal = await KEYWORD_COLL.findById(keywordID);
                if (!dataSignal) 
                    return resolve({ error: true, message: 'cannot_get' });

                return resolve({ error: false, data: dataSignal })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByStatus({ status }) {
        return new promise(async resolve => {
            try {
                let infoAfterUpdate = await KEYWORD_COLL.find({ status: 1 });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListOfCustomer({ customerID, limit = 10 }) {
        return new promise(async resolve => {
            try {
                let infoAfterUpdate = await KEYWORD_COLL.find({ customer: customerID, status: 1 }).limit(limit).sort({ createAt: -1 });
                if (!infoAfterUpdate) 
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: infoAfterUpdate })
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;