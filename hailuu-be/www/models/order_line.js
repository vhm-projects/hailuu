"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } = require('../utils/utils');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * MODELS, COLLECTIONS
 */
const ORDER_LINE_COLL 				= require('../database/order_line-coll');
const PRODUCT_COLL	  				= require('../database/product-coll');
const PRODUCT_VARIANT_VALUE_COLL 	= require('../database/product_variant_value-coll');
const VARIANT_VALUE_COLL 			= require('../database/variant_value-coll');
const VARIANT_COLL 					= require('../database/variant-coll');
const IMAGE_MODEL	  				= require('../models/image').MODEL;


class Model extends BaseModel {
    constructor() {
        super(ORDER_LINE_COLL);
    }

	addToCart({ productID, variantID, userCreate }){
		return new Promise(async resolve => {
			try {
				if(!checkObjectIDs([productID, userCreate]))
                    return resolve({ error: true, message: 'params_invalid' });

				const infoProduct = await PRODUCT_COLL
					.findById(productID)
					.lean();

				if(!infoProduct)
					return resolve({ error: true, message: 'product_not_exist' });

				let infoOrderLine = await ORDER_LINE_COLL.findOne({ userCreate, product: productID, status: 1 });
				if(infoOrderLine)
					return resolve({ error: true, message: 'product_is_exist_in_cart' });

				let dataInsert = {
					product: productID,
					userCreate
				}

				if(ObjectID.isValid(variantID)){
					let infoProductVariant = await PRODUCT_VARIANT_VALUE_COLL.findById(variantID);

					dataInsert.amount = infoProductVariant.price || infoProductVariant.price_at_compare;
					dataInsert.productProperty = variantID;
				} else{
					let getMaxSizeVariantValue = await PRODUCT_VARIANT_VALUE_COLL.aggregate([
						{ $match: { product: ObjectID(productID) } },
						{ $unwind: "$variant_values" },
						{ $group: { _id: "$_id", length: { $sum: 1 } } },
						{ $sort: { length: -1 } },
						{ $limit: 1 }
					])

					let variantFirstOfProduct = await PRODUCT_VARIANT_VALUE_COLL
						.findOne({ 
							product: productID,
							variant_values: { $size: getMaxSizeVariantValue[0].length }
						})
						.lean();

					if(variantFirstOfProduct){
						dataInsert.amount = variantFirstOfProduct.price || variantFirstOfProduct.price_at_compare;
						dataInsert.productProperty = variantFirstOfProduct._id;
					}
				}

				let infoAfterAdd = await this.insertData(dataInsert);
				if(!infoAfterAdd)
					return resolve({ error: true, message: 'add_to_cart_failed' });

				infoOrderLine = await ORDER_LINE_COLL
					.findById(infoAfterAdd._id)
					.populate({
						path: 'product productProperty files',
						populate: {
							path: 'avatar variant_values',
							populate: 'parent variant'
						}
					})
					.lean();

				let listVariantAndVariantValue = [];
				let firstVariantProduct = '';
				let listVariant = await VARIANT_COLL
					.find({ product: productID, status: 1 })
					.sort({ createAt: 1})
					.lean();

				for (const variant of listVariant) {
					let conditionVariant = { variant: variant._id, status: 1 };

					if(firstVariantProduct){
						conditionVariant.parent = firstVariantProduct;
					}

					let listVariantValue = await VARIANT_VALUE_COLL
						.find(conditionVariant)
						.sort({ createAt: 1 })
						.lean();

					if(!firstVariantProduct){
						firstVariantProduct = listVariantValue[0]._id;
					}

					listVariantAndVariantValue[listVariantAndVariantValue.length] = { variant, listVariantValue };
				}

				return resolve({ error: false, data: { listVariantAndVariantValue, infoOrderLine } });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    update({ orderLineID, variantID, files = [], userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs([orderLineID, variantID, userUpdate]))
                    return resolve({ error: true, message: "params_invalid" });

                let checkExists = await ORDER_LINE_COLL.findById(orderLineID);
                if(!checkExists)
                    return resolve({ error: true, message: "order_line_is_not_exists" });

				let listFilesID = [];
                let dataUpdate 	= { 
					productProperty: variantID,
					userUpdate 
				};

				if(files.length){
					let listFilesPromise = files.map(file => IMAGE_MODEL.insert({ 
						name: file.name,
						path: file.path,
						size: file.size,
						type: file.type,
						userUpdate
					}))

					let listFiles = await Promise.all(listFilesPromise);
					listFilesID = listFiles.map(file => file.data._id);
					dataUpdate.files = listFilesID;
				}

				let infoProductVariant = await PRODUCT_VARIANT_VALUE_COLL.findById(variantID);
				dataUpdate.amount = infoProductVariant.price || infoProductVariant.price_at_compare;

                await this.updateWhereClause({ _id: orderLineID }, dataUpdate);

				const infoOrderLine = await ORDER_LINE_COLL
					.findById(orderLineID)
					.populate({
						path: 'productProperty',
						populate: {
							path: 'variant_values',
							populate: 'variant'
						}
					});

                return resolve({ error: false, data: infoOrderLine });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ orderLineID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderLineID))
                    return resolve({ error: true, message: "params_invalid" });

                const infoAfterDelete = await ORDER_LINE_COLL.findByIdAndDelete(orderLineID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: "cannot_delete_order_line" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ orderLineID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(orderLineID))
                    return resolve({ error: true, message: "params_invalid" });

                const infoOrderLine = await ORDER_LINE_COLL
					.findById(orderLineID)
					.populate({
						path: 'product productProperty',
						populate: {
							path: 'variant_values',
							populate: 'parent variant'
						}
					})
					.lean();

                if(!infoOrderLine)
                    return resolve({ error: true, message: "order_line_is_not_exists" });

				let { product, productProperty } = infoOrderLine;
				let listVariantSelected = productProperty.variant_values.map(variantValue => variantValue._id);
				let listVariantAndVariantValue = [];
				let listVariant = await VARIANT_COLL
					.find({ product: product._id, status: 1 })
					.sort({ createAt: 1})
					.lean();

				let index = 0;
				for (const variant of listVariant) {
					let conditionVariant = { variant: variant._id, status: 1 };

					if(index !== 0){
						conditionVariant.parent = listVariantSelected[index - 1];
					}

					let listVariantValue = await VARIANT_VALUE_COLL
						.find(conditionVariant)
						.sort({ createAt: 1 })
						.lean();

					listVariantAndVariantValue[listVariantAndVariantValue.length] = { variant, listVariantValue };
					index++;
				}

                return resolve({ error: false, data: { infoOrderLine, listVariantAndVariantValue } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ customerID, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "params_invalid" });

				const conditionObj = {
					userCreate: customerID
				}
				status && (conditionObj.status = status);

                const listOrderLine = await ORDER_LINE_COLL
					.find(conditionObj)
					.populate({
						path: 'product productProperty files',
						populate: {
							path: 'avatar variant_values',
							populate: 'parent variant'
						}
					})
					.sort({ modifyAt: -1 })
					.lean();

                if(!listOrderLine)
                    return resolve({ error: true, message: "cannot_get_list_order_line" });

                return resolve({ error: false, data: listOrderLine });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
