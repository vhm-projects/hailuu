"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');

/**
 * INTERNAL PAKCAGE
 */
const cfJWS                         = require('../config/cf_jws');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const COMPANY_INFOMATION_COLL = require('../database/company_infomation-coll');
const IMAGE_COLL              = require('../database/image-coll');


class Model extends BaseModel {
    constructor() {
        super(require('../database/company_infomation-coll'))
    }

    insert({ name, sortDescription, longDescription, phone, email, address, slogan, zalo, facebook, youtube, twitter, linkGoogleMap, userCreate, image, businessImages, oldBusinessImage, oldMainImage }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: "userCreateID_not_valid" });

                let data = {};
                let checkExistsInfo =  await this.getAllData();

                if ( checkExistsInfo && checkExistsInfo.length ){
                    /**
                     * Đã tồn tại thông tin công ty
                     */
                    /**
                     * Kiểm tra sortDescription có quá 60 ký tự
                     */
                    if ( sortDescription && sortDescription.length > 60){
                        return resolve({ error: true, message: "cannot_insert_more_30_character" });
                    }else{
                        if ( sortDescription != '') {
                            data.sortDescription = sortDescription;
                        }
                    }
                    /**
                     * Kiểm tra phone có 10 ký tự -> 11 ký tự
                     */
                    if ( phone ){
                        if ( phone.length == 11 || phone.length == 10 ){
                            data.phone = phone;
                        }else{
                            return resolve({ error: true, message: "phone_invalid" });
                        }
                    }

                    name            && (data.name             = name);
                    longDescription && (data.longDescription  = longDescription);
                    email           && (data.email            = email);
                    address         && (data.address          = address);
                    slogan          && (data.slogan           = slogan);
                    zalo            && (data.zalo             = zalo);
                    facebook        && (data.facebook         = facebook);
                    youtube         && (data.youtube          = youtube);
                    // businessImages  && (data.businessImages   = businessImages);
                    twitter         && (data.twitter          = twitter);
                    linkGoogleMap   && (data.linkGoogleMap    = linkGoogleMap);
                    // image           && (data.image            = image[0].filename);
                    userCreate      && (data.userUpdate       = userCreate);
                    // oldBusinessImage, oldMainImage
                    if ( oldMainImage ){
                        data.image = undefined;
                    }else if( image ) {
                        const infoImage = await IMAGE_COLL.findOne({ name: image[0].filename });
                        if( !infoImage ){
                            let dataImage = {
                                name: `/files/${image[0].filename}`,
                                main: 0,
                                status: 1,
                            };
                            userCreate      && (dataImage.userCreate       = userCreate);
                            
                            const newImage           = new IMAGE_COLL(dataImage);
                            let infoImageAfterInsert = await newImage.save();
                            
                            if(!infoImageAfterInsert){
                                return resolve({ error: true, message: 'insert_image_failed' });
                            }else{
                                data.image = infoImageAfterInsert._id;
                            }
                        }else{
                            data.image = infoImage._id;
                        }
                    }

                    /**
                     * b1: Insert business Image vào coll image 
                     * b2: Lấy ID image để thêm vào businessImage trong company-infomation coll
                     */
                    if ( oldBusinessImage ){
                        data.businessImages = undefined;
                    }else if ( businessImages ){
                        const infoImage = await IMAGE_COLL.findOne({ name: businessImages[0].filename });
                        if( !infoImage ){
                            let dataImage = {
                                name: `/files/${businessImages[0].filename}`,
                                main: 0,
                                status: 1,
                            };
                            userCreate      && (dataImage.userCreate       = userCreate);
                            
                            const newImage           = new IMAGE_COLL(dataImage);
                            let infoImageAfterInsert = await newImage.save();
                            
                            if(!infoImageAfterInsert){
                                return resolve({ error: true, message: 'insert_image_failed' });
                            }else{
                                data.businessImages       = infoImageAfterInsert._id;
                            }
                        }else{
                            data.businessImages       = infoImage._id;
                        }
                    }

                    let dataAfterUpdate = await COMPANY_INFOMATION_COLL.findByIdAndUpdate({ _id: checkExistsInfo[0]._id }, {
                        ...data
                    },
                    {new: true})
                    .populate({
                        path: 'businessImages',
                        select:'_id name'
                    })
                    .populate({
                        path: 'image',
                        select:'_id name'
                    });

                    return resolve({ error: false, data: dataAfterUpdate, message: "update_info_company" });
                   
                }else{
                    /**
                     * Tạo mới thông tin công ty
                     */
                    const checkExists = await COMPANY_INFOMATION_COLL.findOne({
                        $or: [
                            { name: name },
                        ]
                    });

                    if(checkExists)
                        return resolve({ error: true, message: "name_existed" });


                    /**
                     * Kiểm tra sortDescription có quá 60 ký tự
                     */
                    if ( sortDescription && sortDescription.length > 60){
                        return resolve({ error: true, message: "cannot_insert_more_30_character" });
                    }else{
                        data.sortDescription = sortDescription;
                    }

                    /**
                     * Kiểm tra phone có 10 ký tự -> 11 ký tự
                     */
                    if ( phone ){
                        if ( phone.length == 11 || phone.length == 10 ){
                            data.phone = phone;
                        }else{
                            return resolve({ error: true, message: "phone_invalid" });
                        }
                    }

                    name            && (data.name             = name);
                    longDescription && (data.longDescription  = longDescription);
                    email           && (data.email            = email);
                    address         && (data.address          = address);
                    slogan          && (data.slogan           = slogan);
                    zalo            && (data.zalo             = zalo);
                    facebook        && (data.facebook         = facebook);
                    youtube         && (data.youtube          = youtube);
                    // businessImages  && (data.businessImages   = businessImages);
                    twitter         && (data.twitter          = twitter);
                    linkGoogleMap   && (data.linkGoogleMap    = linkGoogleMap);
                    image           && (data.image            = `/files/${image[0].filename}`);
                    userCreate      && (data.userCreate       = userCreate);

                    /**
                     * b1: Insert business Image vào coll image 
                     * b2: Lấy ID image để thêm vào businessImage trong company-infomation coll
                     */
                    if ( businessImages ){
                        const infoImage           = await IMAGE_COLL.findOne({ name: businessImages[0].filename });
                        if( !infoImage ){
                            let dataImage = {
                                
                                name: `/files/${businessImages[0].filename}`,
                                main: 0,
                                status: 1,
                            };
                            userCreate      && (dataImage.userCreate       = userCreate);
                            
                            const newImage           = new IMAGE_COLL(dataImage);
                            let infoImageAfterInsert = await newImage.save();
                            
                            if(!infoImageAfterInsert){
                                return resolve({ error: true, message: 'insert_image_failed' });
                            }else{
                                data.businessImages       = infoImageAfterInsert._id;
                            }
                        }else{
                            data.businessImages       = infoImage._id;
                        }
                        
                    }
                    
                    // console.log({ data });

                    const infoAfterInsert = await this.insertData(data);
                    if(!infoAfterInsert)
                        return resolve({ error: true, message: 'insert_company_infomation_failed' });

                    return resolve({ error: false, data: infoAfterInsert, message: "insert_info_company" });
                }
                
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    update({ companyInfoID, name, sortDescription, longDescription, phone, email, address, slogan, zalo, facebook, youtube, twitter, linkGoogleMap, userUpdate, image, businessImages }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate))
                    return resolve({ error: true, message: "userUpdateID_not_valid" });
                
                if(!ObjectID.isValid(companyInfoID))
                    return resolve({ error: true, message: "companyInfoID_not_valid" });

                const dataUpdateUser = {};
                name            && (dataUpdateUser.name             = name);
                longDescription && (dataUpdateUser.longDescription  = longDescription);
                email           && (dataUpdateUser.email            = email);
                address         && (dataUpdateUser.address          = address);
                slogan          && (dataUpdateUser.slogan           = slogan);
                zalo            && (dataUpdateUser.zalo             = zalo);
                facebook        && (dataUpdateUser.facebook         = facebook);
                youtube         && (dataUpdateUser.youtube          = youtube);
                // businessImages  && (dataUpdateUser.businessImages   = businessImages);
                twitter         && (dataUpdateUser.twitter          = twitter);
                linkGoogleMap   && (dataUpdateUser.linkGoogleMap    = linkGoogleMap);
                image           && (dataUpdateUser.image            = image[0].filename);
                userUpdate      && (dataUpdateUser.userUpdate       = userUpdate);
    
                /**
                 * Kiểm tra sortDescription có quá 60 ký tự
                 */
                if ( sortDescription && sortDescription.length > 60){
                    return resolve({ error: true, message: "cannot_insert_more_60_character" });
                }else{
                    dataUpdateUser.sortDescription = sortDescription;
                }

                /**
                 * Kiểm tra phone có 10 ký tự -> 11 ký tự
                 */
                if ( phone ){
                    if ( phone.length == 11 || phone.length == 10 ){
                        dataUpdateUser.phone = phone;
                    }else{
                        return resolve({ error: true, message: "phone_invalid" });
                    }
                }

                /**
                 * b1: Insert business Image vào coll image 
                 * b2: Lấy ID image để thêm vào businessImage trong company-infomation
                 */
                if ( businessImages ){
                    const infoImage           = await IMAGE_COLL.findOne({ name: businessImages[0].originalname });
                    if( !infoImage ){
                        let dataImage = {
                            name: businessImages[0].filename,
                            main: 0,
                            status: 1,
                        };
                        userUpdate      && (dataImage.userUpdate       = userUpdate);
    
                        const newImage           = new IMAGE_COLL(dataImage);
                        let infoImageAfterInsert = await newImage.save();
                        if(!infoImageAfterInsert){
                            return resolve({ error: true, message: 'Insert_image_failed' });
                        }else{
                            dataUpdateUser.businessImages       = infoImageAfterInsert._id;
                        }
                    }else{
                        dataUpdateUser.businessImages       = infoImage._id;
                    }
                    
                }

                await this.updateWhereClause({ _id: companyInfoID }, {
                    ...dataUpdateUser
                });

                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // deleteImage({ mainImage, businessImage }) {
    //     return new Promise(async resolve => {
    //         try {
    //             if ( mainImage ){
    //                 console.log("aaaa");
    //             }else if ( businessImage ){
    //                 console.log("aaaa");
    //             }
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    getInfo({ companyInfoID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(companyInfoID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoCompany = await COMPANY_INFOMATION_COLL
                    .findById(companyInfoID)
                    .populate({
                        path: 'businessImages',
                        select:'_id name'
                    });

                if(!infoCompany)
                    return resolve({ error: true, message: "info_company_is_not_exists" });

                return resolve({ error: false, data: infoCompany });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ }){
        return new Promise(async resolve => {
            try {
                const listCompanyInfo = await COMPANY_INFOMATION_COLL
                .find()
                .populate({
                    path: 'businessImages',
                    select:'_id name'
                })
                .populate({
                    path: 'image',
                    select:'_id name'
                });
                
                if(!listCompanyInfo)
                    return resolve({ error: true, message: "not_found_company_infomation_list" });

                return resolve({ error: false, data: listCompanyInfo });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
