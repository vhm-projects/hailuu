"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');

/**
 * INTERNAL PAKCAGE
 */
const cfJWS                         = require('../config/cf_jws');

/**
 * BASE
 */
const BaseModel = require('./intalize/base_model');

/**
 * COLLECTIONS
 */
const USER_COLL = require('../database/user-coll');
let { randomStringFixLengthNumber } = require("../utils/utils");
const CODE_RESET_PASSWORD_MODEL = require('../models/code_reset_password').MODEL;
const { sendMailForgetPassword }    = require('../mailer/module/mail_user');
const CUSTOMER_COLL = require('../database/customer-coll');

class Model extends BaseModel {
    constructor() {
        super(require('../database/user-coll'))
    }

    insert({ username, email, password, level }) {
        return new Promise(async resolve => {
            try {
                if(!username || !email || !password)
                    return resolve({ error: true, message: 'params_not_valid' });

                const emailValid = email.toLowerCase().trim();
                const userValid = username.toLowerCase().trim();

                const checkExists = await USER_COLL.findOne({
                    $or: [
                        { username: userValid },
                        { email: emailValid },
                    ]
                });
                if(checkExists)
                    return resolve({ error: true, message: "name_or_email_existed" });

                const hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });
    
                const dataInsert = {
                    username: userValid, 
                    email: emailValid, 
                    password: hashPassword,
                    level
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_account_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoUser = await USER_COLL.findById(userID);
                if(!infoUser)
                    return resolve({ error: true, message: "user_is_not_exists" });

                return resolve({ error: false, data: infoUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateUser({ userID, email, password, oldPassword, status, level }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

				if(oldPassword){
					const isMatchPass = await compare(oldPassword, checkExists.password);
					if (!isMatchPass) 
						return resolve({ error: true, message: 'old_password_wrong' });
				}

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email    = email.toLowerCase().trim());
                password && (dataUpdateUser.password = hashSync(password, 8));
                status   && (dataUpdateUser.status   = status);
                level    && (dataUpdateUser.level    = level);

                await this.updateWhereClause({ _id: userID }, dataUpdateUser);
                password && delete dataUpdateUser.password;

                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    deleteUser({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                let amountUSer = await USER_COLL.count();
                if(amountUSer == 1)
                    return resolve({ error: true, message: "Cannot_remove" });
                    
                const infoAfterDelete = await USER_COLL.findByIdAndDelete(userID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "user_is_not_exists" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListUsers({ keyword }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { email: new RegExp(key, 'i') }
                    ]
                }

                const listUsers = await USER_COLL.find(condition);
                if(!listUsers)
                    return resolve({ error: true, message: "not_found_accounts_list" });

                return resolve({ error: false, data: listUsers });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim() });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

                const infoUser = {
                    _id: checkExists._id,
                    username: checkExists.username,
                    email: checkExists.email,
                    status: checkExists.status,
                    level: checkExists.level,
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    forgetPassword({ email }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await CUSTOMER_COLL.findOne({ email: email.toLowerCase().trim(), status: 1 });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                let codeReset = randomStringFixLengthNumber(6);

                let infoCodeAfterInsert =  await CODE_RESET_PASSWORD_MODEL.insert({ email, code: codeReset });
                if (infoCodeAfterInsert.error) {
                    return resolve(infoCodeAfterInsert);
                }

                sendMailForgetPassword(email, codeReset);

                return resolve(infoCodeAfterInsert);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
