"use strict";

let util = require('util');
let moment = require('moment');

let { PAYMENT_TYPE } = require('../../config/cf_constants');
let baseUrl = require('../../config/cf_host').domain;
let logo1 = require('../../utils/constant')._base_logo1_url;
let webName = require('../../utils/constant').webName;
let mailer = require('../mailer');
let language = require('../../../language/language_routes');
const defaultLanguage = language.defaultLanguage;
let formatter = new Intl.NumberFormat('en-US');

exports.verifyEmailUser = function (userName, email, link, lang) {
    let emailContent = getHeaderEmail(util.format(language.getLanguage('welcome_to_website', lang), ['EXT GROUP']))
        + getContentVerifyEMail(language.getLanguage('xac_minh_dia_cho_email_cua_ban_click_vao_ben_duoi', lang), link, lang);
    +getFooterEmail();

    mailer(email, util.format(language.getLanguage('vui_long_xac_thuc_email_cua_ban', lang), [userName]), emailContent, function (callback) {
    })
};

/**
 * @param email
 * @param userName
 * @param link
 * @param type
 */
exports.resetPassAccount = function (email, userName, link, type) {

    let emailContent = getHeaderEmail((type === 1) ? "RESET PASSWORD" : "RESET SECOND PASSWORD")
        + getContentEmailResetPass(userName, baseUrl + link, type);
    +getFooterEmail();

    mailer(email, `${webName}: Reset password`, emailContent, function (callback) {
    });
};


function getHeaderEmail(title) {
    return '<!DOCTYPE html>' +
        '<html lang="en">' +
        '<head> ' +
        '<meta charset="UTF-8"> ' +
        '<title>Document</title>' +
        '</head>' +
        '<body>' +
        '<div class="gmail_quote"> ' +
        '<div style="margin:0px;background-color:#f4f3f4;font-family:Helvetica,Arial,sans-serif;font-size:12px" ' +
        'text="#444444" bgcolor="#F4F3F4" link="#21759B" ' +
        'alink="#21759B" vlink="#21759B" marginheight="0" ' +
        'marginwidth="0"> <table border="0" width="100%" ' +
        'cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">' +
        ' <tbody>' +
        ' <tr>' +
        ' <td style="padding:15px"> ' +
        '<center> ' +
        '<table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff"> ' +
        '<tbody> ' +
        '<tr> ' +
        '<td align="left">' +
        '<div style="border:solid 1px #d9d9d9"> ' +
        '<table style="line-height:1.6;font-size:12px;font-family:Helvetica,Arial,sans-serif;border:solid 1px #ffffff;color:#444" ' +
        'border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff"> ' +
        '<tbody> <tr> <td style="color:#ffffff" ' +
        'colspan="2" valign="bottom" height="30">.</td>' +
        '</tr>' +
        '<tr> ' +
        '<td style="line-height:25px;padding: 10px 20px; text-align: center">' +
        '<a style="text-decoration:none" href="' + baseUrl + '" target="_blank" ' +
        'class="CToWUd">' +
        '<img style="width: 200px" src="' + logo1 + '">' +
        '</a>' +
        '<h1 style="color: #2e3192; font-size: 30px; margin-bottom: 40px; text-align: center">' + title + '</h1>' +
        '</td>' +
        '</tr>' +
        '</tbody> ' +
        '</table> ' +
        '<table style="margin-top:15px;margin-right:30px;margin-left:30px;color:#444;line-height:1.6;font-size:12px;' +
        'font-family:Arial,sans-serif" border="0" width="490" ' +
        'cellspacing="0" cellpadding="0" bgcolor="#ffffff"> ';
}

function getFooterEmail() {
    return '</table> <table style="line-height:1.5;font-size:12px;font-family:Arial,' +
        'sans-serif;margin-right:30px;margin-left:30px" border="0" width="490" ' +
        'cellspacing="0" cellpadding="0" bgcolor="#ffffff"> ' +
        '<tbody> <tr style="font-size:11px;color:#999999">' +
        ' <td style="border-top:solid 1px #d9d9d9" colspan="2"> ' +
        '<div style="padding:15px 0"> This is automatic mailbox please do not Replied to this message! </div>' +
        '</td></tr><tr> <td style="color:#ffffff" colspan="2" height="15">.</td></tr></tbody> </table> ' +
        '</div></td></tr></tbody> </table> </center> </td></tr>' +
        '</tbody> </table> <div class="yj6qo"></div><div class="adL">' +
        '</div></div><div class="adL"></div></div></body></html>';
}

function getHeaderMailV2() {
	return '<!DOCTYPE html>' +
        '<html lang="en">' +
        '<head> ' +
        '<meta charset="UTF-8"> ' +
        '<title>Document</title>' +
        '</head>' +
        '<body>' +
        '<div class="gmail_quote"> ' +
        '<div style="margin:0px;background-color:#f4f3f4;font-family:Helvetica,Arial,sans-serif;font-size:12px" ' +
        'text="#444444" bgcolor="#F4F3F4" link="#21759B" ' +
        'alink="#21759B" vlink="#21759B" marginheight="0" ' +
        'marginwidth="0"> <table border="0" width="100%" ' +
        'cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">' +
        ' <tbody>' +
        ' <tr>' +
        ' <td style="padding:15px"> ' +
        '<center> ' +
        '<table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff"> ' +
        '<tbody> ' +
        '<tr> ' +
        '<td align="left">' +
        '<div style="border:solid 1px #d9d9d9"> ' +
        '<table style="margin-top:15px;margin-right:30px;margin-left:30px;color:#444;line-height:1.6;font-size:12px;' +
        'font-family:Arial,sans-serif" border="0" width="490" ' +
        'cellspacing="0" cellpadding="0" bgcolor="#ffffff"> ';
}

function getContentVerifyEMail(message, link, lang) {
    return '<tbody>' +
        '<tr>' +
        '<td style="border-top:solid 1px #d9d9d9" colspan="2"> ' +
        '<div style="padding:15px 0; line-height: 1.6;">' +
        '<br><b style="font-size: 16px; text-align: center; display: block;">'+message+'</b>'+
        '<br>' +
        '<br><p style="text-align: center;"><a style="text-decoration: none; background: #2e3291; color: #fff; padding: 7px 25px; font-size: 15px; border-radius: 5px;" href="'+link+'">'+language.getLanguage('xac_nhan_dia_chi_email', lang)+'</a></p>' +
        '<br>' +
        '</div>' +
        '</td></tr></tbody>';
}

function getContentEmailResetPass(userName, link, type) {
    return '<tbody>' +
        '<tr>' +
        '<td style="border-top:solid 1px #d9d9d9" colspan="2"> ' +
        '<div style="padding:15px 0">' +
        '<p class="title" style="line-height: 1.7; font-size: 19px;">' + util.format(language.getLanguage((type === 1) ? 'reset_password_mail' : 'reset_second_password_mail', defaultLanguage), userName, link) + '</p>' +
        '<br>' +
        '</div>' +
        '</td></tr></tbody>';
}

function templateForgetAccount(email, code) {
    return `
        ${getHeaderEmail("HAILUU")}
        Xin chào ${email}, Code mặc định của bạn là <b>${code}</b>
        <p><i>Email này được gửi từ hệ thống HAILUU. Hãy nhập code để đổi lại mật khẩu</i></p>
        <hr>
        <p>Hi ${email}, Your default code is <b>${code}</b></p>
        <hr>
        <p><i>This email is send from HAILUU. Please enter the code to change the password </i></p>
        ${getFooterEmail()}
    `;
}

function templateReportStatusOrderV1(email, infoOrder, infoAddress) {
	let totalPrice = 0;
	let address = '', htmlOrderLine = '';

	if (infoOrder.orderLine && infoOrder.orderLine.length) {
		infoOrder.orderLine.forEach(orderLine => {
			let optionsChoosed = orderLine.productProperty;
			let htmlPropertyProduct = '';

			if (optionsChoosed) {
				totalPrice += orderLine.amount || optionsChoosed.price || optionsChoosed.price_at_compare;

				optionsChoosed.variant_values && optionsChoosed.variant_values.map(variantValue => {
					htmlPropertyProduct += `
						<div> ${variantValue.variant.name}: ${variantValue.name} </div>
					`;
				})
			}

			htmlOrderLine += `
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:20px 20px 0px 30px;" bgcolor="#FFFFFF">
				<tbody>
				<tr role="module-content">
					<td height="100%" valign="top">
					<table class="column" width="137" style="width:137px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="">
						<tbody>
						<tr>
							<td style="padding:0px;margin:0px;border-spacing:0;"><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="239f10b7-5807-4e0b-8f01-f2b8d25ec9d7">
								<tbody>
								<tr>
									<td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="left">
									<img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" width="104" alt="" data-proportionally-constrained="true" data-responsive="false" src="${orderLine.product.avatar && orderLine.product.avatar.path}" height="104">
									</td>
								</tr>
								</tbody>
							</table></td>
									</tr>
									</tbody>
								</table>
								<table class="column" width="137" style="width:137px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="">
									<tbody>
									<tr>
										<td style="padding:0px;margin:0px;border-spacing:0;"><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f404b7dc-487b-443c-bd6f-131ccde745e2">
							<tbody>
							<tr>
								<td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><h4>${orderLine.product.name}</h4></div>
						<div style="font-family: inherit; text-align: inherit">${htmlPropertyProduct}</div>
						<div style="font-family: inherit; text-align: inherit"><span style="color: #0055ff">${optionsChoosed ? formatter.format(optionsChoosed.price || optionsChoosed.price_at_compare) : 0} VNĐ &nbsp;</span></div><div></div></div></td>
							</tr>
							</tbody>
						</table></td>
									</tr>
									</tbody>
								</table>
								<table width="137" style="width:137px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-2">
							<tbody>
								<tr>
								<td style="padding:0px;margin:0px;border-spacing:0;"></td>
								</tr>
							</tbody>
							</table><table width="137" style="width:137px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-3">
							<tbody>
								<tr>
								<td style="padding:0px;margin:0px;border-spacing:0;"></td>
								</tr>
							</tbody>
							</table></td>
							</tr>
					</tbody>
				</table>
			`;
		})
	}

	if(infoAddress){
		address = `
			${infoAddress.info.address}, 
			${infoAddress.infoWard[1].name_with_type},
			${infoAddress.infoDistrict[1].name_with_type},
			${infoAddress.infoProvince[1].name_with_type}
		`;
	} else{
		address = 'Nhận sản phẩm in tại cửa hàng của Hailuu.vn';
	}

    return `
        ${getHeaderEmail("HAILUU")}
		<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="8b5181ed-0827-471c-972b-74c77e326e3d">
			<tbody>
				<tr>
				<td style="padding:30px 20px 18px 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
					<div>
					<div style="font-family: inherit; text-align: inherit">
						<span style="color: #0055ff; font-size: 24px"> Đơn hàng: <b>${infoOrder.orderID}</b>
						</span>
					</div>
					<div></div>
					</div>
				</td>
				</tr>
			</tbody>
			</table>
			<table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f7373f10-9ba4-4ca7-9a2e-1a2ba700deb9">
			<tbody>
				<tr>
				<td style="padding:0px 30px 0px 30px;" role="module-content" height="100%" valign="top" bgcolor="">
					<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;">
					<tbody>
						<tr>
						<td style="padding:0px 0px 3px 0px;" bgcolor="#e7e7e7"></td>
						</tr>
					</tbody>
					</table>
				</td>
				</tr>
			</tbody>
			</table>
			<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="264ee24b-c2b0-457c-a9c1-d465879f9935">
			<tbody>
				<tr>
				<td style="padding:18px 20px 18px 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
					<div>
					<div style="font-family: inherit; text-align: inherit">
						<span style="color: #0055ff">
						<strong>Trạng thái đơn hàng: Đóng gói và vận chuyển</strong>
						</span>
					</div>
					<div style="font-family: inherit; text-align: inherit">
						<br>
					</div>
					<div style="font-family: inherit; text-align: inherit">Ngày tạo đơn: <b> ${moment(infoOrder.createAt).format('DD/MM/YYYY HH:mm')} </b></div>
					<div style="font-family: inherit; text-align: inherit">Hình thức thanh toán: <b> ${PAYMENT_TYPE[infoOrder.payment].text} </b></div>
					<div style="font-family: inherit; text-align: inherit">Trạng thái thanh toán: <b>${[1,2,3].includes(infoOrder.statusPayment) ? 'Đã thanh toán' : 'Chưa thanh toán'}</b> &nbsp;</div>
					<div style="font-family: inherit; text-align: inherit">
						Địa chỉ: <b>${address}</b>
					</div>
					<div></div>
					</div>
				</td>
				</tr>
			</tbody>
			</table> 
			${htmlOrderLine} 
			<table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f7373f10-9ba4-4ca7-9a2e-1a2ba700deb9.1">
			<tbody>
				<tr>
				<td style="padding:20px 30px 0px 30px;" role="module-content" height="100%" valign="top" bgcolor="">
					<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;">
					<tbody>
						<tr>
						<td style="padding:0px 0px 3px 0px;" bgcolor="E7E7E7"></td>
						</tr>
					</tbody>
					</table>
				</td>
				</tr>
			</tbody>
			</table>
			<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="264ee24b-c2b0-457c-a9c1-d465879f9935.1">
			<tbody>
				<tr>
				<td style="padding:18px 20px 30px 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
					<div>
					<div style="font-family: inherit; text-align: inherit">Số tiền đơn hàng: ${formatter.format(totalPrice || 0)}</div>
					<div style="font-family: inherit; text-align: inherit">Phí vận chuyển: ${formatter.format(infoOrder.deliveryPrice || 0)} </div>
					<div style="font-family: inherit; text-align: inherit">
						<br> THÀNH TIỀN&nbsp;
					</div>
					<div style="font-family: inherit; text-align: inherit">
						<br>
					</div>
					<div style="font-family: inherit; text-align: inherit">
						<span style="color: #0055ff; font-size: 32px; font-family: inherit">${formatter.format(infoOrder.amount || 0)}</span>
					</div>
					<div></div>
					</div>
				</td>
				</tr>
			</tbody>
		</table>

        <p><i>Email này được gửi từ hệ thống HAILUU.</i></p>
        <hr>
        <p><i>This email is send from HAILUU.</i></p>
        ${getFooterEmail()}
    `;
}

function templateReportStatusOrderV2(infoOrder, infoAddress) {
	let address = '', htmlOrderLine = '';

	if (infoOrder.orderLine && infoOrder.orderLine.length) {
		infoOrder.orderLine.forEach(orderLine => {
			htmlOrderLine += `${orderLine.product.name}<br />`;
		})
	}

	if(infoAddress){
		address = `
			${infoAddress.info.address}, 
			${infoAddress.infoWard[1].name_with_type},
			${infoAddress.infoDistrict[1].name_with_type},
			${infoAddress.infoProvince[1].name_with_type}
		`;
	} else{
		address = 'Nhận sản phẩm in tại cửa hàng của Hailuu.vn';
	}

	return `
		${getHeaderMailV2()}
		<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="8b5181ed-0827-471c-972b-74c77e326e3d">
			<tbody>
				<tr>
					<td style="padding:30px 20px 18px 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div>
							<div style="font-family: inherit; text-align: inherit">
								<span style="font-size: 24px">
									<b>Cập nhật đơn hàng</b>
								</span>
							</div>
							<div></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f7373f10-9ba4-4ca7-9a2e-1a2ba700deb9">
			<tbody>
				<tr>
					<td style="padding:0px 30px 0px 30px;" role="module-content" height="100%" valign="top" bgcolor="">
						<div>
							<div style="font-family: inherit; text-align: inherit">
								<span>
									Xin chào <b>${infoOrder.customer.fullname}</b>. Đơn hàng ${infoOrder.orderID} của bạn vừa được cập nhật trạng thái mới
								</span>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="264ee24b-c2b0-457c-a9c1-d465879f9935">
			<tbody>
				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Mã đơn hàng
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							<b> ${infoOrder.orderID} </b>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Tên sản phẩm
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							<b>${htmlOrderLine}</b>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Giá tiền
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							<b>${infoOrder.amount}</b>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Trạng thái đơn hàng
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							<b>Đóng gói và vận chuyển</b>
						</div>
					</td>
				</tr>

				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Trạng thái thanh toán
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div>
							<div style="font-family: inherit; text-align: inherit">
								<b>${[1,2,3].includes(infoOrder.statusPayment) ? 'Đã thanh toán' : 'Chưa thanh toán'}</b>
							</div>
						</div>
					</td>
				</tr>
				
				<tr>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							Địa chỉ giao hàng
						</div>
					</td>
					<td style="padding:15px 20px 0 30px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content">
						<div style="font-family: inherit; text-align: inherit">
							<b>${address}</b>
						</div>
					</td>
				</tr>	
			</tbody>
		</table>

		${getFooterEmail()}
	`;
}

exports.sendMailForgetPassword = function (email, code){
    let emailContent = templateForgetAccount(email, code);
    mailer(email, `HAILUU - HAILUU.COM`, emailContent, function(cb){
        console.log(cb);
    });
}

exports.sendMailReportStatusOrder = function (email, infoOrder, infoAddress){
    let emailContent = templateReportStatusOrderV2(infoOrder, infoAddress);
    mailer(email, `HAILUU - HAILUU.COM`, emailContent, function(cb){
        console.log(cb);
    });
}
