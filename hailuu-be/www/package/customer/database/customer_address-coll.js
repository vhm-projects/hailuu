"use strict";
const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION ACCOUNT
 */
module.exports  = BASE_COLL("customer_address", {
    // Tên người nhận
    receiverName: String,
    // Số điện thoại người nhận
    receiverPhone: String,
    customer: {
        type:  Schema.Types.ObjectId,
        ref : 'customer'
    },
    address: {
        type: String
    },

    city: {
        type: String
    },

    district: {
        type: String
    },

    ward: {
        type: String
    },
    // Bổ sung
    cityText: {
        type: String
    },

    districtText: {
        type: String
    },

    wardText: {
        type: String
    },

    fullAddress: {
        type: String
    },
    /**
     * 1: Địa chỉ mặc định
     * 0: Địa chỉ không mặc đinh
     */
    default: {
        type: Number,
        default: 0
    },
    /**
     * Nhận thông báo
     * 1: Hoạt động
     * 0: Không hoạt động
     * -1: Tạm xóa
     */
    status: {
        type: Number,
        default: 1
    }
 
});