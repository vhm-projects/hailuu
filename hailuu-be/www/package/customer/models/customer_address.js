"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                    = require('mongoose').Types.ObjectId;
let moment                        = require('moment');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CUSTOMER_ADDRESS_COLL = require('../database/customer_address-coll');
const CUSTOMER_COLL = require('../../../database/customer-coll');

const { checkObjectIDs }    = require("../../../utils/utils");


class Model extends BaseModel {
    constructor() {
        super(require('../database/customer_address-coll'))
    }

    insert({ receiverName, receiverPhone, city, district, ward, address, customerID, addressDefault }) {
        return new Promise(async resolve => {
            try {
                if (!address) {
                    return resolve({ error: true, message: 'address_invalid' });
                }
                if (!city) {
                    return resolve({ error: true, message: 'city_invalid' });
                }
                if (!district) {
                    return resolve({ error: true, message: 'district_invalid' });
                }
                if (!ward) {
                    return resolve({ error: true, message: 'ward_invalid' });
                }
                // Nếu địa chỉ là mặc định thì set tất cả các địa chỉ của khách hàng này về ko mặc định
                if(addressDefault == 1){
                    await CUSTOMER_ADDRESS_COLL.updateMany({ customer: customerID }, { $set: { default: 0 }});
                }

                let dataInsert = {
                    receiverName, receiverPhone, customer: customerID, city, district, ward, address, default: addressDefault
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_customer_address_failed' });

                if(addressDefault == 1)
                    await CUSTOMER_COLL.findByIdAndUpdate(customerID, { addressDefault: infoAfterInsert._id  });

                await CUSTOMER_COLL.findByIdAndUpdate(customerID, { $addToSet: {  address: infoAfterInsert._id }});
                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateEnduer({ addressID, receiverName, receiverPhone, city, district, ward, address, addressDefault, customerID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }

                if (!address) {
                    return resolve({ error: true, message: 'address_invalid' });
                }
                if (!city) {
                    return resolve({ error: true, message: 'city_invalid' });
                }
                if (!district) {
                    return resolve({ error: true, message: 'district_invalid' });
                }
                if (!ward) {
                    return resolve({ error: true, message: 'ward_invalid' });
                }
                // Nếu địa chỉ là mặc định thì set tất cả các địa chỉ của khách hàng này về ko mặc định
                if(addressDefault == 1){
                    await CUSTOMER_ADDRESS_COLL.updateMany({ customer: customerID }, { $set: { default: 0 }});
                }

                let dataUpdate = {
                    receiverName, receiverPhone, city, district, ward, address, default: addressDefault
                }

                const infoAfterInsert = await CUSTOMER_ADDRESS_COLL.findByIdAndUpdate(addressID, dataUpdate);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_customer_address_failed' });

                if(addressDefault == 1)
                    await CUSTOMER_COLL.findByIdAndUpdate(customerID, { addressDefault: infoAfterInsert._id  });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    getInfoCustomer({ addressID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }

                const infoData = await CUSTOMER_ADDRESS_COLL.findById(addressID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get_info_address' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByCustomer({ customerID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(customerID)) 
                    return resolve({ error: true, message: 'params_invalid' });

                const listData = await CUSTOMER_ADDRESS_COLL.find({ customer: customerID, status: 1 }).populate("customer");
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_info_address' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ addressID, city, district, ward, address }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }

                if (!city || !district || !ward || !address)
                    return resolve({ error: true, message: 'city_district_ward_address_invalid' });

                let dataUpdate = {
                    city, district, ward, address
                }

                const infoData = await CUSTOMER_ADDRESS_COLL.findByIdAndUpdate(addressID, {
                    ...dataUpdate
                }, {
                    new: true
                });
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_update_info_address' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ addressID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(addressID)) {
                    return resolve({ error: true, message: 'params_invalid' });
                }

                const STATUS_DELETE = -1;
                const infoData = await CUSTOMER_ADDRESS_COLL.findByIdAndUpdate(addressID, {
                   status: STATUS_DELETE
                }, {
                    new: true
                });
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_update_info_address' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
