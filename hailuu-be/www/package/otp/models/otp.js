"use strict";
/**
 * EXTERNAL PAKCAGE
 */
const OBJECT_ID = require('mongoose').Types.ObjectId;
const request  = require('request');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const OTP_COLL              = require('../database/otp');
const { URL_AUTHY, AUTHY_API_KEY } = require("../../../config/cf_authy");

class Model extends BaseModel {
    constructor() {
        super(OTP_COLL);
    }

    insert({ phone, authyID }) {
        return new Promise(async resolve => {
            try {
                if (!phone || !authyID)
                    return resolve({ error: true, message: 'param_invalid' });
                let infoAfterInsert = await this.insertData({
                    phone, authyID
                });
                if (!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });
                return resolve({ error: false, data: infoAfterInsert })
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }
    updateAuthy({ phone, authyID }) {
        return new Promise(async resolve => {
            try {
                if (!phone || !authyID)
                    return resolve({ error: true, message: 'param_invalid' })
                let infoAfterUpdate = await OTP_COLL.findByIdAndUpdate(phone, {
                    authyID, modifyAt: Date.now()
                }, { new: true });
                if (!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }
    updateIsActive({ phone, isActive }) {
        return new Promise(async resolve => {
            try {
                if (!phone || !isActive)
                    return resolve({ error: true, message: 'param_invalid' })
                let infoAfterUpdate = await OTP_COLL.findOneAndUpdate(phone, {
                    isActive, modifyAt: Date.now()
                }, { new: true });
                if (!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    /**
     * user[cellphone]      : 0964209752
     * user[country_code]   : 84
     */
    sendOTP({ phone }) {
        return new Promise(async (resolve) => {
            try {
                // let infoUser = await USER_COLL.findById(userID);
                // if (!infoUser) return resolve({ error: true, message: 'cannot_get_info' });
                let infoUser = await OTP_COLL.findOne({ phone });
                if(!infoUser) {
                    var options_create_authy_user = { method: 'POST',
                        url: `${URL_AUTHY}/protected/json/users/new`,
                        headers: 
                        { 
                            'cache-control': 'no-cache',
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'X-Authy-API-Key': AUTHY_API_KEY },
                        form: 
                        { 
                            // 'user[email]': infoUser.email,
                            'user[email]': `lephuocson1999@gmail.com`,
                            'user[cellphone]': phone,
                            'user[country_code]': '84' } };
                    request(options_create_authy_user, async (error, response, body) => {
                        if (error) throw new Error(error);
                        let resp = JSON.parse(body);
                        console.log({ resp });
                        if (resp.success) {
                            /**
                             * CẬP NHẬT AUTHY USER VỪA TẠO CHO authyID 
                             */
                            // let infoUpdate = await USER_COLL.findByIdAndUpdate(userID, {
                            //     authyID: resp.user.id,
                            // }, { new: true });
                            // if (!infoUpdate) return resolve({ error: true, message: 'cannot_update_authy_id' });
                            let infoAfterInsertAuthy = await this.insert({ phone, authyID: resp.user.id })
                            console.log({ infoAfterInsertAuthy })
                            if (!infoAfterInsertAuthy || infoAfterInsertAuthy.error) 
                                return resolve({ error: true, message: 'cannot_update_authy_id' });
                            /**
                             * SEND SMS WITH AUTH_ID
                             */
                            var options_get_code = { method: 'GET',
                                url: `${URL_AUTHY}/protected/json/sms/${resp.user.id}`,
                                qs: { locale: 'vi', force: 'true' },
                                headers: 
                                { 
                                    'cache-control': 'no-cache',
                                    'X-Authy-API-Key': AUTHY_API_KEY 
                                } 
                            };
                            request(options_get_code, function (error_get_code, response_get_code, body_get_code) {
                                // console.log({ error_get_code, response_get_code, body_get_code })
                                if (error_get_code) throw new Error(error_get_code);
                                // console.log(body);
                                let resp2 = JSON.parse(body_get_code);
                                // console.log({ __otp: JSON.parse(response_get_code) })
                                // console.log({ resp2 })
                                if (resp2.success) {
                                    return resolve({ error: false, message: 'send_success' });
                                } else {
                                    return resolve({ error: true, message: 'send_otp_error' });
                                }
                            });
                        } else {
                            return resolve({ error: true, message: 'send_otp_error' });
                        }
                    });
                } else {
                    // console.log({ infoUser })
                    var options_resend_if_exist_user_and_exist_authyID = { 
                        method: 'GET',
                        url: `${URL_AUTHY}/protected/json/sms/${infoUser.authyID}`,
                        qs: { locale: 'vi', force: 'true' },
                        headers: 
                        { 
                            'cache-control': 'no-cache',
                            'X-Authy-API-Key': AUTHY_API_KEY 
                        } 
                    };
                    request(options_resend_if_exist_user_and_exist_authyID, async (error, response, body) => {
                        if (error) throw new Error(error);
                        // console.log(body);
                        let resp = JSON.parse(body);
                        if (resp.success) {
                            /**
                             * CẬP NHẬT AUTHY USER VỪA TẠO CHO authyID 
                             */
                            // let infoUpdate = await USER_COLL.findByIdAndUpdate(userID, {
                            //    isActiveOTP: 0
                            // }, { new: true });
                            let INACTIVE_STATUS = 0;
                            let infoUpdate = await this.updateIsActive({ phone, isActive: INACTIVE_STATUS })
                            if (!infoUpdate) return resolve({ error: true, message: 'cannot_update_authy_id' });
                            return resolve({ error: false, message: 'send_success' });
                        } else {
                            return resolve({ error: true, message: 'send_otp_error' });
                        }
                    });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    verificationOTP({ phone, code }) {
        return new Promise(async resolve => {
            try {
                // let infoUser = await USER_COLL.findById(userID);
                // if (!infoUser) return resolve({ error: true, message: 'cannot_get_info' });
                let infoUser = await OTP_COLL.findOne({ phone });
                if (!infoUser) return resolve({ error: true, message: 'cannot_get_info' });
                var option_verify_code = { 
                    method: 'GET',
                    url: `${URL_AUTHY}/protected/json/verify/${code}/${infoUser.authyID}`,
                    headers: 
                    {
                        'cache-control': 'no-cache',
                        'X-Authy-API-Key': AUTHY_API_KEY 
                    } 
                };
                request(option_verify_code, async (error, response, body) => {
                    if (error) throw new Error(error);
                    let resp = JSON.parse(body);
                    if (!resp.success) {
                        return resolve({ error: true, message: 'verify_error' });
                    } else {
                        /**
                         * UPDATE isActiveOTP = 1
                         */
                        // let infoUpdate = await USER_COLL.findByIdAndUpdate(userID, {
                        //     isActiveOTP: 1
                        // }, { new: true });
                        let ACTIVE_STATUS = 1;
                        let infoUpdate = await this.updateIsActive({ phone, isActive: ACTIVE_STATUS });
                        if (!infoUpdate) return resolve({ error: true, message: 'cannot_update_authy_id' });
                        return resolve({ error: false, message: 'verify_success' });
                    }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}
exports.MODEL = new Model;