"use strict";
 /**
  * bảng log tạm cho các user verify OTP khi chưa đăng nhập (đang đăng ký)
  */
const BASE_COLL  = require('../../../database/intalize/base-coll');

module.exports  = BASE_COLL("otp", {
    phone: { type: String, require: true },
    authyID: String,
    /**
     * 0: Inactive
     * 1: Active
     */
    isActive: {
        type: Number,
        default: 0
    } 
});