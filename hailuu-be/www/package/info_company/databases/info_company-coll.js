"use strict"

const Schema    = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CATEGORY CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('info_company', {
	hotline: {
		type: String,
		trim: true,
		unique : true
	},
    email: {
		type: String,
		trim: true,
		unique : true
	},
	address: {
		type: String,
		trim: true,
		unique : true
	},
    
    slogun: {
        type: String,
    },

    openTime: {
        type: String,
    },
  
    closeTime: {
        type: String,
    },
    
    sortDes: {
        type: String,
    },

    // zalo: {
    //     type: String,
    // },

    longDes: {
        type: String,
    },

    facebook: {
        type: String,
    },
    // Ảnh chính sản phẩm
	avatar: {
		type: Schema.Types.ObjectId,
		ref: 'image'
	},

	// Hình ảnh sản phẩm
	gallery: [{
		type: Schema.Types.ObjectId,
		ref: 'image'
	}],
    //_________Hình thức thanh toán
    paymentforms: String,

    //_________Giao hàng và vận chuyển
    deliveryAndShipping: String,

    //_________Đặt hàng online
    orderOnline: String,

    //_________Chính sách bảo hành
    warrantyPolicy: String,

    //_________Quy định sử dụng
    usageRules: String,

    //_________Bảo mật thông tin
    informationSecurity: String,

    //_________Chính sách đổi trả hàng
    refundPolicy: String,
    
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});
