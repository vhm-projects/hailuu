"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs   = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
// const { uploadSingle, uploadFieldsAvatarGallery }                      = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_INFO_COMPANY } 			= require('../constants/info_company.uri');
const { responseJsonError } 	                = require('../../../utils/utils');

/**
 * MODELS
 */
const INFO_COMPANY_MODEL   = require('../models/info_company').MODEL;
const COMMON_MODEL         = require('../../../config/constants/constant/common').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ====================== ************************ ================================
             * =====================      QUẢN LÝ DANH MỤC     ================================
             * ====================== ************************ ================================
             */

            /**
             * Function: Tạo Category (View, API)
             * Date: 20/06/2021
             * Dev: VyPQ
             */
            [CF_ROUTINGS_INFO_COMPANY.INFO_COMPANY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    title: 'Info Company',
                    type: 'view',
                    code: CF_ROUTINGS_INFO_COMPANY.INFO_COMPANY,
                    inc: path.resolve(__dirname, '../views/info_company.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let infoCompany  = await INFO_COMPANY_MODEL.getList({ });
                        ChildRouter.renderToView(req, res, {
                            infoCompany: infoCompany.data[0],
                        });
					}],
                    post: [ async function (req, res) {
                        let { _id: userCreate  } = req.user;
                        const { 
                            email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, arrImageMain, arrImageGallery, arrImageGalleryOld,
                            paymentforms, deliveryAndShipping, orderOnline, warrantyPolicy, usageRules, informationSecurity, refundPolicy
                        } = req.body;

                        let infoCompany  = await INFO_COMPANY_MODEL.getList({ });

                        let infoInsert;
                        if (infoCompany.error == false && infoCompany.data.length) {
                            infoInsert = await INFO_COMPANY_MODEL.update({ 
                                companyId: infoCompany.data[0]._id, email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, userUpdate: userCreate, avatar:  arrImageMain,  gallery: arrImageGallery, galleryDelete: arrImageGalleryOld,
                                paymentforms, deliveryAndShipping, orderOnline, warrantyPolicy, usageRules, informationSecurity, refundPolicy
                            });
                        } else {
                            infoInsert = await INFO_COMPANY_MODEL.insert({ 
                                email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, userCreate, avatar:  arrImageMain,  gallery: arrImageGallery
                                
                            });
                        }

                        res.json(infoInsert);
                    }]
                },
            },
        }
    }
};
