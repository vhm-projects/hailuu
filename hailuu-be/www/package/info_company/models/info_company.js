 "use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');
const { checkObjectIDs }            = require("../../../utils/utils");
/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const INFO_COMPANY_COLL  		= require('../databases/info_company-coll');
const IMAGE_MODEL               = require('../../../models/image').MODEL;


class Model extends BaseModel {
    constructor() {
        super(INFO_COMPANY_COLL);
    }

	insert({ email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, userCreate, avatar, gallery }) {
        return new Promise(async resolve => {
            try {
                if(!hotline)
                    return resolve({ error: true, message: 'hotline_invalid' });
                if(!email)
                    return resolve({ error: true, message: 'email_invalid' });
                if(!address)                  
                    return resolve({ error: true, message: 'address_invalid' });
                if(!slogun)                  
                    return resolve({ error: true, message: 'slogun_invalid' });
                if(!openTime)                  
                    return resolve({ error: true, message: 'openTime_invalid' });
                if(!closeTime)                  
                    return resolve({ error: true, message: 'closeTime_invalid' });
                if(!checkObjectIDs(userCreate))                  
                    return resolve({ error: true, message: 'id_invalid' });
                if(!sortDes)                  
                    return resolve({ error: true, message: 'sortDes_invalid' });
                if(!longDes)                  
                    return resolve({ error: true, message: 'longDes_invalid' });
                if(!facebook)                  
                    return resolve({ error: true, message: 'longDes_invalid' });
                if (!avatar || !avatar.name) {
                    return resolve({ error: true, message: "avatar_invalid" });
                }

                let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                    name: avatar.name, 
                    size: avatar.size,
                    path: avatar.pathAvatar,
                    userCreate: userCreate 
                });

                let dataInsert = {
                    avatar: infoImageAfterInsert.data._id, email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, userCreate
                }

                if (gallery && gallery.length) {
                    let listGallery = gallery.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userCreate 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    // console.log({ resultPromiseAll });
                    let listGalleryID    = resultPromiseAll.map( gallery => gallery.data._id );

                    dataInsert.gallery = listGalleryID;
                }
                
                let infoInsert = await this.insertData(dataInsert);
                if(!infoInsert)
                    return resolve({ error: true, message: 'insert_failed' });

                return resolve({ error: false, data: infoInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
            companyId, email, hotline, slogun, openTime, closeTime, sortDes, longDes, address, facebook, userUpdate, galleryDelete, avatar, gallery,
            paymentforms, deliveryAndShipping, orderOnline, warrantyPolicy, usageRules, informationSecurity, refundPolicy
        }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(userUpdate))                  
                    return resolve({ error: true, message: 'id_invalid' });
                if(!checkObjectIDs(companyId))                  
                    return resolve({ error: true, message: 'id_invalid' });
                let dataUpdate = { userUpdate }

                hotline         && (dataUpdate.hotline        = hotline);
                email           && (dataUpdate.email          = email);
                address         && (dataUpdate.address        = address);
                slogun          && (dataUpdate.slogun         = slogun);
                openTime        && (dataUpdate.openTime       = openTime);
                closeTime       && (dataUpdate.closeTime      = closeTime);
                sortDes         && (dataUpdate.sortDes        = sortDes);
                longDes         && (dataUpdate.longDes        = longDes);
                facebook        && (dataUpdate.facebook       = facebook);
                paymentforms    && (dataUpdate.paymentforms   = paymentforms);
                deliveryAndShipping    && (dataUpdate.deliveryAndShipping   = deliveryAndShipping);
                orderOnline     && (dataUpdate.orderOnline   = orderOnline);
                warrantyPolicy  && (dataUpdate.warrantyPolicy   = warrantyPolicy);
                usageRules      && (dataUpdate.usageRules   = usageRules);
                informationSecurity && (dataUpdate.informationSecurity   = informationSecurity);
                refundPolicy && (dataUpdate.refundPolicy   = refundPolicy);

                if (avatar) {
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        name: avatar.name, 
                        size: avatar.size,
                        path: avatar.pathAvatar,
                        userCreate: userUpdate 
                    });
                    dataUpdate.avatar = infoImageAfterInsert.data._id;
                }

                if (gallery && gallery.length) {
                    let listGallery = gallery.map( image => {
                        return IMAGE_MODEL.insert({ 
                            name: image.name,
                            path: image.pathAvatar,
                            size: image.size, 
                            userCreate: userUpdate 
                        });
                    });
                    let resultPromiseAll = await Promise.all(listGallery);
                    let listGalleryID    = resultPromiseAll.map( gallery => gallery.data._id );

                    dataUpdate.$addToSet = {
                        gallery: listGalleryID
                    };
                }

                let infoUpdate = await INFO_COMPANY_COLL.findByIdAndUpdate(companyId, {
                    ...dataUpdate
                }, {
                    new: true
                });

                if (galleryDelete && galleryDelete.length) {
                    let infoAfterDeleteImage = await INFO_COMPANY_COLL.findByIdAndUpdate(companyId, {
                        $pull: {
                            gallery: { $in: [...galleryDelete.map(image => ObjectID(image))] }
                        }
                    }, {
                        new: true
                    });
                }
                if(!infoUpdate)
                    return resolve({ error: true, message: 'update_failed' });

                return resolve({ error: false, data: infoUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({  }) {
        return new Promise(async resolve => {
            try {
                
                let listData = await INFO_COMPANY_COLL.find({  })
                    .populate('avatar gallery')
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_List_category' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
