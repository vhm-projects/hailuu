const INFO_COMPANY_MODEL    = require('./models/info_company').MODEL;
const INFO_COMPANY_COLL     = require('./databases/info_company-coll');
const INFO_COMPANY_ROUTES   = require('./apis/info_company');

module.exports = {
    INFO_COMPANY_ROUTES,
    INFO_COMPANY_COLL,
    INFO_COMPANY_MODEL,
}