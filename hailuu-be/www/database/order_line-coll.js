"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION ORDER-LINE
 */
module.exports = BASE_COLL("order_line", {
    /**
     * SẢN PHẨM
     */
    product: {
        type:  Schema.Types.ObjectId,
        ref: 'product'
    },
	productProperty: {
		type: Schema.Types.ObjectId,
		ref: 'product_variant_value'
	},
	// Ghi chú
    note: {
        type: String,
        default: ''
    },
	// Tổng tiền
    amount: {
		type: Number,
        default: 0
    },

	// Tệp tin in ấn
	files: [{
		type: Schema.Types.ObjectId,
		ref: 'image',
		default: []
	}],
	/**
     * 1: Hoạt động
     * -1: Đã xoá
     */
	status: {
        type: Number,
        default: 1
    },
    //_________Người tạo
    userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'customer'
    },
    //_________Người cập nhật
    userUpdate: {
        type: Schema.Types.ObjectId,
        ref: 'customer'
    }
});
