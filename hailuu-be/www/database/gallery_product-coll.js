"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION ĐỘI NGŨ NHÂN SỰ
 */
module.exports  = BASE_COLL("gallery_product", {

    /**
     * HỌ TÊN 
     */
    name: {
        type: String,
    }, 
    // Link 
    url: {
        type: String,
    }, 

    // Mô tả
    description: {
        type: String,
        require: true
    },

    //Sắp xếp
    order: {
        type: Number,
    },

    // Hình ảnh nhân sự
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    //_________Người tạo
    products: {
        type:  Schema.Types.ObjectId,
        ref : 'product'
    },

    /**
     * 1: Hoạt động
     * 0: Khóa
     */
    status: {
        type: Number,
        default: 1
    },
  
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },

    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});