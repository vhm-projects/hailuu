"use strict";

const Schema     = require('mongoose').Schema;
const BASE_COLl  = require('../database/intalize/base-coll');

/**
 * COLLECTION GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = BASE_COLl('transaction', {
	transactionID: {
		type: String,
		unique: true,
		required: true,
	},
	// Nội dung thanh toán
	orderInfo: {
		type: String,
		required: true,
		trim: true
	},
	// Số tiền thanh toán
	amount: {
		type: Number,
		required: true,
	},
	bankCode: String, // (tuỳ chọn)
	ipAddr: {
		type: String,
		default: ''
	},
	order: {
		type: Schema.Types.ObjectId,
		ref: 'order'
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer'
	},
	// Nội dung trạng thái giao dịch
	message: {
		type: String,
		default: '',
		trim: true
	},
	/**
	 * 0. chưa thanh toán
	 * 1. đã thanh toán -> //* thành công
	 * 2. đã thanh toán -> //* thất bại -> những statuscode từ momo
	 */
	status: {
		type: Number,
		default: 0
	}
});
