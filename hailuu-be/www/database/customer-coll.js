"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION ACCOUNT
 */
module.exports  = BASE_COLL("customer", {

    fullname: {
        type: String,
    },

    email: {
        type: String,
    },

    /**
     * Giới tính
     * 0: Nữ
     * 1: Nam
     */
    gender:{
        type: Number,
    },

    birthday:{
        type: Date
    },

    address : [{
        type: Schema.Types.ObjectId,
        ref: 'customer_address'
    }],

    phone: {
        type: String,
    },

    password: {
        type: String,
    },
    /**
     * 
     */
    addressDefault: {
        type: Schema.Types.ObjectId,
        ref: 'customer_address'
    },
    /**
     * Nhận thông báo
     * 1: Hoạt động
     * 0: Không hoạt động
     * -1: Tạm xóa
     */
    status: {
        type: Number,
        default: 1
    }
 
});