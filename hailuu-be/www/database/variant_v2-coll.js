"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION VARIANT-V2 LÀ COLLECTION VARIANT_PRODUCT THEO VERSION MỚI
 */

module.exports  = BASE_COLL("variant_v2", {
    product: {
        type:  Schema.Types.ObjectId,
        ref : 'product'
    },
    variantOptions: [{
        type:  Schema.Types.ObjectId,
        ref : 'variant_option'
    }],

    //Khối lượng
    weigth: Number,

    // Giá sản phẩm trước giảm giá
	priceBefore: {
		type: Number,
	},

    // Giá sản phẩm sau giảm giá
	priceAfter: {
		type: Number,
	},

    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Khoá
     */
    status: {
        type: Number,
        default: 1
    },

    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});