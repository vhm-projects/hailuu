"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION QUY CÁCH IN CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("printing_specification", {

    //Tên
    name: String,

    //Mô tả
    description: String,

    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     */
    status: {
        type: Number,
        default: 1
    },

    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});