"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION ACCOUNT
 */
module.exports  = BASE_COLL("account", {
    /**
     * SẢN PHẨM
     */
    product: {
        type: String,
    }, 

    /**
     * Tên
     */

    // firstName: {
    //     type: String,
    // }, 

    // lastName: {
    //     type: String,
    // }, 
    name: {
        type: String,
    },

    email: {
        type: String,
    },

    company: {
        type: String,
    },

    vatNumber: {
        type: String,
    },
    /**
     * Giới tính
     * 0: Nữ
     * 1: Nam
     */
    gender:{
        type: Number,
    },

    birthday:{
        type: Date
    },

    address: {
        type: String,
    },

    ward: {
        type: String
    },

    district: {
        type: String,
    },

    province: {
        type: String
    },

    phone: {
        type: String,
    },

    password: {
        type: String,
    },
    /**
     * Nhận thông báo
     * 1: Nhận
     * 0: Không nhận
     */
    newSletter: {
        type: Number,
        default: 0
    },

	/**
     * Trạng thái
     * 0: Không hoạt động
     * 1: Hoạt động
     */
	 status: {
        type: Number,
        default: 1
    }
 
});