"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION ORDER
 */
module.exports  = BASE_COLL("order", {
	orderID: {
		type: String,
		required: true,
		unique: true
	},
	orderLine: [{
        type:  Schema.Types.ObjectId,
        ref: 'order_line'
    }],
	// Khách hàng
	customer: {
		type:  Schema.Types.ObjectId,
		ref: 'customer'
	},
	// Ghi chú
    note: {
        type: String,
        default: ''
    },
	// Khối lượng
    weight: {
        type: Number,
        default: 0,
    },
    // Tổng tiền
    amount: {
        type: Number,
        default: 0,
    },
	// Phí giao hàng
	deliveryPrice: {
		type: Number,
        default: 0,
	},
	// Ngày giao hàng dự tính
	deliveryDate: {
		type: Date,
	},
	/**
	 * Phương thức thanh toán:
	 * 0: Thanh toán khi nhận hàng
	 * 1: Chuyển khoản
	 * 2: Ví điện tử (momo)
	 */
	payment: {
		type: Number,
		default: 0
	},
	// Địa chỉ giao hàng
	address: {
		type: Schema.Types.ObjectId,
		ref: 'customer_address',
	},
	/**
	 * Xuất hoá đơn VAT nếu có
	 * 0: Không xuất hoá đơn
	 * 1: Có xuất hoá đơn
	 */
	exportVAT: {
		type: Number,
		default: 0
	},
	/**
	 * 0: Chưa thanh toán
     * 1: Đã thanh toán khi nhận hàng
     * 2: Đã thanh toán qua chuyển khoản
     * 3: Đã thanh toán qua MoMo
     */
	statusPayment: {
        type: Number,
        default: 0
    },
	/**
	 * 0: Đã tiếp nhận
     * 1: Đang phê duyệt
     * 2: Đang in ấn
     * 3: Đóng gói và vận chuyển
     * 4: Hoàn thành giao hàng
     * 5: Đã huỷ
     */
	status: {
        type: Number,
        default: 0
    },
    //_________Người tạo
    userCreate: {
        type: Schema.Types.ObjectId,
        ref : 'customer'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref: 'customer'
    }
});
