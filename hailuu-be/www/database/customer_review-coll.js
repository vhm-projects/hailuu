"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION PHẢN HỒI CỦA KHÁCH HÀNG
 */
module.exports  = BASE_COLL("customer_review", {
    /**
     * HỌ TÊN NGƯỜI REVIEW
     */
    fullname: {
        type: String,
    }, 

    // Hình ảnh khách hàng 
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    // Vị trí của khách hàng
    position: {
        type: String,
        require: true
    },

    // Vị trí của khách hàng
    company: {
        type: String,
        require: true
    },

    // Nội dung phản hồi
    content: {
        type: String,
        require: true
    },
    /**
     * 1: Hoạt động
     * 0: Khóa
     */
    status: {
        type: Number,
        default: 1
    },
  
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});