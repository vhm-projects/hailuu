"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION MENU CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("user_contact", {

    /**
     * Tên liên hệ
     */
    fullname: {
        type: String,
        require: true
    }, 

    email: {
        type: String,
    },

    phone: {
        type: String,
    },

    /**
     * 0: Chưa xem
     * 1: Đã xem
     */
    status: {
        type: Number,
        default: 0
    },

    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});