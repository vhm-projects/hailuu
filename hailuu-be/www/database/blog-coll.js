"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION BÀI VIẾT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("blog", {
    // Tiêu đề bài viết
    title: {
        type: String,
        unique: true,
        require: true
    },

    // Danh mục
    category: {
        type:  Schema.Types.ObjectId,
        ref : 'category'
    },

    slug: {
        type: String, 
        unique: true,
        require: true
    },

    // Mô tả ngắn
    description: {
        type: String,
        default: ''
    },

    // Nội dung
    content: {
        type: String,
    },

    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     */
    status: {
        type: Number,
        default: 1
    },

    // Hình ảnh bài viết
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    // Comment bài viết
    comments: [{
        type:  Schema.Types.ObjectId,
        ref : 'comment'
    }],

    // Chứa những từ khóa seo
    seo: [
        { type: String }
    ],

    /**
     * 0: Bài viết
     * 1: Xu hướng hiện nay
     * 2: Nghệ thuật phối màu
     * 3: Kinh nghiệm in ấn
     */
    type: {
        type: Number,
        default: 1
    },

    // Lượt xem
    views: {
        type: Number,
        default: 0
    },
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
});