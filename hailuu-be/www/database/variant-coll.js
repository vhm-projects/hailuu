"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION VARIANT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("variant", {
     /**
     * Sản Phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    /**
     * Tên Biến Thể: Màu Sắc
     */
    name: {
        type: String,
        required: true,
        trim: true,
    },
    /**
     * Trạng Thái 
     * 1: Đang Hoạt Động,
     * 2: Tắt Hoạt Động
     */
    status: {
        type: String,
        default: 1,
        enum: [1, 2],
    },
    /**
     * Loại hiển thị
     * 1: Checkbox
     * 2: Combobox
     */
    typeShow: {
        type: Number,
        default: 2
    },
});