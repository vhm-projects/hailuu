"use strict";
const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../database/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL("code_reset_password", {
		email: {
			type: String,
			trim: true,
		},
		code: {
			type: String,
			unique : true
		},
        /**
         * Vô hiệu hóa sau 15p
         */
		timeLife: {
			type: Date,
		},
});
