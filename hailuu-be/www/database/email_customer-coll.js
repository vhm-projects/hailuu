"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION EMAIL KHÁCH HÀNG CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("email_customer", {
    email: {
        type: String,
        unique: true,
        require: true
    }
});