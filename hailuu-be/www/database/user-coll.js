"use strict";

const BASE_COLL = require('./intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("user", {
    username: {
        type: String,
        trim: true,
        unique : true
    }, 
    email: {
        type: String,
        trim: true,
        unique : true
    },
    //_______Mật khẩu
    password: String,
    /**
     * Trạng thái hoạt động.
     * 1. Hoạt động
     * 0. Khóa
     */
    status: {
        type: Number,
        default: 1
    },
    /**
     * 0: NGƯỜI SOẠN THẢO
     * 1: ADMIN
     */
    level: {
        type: Number,
        default: 1
    },
});
