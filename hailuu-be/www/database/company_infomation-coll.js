"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION SLIDE CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("company_infomation", {
    /**
     * Mô tả ngắn
     */
    sortDescription: {
        type: String,
    }, 

    /**
     * Mô tả dài
     */
    longDescription: {
        type: String,
    }, 

    // Hình ảnh công ty
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },
    // Số điện thoại công ty
    phone: {
        type: String,
    },
    // Email công ty
    email: {
        type: String,
    },

    // Địa chỉ công ty
    address: {
        type: String,
    },

    // Tên công ty
    name: {
        type: String,
    },

    slogan: {
        type: String,
    },
    
    // Zalo công ty
    zalo: {
        type: String,
    },
    // Facebook công ty
    facebook: {
        type: String,
    },
    // Youtube công ty
    youtube: {
        type: String,
    },
    // Twitter công ty
    twitter: {
        type: String,
    },

    businessImages: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },
    
    linkGoogleMap: {
        type: String,
    },
    
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});