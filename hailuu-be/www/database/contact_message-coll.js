"use strict";
const BASE_COLL = require('./intalize/base-coll');

/**
 * COLLECTION TIN NHĂN LIÊN HỆ CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("contact_message", {
    
    fullname: {
        type: String,
    },

    nickname: {
        type: String,
    },

    email: {
        type: String,
    },

    content: {
        type: String,
    },
    /**
     * 0: Chưa Xem
     * 1: Đã Xem
     */
    status: {
        type: Number,
        default: 0
    }
   
});