"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION VARIANT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("product_variant_value", {
    variant_values: [{
        type: Schema.Types.ObjectId,
        ref: 'variant_value',
    }],
    /**
     * Sản Phẩm
     */
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    // Giá sau khi giảm
    price: {
        type: Number,
    },
    // Giá gốc
    price_at_compare: {
        type: Number,
    },
    // Cân nặng
    weight: {
        type: Number,
        default: 0
    },
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
});