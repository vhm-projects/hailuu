"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION PRODUCT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("product", {
    // Tên MENU
    menu: {
        type: Schema.Types.ObjectId,
        ref: 'menu'
    },

	// Tên sản phẩm
    name: {
        type: String,
        // unique: true,
        required: true
    },

	// Thông tin cơ bản
	infoBasic: {
		type: String,
		default: ''
	},

    basicParameters: {
		type: Object,
    },

    //Màu sắc
    color: String,

    //Kỹ thuật in
    printingTechnique: String,

    //Kỹ thuật thành phẩm
    productEngineering: String,
    
    // Danh sách variants option
    // variantOption: [{
    //     type:  Schema.Types.ObjectId,
    //     ref : 'variant_option'
    // }],

    // Danh sách variants
    variants: [{
        type:  Schema.Types.ObjectId,
        ref : 'variant'
    }],

    //Giới thiệu sản phẩm
    introProduct: String,

    //Lưu ý khi đặt hàng
    noteOrder: String,

    //Bảng giá
    priceList: String,

    //Mảng file
    arrayFile: [{
        type:  Schema.Types.ObjectId,
        ref : 'image'
    }],

    // file Excel bảng giá
    fileExcel: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    // file ILLustrator
    fileIllustrator: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    fileVectorEPS: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },
    
    filePDF: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

	// Giá sản phẩm trước giảm giá
	priceBefore: {
		type: Number,
		default: 0
	},

    // Giá sản phẩm sau giảm giá
	priceAfter: {
		type: Number,
		default: 0
	},

    // Tỉ lệ giảm giá
	ratioSale: {
		type: Number,
		default: 0
	},

    // In nhanh
	flashPrint: {
		type: Boolean,
		default: false
	},

    // Giảm giá
	flashSale: {
		type: Boolean,
		default: false
	},

	// Giảm giá
	discount: {
		type: Number,
		default: 0
	},

    slug: {
        type: String,
        unique: true,
        require: true
    },

	description: {
        type: String,
		default: ''
    },

    content: {
        type: String,
		default: ''
    },

    // Ảnh chính sản phẩm
	avatar: {
		type: Schema.Types.ObjectId,
		ref: 'image'
	},

	// Hình ảnh sản phẩm
	images: [{
		type: Schema.Types.ObjectId,
		ref: 'image'
	}],

	// Đánh giá sản phẩm
    comments: [{
        type:  Schema.Types.ObjectId,
        ref : 'comment'
    }],

	/** Trạng thái sản phẩm
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Khoá
     */
    status: {
        type: Number,
        default: 1
    },

    //_________Người tạo
    userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },

    //_________Người cập nhật
    userUpdate: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
});
