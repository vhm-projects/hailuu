"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION PHẢN HỒI CỦA KHÁCH HÀNG
 */
module.exports  = BASE_COLL("keyword", {
    customer: {
        type:  Schema.Types.ObjectId,
        ref : 'customer'
    },
    name: {
        type: String,
    }, 
  
    /**
     * 1: Hoạt động
     * -1: Xóa Tạm
     */
    status: {
        type: Number,
        default: 1
    },
  
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});