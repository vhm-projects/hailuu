"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION BẢNG GIÁ VẬN CHUYỂN
 */
module.exports  = BASE_COLL("price_ship", {
    name: {
        type: String,
        require: true
    }, 
    price: {
        type: Number,
        require: true
    },
    from: {
        type: Number,
		default: 0
    },
	to: {
        type: Number,
		default: 0
    },
    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Xoá
     */
    status: {
        type: Number,
        default: 1
    },
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});
