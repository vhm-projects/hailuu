"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("slide", {
   /**
     * Hình ảnh slide
     */
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    }, 

    title: {
        type: String,
    },

    description: {
        type: String,
    },
    // Khi bấm nút xem thêm
    link: {
        type: String,
    },
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    /**
     * 1: Hiển thị
     * 0: Không hiển thị
     * -1 Tạm xóa
     */
    status : {
        type: Number,
        default: 1
    },

    //Sắp xếp
    order: {
        type: Number,
    },
});
