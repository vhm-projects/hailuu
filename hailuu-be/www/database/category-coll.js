"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION DANH MỤC BÀI VIẾT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("category", {
    /**
     * Tên danh mục
     */
    name: {
        type: String,
        unique: true,
        require: true
    }, 

    slug: {
        type: String,
        unique: true,
        require: true
    },

    description: {
        type: String,
    },
    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Xoá
     */
    status: {
        type: Number,
        default: 1
    },

    //Sắp xếp
    order: {
        type: Number,
    },
    
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});