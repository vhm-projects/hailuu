"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION COMMENT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("comment", {
    /**
     * Tên
     */
    fullname: {
        type: String,
    },

    email: {
        type: String,
    },

    content: {
        type: String,
    },

    product: {
        type:  Schema.Types.ObjectId,
        ref : 'product'
    },

    blog: {
        type:  Schema.Types.ObjectId,
        ref : 'blog'
    },

    /**
     * số sao
     */
    star: {
		type: Number,
		default: 0
	},

    /**
     * 0: Không duyệt
     * 1: Đã duyệt
     */
    status: {
        type: Number,
        default: 1
    },
   
});