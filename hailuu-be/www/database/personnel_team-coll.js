"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION ĐỘI NGŨ NHÂN SỰ
 */
module.exports  = BASE_COLL("personnel_team", {

    /**
     * HỌ TÊN 
     */
    fullname: {
        type: String,
    },

    //Dành cho nhân viên tư vấn*
    email: String,

    //Dành cho nhân viên tư vấn*
    phone: String,

    /**
     * 1: Đội ngũ nhân sự
     * 2: Nhân viên tư vấn
     */
    type: {
        type: Number,
        default: 1
    },

    // Hình ảnh
    avatar: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    // Chức vụ
    position: {
        type: String,
        require: true
    },

    // Mô tả
    description: {
        type: String,
        require: true
    },

    /**
     * 1: Hoạt động
     * 0: Khóa
     */
    status: {
        type: Number,
        default: 1
    },
  
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },

    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});