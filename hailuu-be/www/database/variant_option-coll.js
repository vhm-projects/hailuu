"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION VARIANT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("variant_option", {
    name: String,
    parent:  {
        type:  Schema.Types.ObjectId,
        ref : 'variant_option'
    },

    childs: [
        {
            type:  Schema.Types.ObjectId,
            ref : 'variant_option'
        }
    ],

    level: {
        type: Number,
        default: 1
    },
   
    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Tạm xóa
     */
    status: {
        type: Number,
        default: 1
    },

    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});