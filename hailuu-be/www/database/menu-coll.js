"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;
/**
 * COLLECTION MENU CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("menu", {
    /**
     * Tên MENU
     */
    name: {
        type: String,
        unique: true,
    }, 

    slug: {
        type: String,
        unique: true,
        require: true
    },

    description: {
        type: String,
    },
    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     * -1: Tạm xóa
     */
    status: {
        type: Number,
        default: 1
    },

    // ĐỆ QUY
    parent: {
        type:  Schema.Types.ObjectId,
        ref : 'menu'
    },
    childs: [{
        type:  Schema.Types.ObjectId,
        ref : 'menu'
    }],

    /**
     * 1: Cha
     * 2: Con
    */
    level: {
        type: Number,
        default: 1
    },

    //Sắp xếp trên trang chủ
    order: {
        type: Number,
    },
    // Hình ảnh
    image: {
        type:  Schema.Types.ObjectId,
        ref : 'image'
    },

    products: [{
        type:  Schema.Types.ObjectId,
        ref : 'product'
    }],
    
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});