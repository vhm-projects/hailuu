"use strict";
const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION IMAGE CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("image", {
    /**
     * Tên image
     */
    name: {
        type: String,
    },

    product: {
        type:  Schema.Types.ObjectId,
        ref : 'product'
    },

    blog: {
        type:  Schema.Types.ObjectId,
        ref : 'blog'
    },

    path: String,
    size: String,
	type: String,

    /**
     * 1: ảnh chính
     * 0: ảnh phụ
     */
    main: {
        type: Number,
        default: 0
    },
   
    /**
     * 0: Không hoạt động
     * 1: Hoạt động
     */
    status: {
        type: Number,
        default: 1
    },
  
    //_________Người tạo
    userCreate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    },
    //_________Người cập nhật
    userUpdate: {
        type:  Schema.Types.ObjectId,
        ref : 'user'
    }
});
