"use strict";

const BASE_COLL = require('./intalize/base-coll');
const Schema    = require('mongoose').Schema;

/**
 * COLLECTION VARIANT CỦA HỆ THỐNG
 */
module.exports  = BASE_COLL("variant_value", {
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product',
    },
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'variant_value',
    },
    /**
     * Loại Biến Thể
     */
    variant: {
        type: Schema.Types.ObjectId,
        ref: 'variant',
    },
    /**
     * Tên giá trị biến thể: Đỏ, XXL, S, M, ...
     */
    name: {
        type: String,
        required: true,
        trim: true,
    },
    status: {
        type: Number,
        default: 1,
        enum: [1, 2],
    },
   
});