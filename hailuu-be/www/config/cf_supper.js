let multer                     = require('multer');
let path                       = require('path');
const { uploadFilter }         = require('../utils/file_utils');
const { md5 }                  = require('../utils/string_utils');
const { MIME_TYPES_IMAGE }     = require('../config/cf_constants');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let { pathFile } = req.query;
        if(pathFile){
            // Phục vụ việc upload ảnh
            let outputPath = path.resolve(__dirname, `../../files/${pathFile}`);
            cb(null, outputPath)
        }else{
            let outputPath = path.resolve(__dirname, `../../files/`); //cấu hình đường dẫn cho từng loại, từng app
            cb(null, outputPath)
        }
    },
    
    filename: function (req, file, cb) {
        // console.log(path.extname(file.originalname).toLowerCase())
        // Trả về tên file
        let fileName = '';

        if(MIME_TYPES_IMAGE.includes(file.mimetype)){
            let newFileName = md5(`${file.originalname}_${Date.now()}`);
            fileName = `${newFileName}${path.extname(file.originalname)}`;

            cb(null, fileName);
        }else{
            // Bỏ khoảng cách trong tên (đã map với thư viện upload nén ảnh)
            fileName = `${file.originalname}`;
            cb(null, fileName);
        }

        req.fileName = fileName;
    }
})

let upload = multer({ storage: storage });
exports.upload = upload;
