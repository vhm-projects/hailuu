"use strict";

module.exports = {
    supportMail: "no-reply@hailuu.com",

    email: process.env.MAILER_SENDER || '...',
    pass:  process.env.MAILER_PWD    || '...',
    siteName: 'https://hailuu.vn/',
    password: process.env.MAILER_PWD || '...',

    host: process.env.MAILER_HOST || 'smtp.gmail.com',
    port: process.env.MAILER_PORT || '587',
    service: 'Gmail',
};
