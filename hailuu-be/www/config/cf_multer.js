let { upload }   = require('./cf_supper');
let uploadSingle = upload.single('file');
let uploadArray  = upload.array('files', 15); // Số lượng file được phép upload/1 lần

let uploadFields = upload.fields([
    { name: 'files' , maxCount: 10 },
    { name: 'images', maxCount: 10 },
    { name: 'audios', maxCount: 10 },
    { name: 'avatar', maxCount: 10 },
    { name: 'gallery', maxCount: 10 },
]);

let uploadFieldMainAndBusiness = upload.fields([
    { name: 'isMainImage', maxCount: 1 }, 
    { name: 'businessImages', maxCount: 1 }
]);

module.exports = {
    uploadSingle,
    uploadArray,
    uploadFields,
    uploadFieldMainAndBusiness
}