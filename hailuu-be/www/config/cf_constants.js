// KIỂU TÀI KHOẢN
exports.ADMIN_LEVEL = [
    { value: 0, text: 'Editor' },
    { value: 1, text: 'Admin' },
]

// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

// TRẠNG THÁI PRODUCT
exports.PRODUCT_STATUS = [
    { value: 1, text: 'Hoạt động' },
    { value: 0, text: 'Không hoạt động' },
    { value: -1, text: 'Khoá' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml' ];

// TRẠNG THÁI ĐƠN HÀNG
exports.ORDER_STATUS = [
    { value: 0, text: 'Đã tiếp nhận', color: 'warning' },
    { value: 1, text: 'Đang phê duyệt', color: 'warning' },
    { value: 2, text: 'Đang in ấn', color: 'warning' },
    { value: 3, text: 'Đóng gói và vận chuyển', color: 'warning' },
    { value: 4, text: 'Hoàn thành giao hàng', color: 'success' },
    { value: 5, text: 'Đã huỷ', color: 'danger' },
]

// TRẠNG THÁI THANH TOÁN
exports.PAYMENT_STATUS = [
    { value: 0, text: 'Chưa thanh toán', color: 'danger' },
    { value: 1, text: 'Đã thanh toán khi nhận hàng', color: 'success' },
    { value: 2, text: 'Đã thanh toán qua chuyển khoản', color: 'success' },
    { value: 3, text: 'Đã thanh toán qua MoMo', color: 'success' },
]

// HÌNH THỨC THANH TOÁN
exports.PAYMENT_TYPE = [
    { value: 0, text: 'Thanh toán khi nhận hàng' },
    { value: 1, text: 'Thanh toán qua chuyển khoản' },
    { value: 2, text: 'Thanh toán qua momo' },
]

// TỪ KHÓA HAY DÙNG
exports.KEY_WORD = [
    { key: 'Thiết kế bao bì' },
    { key: 'In ấn logo gỗ' },
    { key: 'In ấn bìa sách' },
    { key: 'In ấn nhãn máy cafe' },
    { key: 'In tờ rơt' },
]
