"use strict";

const jwt 			= require('jsonwebtoken');
const cfJwt 		= require('./cf_jws');
const USER_SESSION 	= require('../session/user-session');

const ADMIN_ACCESS 		= [0,1];
const USER_ACCESS  		= [1];
const CUSTOMER_ACCESS  	= [0,1,3];
const CUSTOMER_COLL     = require("../database/customer-coll");

module.exports = {
    role: {
        all: {
            bin: 1,
            auth: (req, res, next) =>{
                /**
                 * CHECK LOGGED
                 */
                let isExistLogin 	= USER_SESSION.getCustomer(req.session);
                let infoUser 		= null;
                if(isExistLogin){
                    let token = isExistLogin.token || '';

                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if(err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        }
                        if(CUSTOMER_ACCESS.includes(+decoded.level)){
                            infoUser = decoded;
                            req.user = infoUser;
                        }
                    });
                }
                next()
            }
        },
        user: {
            bin: 2,
            auth: function (req, res, next) {
                var token = req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;
                
                if (token) {
                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if (err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            if (USER_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return res.json({success: false, message: 'Error: Permission denied.'});
                            }
                        }
                    });
                } else {
                    return res.redirect("/admin/logout");
                    // return res.status(403).send({
                    //     success: false,
                    //     message: 'No token provided.'
                    // });
                }
            }
        },
        admin: {
            bin: 3,
            auth: function (req, res, next) {
                let session = USER_SESSION.getUser(req.session);
				let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;

                if (token) {
                    console.log({ token });
                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if (err) {
                            return res.redirect('/admin/logout')
                            // return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            if (ADMIN_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return res.redirect('/admin/logout')
                                // return res.json({success: false, message: 'Error: Permission denied.'});
                            }
                        }
                    });
                } else {
                    return res.redirect("/admin/logout");
                }
            }
        },
		customer: {
            bin: 4,
            auth: function (req, res, next) {
                let session = USER_SESSION.getCustomer(req.session);
				let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;

                if (token){
                    jwt.verify(token, cfJwt.secret, async function (err, decoded) {
                        if (err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            let customerID = decoded._id;
                            let infoCustomer = await CUSTOMER_COLL.findById(customerID);
                            if(infoCustomer.status != 1){
                                return res.redirect("/customer/sign-out");
                            }
                            if (CUSTOMER_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return res.json({success: false, message: 'Error: Permission denied.'});
                            }
                        }
                    });
                } else{
                    return res.redirect("/customer/sign-out");
                    // return res.status(403).send({
                    //     success: false,
                    //     message: 'No token provided.'
                    // });
                }
            }
        },
    },

    authorization: function (req, res, next) {
        var hasRole = false;
        var currentRole = null;

        for (var itemRole in this.role) {
            if (!hasRole) {
                if (res.bindingRole.config.auth.includes(this.role[ itemRole ].bin)) {
                    hasRole = true;
                    currentRole = this.role[ itemRole ];
                }
            }
        }
        currentRole.auth(req, res, next);
    }
};