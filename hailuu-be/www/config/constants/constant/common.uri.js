const BASE_ROUTE = '/';

const CF_ROUTINGS_COMMON = {
    HOME: `/home`,
    LOGIN: `/login`,
    LOGOUT: `/logout`,
    LIST_DISTRICTS: `/list-districts/:province`,
    LIST_WARDS: `/list-wards/:district`,
    CHOOSE_AGENCY: `/choose-agency`,
    GENERATE_LINK_S3: `/generate-link-s3`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_COMMON = CF_ROUTINGS_COMMON;