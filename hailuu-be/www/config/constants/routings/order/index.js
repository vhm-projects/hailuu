const BASE_ROUTE_ORDER = '/order';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_ORDER = {
    // ROUTE QUẢN LÝ ORDER
	ADD_ORDER: `${BASE_ROUTE_ORDER}/add-order`,
	UPDATE_ORDER: `${BASE_ROUTE_ORDER}/update-order`,

	INFO_ORDER: `${BASE_ROUTE_ORDER}/info-order`,
	LIST_ORDER: `${BASE_ROUTE_ADMIN}/list-order`,
	DELETE_ORDER: `${BASE_ROUTE_ORDER}/delete-order`,
	REPORT_ORDER: `${BASE_ROUTE_ORDER}/report-order`,

	// ROUTE QUẢN LÝ ORDER-LINE
	ADD_TO_CART: `${BASE_ROUTE_ORDER}/add-to-cart`,
	UPDATE_ORDER_LINE: `${BASE_ROUTE_ORDER}/update-order-line`,
	PUSH_FILE_TO_ORDER_LINE: `${BASE_ROUTE_ORDER}/push-file-to-order-line`,
	UPDATE_FILE_TO_ORDER_LINE: `${BASE_ROUTE_ORDER}/update-file-to-order-line`,
	DELETE_FILE_IN_ORDER_LINE: `${BASE_ROUTE_ORDER}/delete-file-in-order-line`,
	API_DELETE_FILE_IN_ORDER_LINE: `/api${BASE_ROUTE_ORDER}/delete-file-in-order-line`,

	INFO_ORDER_LINE: `${BASE_ROUTE_ORDER}/info-order-line`,
	LIST_ORDER_LINE: `${BASE_ROUTE_ORDER}/list-order-line`,
	DELETE_ORDER_LINE: `${BASE_ROUTE_ORDER}/delete-order-line`,

	// ROUTE QUẢN LÝ BOARD PRICE - SHIP
	ADD_PRICE_SHIP: `${BASE_ROUTE_ADMIN}/add-price-ship`,
	UPDATE_PRICE_SHIP: `${BASE_ROUTE_ADMIN}/update-price-ship`,
	DELETE_PRICE_SHIP: `${BASE_ROUTE_ADMIN}/delete-price-ship`,
	INFO_PRICE_SHIP: `${BASE_ROUTE_ADMIN}/info-price-ship`,
	LIST_PRICE_SHIP: `${BASE_ROUTE_ADMIN}/list-price-ship`,

	// BASE ROUTE
    ORIGIN_APP: BASE_ROUTE_ORDER
}

exports.CF_ROUTINGS_ORDER = CF_ROUTINGS_ORDER;