const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_ACCOUNT = {
    // ACCOUNT
    ADD_ACCOUNT: `${BASE_ROUTE_ADMIN}/add-account`,
    DELETE_ACCOUNT: `${BASE_ROUTE_ADMIN}/delete-account/:userID`,
    UPDATE_ACCOUNT: `${BASE_ROUTE_ADMIN}/update-account`,
    UPDATE_ACCOUNT_USER: `${BASE_ROUTE_ADMIN}/update-account/:userID`,
    INFO_ACCOUNT: `${BASE_ROUTE_ADMIN}/info-account`,
    LIST_ACCOUNT: `${BASE_ROUTE_ADMIN}/list-account`,

    //COMPANY_INFORMATION
    ADD_COMPANY_INFOMATION: `${BASE_ROUTE_ADMIN}s/info-company`,
    UPDATE_COMPANY_INFOMATION: `${BASE_ROUTE_ADMIN}s/info-company/:companyInfoID`,
    LIST_COMPANY_INFOMATION: `${BASE_ROUTE_ADMIN}s/info-companys`,
    DELETE_IMAGE_COMPANY_INFOMATION: `${BASE_ROUTE_ADMIN}s/info-company/image/delete`,
    
    //ACCOUNT USER
    ADD_ACCOUNT_USER: `${BASE_ROUTE_ADMIN}/account`,
    UPDATE_NEWSLETTE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/newsletter`,
    UPDATE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/update/:accountID`,
    DELETE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/delete/:accountID`,
    LIST_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/accounts`,
    LIST_ACCOUNT_CREATED_MONTH_OF_USER: `${BASE_ROUTE_ADMIN}/accounts/created-month`,
    INFO_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/accounts/info/:accountID`,
    SEARCH_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/search`,
    LIST_REPORTS: `${BASE_ROUTE_ADMIN}/reports`,

	// ADMIN
	ADMIN_HOME: `${BASE_ROUTE_ADMIN}/home`,
	ADMIN_SIGNIN: `${BASE_ROUTE_ADMIN}/sign-in`,
	ADMIN_LOGIN: `${BASE_ROUTE_ADMIN}/login`,
    ADMIN_SIGNOUT: `${BASE_ROUTE_ADMIN}/logout`,
    ADMIN_PROFILE: `${BASE_ROUTE_ADMIN}/profile`,
	ADMIN_LIST: `${BASE_ROUTE_ADMIN}/list-admin`,
	ADMIN_ADD_USER: `${BASE_ROUTE_ADMIN}/add-user`,

    // BLOG
    LIST_POST: `${BASE_ROUTE_ADMIN}/list-post`,
    ADD_POST: `${BASE_ROUTE_ADMIN}/add-post`,
    UPDATE_POST: `${BASE_ROUTE_ADMIN}/update-post`,
    DELETE_POST: `${BASE_ROUTE_ADMIN}/delete-post`,
    INFO_POST: `${BASE_ROUTE_ADMIN}/info-post`,

    // CATEGORY
    LIST_CATEGORY: `${BASE_ROUTE_ADMIN}/list-category`,
    ADD_CATEGORY: `${BASE_ROUTE_ADMIN}/add-category`,
    UPDATE_CATEGORY: `${BASE_ROUTE_ADMIN}/update-category`,
    DELETE_CATEGORY: `${BASE_ROUTE_ADMIN}/delete-category`,
    INFO_CATEGORY: `${BASE_ROUTE_ADMIN}/info-category`,
    UPDATE_ORDER_CATEGORY: `${BASE_ROUTE_ADMIN}/update-order-category`,
    

    // COMMENTS
    LIST_COMMENT: `${BASE_ROUTE_ADMIN}/list-comment`,
    ADD_COMMENT: `${BASE_ROUTE_ADMIN}/add-comment`,
    UPDATE_COMMENT: `${BASE_ROUTE_ADMIN}/update-comment`,
    DELETE_COMMENT: `${BASE_ROUTE_ADMIN}/delete-comment`,
    INFO_COMMENT: `${BASE_ROUTE_ADMIN}/info-comment`,
    VIEW_COMMENT : `${BASE_ROUTE_ADMIN}/view-comment`,

    //MENU
    ADMIN_MANAGE_MENU: `${BASE_ROUTE_ADMIN}/menu`,
    ADMIN_MANAGE_MENU_DETAIL: `${BASE_ROUTE_ADMIN}/menu/:menuID`,

    //PRODUCT
    ADMIN_ADD_PRODUCT: `${BASE_ROUTE_ADMIN}/add-product`,
    ADMIN_INFO_PRODUCT: `${BASE_ROUTE_ADMIN}/info-product/:productID`,
    ADMIN_LIST_PRODUCT: `${BASE_ROUTE_ADMIN}/list-product`,
    ADMIN_UPDATE_PRODUCT: `${BASE_ROUTE_ADMIN}/update-product/:productID`,
    ADMIN_REMOVE_PRODUCT: `${BASE_ROUTE_ADMIN}/remove-product/:productID`,
    LIST_COMMENT_PRODUCT: `${BASE_ROUTE_ADMIN}/list-comment-product`,

    //CUSTOMER
    LIST_REVIEW_CUSTOMER: `${BASE_ROUTE_ADMIN}/list-review-customer`,
    LIST_KEYWORD: `${BASE_ROUTE_ADMIN}/list-keyword`,
    ADD_KEYWORD: `${BASE_ROUTE_ADMIN}/add-keyword`,
    INFO_KEYWORD: `${BASE_ROUTE_ADMIN}/info-keyword/:keywordID`,
    UPDATE_KEYWORD: `${BASE_ROUTE_ADMIN}/update-keyword/:keywordID`,
    REMOVE_KEYWORD: `${BASE_ROUTE_ADMIN}/remove-keyword/:keywordID`,
    ADD_CUSTOMER_REVIEW: `${BASE_ROUTE_ADMIN}/add-review-customer`,
    UPDATE_CUSTOMER_REVIEW: `${BASE_ROUTE_ADMIN}/update-customer-review/:customerReviewID`,
    INFO_CUSTOMER_REVIEW: `${BASE_ROUTE_ADMIN}/info-customer-review/:customerReviewID`,

    // MESSAGE
    LIST_CONTACT_MESSAGE: `${BASE_ROUTE_ADMIN}/list-contact-message`,
    ADD_CONTACT_MESSAGE: `${BASE_ROUTE_ADMIN}/add-contact-message`,
    INFO_CONTACT_MESSAGE: `${BASE_ROUTE_ADMIN}/info-contact-message/:contactMessageID`,
    UPDATE_STATUS_CONTACT_MESSAGE: `${BASE_ROUTE_ADMIN}/update-status-contact-message/:contactMessageID`,

    // EMAIL CUSTOMER
    LIST_EMAIL_CUSTOMER: `${BASE_ROUTE_ADMIN}/list-email-customer`,
    DELETE_EMAIL_CUSTOMER: `${BASE_ROUTE_ADMIN}/delete-email-customer/:emailCustomerID`,


    // UPDATE_VARIANT
    UPDATE_VARIANT: `${BASE_ROUTE_ADMIN}/variant/update-variant`,
    UPDATE_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/update-variant-value`,
    UPDATE_PRODUCT_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/update-product-variant-value`,
    ADD_VARIANT_VALUE_AND_PRODUCT_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/add-variant-value-and-product-variant-value`,
    REMOVE_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/remove-variant-value/:variantValueID`,
    UPDATE_TYPE_SHOW_VARIANT: `${BASE_ROUTE_ADMIN}/variant/update-type-show`,
    LIST_PRODUCT_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/list-product-variant-value`,
    ADD_VARIANT_VALUE_CHILD: `${BASE_ROUTE_ADMIN}/variant/add-variant-value-child`,
    lIST_VARIANT_VALUE_CHILD: `${BASE_ROUTE_ADMIN}/variant/list-variant-value-child/:variantValueID`,
    INFO_PRODUCT_VARIANT_VALUE: `${BASE_ROUTE_ADMIN}/variant/info-product-variant-value`,
    ORIGIN_APP: BASE_ROUTE_ADMIN


}

exports.CF_ROUTINGS_ACCOUNT = CF_ROUTINGS_ACCOUNT;
