const BASE_ROUTE_PRODUCT = '/product';
const BASE_ROUTE_PRODUCT_VI = '/san-pham';

const CF_ROUTINGS_PRODUCT = {
    //PRODUCT => có thể tách ra thành một route riêng
	ADD_PRODUCT: `${BASE_ROUTE_PRODUCT}/add`,
	SEARCH_PRODUCT: `${BASE_ROUTE_PRODUCT}/search/:key`,
	GET_INFO_PRODUCT: `${BASE_ROUTE_PRODUCT}/:productID`,
	GET_LIST_PRODUCT_BY_MENU: `${BASE_ROUTE_PRODUCT}/list/:menuID`,
	REMOVE_PRODUCT: `${BASE_ROUTE_PRODUCT}/remove/:productID`,
	REMOVE_PRODUCT_V2: `${BASE_ROUTE_PRODUCT}/remove-v2/:productID`,
	UPDATE_PRODUCT: `${BASE_ROUTE_PRODUCT}/update/:productID`,
	UPDATE_IMAGE_PRODUCT: `${BASE_ROUTE_PRODUCT}/update-image/:productID`,
	UPDATE_STATUS_PRODUCT: `${BASE_ROUTE_PRODUCT}/update-status/:productID/:status`,
	UPDATE_MAIN_IMAGE_PRODUCT: `${BASE_ROUTE_PRODUCT}/update-main-image/:productID/:imageID`,
	GET_LIST_PRODUCT_BY_MENU_PARENT: `${BASE_ROUTE_PRODUCT}/list-product-by-menu-parent/:menuID`,
	GET_INFO_PRODUCT_CHILD: `${BASE_ROUTE_PRODUCT}/info-product-child`,
	LIST_COMMENT_RATING: `/comment/list-comment-rating`,

	INFO_PRODUCT: `${BASE_ROUTE_PRODUCT_VI}/:slug`,
}

exports.CF_ROUTINGS_PRODUCT = CF_ROUTINGS_PRODUCT;