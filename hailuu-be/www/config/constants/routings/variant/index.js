const BASE_ROUTE_VARIANT = '/variant';

const CF_ROUTINGS_VARIANT = {
	LIST_VARIANT: `${BASE_ROUTE_VARIANT}/list-variant`,
	LIST_VARIANT2: `${BASE_ROUTE_VARIANT}/list-variant-v2`,

	
	ADD_VARIANT: `${BASE_ROUTE_VARIANT}/add`,
	SEARCH_VARIANT: `${BASE_ROUTE_VARIANT}/search/:key`,
	GET_INFO_VARIANT: `${BASE_ROUTE_VARIANT}/:variantID`,
	UPDATE_VARIANT: `${BASE_ROUTE_VARIANT}/update-variant/:variantID`,
	REMOVE_VARIANT: `${BASE_ROUTE_VARIANT}/remove/:variantID`,

    //FACE_NUMBER
    ADD_FACE_NUMBER: `${BASE_ROUTE_VARIANT}/add-face-number`,
    UPDATE_FACE_NUMBER: `${BASE_ROUTE_VARIANT}/update-face-number`,
    GET_INFO_FACE_NUMBER: `${BASE_ROUTE_VARIANT}/info-face-number/:faceNumberID`,
    REMOVE_FACE_NUMBER: `${BASE_ROUTE_VARIANT}/remove-face-number/:faceNumberID`,

	//PRINT_SPECIFICATION
    ADD_PRINT_SPECIFICATION: `${BASE_ROUTE_VARIANT}/add-print-specification`,
    UPDATE_PRINT_SPECIFICATION: `${BASE_ROUTE_VARIANT}/update-print-specification`,
    GET_INFO_PRINT_SPECIFICATION: `${BASE_ROUTE_VARIANT}/info-print-specification/:printSpecificationID`,
    REMOVE_PRINT_SPECIFICATION: `${BASE_ROUTE_VARIANT}/remove-print-specification/:printSpecificationID`,

    //FACE_NUMBER
    ADD_AMOUNT: `${BASE_ROUTE_VARIANT}/add-amount`,
    UPDATE_AMOUNT: `${BASE_ROUTE_VARIANT}/update-amount`,
    GET_INFO_AMOUNT: `${BASE_ROUTE_VARIANT}/info-amount/:amountID`,
    REMOVE_AMOUNT: `${BASE_ROUTE_VARIANT}/remove-amount/:amountID`,
}

exports.CF_ROUTINGS_VARIANT = CF_ROUTINGS_VARIANT;