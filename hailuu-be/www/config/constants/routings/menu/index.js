const BASE_ROUTE_MENU = '/menu';

const CF_ROUTINGS_MENU = {
	
    //MENU => có thể tách ra thành một route riêng
	ADD_MENU: `${BASE_ROUTE_MENU}/add`,
	SEARCH_MENU: `${BASE_ROUTE_MENU}/search/:key`,
	GET_INFO: `${BASE_ROUTE_MENU}/:menuID`,
	REMOVE_MENU: `${BASE_ROUTE_MENU}/remove/:menuID`,
	GET_LIST_MENU_CHILD_OF_PARENT: `${BASE_ROUTE_MENU}/get-list-menu-child-of-parent/:menuID`,
	UPDATE_MENU: `${BASE_ROUTE_MENU}/update/:menuID`,
	UPDATE_STATUS_MENU: `${BASE_ROUTE_MENU}/update-status/:menuID/:status`,
	UPDATE_ORDER_MENU: `${BASE_ROUTE_MENU}/update-order`,
}

exports.CF_ROUTINGS_MENU = CF_ROUTINGS_MENU;