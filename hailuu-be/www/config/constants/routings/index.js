const { CF_ROUTINGS_ACCOUNT }                = require('./account');
const { CF_ROUTINGS_PUBLIC }                 = require('./public');
const { CF_ROUTINGS_PRODUCT }                = require('./product');
const { CF_ROUTINGS_MENU }                   = require('./menu');
const { CF_ROUTINGS_ORDER }                  = require('./order');
const { CF_ROUTINGS_CUSTOMER }               = require('./account_user');
const { CF_ROUTINGS_USER_CONTACT }           = require('./user_contact');
const { CF_ROUTINGS_VARIANT }                = require('./variant');
const { CF_ROUTINGS_SLIDE }                  = require('./slide');
const { CF_ROUTINGS_GOOD_PROJECT }           = require('./good_project');
const { CF_ROUTINGS_PERSONNEL_TEAM }         = require('./personnel_team');
const { CF_ROUTINGS_GALLERY_PRODUCT }        = require('./gallery_product');

module.exports = {
    CF_ROUTINGS_ACCOUNT,
    CF_ROUTINGS_PUBLIC,
    CF_ROUTINGS_PRODUCT,
    CF_ROUTINGS_MENU,
    CF_ROUTINGS_ORDER,
    CF_ROUTINGS_CUSTOMER,
    CF_ROUTINGS_USER_CONTACT,
    CF_ROUTINGS_VARIANT,
    CF_ROUTINGS_SLIDE,
    CF_ROUTINGS_GOOD_PROJECT,
    CF_ROUTINGS_PERSONNEL_TEAM,
    CF_ROUTINGS_GALLERY_PRODUCT
}
