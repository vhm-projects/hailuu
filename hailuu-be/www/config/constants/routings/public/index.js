const BASE_ROUTE = '/api';

const CF_ROUTINGS_PUBLIC = {
    ADD_IMAGE_PROJECT : `${BASE_ROUTE}/add-image-project`,
    UPDATE_IMAGE_PROJECT : `${BASE_ROUTE}/update-image-project/:imageProjectID`,

    ADD_CUSTOMER_REVIEW : `${BASE_ROUTE}/add-customer-review`,
    UPDATE_CUSTOMER_REVIEW : `${BASE_ROUTE}/update-customer-review/:customerReviewID`,

    LIST_SLIDE : `${BASE_ROUTE}/list-slide`,

    ADD_COMMENT : `${BASE_ROUTE}/add-comment`,
    UPDATE_STATUS_COMMENT : `${BASE_ROUTE}/update-comment/:commentID`,
    LIST_COMMENT_APPROVED : `${BASE_ROUTE}/list-comment`,

    ADD_EMAIL_CUSTOMER: `${BASE_ROUTE}/add-email-customer`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PUBLIC = CF_ROUTINGS_PUBLIC;
