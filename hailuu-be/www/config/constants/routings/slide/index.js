const BASE_ROUTE_SLIDE = '/slide';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_SLIDE = {
	LIST_SILDE: `${BASE_ROUTE_ADMIN}/list-slide`,
	ADD_SLIDE: `${BASE_ROUTE_SLIDE}/add-slide`,
	UPDATE_SLIDE: `${BASE_ROUTE_SLIDE}/update-slide/:slideID`,
	INFO_SLIDE: `${BASE_ROUTE_SLIDE}/info-slide/:slideID`,
	UPDATE_STATUS_SLIDE: `${BASE_ROUTE_SLIDE}/update-status-slide/:slideID`,
	REMOVE_SLIDE: `${BASE_ROUTE_SLIDE}/remove-slide/:slideID`,

	UPDATE_ORDER_SLIDE: `${BASE_ROUTE_SLIDE}/update-order`,
}

exports.CF_ROUTINGS_SLIDE = CF_ROUTINGS_SLIDE;