const BASE_ROUTE_GOOD_PROJECT = '/good-project';
const BASE_ROUTE_ADMIN = '/admin';

const CF_ROUTINGS_GOOD_PROJECT = {
	LIST_GOOD_PROJECT: `${BASE_ROUTE_ADMIN}/list-good-project`,
	ADD_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/add-good-project`,
	UPDATE_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/update-good-project/:goodProjectID`,
	INFO_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/info-good-project/:goodProjectID`,
	UPDATE_STATUS_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/update-status-good-project/:goodProjectID`,
	REMOVE_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/remove-good-project/:goodProjectID`,

	UPDATE_ORDER_GOOD_PROJECT: `${BASE_ROUTE_GOOD_PROJECT}/update-order`,
}

exports.CF_ROUTINGS_GOOD_PROJECT = CF_ROUTINGS_GOOD_PROJECT;