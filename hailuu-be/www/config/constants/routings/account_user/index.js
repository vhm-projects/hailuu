const BASE_ROUTE = '/customer';

const CF_ROUTINGS_CUSTOMER = {
    //ACCOUNT USER
    ADD_ACCOUNT_USER: `${BASE_ROUTE}/add-customer`,
    LOGIN:            `${BASE_ROUTE}/sign-in`,
    REGISTER:         `${BASE_ROUTE}/register`,
    LOGOUT:           `${BASE_ROUTE}/sign-out`,
    UPDATE_ACCOUNT:   `${BASE_ROUTE}/update-account`,
    CHANGE_PASSWORD:  `${BASE_ROUTE}/change-password`,
    CHANGE_ADDRESS:   `${BASE_ROUTE}/change-address/:accountID`,
    INFO_CUSTOMER:    `${BASE_ROUTE}/info-customer/:accountID`,

    ADD_ADDRESS_CUSTOMER:    `${BASE_ROUTE}/add-address-customer/:accountID`,
    LIST_ADDRESS_CUSTOMER:    `${BASE_ROUTE}/list-address/:accountID`,
    GET_ADDRESS_CUSTOMER:    `${BASE_ROUTE}/info-address-customer/:addressID`,
    UPDATE_ADDRESS_CUSTOMER: `${BASE_ROUTE}/update-address-customer/:addressID`,

    // UPDATE_NEWSLETTE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/newsletter`,
    // UPDATE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/update/:accountID`,
    // DELETE_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/delete/:accountID`,
    // LIST_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/accounts`,
    // LIST_ACCOUNT_CREATED_MONTH_OF_USER: `${BASE_ROUTE_ADMIN}/accounts/created-month`,
    // INFO_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/accounts/info/:accountID`,
    // SEARCH_ACCOUNT_OF_USER: `${BASE_ROUTE_ADMIN}/account/search`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CUSTOMER = CF_ROUTINGS_CUSTOMER;
