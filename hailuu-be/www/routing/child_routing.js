"use strict";

/**
 * EXTERNAL PACKAGE
 */
let express 			= require('express');
let url 				= require('url');
let moment 				= require('moment');
let lodash 				= require('lodash');
let formatCurrency    	= require('number-format.js');
let APP 				= require('../../app');

let { 
    CF_ROUTINGS_ACCOUNT, 
    CF_ROUTINGS_PUBLIC,
    CF_ROUTINGS_VARIANT,
	CF_ROUTINGS_ORDER,
    CF_ROUTINGS_CUSTOMER,
    CF_ROUTINGS_SLIDE,
    CF_ROUTINGS_GOOD_PROJECT ,
    CF_ROUTINGS_PERSONNEL_TEAM,
    CF_ROUTINGS_GALLERY_PRODUCT,
    CF_ROUTINGS_PRODUCT 
} = require('../config/constants/routings');

const CF_ROUTINGS_INFO_COMPANY 							= require('../package/info_company/constants/info_company.uri');
const { ORDER_STATUS, PAYMENT_STATUS, PAYMENT_TYPE } 	= require('../config/cf_constants');
const { percentDiscount, unique }                		= require('../utils/utils');

// cache top keyword frequently
const { LRANGE }                     = require('../cache/list');
const { GET_KEY_KEYWORD_FREQUENTLY } = require('../cache/utils/getKey');

/**
 * MODELS
 */
const MENU_MODEL 		= require('../models/menu').MODEL;
const ORDER_MODEL 		= require('../models/order').MODEL;
const ORDER_LINE_MODEL	= require('../models/order_line').MODEL;
const KEYWORD_MODEL	    = require('../models/keyword').MODEL;

const INFO_COMPANY_COLL = require("../package/info_company/databases/info_company-coll");
const PERSONNEL_TEAM_MODEL = require("../models/personnel_team").MODEL;


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static async renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;
        if (title) {
            res.bindingRole.config.title = title;
        }

        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });
		data.hrefCurrent = req.originalUrl;

		//_________Thư viện
		data.moment = moment;
        data.formatCurrency = formatCurrency;
        data._lodash = lodash;

        data.CF_ROUTINGS_ACCOUNT 	= CF_ROUTINGS_ACCOUNT;
        data.CF_ROUTINGS_PUBLIC 	= CF_ROUTINGS_PUBLIC;
        data.CF_ROUTINGS_VARIANT 	= CF_ROUTINGS_VARIANT;
		data.CF_ROUTINGS_ORDER		= CF_ROUTINGS_ORDER;
        data.CF_ROUTINGS_CUSTOMER   = CF_ROUTINGS_CUSTOMER;
        data.CF_ROUTINGS_SLIDE      = CF_ROUTINGS_SLIDE;
        data.CF_ROUTINGS_GOOD_PROJECT  = CF_ROUTINGS_GOOD_PROJECT;
        data.CF_ROUTINGS_PERSONNEL_TEAM  = CF_ROUTINGS_PERSONNEL_TEAM;
        data.CF_ROUTINGS_GALLERY_PRODUCT  = CF_ROUTINGS_GALLERY_PRODUCT;
        data.CF_ROUTINGS_INFO_COMPANY    = CF_ROUTINGS_INFO_COMPANY;
        data.CF_ROUTINGS_PRODUCT     = CF_ROUTINGS_PRODUCT;
         
		// CONSTANTS
		data.ORDER_STATUS	= ORDER_STATUS;
		data.PAYMENT_STATUS = PAYMENT_STATUS;
		data.PAYMENT_TYPE	= PAYMENT_TYPE;
		data.KEY_WORD		= [];

        // Viết tạm tìm cách xử lý sau,̣ report báo cáo
        let { from, to } = req.query;
        let listDataReport = await ORDER_MODEL.listDataOrderReport({ from, to });
        data.listDataReport = listDataReport.data;

        let listMenu = await MENU_MODEL.getlistAllMenu();
        data.listMenu = listMenu.data;
        data.percentDiscount = percentDiscount; 
        data.unique = unique; 

		if(req.user){
			data.infoUser = req.user;

			let listOrderLine = await ORDER_LINE_MODEL.getList({ customerID: req.user._id, status: 1 });
			data.listOrderLine = listOrderLine.data || [];

            let listKeyword = await KEYWORD_MODEL.getListOfCustomer({ customerID: req.user._id, status: 1 });
            data.KEY_WORD = listKeyword.data;

		} else{
			data.infoUser = null;
			data.listOrderLine = [];
        }

		if(!data.KEY_WORD.length){
			const INDEX_START = 0;
			const INDEX_END   = 10;

			let listKeywordFrequenly = await LRANGE(GET_KEY_KEYWORD_FREQUENTLY, INDEX_START, INDEX_END);
			listKeywordFrequenly 	 = [...new Set(listKeywordFrequenly)].map(key => ({ name: key }));

			data.KEY_WORD = listKeywordFrequenly;
		}

		let arrIDCompany = ['60fb6187fe9df5457faddec7', '610d6a15d9f9b82e60e97361'];

        let infoCompany = await INFO_COMPANY_COLL.findOne({ _id: { $in: arrIDCompany } });
        data.infoCompanyEndUser = infoCompany;

        const TYPE_COUNSELING_STAFF = 2;
        // Danh sách nhân viên tư vấn
        let listCounselingStaff  = await PERSONNEL_TEAM_MODEL.getList({ type: TYPE_COUNSELING_STAFF });
        data.listCounselingStaff2 = listCounselingStaff.data;
        
        // console.log({ __: res.bindingRole.config })

        // =============BIND ALL DATA TO VIEW=============//
        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;