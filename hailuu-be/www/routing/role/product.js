"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_PRODUCT }                       = require('../../config/constants/routings');
const USER_SESSION						            = require('../../session/user-session');

/**
 * COLLECTIONS
 */

const PRODUCT_COLL = require('../../database/product-coll');
const VARIANT_COLL = require('../../database/variant-coll');
const VARIANT_VALUE_COLL = require('../../database/variant_value-coll');
const { unique }   = require('../../utils/utils')

/**
 * MODELS
 */
const IMAGE_MODEL = require('../../models/image').MODEL;
const PRODUCT_MODEL = require('../../models/product').MODEL;
const VARIANT_MODEL = require('../../models/variant').MODEL;
const PERSONNEL_TEAM_MODEL = require('../../models/personnel_team').MODEL;
const COMMENT_MODEL = require('../../models/comment').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_BY_MENU_PARENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } = req.params;
                        let listProduct = await PRODUCT_MODEL.listProductByMenuParent({ limit: 10, menuID });
                        res.json(listProduct);
                    }]
                },
            },
            /**
             * VIEW CHI TIẾT SẢN PHẨM
             * Date: 29/07/2021
			 * Dev: DEPV
             */
            [CF_ROUTINGS_PRODUCT.INFO_PRODUCT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Home Page - HAILUU',
					code: CF_ROUTINGS_PRODUCT.INFO_PRODUCT,
					inc: path.resolve(__dirname, '../../views/inc/enduser/info_product.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { slug  } = req.params
                        let infoProduct = await PRODUCT_MODEL.getInfoBySlug({ slug });
                        if(infoProduct.error)
                            return res.redirect("/")
                        let menuID = infoProduct.data.menu._id;
                        let productID = infoProduct.data._id;

                        // Danh sách sản phẩm liên quan
                        let listProductRelated = await PRODUCT_MODEL.getListProductOfMenu({ menuID, limit: 5, productID });
                        const TYPE_COUNSELING_STAFF = 2;
                        // Danh sách nhân viên tư vấn
                        let listCounselingStaff  = await PERSONNEL_TEAM_MODEL.getList({ type: TYPE_COUNSELING_STAFF });

                        // Tạm thời sau sẽ xử lý phân trang sau
                        let listComment  = await COMMENT_MODEL.getListByBlogOrProduct({ productID, status: 1 });

                        // Danh sách tất cả comment để tính rating
                        let listCommentRating  = await COMMENT_MODEL.getListByBlogOrProductRating({ productID, status: 1 });

                        // Xử lý variant
                        let listVariant = await VARIANT_COLL.find({ product: productID }).sort({ createAt: 1});
                        let listVariantAndVariantValue = [];

						for (const variant of listVariant) {
							let listVariantValue = await VARIANT_VALUE_COLL
								.find({ variant: variant._id, status: 1 })
								.sort({ createAt: 1})
								.lean();
		
							listVariantAndVariantValue[listVariantAndVariantValue.length] = { variant, listVariantValue };
						}

                        ChildRouter.renderToView(req, res, {
                            infoProduct: infoProduct.data,
                            listCounselingStaff: listCounselingStaff.data,
                            listAmount: infoProduct.listAmount,
                            listProductRelated: listProductRelated.data,
                            listComment: listComment.data.listData,
                            infoListComment: listComment.data,
                            listVariantAndVariantValue,
                            listCommentRating: listCommentRating.data
                        });
                    }]
                },
            },

            /**
             * Danh sách comment rating
             * Date: 29/07/2021
			 * Dev: DEPV
             */
            [CF_ROUTINGS_PRODUCT.LIST_COMMENT_RATING]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { productID } = req.body;
                        let listCommentRating  = await COMMENT_MODEL.getListByBlogOrProductRating({ productID, status: 1 });
                        res.json(listCommentRating);
                    }]
                },
            },


            /**
             * THÔNG TIN SẢN PHẨM CON
             * Date: 29/07/2021
			 * Dev: DEPV
             */
            [CF_ROUTINGS_PRODUCT.GET_INFO_PRODUCT_CHILD]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { productID, faceNumberID, printSpecificationID, amountID  } = req.body;
                        let infoProduct = await PRODUCT_MODEL.getInfoProductChild({ productID, faceNumberID, printSpecificationID, amountID  });
                        res.json(infoProduct);
                    }]
                },
            },



        }
    }
};
