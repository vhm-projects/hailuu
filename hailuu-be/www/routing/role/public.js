"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path   	= require('path');
const moment 	= require('moment');
const uuidv4 	= require('uuid').v4;
const fs     	= require('fs');
const jwt 			= require('jsonwebtoken');
const cfJwt 		= require('../../config/cf_jws');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter 						= require('../child_routing');
const USER_SESSION						= require('../../session/user-session');
const roles 							= require('../../config/cf_role');
const { GENERATE_LINK_S3 } 				= require('../../config/cf_upload_s3');
const { uploadFieldMainAndBusiness }	= require('../../config/cf_multer');
const { districts }                     = require('../../config/constants/constant/districts');
const { provinces }                     = require('../../config/constants/constant/provinces');
const { ORDER_STATUS }                  = require("../../config/cf_constants");
const { uploadArray } 					= require('../../config/cf_multer');
const { LPUSH } 						= require('../../cache/list');
const { GET_KEY_KEYWORD_FREQUENTLY } 	= require('../../cache/utils/getKey');

/**
 * CONSTANTS URL
 */
const { 
	CF_ROUTINGS_ACCOUNT, CF_ROUTINGS_PUBLIC, CF_ROUTINGS_MENU, CF_ROUTINGS_PRODUCT, CF_ROUTINGS_CUSTOMER
} = require('../../config/constants/routings');

const { CF_ROUTINGS_COMMON } 	= require('../../config/constants/constant/common.uri');
const { CF_ROUTINGS_UPLOAD_S3 } = require('../../config/cf_upload_s3/constants');

/**
 * MODELS
 */
const MENU_MODEL  						= require("../../models/menu").MODEL;
const PRODUCT_MODEL  					= require("../../models/product").MODEL;
const COMMENT_MODEL  					= require("../../models/comment").MODEL;
const BLOG_MODEL  						= require("../../models/blog").MODEL; 
const CUSTOMER_REVIEW_MODEL 			= require('../../models/customer_review').MODEL;
const KEYWORD_MODEL 			        = require('../../models/keyword').MODEL;
const CONTACT_MESSAGE_MODEL 			= require('../../models/contact_message').MODEL;
const EMAIL_CUSTOMER_MODEL 				= require('../../models/email_customer').MODEL;
const IMAGE_MODEL 						= require('../../models/image').MODEL; 
const COMPANY_INFO_MODEL                = require("../../models/company_infomation").MODEL;
const CUSTOMER_MODEL                    = require("../../models/customer").MODEL;
const CUSTOMER_ADDRESS_MODEL            = require("../../package/customer/models/customer_address").MODEL;
const COMMON_MODEL                      = require("../../config/constants/constant/common").MODEL;
const SLIDE_MODEL 		                = require('../../models/slide').MODEL;
const GALLERY_MODEL 		            = require('../../models/gallery_product').MODEL;
const USER_MODEL						= require('../../models/user').MODEL;
const VARIANT_OPTION_MODEL			    = require('../../models/variant_option').MODEL;
const POST_MODEL						= require('../../models/blog').MODEL;
const VARIANT_V2_MODEL			        = require('../../models/variant_v2').MODEL;
const ORDER_MODEL			        	= require('../../models/order').MODEL;
const ORDER_LINE_MODEL			        = require('../../models/order_line').MODEL;
const PERSONNEL_TEAM_MODEL              = require('../../models/personnel_team').MODEL;
const GOOD_PROJECT_MODEL                = require('../../models/good_project').MODEL;
const PRICE_SHIP_MODEL                	= require('../../models/price_ship').MODEL;
const CATEGORY_MODEL					= require('../../models/category').MODEL;

/**
 * COLLECTIONS
 */
const POST_COLL							= require('../../database/blog-coll');
const CUSTOMER_COLL						= require('../../database/customer-coll');
const CUSTOMER_ADDRESS_COLL				= require('../../package/customer/database/customer_address-coll');
const INFO_COMPANY_COLL                 = require("../../package/info_company/databases/info_company-coll");


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

			/**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
			'/': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Home Page - HAILUU',
					code: 'HOME_ENDUSER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/index.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listSlide = await SLIDE_MODEL.getListClient();
                        let listFlashSale = await PRODUCT_MODEL.listFlashsale({ limit: 10 });
                        let listProductFavorite = await PRODUCT_MODEL.listProductFavorite({ limit: 10, type: 1 });
                        let listProductNews = await PRODUCT_MODEL.listProductNews({ limit: 10 });
                        let listAllProduct = await PRODUCT_MODEL.listAllProduct({ limit: 10 });
                        let listGallery = await GALLERY_MODEL.getListForEnduser();
                        ChildRouter.renderToView(req, res, {
                            listSlide: listSlide.data,
                            listFlashSale: listFlashSale.data,
                            listProductFavorite: listProductFavorite.data,
                            listProductNews: listProductNews.data,
                            listAllProduct: listAllProduct.data,
                            listGallery: listGallery.data
                        });
                    }]
                },
            },

            /**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
			'/tim-kiem': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Tìm kiếm - HAILUU',
					code: 'HOME_ENDUSER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/result_search.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let key = req.query["tu-khoa"];
                        let sort = req.query["hien-thi"];
                        let listProduct = await PRODUCT_MODEL.searchWithKey({ key, limit: 100, sort });
                        let listProductRelated = await PRODUCT_MODEL.listProductNews({ limit: 5 });

                        // Nếu khách hàng đã đăng nhập
                        const CUSTOMER_ACCESS  	= [0,1,3];
                        let session = USER_SESSION.getCustomer(req.session);
                        let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;

                        if (token){
                            jwt.verify(token, cfJwt.secret, async function (err, decoded) {
                                if (err) {
                                    return res.json({error: true, message: 'Failed to authenticate token.'});
                                } else {
                                    if (CUSTOMER_ACCESS.includes(+decoded.level)) {
                                        let { _id: customerID } = decoded;
                                        // Insert keyword
                                        let dataSignal = await KEYWORD_MODEL.insert({ name: key, customerID });
                                    } else {
                                        return res.json({success: false, message: 'Error: Permission denied.'});
                                    }
                                }
                            });
                        }

						// Cache keyword frequenly
						await LPUSH(GET_KEY_KEYWORD_FREQUENTLY, key);

                        ChildRouter.renderToView(req, res, {
                            listProduct: listProduct.data,
                            listProductRelated: listProductRelated.data,
                            sort,
                            key
                        });
                    }]
                },
            },

             /**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: Depv
             */
			'/danh-muc/:slug': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Danh mục - HAILUU',
					code: 'HOME_ENDUSER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/category.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { slug } = req.params;
                        let sort = req.query["hien-thi"];

                        if(slug == "flash-sale"){
                            let listProduct = await PRODUCT_MODEL.listFlashsale({ limit: 1000, sort });
                            let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                            return ChildRouter.renderToView(req, res, {
                                listCustomerReview: listCustomerReview.data,
                                listProduct: listProduct.data,
                                infoMenu: null,
                                slug,
                                sort,
                            });
                        }

                        if(slug == "moi-nhat"){
                            // let listProduct = await PRODUCT_MODEL.listFlashsale({ limit: 1000, sort });
                            let listProduct = await PRODUCT_MODEL.listProductNews({ limit: 30, sort });
                            let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                            
                            return ChildRouter.renderToView(req, res, {
                                listCustomerReview: listCustomerReview.data,
                                listProduct: listProduct.data,
                                infoMenu: null,
                                slug,
                                sort,
                            });
                        }

                        if(slug == "tat-ca"){
                            // let listProduct = await PRODUCT_MODEL.listFlashsale({ limit: 1000, sort });
                            // let listProduct = await PRODUCT_MODEL.listProductNews({ limit: 30, sort });
                            let listProduct = await PRODUCT_MODEL.listProductNews({ limit: 1000, sort });
                            let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                            
                            return ChildRouter.renderToView(req, res, {
                                listCustomerReview: listCustomerReview.data,
                                listProduct: listProduct.data,
                                infoMenu: null,
                                slug,
                                sort,
                            });
                        }

                        if(slug == "yeu-thich"){
                            let type = req.query.type;
                            let listProductFavorite = await PRODUCT_MODEL.listProductFavorite({ limit: 50, type });
                            let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                            return ChildRouter.renderToView(req, res, {
                                listCustomerReview: listCustomerReview.data,
                                listProduct: listProductFavorite.data.listData,
                                infoMenu: null,
                                slug,
                                sort,
                            });
                        }

                        let infoMenu = await MENU_MODEL.getInfoWithSlug({ slug })
                        if(infoMenu.error)
                            return res.redirect("/");
                        let { _id: menuID } = infoMenu.data;
                        let listProduct = await PRODUCT_MODEL.listProductByMenuParent({ limit: 1000, menuID, sort });
                        let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });

                        return ChildRouter.renderToView(req, res, {
                            listCustomerReview: listCustomerReview.data,
                            listProduct: listProduct.data,
                            infoMenu: infoMenu.data,
                            slug,
                            sort,
                        });
                    }]
                },
            },

			/**
             * Function: Giỏ hàng của tôi
             * Date: 04/08/2021
             * Dev: MinhVH
             */
			 '/gio-hang': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Giỏ hàng - HAILUU',
					code: 'MY_CART',
					inc: path.resolve(__dirname, '../../views/inc/enduser/my_cart.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;

						let listPriceShip	= await PRICE_SHIP_MODEL.getList({ status: 1 });
                        let listOrderLine 	= await ORDER_LINE_MODEL.getList({ customerID, status: 1 });
						let infoCustomer	= await CUSTOMER_COLL
							.findById(customerID)
							.populate({
								path: "address",
								match: { status: 1 }
							});

						let listAddress = [];
						if(infoCustomer.address && infoCustomer.address.length){
							for (const address of infoCustomer.address) {
								let infoProvince = await COMMON_MODEL.getInfoProvince({ 
									provinceCode: address.city 
								});
								let infoDistrict = await COMMON_MODEL.getInfoDistrict({ 
									districtCode: address.district 
								});
								let infoWard = await COMMON_MODEL.getInfoWard({ 
									district: address.district, wardCode: address.ward 
								});
	
								listAddress = [...listAddress, {
									info: address,
									infoProvince: infoProvince.data, 
									infoDistrict: infoDistrict.data, 
									infoWard: infoWard.data
								}];	
							}
						}

						ChildRouter.renderToView(req, res, {
                            listOrderLine: listOrderLine.data || [],
							listPriceShip: listPriceShip.data || [],
							listAddress,
							infoCustomer
                        });
                    }]
                },
            },

			/**
             * Function: Đặt hàng thành công
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			'/dat-hang-thanh-cong': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Đặt hàng thành công - HAILUU',
					code: 'ORDER_SUCCESS',
					inc: path.resolve(__dirname, '../../views/inc/enduser/order_success.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        ChildRouter.renderToView(req, res);
                    }]
                },
            },

			/**
             * Function: Danh sách đơn đặt hàng
             * Date: 06/08/2021
             * Dev: MinhVH
             */
			 '/don-dat-hang': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Danh sách đơn đặt hàng - HAILUU',
					code: 'LIST_ORDER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/list_order.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: customerID } = req.user;

						const listOrderByCustomer = await ORDER_MODEL.getList({ customer: customerID });

                        ChildRouter.renderToView(req, res, {
							listOrder: listOrderByCustomer.data || []
                        });
                    }]
                },
            },

			/**
             * Function: Chi tiết đơn đặt hàng
             * Date: 06/08/2021
             * Dev: MinhVH
             */
			 '/chi-tiet-don-hang/:orderID': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Chi tiết đơn đặt hàng - HAILUU',
					code: 'DETAIL_ORDER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/detail_order.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: customerID } 	= req.user;
						const { orderID } 			= req.params;

						const infoOrder = await ORDER_MODEL.getInfoByOrderID({ 
							customerID, orderID
						});

						let infoAddress = null;
						if(infoOrder.data && infoOrder.data.address){
							infoAddress = await CUSTOMER_ADDRESS_COLL.findById(infoOrder.data.address._id);

							let infoProvince   = await COMMON_MODEL.getInfoProvince({ 
								provinceCode: infoAddress.city 
							});
							let infoDistrict   = await COMMON_MODEL.getInfoDistrict({ 
								districtCode: infoAddress.district 
							});
							let infoWard       = await COMMON_MODEL.getInfoWard({ 
								district: infoAddress.district, wardCode: infoAddress.ward 
							});

							infoAddress = {
								info: infoAddress,
								infoProvince: infoProvince.data, 
								infoDistrict: infoDistrict.data, 
								infoWard: infoWard.data
							}
						}

                        ChildRouter.renderToView(req, res, {
							infoOrder: infoOrder.data || {},
							infoAddress
                        });
                    }]
                },
            },

            /**
             * Function: Tài khoản của tôi
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/tai-khoan': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Tài khoản - HAILUU',
					code: 'MY-ACCOUNT',
					inc: path.resolve(__dirname, '../../views/inc/enduser/my_account.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID: customerID });
                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data
                        });
                    }]
                },
            },

            
            /**
             * Function: Thay đổi mật khẩu
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/mat-khau': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Tài khoản - HAILUU',
					code: 'CHANGE_PASSWORD',
					inc: path.resolve(__dirname, '../../views/inc/enduser/change_password.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID: customerID });
                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data
                        });
                    }]
                },
            },

            /**
             * Function: Địa chỉ
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/so-dia-chi': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Tài khoản - HAILUU',
					code: 'LIST_ADDRESS',
					inc: path.resolve(__dirname, '../../views/inc/enduser/list_address.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID: customerID });
                        let listAddress = await CUSTOMER_ADDRESS_MODEL.getListByCustomer({ customerID });
                        
                        // Lấy thông tin tỉnh quận huyện chi tiết
                        let listAddressFormat = [];
                        if (listAddress) {
                            for (let address of listAddress.data) {
                                let infoProvince           = await COMMON_MODEL.getInfoProvince({ provinceCode : address.city });
                                let infoDistrict           = await COMMON_MODEL.getInfoDistrict({ districtCode : address.district });
                                let infoWard               = await COMMON_MODEL.getInfoWard({ district: address.district, wardCode:  address.ward });
                                listAddressFormat.push({ ...address._doc, infoProvince: infoProvince.data , infoDistrict: infoDistrict.data, infoWard: infoWard.data });
                            }
                        }
                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data,
                            listAddress: listAddressFormat 
                        });
                    }]
                },
            },

            /**
             * Function: Cập nhật địa chỉ
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/cap-nhat-dia-chi/:addressID': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Cập Nhật Tài khoản - HAILUU',
					code: 'UPDATE_ADDRESS',
					inc: path.resolve(__dirname, '../../views/inc/enduser/update_address.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let { addressID } = req.params;
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID: customerID });
                        let listProvince   = COMMON_MODEL.listProvinceAll({ });
                        let infoAddress = await CUSTOMER_ADDRESS_MODEL.getInfoCustomer({ addressID });

                        let listDistrict   = COMMON_MODEL.listDistrict({ province: infoAddress.data.city });
                        let listWard       = await COMMON_MODEL.listWard({ district: infoAddress.data.district });

                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data,
                            listProvince: listProvince.data,
                            infoAddress: infoAddress.data,
                            listDistrict: listDistrict.data,
                            listWard: listWard.data
                        });
                    }]
                },
            },

            /**
             * Function: Thêm địa chỉ
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/them-dia-chi': {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'view',
					title: 'Tài khoản - HAILUU',
					code: 'ADD_ADDRESS',
					inc: path.resolve(__dirname, '../../views/inc/enduser/add_address.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID: customerID });
                        let listProvince   = COMMON_MODEL.listProvinceAll({  });
                        ChildRouter.renderToView(req, res, {
                            infoCustomer: infoCustomer.data,
                            listProvince: listProvince.data
                        });
                    }]
                },
            },

            /**
             * Function: Thêm địa chỉ
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/recover-password': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email } = req.body;
                        const infoSignIn = await USER_MODEL.forgetPassword({ email });
                        if(infoSignIn.error)
                            return res.json(infoSignIn);
                        res.json({ error: false, message: "send_mail_success" });
                    }]
                },
            },

             /**
             * Function: Lấy giá sản phẩm thông qua tiêu chí
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/get-price-product': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { variantOptions, productID } = req.body;
                        const infoVariantV2 = await VARIANT_V2_MODEL.getPriceProduct({ variantOptions, productID  });
                        res.json(infoVariantV2);
                    }]
                },
            },

             /**
             * Function: Thêm địa chỉ
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/add-product-variant-v2': {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { variantOptions, weight, priceBefore, priceAfter, productID } = req.body;
                        const infoVariantV2 = await VARIANT_V2_MODEL.insert({ variantOptions, weight, priceBefore, priceAfter, productID  });
                        res.json(infoVariantV2);
                    }]
                },
            },

             /**
             * Function: Xác nhận code, thay đổi password còn hợp lệ ko
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/change-password': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email, code } = req.body;
                        const infoCheckCode = await CUSTOMER_MODEL.checkCodeForgetPassword({ email, code });
                        res.json(infoCheckCode);
                    }],
                },
            },

             /**
             * Function: Cập nhật password
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/update-pass-recover': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email, code, password  } = req.body;
                        const infoCheckCode = await CUSTOMER_MODEL.updateForgetPassword({ email, code, password });
                        res.json(infoCheckCode);
                    }],
                },
            },


            /**
             * Function: Thêm variant option
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/add-variant-option': {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { name, parentID  } = req.body;
                        const infoProductVariant = await VARIANT_OPTION_MODEL.insert({ name, parentID });
                        res.json(infoProductVariant);
                    }],
                },
            },

             /**
             * Function: Thêm variant option
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/info-variant-option/:variantOptionID': {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { variantOptionID  } = req.params;
                        console.log({ variantOptionID });
                        const infoProductVariant = await VARIANT_OPTION_MODEL.getInfo({ variantOptionID });
                        res.json(infoProductVariant);
                    }],
                },
            },

             /**
             * Function: cập nhật variant option
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/update-variant-option/:variantOptionID': {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { variantOptionID  } = req.params;
                        const { name  } = req.body;
                        const infoProductVariant = await VARIANT_OPTION_MODEL.update({ variantOptionID, name });
                        res.json(infoProductVariant);
                    }],
                },
            },

             /**
             * Function: cập nhật variant option
             * Date: 29/07/2021
             * Dev: Depv
             */
			'/remove-variant-option/:variantOptionID': {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { variantOptionID  } = req.params;
                        const infoProductVariant = await VARIANT_OPTION_MODEL.updateStatus({ variantOptionID, status: -1 });
                        res.json(infoProductVariant);
                    }],
                },
            },

            /**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
			'/du-an-noi-bat': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Danh sách bài viết - HAILUU',
					code: 'OUTSTANDING_PRODUCT',
					inc: path.resolve(__dirname, '../../views/inc/enduser/outstanding_project.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type } = req.query;
                        let listProjectGood  = await GOOD_PROJECT_MODEL.getListEndUserByType({ type });
                        let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                        ChildRouter.renderToView(req, res, {
                            listCustomerReview: listCustomerReview.data,
                            listProjectGood: listProjectGood.data,
                            type
						});
                    }]
                },
            },

			/**
         	 * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
			'/lien-he': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Thông tin liên hệ - HAILUU',
					code: 'CONTACT',
					inc: path.resolve(__dirname, '../../views/inc/enduser/contact.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const TYPE_DOI_NGU_NHAN_SU = 1;
                        let listCustomerReview = await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });
                        let listPersonnel  = await PERSONNEL_TEAM_MODEL.getList({ type: TYPE_DOI_NGU_NHAN_SU });

                        ChildRouter.renderToView(req, res, {
                            listCustomerReview: listCustomerReview.data,
                            listPersonnel: listPersonnel.data
						});
                    }]
                },
            },

			/**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
			'/danh-sach-bai-viet': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Danh sách bài viết - HAILUU',
					code: 'LIST_BLOG_ENDUSER',
					inc: path.resolve(__dirname, '../../views/inc/enduser/list_blog.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						let categoryName 		= req.query['danh-muc'];
						let listCategory		= await CATEGORY_MODEL.getList({ status: 1 });
                        let listPosts 			= await POST_MODEL.getList({ status: 1 });
                        let listCustomerReview 	= await CUSTOMER_REVIEW_MODEL.getListByStatus({ status: 1 });

                        ChildRouter.renderToView(req, res, {
							listPosts: listPosts.data || [],
							listCategory: listCategory.data || [],
                            listCustomerReview: listCustomerReview.data,
							categoryName: categoryName || ''
						});
                    }]
                },
            },

			/**
             * Function: Home Page End-User (VIEW)
             * Date: 29/07/2021
			 * Dev: MinhVH
             */
            '/bai-viet/:slug': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Chi tiết bài viết - HAILUU',
					code: 'POST_DETAIL',
					inc: path.resolve(__dirname, '../../views/inc/enduser/info_blog.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						let { slug } = req.params;
                        let infoPost = await POST_MODEL.getPostBySlug({ slug });

						let { _id: postID, category } = infoPost.data;

                        let listPostRelated 	= await POST_MODEL.getRelatedPost({ postID, category });
						let listCommentByPost 	= await COMMENT_MODEL.getListByPostWithPage({ postID });

						// Increase views
						new Promise(async resolve => {
							try {
								await POST_COLL.updateOne({ _id: postID }, {
									$inc: { views: 1 }
								});
								resolve();
							} catch (error) {
								resolve();
							}
						})

						ChildRouter.renderToView(req, res, {
							infoPost: infoPost.data || {},
							infoListComment: listCommentByPost.data || {},
							listPostRelated: listPostRelated.data || []
						});
                    }]
                },
            },

             /**
             * Thông tin menu con + sản phẩm để hiển thị lên popup
             * Date: 05/08/2021
			 * Dev: Depv
             */
            '/info-menu-child-and-product/:menuID': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } 	= req.params;
                        let infoMenu = await MENU_MODEL.getInfoMenuChild({ menuID });
                        return res.json(infoMenu);
                    }]
                },
            }, 

            '/get-product-favorite-by-type': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type } 	= req.query;
                        let listProductFavorite = await PRODUCT_MODEL.listProductFavorite({ limit: 10, type });
                        return res.json(listProductFavorite);
                    }]
                },
            }, 

            /**
             * Redirect /admin/home
             * Depv247
             */
            [CF_ROUTINGS_ACCOUNT.ORIGIN_APP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        res.redirect("/admin/home");
                    }]
                },
            },         

			 /**
             * Function: Upload S3 (API)
             * Date: 07/07/2021
			 * Dev: MinhVH
             */
			  [CF_ROUTINGS_UPLOAD_S3.GENERATE_LINK_S3]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let fileName 	= uuidv4();
                        let type 		= req.query.type;
                        let fileNameExtension = req.query.fileName.split(".")[1].toLowerCase();;
                        fileName = `${fileName}.${fileNameExtension}`;
                        let linkUpload 	= await GENERATE_LINK_S3(fileName, type);

                        if(linkUpload.error)
                            return res.json({error: true, message: "cannot_create_link"});

                        return res.json({ error: false, linkUpload, fileName });
                    }]
                },
            }, 

            /**
             * Function: tạo company infomation (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_ACCOUNT.ADD_COMPANY_INFOMATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
					title: 'Add Company Infomation - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADD_COMPANY_INFOMATION,
					inc: path.resolve(__dirname, '../../views/inc/admin/add-company-infomation.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        ChildRouter.renderToView(req, res, { });
                    }],
                    post: [ uploadFieldMainAndBusiness, async function (req, res) {
                        const { name, sortDescription, longDescription, phone, email, address, slogan, zalo, facebook, youtube, twitter, linkGoogleMap, oldImage, oldBusiness } = req.body;
                        let isExistLogin = USER_SESSION.getUser(req.session);
                        
                        let files      = req.files;
                        // console.log({ isMainImage: files.isMainImage });
                        // console.log({ businessImages: files.businessImages });
                        if( !files ){
                            files = [];
                        }

                        let infoCompany = await COMPANY_INFO_MODEL.insert({ 
                            name, sortDescription, longDescription, 
                            phone, email, address, slogan, zalo, 
                            facebook, youtube, twitter, linkGoogleMap, 
                            userCreate: isExistLogin.user._id, 
                            image: files.isMainImage,
                            businessImages: files.businessImages,
                            oldBusinessImage: oldBusiness,
                            oldMainImage: oldImage
                        });

                        res.json({ infoCompany });
                    }]
                },
            },

            /**
             * Function: Danh sách company infomation (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_ACCOUNT.LIST_COMPANY_INFOMATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {

                        const listCompanyInfo = await COMPANY_INFO_MODEL.getList({});

                        res.json({ listCompanyInfo })
                    }],
                },
            },

            /**
             * Function: update company infomation (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_ACCOUNT.UPDATE_COMPANY_INFOMATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
					title: 'Cập nhật thông tin - hailuu',
					code: CF_ROUTINGS_ACCOUNT.UPDATE_COMPANY_INFOMATION,
					inc: path.resolve(__dirname, '../../views/inc/admin/info-company.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { companyInfoID } = req.params;

                        let infoCompany = await COMPANY_INFO_MODEL.getInfo({ companyInfoID });
                        
                        ChildRouter.renderToView(req, res, { infoCompany: infoCompany.data });
                    }],
                    post: [ uploadFieldMainAndBusiness, async function (req, res) {
                        const { name, sortDescription, longDescription, phone, email, address, slogan, zalo, facebook, youtube, twitter, linkGoogleMap } = req.body;
                        const { companyInfoID } = req.params;
                        let isExistLogin = USER_SESSION.getUser(req.session);
                        
                        let files      = req.files;

                        if( !files ){
                            files = [];
                        }

                        let infoCompany = await COMPANY_INFO_MODEL.update({ 
                            companyInfoID, name, sortDescription, longDescription, 
                            phone, email, address, slogan, zalo, 
                            facebook, youtube, twitter, linkGoogleMap, 
                            userUpdate: isExistLogin.user._id, 
                            image: files.isMainImage,
                            businessImages: files.businessImages,
                        });

                        res.json({ infoCompany });
                    }]
                },
            },

            /**
             * Function: tạo Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.ADD_ACCOUNT_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Thêm tài khoản - hailuu',
					code: CF_ROUTINGS_CUSTOMER.ADD_ACCOUNT_USER,
					inc: path.resolve(__dirname, '../../views/inc/admin/add-account.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listProvince = COMMON_MODEL.listProvinceAll({  })
                        ChildRouter.renderToView(req, res, { listProvince: listProvince.data });
                    }],
                    post: [ async function (req, res) {
                        const { fullname, email, password } = req.body;
                        let infoAccount = await CUSTOMER_MODEL.insert({ 
                            fullname, email, password
                        });
                        res.json({ infoAccount });
                    }]
                },
            },

            /**
             * Function: update Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_ACCOUNT.UPDATE_NEWSLETTE_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID, newSletter } = req.query;
                        // let isExistLogin = USER_SESSION.getUser(req.session);
                        let infoAccount = await CUSTOMER_MODEL.updateNewSletter({ 
                            status: newSletter, accountID
                        });
                        res.json({ infoAccount });
                    }]
                },
            },

             /**
             * Function: update Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
              [CF_ROUTINGS_ACCOUNT.UPDATE_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Cập nhật tài khoản - hailuu',
					code: CF_ROUTINGS_ACCOUNT.UPDATE_ACCOUNT_OF_USER,
					inc: path.resolve(__dirname, '../../views/inc/admin/update-account'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;
                        
                        let infoAccount = await CUSTOMER_MODEL.getInfo({ accountID });

                        ChildRouter.renderToView(req, res, { 
                            infoAccount: infoAccount.data,  
                        });
                    }],
                    post: [ async function (req, res) {
                        const { name, email, phone, password, birthday, gender, oldPassword } = req.body;
                        const { accountID } = req.params;
                        // let isExistLogin = USER_SESSION.getUser(req.session);
                        let infoAccount = await CUSTOMER_MODEL.updateUser({ 
                            accountID, fullname: name, email, phone, password, birthday, gender, oldPassword
                        });

                        res.json({ infoAccount });
                    }]
                },
            },

            /**
             * Function: delete Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_ACCOUNT.DELETE_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;

                        let infoAccount = await CUSTOMER_MODEL.delete({ accountID });
                        res.json({ infoAccount });
                    }]
                },
            },

            /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_ACCOUNT.LIST_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Danh sách tài khoản - hailuu',
					code: CF_ROUTINGS_ACCOUNT.LIST_ACCOUNT_OF_USER,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-account.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listAccount = await CUSTOMER_MODEL.getList({});
                        ChildRouter.renderToView(req, res, { listAccount: listAccount.data });
                    }],
                },
            },
            // View báo cáo
            [CF_ROUTINGS_ACCOUNT.LIST_REPORTS]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Báo cáo - hailuu',
					code: CF_ROUTINGS_ACCOUNT.LIST_REPORTS,
					inc: path.resolve(__dirname, '../../views/inc/admin/index.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        ChildRouter.renderToView(req, res, {});
                    }],
                },
            },

             /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
              [CF_ROUTINGS_ACCOUNT.LIST_ACCOUNT_CREATED_MONTH_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { month } = req.query;
                        const dataDate = await CUSTOMER_MODEL.getListAccMonthBest({ month });
                        // console.log({ dataDate });
                        res.json({ dataDate })
                    }],
                },
            },

            /**
             * Function: Thông tin Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_ACCOUNT.INFO_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;
                        const infoAccount = await CUSTOMER_MODEL.getInfo({ accountID });
                        res.json({ infoAccount })
                    }],
                },
            },

            /**
             * Function: Search theo tên, email Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_ACCOUNT.SEARCH_ACCOUNT_OF_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { key, limit } = req.query;
                        let nLimit = Number(limit);
                        const listAccount = await CUSTOMER_MODEL.searchNameEmail({ keyword: key, limit: nLimit });
                        res.json({ listAccount })
                    }],
                },
            },

			/**
             * Function: Clear session with render site and script (API)
             * Date: 29/04/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CUSTOMER.LOGOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ (req, res) => {
                        USER_SESSION.destroySessionCustomer(req.session);
                        res.redirect("/");
                    }]
                },
            },

            /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.LOGIN]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email, password } = req.body;

                        const infoSignIn = await CUSTOMER_MODEL.signIn({ email, password });
						if (!infoSignIn.error) {
                            USER_SESSION.saveCustomer(req.session, {
                                user: infoSignIn.data.user, 
                                token: infoSignIn.data.token,
                            });
                        }

                        res.json(infoSignIn);
                    }],
                },
            },
            /**
             * Function: Đăng kí Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.REGISTER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fullname, email, password } = req.body;
                        const infoSignIn = await CUSTOMER_MODEL.register({ fullname, email, password });
                        res.json(infoSignIn);
                    }],
                },
            },

            /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
             [CF_ROUTINGS_CUSTOMER.INFO_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'List account customer - hailuu',
					code: CF_ROUTINGS_CUSTOMER.INFO_CUSTOMER,
					inc: path.resolve(__dirname, '../../package/customer/views/info_customer.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;
                        const infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID });
                        let listOrder      = await ORDER_MODEL.getListCustomer({ customer: accountID });
                        ChildRouter.renderToView(req, res, { 
                            infoCustomer: infoCustomer.data, 
                            listOrder: listOrder.data,
                            ORDER_STATUS
                        });
                    }],
                },
            },
            /** 
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.UPDATE_ACCOUNT]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        const { fullname, gender, address, phone, email, birthday, password, oldPassword } = req.body;
                        const infoUpdate = await CUSTOMER_MODEL.updateUser({ accountID: customerID, fullname, gender, address, phone, email, birthday, password, oldPassword });
                        res.json(infoUpdate);
                    }],
                },
            },
            /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.CHANGE_PASSWORD]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: customerID } = req.user;
                        const { password, oldPassword } = req.body;
                        const infoUpdate = await CUSTOMER_MODEL.updateUser({ accountID: customerID, password, oldPassword });
                        res.json(infoUpdate);
                    }],
                },
            },
            /**
             * Function: Danh sách Account (API)
             * Date: 06/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.CHANGE_ADDRESS]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { accountID } = req.params;
                        const { name, phone, ward, district, province, address } = req.body;
                        // console.log({ ward, district, province, address });

                        const infoUpdate = await CUSTOMER_MODEL.updateUser({ accountID, name, phone, ward, district, province, address });

                        res.json(infoUpdate);
                    }],
                },
            },

            /**
             * Add menu
             * Dattv - Đã check [X]
             */
            [CF_ROUTINGS_MENU.ADD_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user
                        let { name, description, parent, image } = JSON.parse(req.body.data) ;

                        let infoAfterInsertMenu = await MENU_MODEL.insert({ 
                            name, description, parent, userID, image
                        });
                        res.json(infoAfterInsertMenu);
                    }]
                },
            },     
            
            /**
             * Search menu
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_MENU.SEARCH_MENU]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { key } = req.params;
                        let dataSearch = await MENU_MODEL.searchWithKey(key);
                        res.json(dataSearch);
                    }]
                },
            },  

            /**
             * Get info menu
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_MENU.GET_INFO]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } = req.params;
                        let infoMenu = await MENU_MODEL.getInfo({ menuID });
                        res.json(infoMenu);
                    }]
                },
            }, 

            /**
             * Remove Menu
             * Dattv - Đã check [X]
             */
            [CF_ROUTINGS_MENU.REMOVE_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } = req.params;
                        let infoMenuRemove = await MENU_MODEL.remove({ menuID });
                        res.json(infoMenuRemove);
                    }]
                },
            },

            /**
             * Get list menu child of menu parent
             * Dattv
             */
             [CF_ROUTINGS_MENU.GET_LIST_MENU_CHILD_OF_PARENT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } = req.params;
                        let listMenuChildOfParent = await MENU_MODEL.getlistMenuOfParent({ menuID, status: 1 });
                        res.json(listMenuChildOfParent);
                    }]
                },
            },

            /**
             * Update Menu
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_MENU.UPDATE_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { menuID } = req.params;
                        let { name, description, image } = JSON.parse(req.body.data) ;
                        let infoMenuUpdate = await MENU_MODEL.update({ menuID, userID, name, description, image });
                        res.json(infoMenuUpdate);
                    }]
                },
            },

            /**
             * Update Status Menu
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_MENU.UPDATE_STATUS_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { menuID, status } = req.params;
                        let infoMenuUpdateStatus = await MENU_MODEL.updateStatus({ menuID, status, userID });
                        //console.log({ infoMenuUpdateStatus });
                        res.json(infoMenuUpdateStatus);
                    }]
                },
            },

            /**
             * Update order Menu
             * Dattv
             */
             [CF_ROUTINGS_MENU.UPDATE_ORDER_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObjectMenu } = req.body;
                        let infoResult = await MENU_MODEL.updateOrderMenu({ arrayObjectMenu });
                        res.json(infoResult);
                    }]
                },
            },


            //===================================== PRODUCT =====================================
             /**
             * Add Product
             * Update: 11/09/2021
             * Dattv
             */
              [CF_ROUTINGS_PRODUCT.ADD_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { name, description, menu, introProduct, noteOrder, priceList, priceBefore, priceAfter, pathImage, gallery, pathfileExcel, pathfileIllustrator, pathfileVectorEPS, pathfilePDF, ratioSale, flashSale, flashPrint, basicParameters, variant } = req.body;
                        let avatar = null;
                        let excelFile = null;
                        let illustratorFile = null;
                        let vectorFile = null;
                        let pdfFile = null;
                        let arrGalleryID = [];

                        //Insert avatar
                        if(pathImage){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "avatar_product",
                                path: pathImage,
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        if(pathfileExcel){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_excel_product",
                                path: pathfileExcel,
                            });
                            excelFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfileIllustrator){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_illustrator_product",
                                path: pathfileIllustrator,
                            });
                            illustratorFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfileVectorEPS){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_vector_product",
                                path: pathfileVectorEPS,
                            });
                            vectorFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfilePDF){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_pdf_product",
                                path: pathfilePDF,
                            });
                            pdfFile = infoImageAfterInsert.data._id;
                        }
                        
                        //Insert images product
                        let galleryParse = gallery ? gallery : null
                        if(galleryParse && galleryParse.length){
                            for (const image of galleryParse) {
                                let { name, size, pathImageGallery } = image;
                                let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                    size,
                                    name,
                                    path: pathImageGallery,
                                });
                                arrGalleryID = [...arrGalleryID, infoImageAfterInsert.data._id];
                            }
                        }

                        let dataInsert = {
                            name, description, menu, introProduct, noteOrder, priceList, priceBefore, priceAfter, avatar, 
                            excelFile, illustratorFile, vectorFile, pdfFile, images: arrGalleryID, ratioSale, flashSale, flashPrint, userID,
                            basicParameters, variant
                        }

                        let infoAfterInsertProduct = await PRODUCT_MODEL.insert(dataInsert);

                        res.json(infoAfterInsertProduct);
                    }]
                },
            },     
            
            /**
             * Search product
             * Dattv - Đã check [X]
             */
            [CF_ROUTINGS_PRODUCT.SEARCH_PRODUCT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { key } = req.params;
                        let dataSearch = await PRODUCT_MODEL.searchWithKey({ key, limit: 5 });

                        res.json(dataSearch);
                    }]
                },
            },  

            /**
             * Get info product
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_PRODUCT.GET_INFO_PRODUCT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { productID } = req.params;
                        let infoProduct = await PRODUCT_MODEL.getInfo({ productID });
                        res.json(infoProduct);
                    }]
                },
            }, 

            /**
             * Get list product of menu
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.GET_LIST_PRODUCT_BY_MENU]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { menuID } = req.params;
                        let listProductOfMenu = await PRODUCT_MODEL.getListProductOfMenu({ menuID });
                        res.json(listProductOfMenu);
                    }]
                },
            }, 

            /**
             * Remove Product
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.REMOVE_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { productID } = req.params;
                        let infoProductRemove = await PRODUCT_MODEL.remove({ productID });
                        res.json(infoProductRemove);
                    }]
                },
            },

            /**
             * Remove Product
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.REMOVE_PRODUCT_V2]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { productID } = req.params;
                        let infoProductRemove = await PRODUCT_MODEL.removeProductNewVer({ productID });
                        res.json(infoProductRemove);
                    }]
                },
            },

           
            /**
             * Update Product
             * Update: 11/09/2021
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.UPDATE_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID } = req.params;
                        let { name, description, url, menu, introProduct, noteOrder, priceList, status, 
                            color, priceBefore, priceAfter, ratioSale, flashSale, flashPrint, 
                            gallery, galleryIDRemove, basicParameters, pathImage, 
                            pathfileExcel, pathfileIllustrator, pathfileVectorEPS, pathfilePDF } = req.body;
                        
                        let avatar = null;
                        let excelFile = null;
                        let illustratorFile = null;
                        let vectorFile = null;
                        let pdfFile = null;

                        if(pathImage){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "avatar_product",
                                path: pathImage,
                            });
                            avatar = infoImageAfterInsert.data._id;
                        }

                        if(pathfileExcel){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_excel_product",
                                path: pathfileExcel,
                            });
                            excelFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfileIllustrator){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_illustrator_product",
                                path: pathfileIllustrator,
                            });
                            illustratorFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfileVectorEPS){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_vector_product",
                                path: pathfileVectorEPS,
                            });
                            vectorFile = infoImageAfterInsert.data._id;
                        }

                        if(pathfilePDF){
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size: 123,
                                name: "file_pdf_product",
                                path: pathfilePDF,
                            });
                            pdfFile = infoImageAfterInsert.data._id;
                        }

                        //Xoá mảng ảnh nếu có
                        let imageIDRemmove = galleryIDRemove && JSON.parse(galleryIDRemove);

                        //Thêm mảng ảnh nếu có
                        let images = gallery && JSON.parse(gallery);
                        let arrGalleryID = [];
                        if(images && images.length){
                            for (const image of images) {
                                let { name, size, pathImageGallery } = image;
                                let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                    size,
                                    name,
                                    path: pathImageGallery,
                                });
                                arrGalleryID = [...arrGalleryID, infoImageAfterInsert.data._id];
                            }
                        }

                        //Xoá mảng file nếu có
                        //let fileIDRemove = documentIDRemove && JSON.parse(documentIDRemove);

                        //Thêm mảng file nếu có
                        // let arrDocument = document && JSON.parse(document);
                        // let arrDocumentID = [];
                        // if(arrDocument && arrDocument.length){
                        //     for (const file of arrDocument) {
                        //         let { name, size, pathDocument } = file;
                        //         let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        //             size,
                        //             name,
                        //             path: pathDocument,
                        //         });
                        //         arrDocumentID = [...arrDocumentID, infoImageAfterInsert.data._id];
                        //     }
                        // }

                        let dataUpdate = {
                            productID, name, description, url, menu, introProduct, noteOrder, priceList, 
                            status, color, priceBefore, priceAfter, ratioSale, flashSale, flashPrint, basicParameters,
                            avatar, excelFile, illustratorFile, vectorFile, pdfFile, imageIDRemmove, images: arrGalleryID, userID
                        }

                        let infoProductUpdate = await PRODUCT_MODEL.update(dataUpdate);
                        res.json(infoProductUpdate);
                    }]
                },
            },

            /**
             * Update image Product
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.UPDATE_IMAGE_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadArray, async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID } = req.params;
                        let { imageIDRemove } = req.body;
                        let files = req.files;
                        //console.log({ productID, imageIDRemove, files });
                        let infoProductUpdateImage = await PRODUCT_MODEL.updateImageProduct({ productID, imageIDRemove, filesArrayInsert: files });
                        res.json(infoProductUpdateImage);
                    }]
                },
            },
            

            /**
             * Update status Product
             * Dattv - Đã check [X]
             */
             [CF_ROUTINGS_PRODUCT.UPDATE_STATUS_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID, status } = req.params;
                        let infoProductUpdateStatus = await PRODUCT_MODEL.updateStatus({ productID, status, userID });
                        res.json(infoProductUpdateStatus);
                    }]
                },
            },

            /**
             * Update main image Product
             * Dattv
             */
             [CF_ROUTINGS_PRODUCT.UPDATE_MAIN_IMAGE_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { productID, imageID } = req.params;
                        let infoProductUpdateStatus = await PRODUCT_MODEL.updateMainImageProduct({ productID, imageID, userID });
                        res.json(infoProductUpdateStatus);
                    }]
                },
            },

            [CF_ROUTINGS_PUBLIC.ADD_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { fullname, email, content, star, blogID, productID } = req.body;

                        let infoCommentInsert = await COMMENT_MODEL.insert({ fullname, email, content, star, blogID, productID });
                        
                        res.json({ infoCommentInsert });
                    }]
                },
            },

            [CF_ROUTINGS_PUBLIC.UPDATE_STATUS_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { commentID } = req.params;
                        let { status } = req.query;
                        
                        let infoCommentAfterUpdate = await COMMENT_MODEL.updateStatus({ commentID, status });

                        res.json({ infoCommentAfterUpdate });
                    }]
                },
            },

            [CF_ROUTINGS_PUBLIC.LIST_COMMENT_APPROVED]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { page } = req.query;

                        let listCommentApproved = await COMMENT_MODEL.getListCommentApproved({ page: Number(page) });
                        
                        res.json({ listCommentApproved });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.VIEW_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'View Comment - hailuu',
					code: CF_ROUTINGS_PUBLIC.VIEW_COMMENT,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-comment.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { blogID, productID } = req.query;
                        let { status } = req.query;
                        let listComment = await COMMENT_MODEL.getListByBlogOrProduct({ blogID, productID, status });

                        // lấy thông tin product và blog để gửi qua view render ra title
                        let infoProduct = await PRODUCT_MODEL.getInfo({ productID });
                        let infoBlog = await BLOG_MODEL.getInfo({ postID: blogID });
                        ChildRouter.renderToView(req, res, { 
                            type: listComment.data.type, 
                            listComment: listComment.data.listData, 
                            infoBlog,
                            infoProduct,
                            moment
                        });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.LIST_REVIEW_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                    view: 'index-admin.ejs',
					code: CF_ROUTINGS_ACCOUNT.LIST_REVIEW_CUSTOMER,
                    inc: path.resolve(__dirname, '../../views/inc/admin/list-review-customer.ejs'),
                    title: 'Danh sách khách hàng review',
                    type: 'view',
                },
                methods: {
                    get: [async  function (req, res) {
                        let listData = await CUSTOMER_REVIEW_MODEL.getAll();
                        return ChildRouter.renderToView(req, res, {
                            listData: listData.data
                        });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.LIST_KEYWORD]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                    view: 'index-admin.ejs',
					code: CF_ROUTINGS_ACCOUNT.LIST_KEYWORD,
                    inc: path.resolve(__dirname, '../../views/inc/admin/list-keyword.ejs'),
                    title: 'Danh sách từ khóa tìm kiếm',
                    type: 'view',
                },
                methods: {
                    get: [async  function (req, res) {
                        let listData = await KEYWORD_MODEL.getListByStatus({ status: 1 });
                        return ChildRouter.renderToView(req, res, {
                            listData: listData.data
                        });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.ADD_KEYWORD]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let { name } = req.body;
                        let dataSignal = await KEYWORD_MODEL.insert({ name, customerID });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.INFO_KEYWORD]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { keywordID } = req.params;
                        let dataSignal = await KEYWORD_MODEL.getInfo({ keywordID });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.UPDATE_KEYWORD]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { keywordID } = req.params;
                        let { name } = req.body;
                        let dataSignal = await KEYWORD_MODEL.update({ keywordID, name });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.REMOVE_KEYWORD]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { keywordID } = req.params;
                        let dataSignal = await KEYWORD_MODEL.updateStatus({ keywordID, status: -1 });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.ADD_CUSTOMER_REVIEW]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { fullname, position, company, content, avatar } = JSON.parse(req.body.data);
                        
                        let imageID;
                        if(avatar){
                            let { name, size, path } = avatar;
                            
                            let infoAvatar = await IMAGE_MODEL.insert({ 
                                size,
                                name,
                                path,
                            });

                            imageID = infoAvatar.data._id;
                        }
                        
                        let dataSignal = await CUSTOMER_REVIEW_MODEL.insert({ fullname, image: imageID, position, company, content, userID });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.UPDATE_CUSTOMER_REVIEW]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        let { _id: userID } = req.user;
                        let { customerReviewID } = req.params;
                        let { fullname, position, company, content, status, avatar } = JSON.parse(req.body.data);
                        
                        let imageID;
                        if(avatar){
                            let { name, size, path } = avatar;
                            
                            let infoAvatar = await IMAGE_MODEL.insert({ 
                                size,
                                name,
                                path,
                            });

                            imageID = infoAvatar.data._id;
                        }

                        let dataSignal = await CUSTOMER_REVIEW_MODEL.update({customerReviewID, fullname, position, company, content, status, image: imageID, userID });
                        return res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.INFO_CUSTOMER_REVIEW]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { customerReviewID } = req.params;
                        let dataSignal = await CUSTOMER_REVIEW_MODEL.getInfo({ customerReviewID });
                        res.json(dataSignal);
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.ADD_CONTACT_MESSAGE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { fullname, email, nickname, content } = req.body;

                        let infoContactMessage = await CONTACT_MESSAGE_MODEL.insert({ fullname, email, nickname, content });

                        res.json({ infoContactMessage });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.LIST_CONTACT_MESSAGE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List email - Hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_LIST_CONTACT_MESSAGE,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-contact-message.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { tab } = req.query;

                        let listContactMessage = await CONTACT_MESSAGE_MODEL.getListContactMessage();

                        ChildRouter.renderToView(req, res, {
                            listContactMessage: listContactMessage.data,
                            tab
                        });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.INFO_CONTACT_MESSAGE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Info contact message - Hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_INFO_CONTACT_MESSAGE,
					inc: path.resolve(__dirname, '../../views/inc/admin/info-contact-message.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { contactMessageID } = req.params;

                        let infoContactMessage = await CONTACT_MESSAGE_MODEL.getDetailContactMessage({ contactMessageID });
                        
                        ChildRouter.renderToView(req, res, {
                            infoContactMessage: infoContactMessage.data
                        });
                    }]
                },
            },

            [CF_ROUTINGS_PUBLIC.ADD_EMAIL_CUSTOMER]: {
                config: {
                    auth: [ roles.role.user.bin ],
					type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
                        let { email } = req.body;

                        let infoEmailCustomer = await EMAIL_CUSTOMER_MODEL.insert({ email });

                        res.json({ infoEmailCustomer });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.LIST_EMAIL_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Info email customer - Hailuu',
                    code: CF_ROUTINGS_ACCOUNT.LIST_EMAIL_CUSTOMER,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-email-customer.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listEmailCustomer = await EMAIL_CUSTOMER_MODEL.getListEmailCustomer();

                        ChildRouter.renderToView(req, res, {
                            listEmailCustomer: listEmailCustomer.data,
                            moment
                        });
                    }]
                },
            },

            [CF_ROUTINGS_ACCOUNT.DELETE_EMAIL_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { emailCustomerID } = req.params;
                        let infoEmailCustomerDelete = await EMAIL_CUSTOMER_MODEL.remove({ emailCustomerID });
                        
                        res.json({ infoEmailCustomerDelete });
                    }]
                },
            },

            // ============= API ĐỊA CHỈ ==============
            [CF_ROUTINGS_COMMON.LIST_PROVINCES]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let listProvince = Object.entries(provinces);
                        res.json({ listProvince });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_DISTRICTS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { province } = req.params;
                        let listDistricts = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});

                        if (province && !Number.isNaN(Number(province))) {
                            listDistricts = filterObject(districts, 'parent_code', province.toString())
                        }
                        res.json({ province, listDistricts });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_WARDS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { district } = req.params;
                        let listWards = [];
                        let  filePath = path.resolve(__dirname, `../../config/constants/constant/wards/${district}.json`);
                        fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                res.json({ district, listWards  });
                            } else {
                                res.json({ error: true, message: "district_not_exist" });
                            }
                        });
                    }]
                },
            },

            /**============================= THÊM ĐỊA CHỈ KHÁCH HÀNG */
            /**
             * Function: tạo địa chỉ (API) ADMIN
             * Date: 06/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_CUSTOMER.ADD_ADDRESS_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Add Address - hailuu',
					code: CF_ROUTINGS_CUSTOMER.ADD_ADDRESS_CUSTOMER,
					inc: path.resolve(__dirname, '../../package/customer/views/add_address.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;

                        let listProvince   = COMMON_MODEL.listProvinceAll({  });
                        const infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID });
                        let listAddress = [];
                        if (infoCustomer.error == false && infoCustomer.data.address.length) {
                            for (let address of infoCustomer.data.address) {
                                let listDistrict           = COMMON_MODEL.listDistrict({ province: address.city });
                                let listWard               = await COMMON_MODEL.listWard({ district: address.district });
                                listAddress.push({
                                    listDistrict: Object.entries(listDistrict.data),
                                    listWard: Object.entries(listWard.data),
                                    city: address.city,
                                    district: address.district,
                                    ward: address.ward,
                                    address: address.address,
                                    receiverName: address.receiverName,
                                    receiverPhone: address.receiverPhone,
                                    _id: address._id
                                })
                            }
                        }
                        ChildRouter.renderToView(req, res, { 
                            listProvince: listProvince.data,
                            infoCustomer: infoCustomer.data,
                            infoAddress:  listAddress,
                        });
                    }],
                    post: [ async function (req, res) {
                        const { accountID } = req.params;
                        const { city, district, ward, address } = req.body;

                        let infoAddress = await CUSTOMER_ADDRESS_MODEL.insert({ 
                            accountID, city, district, ward, address
                        });

                        let infoCustomerAfterUpdate = await CUSTOMER_MODEL.updateAddressID({ accountID, addressID: infoAddress.data._id })
                        res.json(infoAddress);
                    }]
                },
            },


             /**
             * Function: Danh sách địa chỉ của users
             * Date: 06/04/2021
             * Dev: SonLP
             */
              [CF_ROUTINGS_CUSTOMER.LIST_ADDRESS_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					title: 'Add Address - hailuu',
					code: CF_ROUTINGS_CUSTOMER.ADD_ADDRESS_CUSTOMER,
					inc: path.resolve(__dirname, '../../package/customer/views/list_address.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { accountID } = req.params;

                        let listProvince   = COMMON_MODEL.listProvinceAll({  });
                        const infoCustomer = await CUSTOMER_MODEL.getInfo({ accountID });
                        let listAddress = [];
                        if (infoCustomer.error == false && infoCustomer.data.address.length) {
                            for (let address of infoCustomer.data.address) {
                                let listDistrict           = COMMON_MODEL.listDistrict({ province: address.city });
                                let listWard               = await COMMON_MODEL.listWard({ district: address.district });
                                listAddress.push({
                                    listDistrict: Object.entries(listDistrict.data),
                                    listWard: Object.entries(listWard.data),
                                    city: address.city,
                                    district: address.district,
                                    ward: address.ward,
                                    address: address.address,
                                    receiverName: address.receiverName,
                                    receiverPhone: address.receiverPhone,
                                    _id: address._id
                                })
                            }
                        }
                        ChildRouter.renderToView(req, res, { 
                            listProvince: listProvince.data,
                            infoCustomer: infoCustomer.data,
                            infoAddress:  listAddress,
                        });
                    }],
                    post: [ async function (req, res) {
                        const { accountID } = req.params;
                        const { city, district, ward, address } = req.body;

                        let infoAddress = await CUSTOMER_ADDRESS_MODEL.insert({ 
                            accountID, city, district, ward, address
                        });

                        let infoCustomerAfterUpdate = await CUSTOMER_MODEL.updateAddressID({ accountID, addressID: infoAddress.data._id })
                        res.json(infoAddress);
                    }]
                },
            },

            /**
             * Function: tạo địa chỉ (API) ENDUSER
             * Date: 06/04/2021
             * Dev: SonLP
             */
            "/api/customer/add-address": {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: customerID } = req.user;
                        let {  receiverName, receiverPhone, city, district, ward, address, addressDefault } = req.body;
                        let infoAddressCustomer = await CUSTOMER_ADDRESS_MODEL.insert({  receiverName, receiverPhone, city, district, ward, address, customerID, addressDefault });
                        res.json(infoAddressCustomer)
                    }]
                },
            },

             /**
             * Function: tạo địa chỉ (API) ENDUSER
             * Dev: depv
             */
            "/api/customer/update-address": {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: customerID } = req.user;
                        let { addressID, receiverName, receiverPhone, city, district, ward, address, addressDefault } = req.body;
                        let infoAddressCustomer = await CUSTOMER_ADDRESS_MODEL.updateEnduer({ addressID, receiverName, receiverPhone, city, district, ward, address, addressDefault, customerID });
                        res.json(infoAddressCustomer)
                    }]
                },
            },

            /**
             * Function: Xóa địa chỉ địa chỉ (API) ENDUSER
             * Dev: depv
             */
            "/api/customer/remove-address/:addressID": {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: customerID } = req.user;
                        let { addressID } = req.params;
                        let infoAddressCustomer = await CUSTOMER_ADDRESS_MODEL.remove({ addressID });
                        res.json(infoAddressCustomer)
                    }]
                },
            },

            /**
             * Function: tạo địa chỉ (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_CUSTOMER.GET_ADDRESS_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { addressID } = req.params;
                        let infoAddressCustomer = await CUSTOMER_ADDRESS_MODEL.getInfoCustomer({ addressID });
                        
                        res.json(infoAddressCustomer)
                    }]
                },
            },

            /**
             * Function: tạo địa chỉ (API)
             * Date: 06/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.UPDATE_ADDRESS_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { addressID } = req.params;
                        const { city, district, ward, address, status, accountID } = req.body;

                        let infoAddressCustomer = await CUSTOMER_ADDRESS_MODEL.update({ addressID, city, district, ward, address });

                        let changeAddressDefault = await CUSTOMER_MODEL.updateChangeAddressDefault({ accountID, addressID, status })

                        res.json(infoAddressCustomer)
                    }]
                },
            },

            //============================ NỘI DUNG CHÍNH SÁCH =======================
            '/hinh-thuc-thanh-toan': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Hình thức thanh toán - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.paymentforms,
                            type: "Hình thức thanh toán"
                        });
                    }]
                },
            },

            '/giao-hang-va-van-chuyen': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Giao hàng và vận chuyển - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.deliveryAndShipping,
                            type: "Giao hàng và vận chuyển"
                        });
                    }]
                },
            },
            
            '/dat-hang-online': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Đạt hàng online - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.orderOnline,
                            type: "Đạt hàng online"
                        });
                    }]
                },
            },

            '/chinh-sach-bao-hanh': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Chinh sách bảo hành - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.warrantyPolicy,
                            type: "Chinh sách bảo hành"
                        });
                    }]
                },
            },

            '/quy-dinh-su-dung': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Quy định sử dụng - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.usageRules,
                            type: "Quy định sử dụng"
                        });
                    }]
                },
            },

            '/bao-mat-thong-tin': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Bảo mật thông tin - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.informationSecurity,
                            type: "Bảo mật thông tin"
                        });
                    }]
                },
            },

            '/chinh-sach-doi-tra': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Chính sách đổi trả - Hailuu',
					inc: path.resolve(__dirname, '../../views/inc/enduser/thong_tin_chung.ejs'),
                    view: 'index-enduser.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let infoCompany = await INFO_COMPANY_COLL.findById("610d6a15d9f9b82e60e97361");
                        ChildRouter.renderToView(req, res, {
                            infoData: infoCompany.refundPolicy,
                            type: "Chính sách đổi trả"
                        });
                    }]
                },
            },
        }
    }
};
