"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { CF_ROUTINGS_VARIANT }                       = require('../../config/constants/routings');

/**
 * MODELS
 */
const VARIANT_MODEL 		                        = require('../../models/variant').MODEL;
const FACE_NUMBER_MODEL 		                    = require('../../models/face_number').MODEL;
const PRINT_SPECIFICATION_MODEL 		            = require('../../models/print_specification').MODEL;
const AMOUNT_MODEL 		                            = require('../../models/amount').MODEL;
const VARIANT_OPTION_MODEL 		                    = require('../../models/variant_option').MODEL;
const VARIANT_V2_MODEL 		                        = require('../../models/variant_v2').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ ORDER ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: List variant (API, VIEW)
             * Date: 14/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_VARIANT.LIST_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					title: 'List Variant - HAILUU',
                    view: 'index-admin.ejs',
					type: 'view',
					code: CF_ROUTINGS_VARIANT.LIST_VARIANT,
					inc: path.resolve(__dirname, '../../views/inc/admin/variant/list_variant.ejs'),
                },
                methods: {
					get : [ async function(req, res){
                        let listFaceNumber = await FACE_NUMBER_MODEL.getList({ });
                        let listPrintSpecification = await PRINT_SPECIFICATION_MODEL.getList({ });
                        let listAmount = await AMOUNT_MODEL.getList({ });
						ChildRouter.renderToView(req, res, {
                            listFaceNumber: listFaceNumber.data,
                            listPrintSpecification: listPrintSpecification.data,
                            listAmount: listAmount.data,
                        });
					}]
                },
            },

              /**
             * Function: List variant (API, VIEW)
             * Date: 14/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_VARIANT.LIST_VARIANT2]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					title: 'List Variant - HAILUU',
                    view: 'index-admin.ejs',
					type: 'view',
					code: CF_ROUTINGS_VARIANT.LIST_VARIANT2,
					inc: path.resolve(__dirname, '../../views/inc/admin/variant/list_variant2.ejs'),
                },
                methods: {
					get : [ async function(req, res){
                        let listData = await VARIANT_OPTION_MODEL.getList({});
						ChildRouter.renderToView(req, res, {
                            listData: listData.data,
                        });
					}]
                },
            },


            /**
             * Function: Create variant (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.ADD_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let arrVariantProduct = JSON.parse(req.body.data);
                        let dataVariant = [];
                        if(arrVariantProduct && arrVariantProduct.length){
                            for (const varient of arrVariantProduct) {
                                let { name, description, faceNumber, printSpecification, amount, mass, priceBefore, priceAfter, productID } = varient;
                                let infoVariantInsert = await VARIANT_MODEL.insert({ name, description, faceNumber, printSpecification, amount, mass, priceBefore, priceAfter, productID });
                                dataVariant = [...dataVariant, infoVariantInsert.data]
                            }
                            res.json(dataVariant);
                        }else{
                            let { name, description, faceNumber, printSpecification, amount, mass, priceBefore, priceAfter, productID } = arrVariantProduct;
                            let infoVariantInsert = await VARIANT_MODEL.insert({ name, description, faceNumber, printSpecification, amount, mass, priceBefore, priceAfter, productID });
                            res.json(infoVariantInsert);
                        }
                        
                        
						
					}]
                },
            },

            /**
             * Function: Lấy thông tin variant (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_VARIANT.GET_INFO_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { variantID } = req.params;
                        let infoVariant = await VARIANT_V2_MODEL.getInfo({ variantID });
						res.json(infoVariant);
					}]
                },
            },

            /**
             * Function: Update variant (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_VARIANT.UPDATE_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let {_id: userID } = req.user;
                        let { variantID } = req.params;
                        let { priceBefore, priceAfter, weight  } = req.body;
                        let infoVariantUpdate = await VARIANT_V2_MODEL.update({ variantID, priceBefore, priceAfter, weight });
						res.json(infoVariantUpdate);
					}]
                },
            },

            /**
             * Function: Update variant (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.REMOVE_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { variantID } = req.params;
                        let infoVariantRemove = await VARIANT_V2_MODEL.updateStatus({ variantID, status: -1 });
						res.json(infoVariantRemove);
					}]
                },
            },

            /**
             * Function: Create face number (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.ADD_FACE_NUMBER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { name, amount } = req.body;
                        let infoFaceNumberInsert = await FACE_NUMBER_MODEL.insert({ name, amount });
						res.json(infoFaceNumberInsert);
					}]
                },
            },

            /**
             * Function: Update face number (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.UPDATE_FACE_NUMBER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { faceNumberID, name, amount, status } = req.body;
                        let infoFaceNumberUpdate = await FACE_NUMBER_MODEL.update({ faceNumberID, name, amount, status });
						res.json(infoFaceNumberUpdate);
					}]
                },
            },

            /**
             * Function: Get info face number (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.GET_INFO_FACE_NUMBER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { faceNumberID } = req.params;
                        let infoFaceNumber = await FACE_NUMBER_MODEL.getInfo({ faceNumberID });
						res.json(infoFaceNumber);
					}]
                },
            },

            /**
             * Function: Remove face number (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.REMOVE_FACE_NUMBER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { faceNumberID } = req.params;
                        let infoFaceNumberRemove = await FACE_NUMBER_MODEL.remove({ faceNumberID });
						res.json(infoFaceNumberRemove);
					}]
                },
            },

            /**
             * Function: Create print specification (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.ADD_PRINT_SPECIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { name, description } = req.body;
                        let infoPrintSpecificationInsert = await PRINT_SPECIFICATION_MODEL.insert({ name, description });
						res.json(infoPrintSpecificationInsert);
					}]
                },
            },

            /**
             * Function: Update print specification (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.UPDATE_PRINT_SPECIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { printSpecificationID, name, description, status } = req.body;
                        let infoPrintSpecificationUpdate = await PRINT_SPECIFICATION_MODEL.update({ printSpecificationID, name, description, status });
						res.json(infoPrintSpecificationUpdate);
					}]
                },
            },

            /**
             * Function: Get info print specification (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.GET_INFO_PRINT_SPECIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { printSpecificationID } = req.params;
                        let infoPrintSpecification = await PRINT_SPECIFICATION_MODEL.getInfo({ printSpecificationID });
						res.json(infoPrintSpecification);
					}]
                },
            },

            /**
             * Function: Remove print specification (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.REMOVE_PRINT_SPECIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { printSpecificationID } = req.params;
                        let infoPrintSpecificationRemove = await PRINT_SPECIFICATION_MODEL.remove({ printSpecificationID });
						res.json(infoPrintSpecificationRemove);
					}]
                },
            },

            /**
             * Function: Create amount (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.ADD_AMOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { name, amount } = req.body;
                        let infoAmountInsert = await AMOUNT_MODEL.insert({ name, amount });
						res.json(infoAmountInsert);
					}]
                },
            },

            /**
             * Function: Update amount (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.UPDATE_AMOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post : [ async function(req, res){
                        let { amountID, name, amount, status } = req.body;
                        let infoAmountUpdate = await AMOUNT_MODEL.update({ amountID, name, amount, status });
						res.json(infoAmountUpdate);
					}]
                },
            },

            /**
             * Function: Update amount (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.GET_INFO_AMOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { amountID } = req.params;
                        let infoAmount = await AMOUNT_MODEL.getInfo({ amountID });
						res.json(infoAmount);
					}]
                },
            },

            /**
             * Function: Remove amount (API)
             * Date: 14/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_VARIANT.REMOVE_AMOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get : [ async function(req, res){
                        let { amountID } = req.params;
                        let infoAmountRemove = await AMOUNT_MODEL.remove({ amountID });
						res.json(infoAmountRemove);
					}]
                },
            },

        }
    }
};
