"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_SLIDE }                         = require('../../config/constants/routings');

/**
 * COLLECTIONS
 */

const SLIDE_COLL = require('../../database/slide-coll');

/**
 * MODELS
 */
const IMAGE_MODEL = require('../../models/image').MODEL;
const SLIDE_MODEL = require('../../models/slide').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            [CF_ROUTINGS_SLIDE.LIST_SILDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					inc: path.resolve(__dirname, '../../views/inc/admin/slide/list-slide.ejs'),
                    view: 'index-admin.ejs',
                    code: CF_ROUTINGS_SLIDE.LIST_SILDE
                },
                methods: {
                    get: [ async function (req, res) {
                        let listSlide  = await SLIDE_MODEL.getListAdmin();
                        ChildRouter.renderToView(req, res, { 
                            listSlide: listSlide.data
                        });
                     }]
                },
            },

             /**
             * Function: Thêm SLIDE
             * Date: 08/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_SLIDE.ADD_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let  data =  JSON.parse(req.body.data)
                        let  {  title, description, link, image  } = data;
                        let infoSlide  = await SLIDE_MODEL.insert({ title, description, link, userID, image  });
                        return res.json(infoSlide);
                    }]
                },
            },

            /**
             * Function: Thêm SLIDE
             * Date: 08/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_SLIDE.INFO_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { slideID } = req.params;
                        let infoSlide  = await SLIDE_MODEL.getInfo({ slideID });
                        return res.json(infoSlide);
                    }]
                },
            },
            
            /**
             * Function: Cập nhật SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_SLIDE.UPDATE_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { slideID } = req.params;
                        let  data =  JSON.parse(req.body.data)
                        let  {  title, description, link, image  } = data;
                        let infoSlide  = await SLIDE_MODEL.update({ slideID, title, description, link, userID, image  });
                        return res.json(infoSlide);
                    }]
                },
            },

            /**
             * Function: Cập nhật SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_SLIDE.UPDATE_STATUS_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { slideID } = req.params;
                        let { status } = data;
                        let infoSlide  = await SLIDE_MODEL.updateStatus({ slideID, status });
                        return res.json(infoSlide);
                    }]
                },
            },

             /**
             * Function: Xóa SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_SLIDE.REMOVE_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { slideID } = req.params;
                        let infoSlide  = await SLIDE_MODEL.remove({ slideID });
                        return res.json(infoSlide);
                    }]
                },
            },

            /**
             * Update order
             * Dattv
             */
             [CF_ROUTINGS_SLIDE.UPDATE_ORDER_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObject } = req.body;
                        let infoResult = await SLIDE_MODEL.updateOrder({ arrayObject });
                        res.json(infoResult);
                    }]
                },
            },

        }
    }
};
