"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_GOOD_PROJECT }                  = require('../../config/constants/routings');

/**
 * COLLECTIONS
 */

const GOOD_PROJECT_COLL = require('../../database/good_project-coll');

/**
 * MODELS
 */
const IMAGE_MODEL = require('../../models/image').MODEL;
const GOOD_PROJECT_MODEL = require('../../models/good_project').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            [CF_ROUTINGS_GOOD_PROJECT.LIST_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					inc: path.resolve(__dirname, '../../views/inc/admin/good_project/list-good-project.ejs'),
                    view: 'index-admin.ejs',
                    code: CF_ROUTINGS_GOOD_PROJECT.LIST_GOOD_PROJECT
                },
                methods: {
                    get: [ async function (req, res) {
                        let listProjectGood  = await GOOD_PROJECT_MODEL.getListAdmin();
                        ChildRouter.renderToView(req, res, { 
                            listProjectGood: listProjectGood.data
                        });
                     }]
                },
            },

             /**
             * Function: Thêm SLIDE
             * Date: 08/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_GOOD_PROJECT.ADD_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let  data =  JSON.parse(req.body.data)
                        let  {  title, description, price, image, type  } = data;
                        let infoGoodProject  = await GOOD_PROJECT_MODEL.insert({ title, description, price, userID, image, type  });
                        return res.json(infoGoodProject);
                    }]
                },
            },

            /**
             * Function: Thêm SLIDE
             * Date: 08/07/2021
             * Dev: Depv
             */
            [CF_ROUTINGS_GOOD_PROJECT.INFO_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { goodProjectID } = req.params;
                        let infoGoodProject  = await GOOD_PROJECT_MODEL.getInfo({ goodProjectID });
                        return res.json(infoGoodProject);
                    }]
                },
            },
            
            /**
             * Function: Cập nhật SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_GOOD_PROJECT.UPDATE_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { goodProjectID } = req.params;
                        let  data =  JSON.parse(req.body.data)
                        let  {  title, description, price, image  } = data;
                        let infoGoodProject  = await GOOD_PROJECT_MODEL.update({ goodProjectID, title, description, price, userID, image  });
                        return res.json(infoGoodProject);
                    }]
                },
            },

            /**
             * Function: Cập nhật SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_GOOD_PROJECT.UPDATE_STATUS_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { goodProjectID } = req.params;
                        let { status } = data;
                        let infoGoodProject  = await GOOD_PROJECT_MODEL.updateStatus({ goodProjectID, status });
                        return res.json(infoGoodProject);
                    }]
                },
            },

             /**
             * Function: Xóa SLIDE
             * Dev: Depv
             */
            [CF_ROUTINGS_GOOD_PROJECT.REMOVE_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { goodProjectID } = req.params;
                        let infoGoodProject  = await GOOD_PROJECT_MODEL.remove({ goodProjectID });
                        return res.json(infoGoodProject);
                    }]
                },
            },

            /**
             * Update order
             * Dattv
             */
             [CF_ROUTINGS_GOOD_PROJECT.UPDATE_ORDER_GOOD_PROJECT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObject } = req.body;
                        let infoResult = await GOOD_PROJECT_MODEL.updateOrder({ arrayObject });
                        res.json(infoResult);
                    }]
                },
            },

        }
    }
};
