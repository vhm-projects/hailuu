"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../child_routing');
const USER_SESSION									= require('../../session/user-session');
const roles                                         = require('../../config/cf_role');
const { ADMIN_LEVEL, ADMIN_STATUS, PRODUCT_STATUS }					= require('../../config/cf_constants');
const { CF_ROUTINGS_ACCOUNT }                       = require('../../config/constants/routings');
const moment                                        = require("moment");

const USER_MODEL                                    = require("../../models/user").MODEL;
const MENU_MODEL                                    = require("../../models/menu").MODEL;
const PRODUCT_MODEL                                 = require("../../models/product").MODEL;
const AMOUNT_MODEL 				                    = require('../../models/amount').MODEL;
const VARIANT_MODEL 				                = require('../../models/variant').MODEL;
const VARIANT_VALUE_MODEL 				            = require('../../models/variant_value').MODEL;
const PRODUCT_VARIANT_VALUE_MODEL 				    = require('../../models/product_variant_value').MODEL;
const PRINT_SPECIFICATION_MODEL 		            = require('../../models/print_specification').MODEL;
const FACE_NUMBER_MODEL 				            = require('../../models/face_number').MODEL;
const CUSTOMER_REVIEW_MODEL 			            = require('../../models/customer_review').MODEL;
const ORDER_MODEL 		                            = require('../../models/order').MODEL;
const VARIANT_OPTION_MODEL 				            = require('../../models/variant_option').MODEL;
const VARIANT_V2_MODEL 				                = require('../../models/variant_v2').MODEL;


const USER_COLL 									= require('../../database/user-coll');
const CUSTOMER_COLL 							    = require('../../database/customer-coll');
const BLOG_COLL 									= require('../../database/blog-coll');
const COMMENT_COLL 									= require('../../database/comment-coll');
const VARIANT_COLL 									= require('../../database/variant-coll');
const VARIANT_VALUE_COLL 							= require('../../database/variant_value-coll');
const PRODUCT_VARIANT_VALUE_COLL 					= require('../../database/product_variant_value-coll');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * 
             * ======================== QUẢN LÝ ACCOUNT  ================================
             *  ***********************
             */

            /**
             * Function: tạo account (API)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.ADD_ACCOUNT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, email, password, level } = req.body;

                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            username, email, password, level 
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

			/**
             * Function: tạo account (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ACCOUNT.ADMIN_ADD_USER]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Add User - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_ADD_USER,
					inc: path.resolve(__dirname, '../../views/inc/admin/create-admin.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						ChildRouter.renderToView(req, res, { 
							infoAdmin: {
								username: '',
								email: '',
								level: 1,
								status: 1
							},
							listGroup: ADMIN_LEVEL,
							listStatus: ADMIN_STATUS
						});
					}]
				},
            },

            /**
             * Function: Thông tin account (API)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.INFO_ACCOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: userID } = req.user;

                        const infoAccount = await USER_MODEL.getInfo({ userID });
                        res.json(infoAccount);
                    }]
                },
            },

            /**
             * Function: Cập nhật account (API)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.UPDATE_ACCOUNT]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, email, password, oldPassword, status, level } = req.body;

                        const infoAfterUpdateAccount = await USER_MODEL.updateUser({ 
                            userID, email, password, oldPassword, status, level 
                        });
                        res.json(infoAfterUpdateAccount);
                    }]
                },
            },

			/**
             * Function: Cập nhật account (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ACCOUNT.UPDATE_ACCOUNT_USER]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Settings Admin - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_SETTINGS,
					inc: path.resolve(__dirname, '../../views/inc/admin/create-admin.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { userID } = req.params;
						const infoUser = await USER_MODEL.getInfo({ userID });

						ChildRouter.renderToView(req, res, { 
							infoAdmin: infoUser.data,
							listGroup: ADMIN_LEVEL,
							listStatus: ADMIN_STATUS
						});
					}],
                },
            },

            /**
             * Function: Xóa account (API)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.DELETE_ACCOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { userID } = req.params;
                        console.log({ userID });
                        const infoAfterDelete = await USER_MODEL.deleteUser({ userID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Danh sách account (API)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.LIST_ACCOUNT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { keyword } = req.query;

                        const listAccounts = await USER_MODEL.getListUsers({ keyword });
                        res.json(listAccounts);
                    }],
                },
            },

			/**
             * Function: Danh sách account (VIEW)
             * Date: 01/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_LIST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List admin - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_LIST,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-admin.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const listAdmins = await USER_MODEL.getListUsers({});

						ChildRouter.renderToView(req, res, {
							admins: listAdmins.data
						});
                    }],
                },
            },

			/**
             * Function: Đăng nhập account (API, VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_SIGNIN]: {
				config: {
					auth: [ roles.role.all.bin ],
					type: 'json',
				},
				methods: {
					post: [ async function (req, res) {
                        const { email, password } = req.body;
                    
                        const infoSignIn = await USER_MODEL.signIn({ email, password });
						if (!infoSignIn.error) {
                            USER_SESSION.saveUser(req.session, {
                                user: infoSignIn.data.user, 
                                token: infoSignIn.data.token,
                            });
                        }

                        res.json(infoSignIn);
                    }],
				},
			},


            /**
             * Function: Đăng nhập account (API, VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_LOGIN]: {
				config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'index.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/admin/home');

                        ChildRouter.renderToView(req, res);
					}]
				},
			},

			/**
             * Function: Trang chủ admin (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_HOME]: {
				config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Admin Manager - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_HOME,
					inc: path.resolve(__dirname, '../../views/inc/admin/index.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [async function (req, res) {
                        let { from, to, type } = req.query;
						/**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						 let isExistLogin = USER_SESSION.getUser(req.session);
						 if (!isExistLogin)
							 return res.redirect('/admin/login');
                        let conditionQuery = {};

                        if(from && to){
                            conditionQuery.createAt = { $gte: moment(from).startOf("day")._d, $lte: moment(to).startOf("day")._d };
                        }
                        
                        let amountCustomer = await CUSTOMER_COLL.count(conditionQuery);
                        let amountBlog = await BLOG_COLL.count(conditionQuery);
                        let amountComment = await COMMENT_COLL.count(conditionQuery);
                        let listOrder = await ORDER_MODEL.getListByFromTo({ from, to });

                        let amountRevenue = 0;
                        let statusNoPayment = 0
                        listOrder.data.forEach(order => {
                            if(order.statusPayment != statusNoPayment){
                                amountRevenue += order.amount;
                            }
                        });

                        let listCustomerOrderMany = await ORDER_MODEL.listCustomerOrderMany({ from, to });
                        let listDataReport = await ORDER_MODEL.listDataOrderReport({ from, to });
                        let dataOrder = await ORDER_MODEL.listDataOrder({ start: from, end: to });

                        ChildRouter.renderToView(req, res, { 
                            amountCustomer, 
                            amountBlog, 
                            amountComment, 
                            amountRevenue: amountRevenue.toLocaleString('vi-VN', {style : 'currency', currency : 'VND'}),
                            listCustomerOrderMany: listCustomerOrderMany.data  ,
                            listDataReport: listDataReport.data,
                            dataOrder: dataOrder.data,
                            from, to, type
                        });
					}]
				},
			},
            
			/**
             * Function: Settings admin (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_PROFILE]: {
				config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Settings Admin - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_PROFILE,
					inc: path.resolve(__dirname, '../../views/inc/admin/create-admin.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						ChildRouter.renderToView(req, res, { 
							infoAdmin: req.user,
							listGroup: ADMIN_LEVEL,
							listStatus: ADMIN_STATUS
						});
					}]
				},
			},

			/**
             * Function: Clear session with render site and script (API)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_SIGNOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'index.ejs'
                },
                methods: {
                    get: [ (req, res) => {
                        USER_SESSION.destroySession(req.session);
                        res.redirect("/admin/login");
                        // ChildRouter.renderToView(req, res);
                    }]
                },
            },


            /**
             * 
             * =================================================================================================   QUẢN LÝ SLIDE  =================================================================================================
             *  ================================================================================================= ************** ==================================================================================================
            */

            /**
             * Function: Thêm Slide
             * Date: 01/04/2021
             * Dev: VinhNH
             */
            [CF_ROUTINGS_ACCOUNT.ADD_SLIDE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                    view: 'index-admin',
                    inc: 'inc/admin/add-slide.ejs',
                    title: 'TRX-STOREFRONT',
                    type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  infoSlide = {}
                        return ChildRouter.renderToView(req, res, {
                            infoSlide
                        });
                    }],
                    post: [ async function (req, res) {
                       let  { _id : userCreateID }              = req.user;
                       let  { image, title, description, link } = req.body;
                       res.bindingRole.config.type = 'json';
                       let resultAdd  = await SLIDE_MODEL.insert({ image, title, description, link, userCreateID  } );
                       return res.json(resultAdd);
                    }]
                },
            },

            /**
             * Function: Danh sách Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_ACCOUNT.GET_LIST_SLIDE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    view: 'index-admin',
                    inc: 'inc/admin/list-slide.ejs',
                    title: 'TRX-STOREFRONT',
                    type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { page } = req.query;
                        let listSlide  = await SLIDE_MODEL.getList({ page : 1 });
                        return ChildRouter.renderToView(req, res, {
                            listSlide : listSlide.data, 
                            currentPage: page,
                            pages : listSlide.page, 
                            perPage : listSlide.perPage 
                        });
                     }]
                },
            },

            /**
             * Function: Cập nhật trạng thái hoạt động Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_ACCOUNT.UPDATE_STATUS_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async  function (req, res) {
                        let  { userUpdateID, slideID, status } = req.body;

                        let resultUpdate  = await SLIDE_MODEL.update({paramsUpdate :{ _id : slideID } ,userUpdateID , status  });
                        return res.json(resultUpdate);
                    }]
                },
            },

            /**
             * Function: Sửa thông tin Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_ACCOUNT.UPDATE_SLIDE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                    view: 'index-admin',
                    inc: 'inc/admin/add-slide.ejs',
                    title: 'TRX-STOREFRONT',
                    type: 'view',
                },
                methods: {
                    get: [async  function (req, res) {
                        let  {  slideID   } = req.query;
                        let infoSlide  = await SLIDE_MODEL.getInfoSlide({ slideID });

                        return ChildRouter.renderToView(req, res, {
                            infoSlide : infoSlide.data
                        });
                    }],
                    post: [async  function (req, res) {
                        let  {  slideID,  image, title, description, link, userUpdateID, status   } = req.body;
                        res.bindingRole.config.type = 'json';
                        let resultUpdate  = await SLIDE_MODEL.update({ paramsUpdate :{ _id : slideID }, image, title, description, link, userUpdateID, status  });
                        return res.json(resultUpdate);
                    }]
                },
            },
            
            /**
             * Function: Xoá Slide
             * Date: 01/04/2021
             * Dev: VinhNH
            */
            [CF_ROUTINGS_ACCOUNT.REMOVE_SLIDE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async  function (req, res) {
                        // let  {  slideID,  image, title, description, link, userUpdateID, status   } = req.body;
                        // let resultUpdate  = await SLIDE_MODEL.update({ paramsUpdate :{ _id : slideID }, image, title, description, link, userUpdateID, status  });
                        return res.json('test');
                    }]
                },
            },

            /**
             * Function: Manage menu (VIEW)
             * Date: 07/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_MANAGE_MENU]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_MANAGE_MENU,
					inc: path.resolve(__dirname, '../../views/inc/admin/manage-menu.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let listMenuLevelHighest = await MENU_MODEL.getlistMenuLevelHighest({ });
						ChildRouter.renderToView(req, res, {
                            listMenuLevelHighest: listMenuLevelHighest.data
						});
                    }],
                },
            },

            /**
             * Function: Manage menu detail (VIEW)
             * Date: 07/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_MANAGE_MENU_DETAIL]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_MANAGE_MENU_DETAIL,
					inc: path.resolve(__dirname, '../../views/inc/admin/manage-menu-detail.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let { menuID } = req.params;
                        //console.log({ menuID });
                        let infoMenu = await MENU_MODEL.getInfo({ menuID })
                        let listMenuChildOfParent = await MENU_MODEL.getlistMenuOfParent({ menuID })
                        let listProduct = await PRODUCT_MODEL.getListProductOfMenu({ menuID });

						ChildRouter.renderToView(req, res, {
                            listMenuChildOfParent: listMenuChildOfParent.data,
                            infoMenu: infoMenu.data,
                            listProduct: listProduct.data
						});
                    }],
                },
            },

            /**
             * Function: Create product (View)
             * Date: 08/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_ADD_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_ADD_PRODUCT,
					inc: path.resolve(__dirname, '../../views/inc/admin/add-product.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let listMenu = await MENU_MODEL.getlistMenuLevelHighest({ status: 1 });
                        let listFaceNumber = await FACE_NUMBER_MODEL.getList({ status: 1 });
                        let listPrintSpecification = await PRINT_SPECIFICATION_MODEL.getList({ status: 1 });
                        let listAmount = await AMOUNT_MODEL.getList({ status: 1 });
                        let listVariantOption = await VARIANT_OPTION_MODEL.getList();

						ChildRouter.renderToView(req, res, {
                            listMenu: listMenu.data,
                            listFaceNumber: listFaceNumber.data,
                            listPrintSpecification: listPrintSpecification.data,
                            listAmount: listAmount.data,
                            listVariantOption: listVariantOption.data,
                            infoProduct: undefined
						});
                    }],
                },
            },

            /**
             * Function: List product (View)
             * Date: 08/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_LIST_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_LIST_PRODUCT,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-product.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let { menuChild, menuParent } = req.query;
                        let listProduct = await PRODUCT_MODEL.getList({ menuID: menuChild });
                        let listCountOrderOfProduct = await PRODUCT_MODEL.getCountOrderOfProduct({ menuID: menuChild });
                        let listMenuLevelHighest = await MENU_MODEL.getlistMenuLevelHighest({ });
						ChildRouter.renderToView(req, res, {
                            listProduct: listProduct.data,
                            listCountOrderOfProduct: listCountOrderOfProduct.data,
                            listMenuLevelHighest: listMenuLevelHighest.data,
                            PRODUCT_STATUS,
                            menuChild, menuParent
						});
                    }],
                },
            },

            /**
             * Function: Info product (View)
             * Date: 08/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_INFO_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_LIST_PRODUCT,
					inc: path.resolve(__dirname, '../../views/inc/admin/detail_product.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let { productID } = req.params;
                        let { menuChild, menuParent } = req.query;
                        let infoProduct = await PRODUCT_MODEL.getInfo({ productID });
                        let listVariantOfProduct = await VARIANT_MODEL.getList({ productID });
                        // let listReviewOfProduct = await CUSTOMER_REVIEW_MODEL.getListByProduct({ productID });
                        
						ChildRouter.renderToView(req, res, {
                            infoProduct: infoProduct.data,
                            listVariantOfProduct: listVariantOfProduct.data,
                            menuChild, menuParent
                            // listReviewOfProduct: listReviewOfProduct.data
						});
                    }],
                },
            },

            /**
             * Function: Update product (View)
             * Date: 08/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ACCOUNT.ADMIN_UPDATE_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Manage menu - hailuu',
					code: CF_ROUTINGS_ACCOUNT.ADMIN_UPDATE_PRODUCT,
					inc: path.resolve(__dirname, '../../views/inc/admin/update-product.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
                        let { productID } = req.params;
                        let listMenu = await MENU_MODEL.getlistMenuLevelHighest({ status: 1 });
                        let infoProduct = await PRODUCT_MODEL.getInfo({ productID });
                        let listVariant = await VARIANT_COLL.find({ product: productID, status: 1 }).sort({ createAt: 1 });

                        let listVariantValue = await VARIANT_VALUE_COLL.find({ variant: listVariant[0]._id , status: 1});
                        let listVariantValueAndProductVariantValue = [];
                        for (const variantValue of listVariantValue) {
                            let infoProductVariantValue = await PRODUCT_VARIANT_VALUE_COLL.findOne({
                                variant_values: [variantValue._id], status: 1
                            })

                            listVariantValueAndProductVariantValue.push({ variantValue, productVariantValue: infoProductVariantValue})
                        }

                        //console.log({ listVariantValueAndProductVariantValue });

                        // let listVariantValueAndProductVariantValue = [];
                        // for (const variant of listVariant) {
                        //     let listVariantValue = await VARIANT_VALUE_COLL.find({ variant: variant._id });
                        //     for (const variantValue of listVariantValue) {
                        //         let infoProductVariantValue = await PRODUCT_VARIANT_VALUE_COLL.find({
                        //             variant_values: [variantValue._id]
                        //         });
                        //         console.log({ variantValue, infoProductVariantValue });
                        //     }
                        // }
						ChildRouter.renderToView(req, res, {
                            listMenu: listMenu.data,
                            infoProduct: infoProduct.data,
                            PRODUCT_STATUS,
                            listVariant,
                            listVariantValueAndProductVariantValue
						});
                    }],
                },
            },

            /**
             * Function: Update variant
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.UPDATE_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variantID, name } = req.body;
                        let infoAfterUpdate = await VARIANT_MODEL.update({ variantID, name });
                        res.json(infoAfterUpdate);
                    }],
                },
            },

            /**
             * Function: Update variant value
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.UPDATE_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variantValueID, name } = req.body;
                        let infoAfterUpdate = await VARIANT_VALUE_MODEL.update({ variantValueID, name });
                        res.json(infoAfterUpdate);
                    }],
                },
            },

             /**
             * Function: Update product variant value
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.UPDATE_PRODUCT_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { productVariantValueID, price, price_at_compare, weight } = req.body;
                        let infoAfterUpdate = await PRODUCT_VARIANT_VALUE_MODEL.update({  productVariantValueID, price, price_at_compare, weight });
                        res.json(infoAfterUpdate);
                    }],
                },
            },
            /**
             * Function: Tạo thuộc tính cha
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.ADD_VARIANT_VALUE_AND_PRODUCT_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variant, product, name, price, price_at_compare } = req.body;
                        let infoVariantValue = await VARIANT_VALUE_MODEL.insert({  name, variant, product });
                        let infoProductVariantValue = await PRODUCT_VARIANT_VALUE_MODEL.insert({  variant_values: [infoVariantValue.data._id], product, price: +price, price_at_compare: +price_at_compare });
                        res.json(infoProductVariantValue);
                    }],
                },
            },

            /**
             * Function: Remove variant value
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.REMOVE_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					get: [ async function (req, res) {
                        let { variantValueID } = req.params;
                        let infoVariantValue = await VARIANT_VALUE_MODEL.remvove({  variantValueID });
                        res.json(infoVariantValue);
                    }],
                },
            },

             /**
             * Function: Cập nhật loại hiển thị variant
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.UPDATE_TYPE_SHOW_VARIANT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variantID, typeShow } = req.body;
                        let infoVariant = await VARIANT_MODEL.update({ variantID, typeShow });
                        res.json(infoVariant);
                    }],
                },
            },

              /**
             * Function: Danh sách productVariantValue
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.LIST_PRODUCT_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { productVariantValueID } = req.body;
                        let listData = await PRODUCT_VARIANT_VALUE_MODEL.getList({ productVariantValueID });
                        res.json(listData);
                    }],
                },
            },

            /**
             * Function: Thêm variant value con
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.ADD_VARIANT_VALUE_CHILD]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variantID, parentID, name, price_at_compare, price, product } = req.body;
                        let infoVariantValue = await VARIANT_VALUE_MODEL.insert({ parentID, name, variant: variantID, product });
                        let infoProductVariantValue = await PRODUCT_VARIANT_VALUE_MODEL.insert({  variant_values: [parentID, infoVariantValue.data._id], product, price: +price, price_at_compare: +price_at_compare });
                        res.json(infoProductVariantValue);
                    }],
                },
            },

            
            /**
             * Function: Danh sách variant value childs
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.lIST_VARIANT_VALUE_CHILD]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
					get: [ async function (req, res) {
                        let { variantValueID } = req.params;
                        let listData = await VARIANT_VALUE_MODEL.listVariantValueChilds({ variantValueID });
                        res.json(listData);
                    }],
                },
            },


             /**
             * Function: Lấy thông tin product variant value
             * Date: 08/04/2021
             */
			[CF_ROUTINGS_ACCOUNT.INFO_PRODUCT_VARIANT_VALUE]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
					post: [ async function (req, res) {
                        let { variant_values } = req.body;
                        let infoData = await PRODUCT_VARIANT_VALUE_MODEL.getInfoByVariant_values({ variant_values });
                        res.json(infoData);
                    }],
                },
            },
        }
    }
};
