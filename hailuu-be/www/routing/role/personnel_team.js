"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_PERSONNEL_TEAM }                = require('../../config/constants/routings');

/**
 * COLLECTIONS
 */

const PERSONNEL_TEAM_COLL                           = require('../../database/personnel_team-coll');

/**
 * MODELS
 */
const IMAGE_MODEL = require('../../models/image').MODEL;
const PERSONNEL_TEAM_MODEL = require('../../models/personnel_team').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            [CF_ROUTINGS_PERSONNEL_TEAM.LIST_PERSONNEL_TEAM]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					inc: path.resolve(__dirname, '../../views/inc/admin/personnel_team/list_personnel.ejs'),
                    view: 'index-admin.ejs',
                    code: CF_ROUTINGS_PERSONNEL_TEAM.LIST_PERSONNEL_TEAM
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type } = req.query;
                        let listPersonnel  = await PERSONNEL_TEAM_MODEL.getList({type});
                        ChildRouter.renderToView(req, res, { 
                            listPersonnel: listPersonnel.data,
                            type
                        });
                     }]
                },
            },

             /**
             * Function: Create personnel
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_PERSONNEL_TEAM.ADD_PERSONNEL_TEAM]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let  data =  JSON.parse(req.body.data)
                        let  {fullname, avatar, position, description, email, phone} = data;
                        let infoPersonnel = await PERSONNEL_TEAM_MODEL.insert({fullname, avatar, position, description, email, phone, userID});
                        return res.json(infoPersonnel);
                    }]
                },
            },

            /**
             * Function: Get info
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_PERSONNEL_TEAM.INFO_PERSONNEL_TEAM]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { personnelID } = req.params;
                        let infoPersonnel  = await PERSONNEL_TEAM_MODEL.getInfo({ personnelID });
                        return res.json(infoPersonnel);
                    }]
                },
            },
            
            /**
             * Function: Update personnel
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_PERSONNEL_TEAM.UPDATE_PERSONNEL_TEAM]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id : userID } = req.user;
                        let { personnelID } = req.params;
                        let data = JSON.parse(req.body.data);
                        let { fullname, avatar, position, description, email, phone } = data;
                        let infoPersonnel  = await PERSONNEL_TEAM_MODEL.update({ personnelID, fullname, avatar, email, position, description, phone, userID });
                        return res.json(infoPersonnel);
                    }]
                },
            },

             /**
             * Function: Remove personnel
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_PERSONNEL_TEAM.REMOVE_PERSONNEL_TEAM]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { personnelID } = req.params;
                        let infoPersonnel  = await PERSONNEL_TEAM_MODEL.remove({ personnelID });
                        return res.json(infoPersonnel);
                    }]
                },
            },

        }
    }
};
