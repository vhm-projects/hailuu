"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_USER_CONTACT }                  = require('../../config/constants/routings');

/**
 * COLLECTIONS
 */

//const ORDER_COLL = require('../../database/order-coll');

/**
 * MODELS
 */
const USER_CONTACT_MODEL = require('../../models/user_contact').MODEL;
const IMAGE_MODEL = require('../../models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ GALLERY ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: THÊM CONTACT (API, VIEW)
             * Date: 15/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_USER_CONTACT.ADD_CONTACT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
                },
                methods: {
					post: [ async function(req, res){
						let { _id: userCreate } = req.user;
						let { fullname, email, phone } = req.body;

                        let dataInsert = { 
                            fullname, 
                            email,
                            phone
                        }

						let infoContactAfterInsert = await USER_CONTACT_MODEL.insert(dataInsert);
						res.json(infoContactAfterInsert);
					}]
                },
            },

             /**
             * Function: Danh sách Contact (API)
             * Date: 08/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_USER_CONTACT.LIST_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { keyword, status } = req.query;
						let listContact = await USER_CONTACT_MODEL.getList();
						res.json(listContact);
					}]
                },
            },

            /**
             * Function: Thông tin Contact (API)
             * Date: 08/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_USER_CONTACT.GET_INFO]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { contactID } = req.params;
						let infoContact = await USER_CONTACT_MODEL.getInfo({ contactID });
						res.json(infoContact);
					}]
                },
            },

            /**
             * Function: Xoá Contact (API)
             * Date: 08/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_USER_CONTACT.REMOVE_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { contactID } = req.params;
						let infoContactAfterRemove = await USER_CONTACT_MODEL.remove({ contactID });
						res.json(infoContactAfterRemove);
					}]
                },
            },

            /**
             * Function: Update status Contact (API)
             * Date: 08/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_USER_CONTACT.UPDATE_STATUS_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { contactID } = req.params;
						let infoContactAfterUpdate = await USER_CONTACT_MODEL.updateStatus({ contactID });
						res.json(infoContactAfterUpdate);
					}]
                },
            },


        }
    }
};
