"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_ACCOUNT }                       = require('../../config/constants/routings');

const POST_COLL     = require('../../database/blog-coll');
const POST_MODEL    = require('../../models/blog').MODEL;

const IMAGE_MODEL   = require('../../models/image').MODEL;
const COMMENT_MODEL = require('../../models/comment').MODEL;

const CATEGORY_COLL = require('../../database/category-coll');
const CATEGORY_MODEL = require('../../models/category').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ BLOG  ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: Tạo post (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.ADD_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Add Post - HAILUU',
					code: CF_ROUTINGS_ACCOUNT.ADD_POST,
					inc: path.resolve(__dirname, '../../views/inc/admin/add-blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res) {
                        const listCategories = await CATEGORY_COLL.find({});

						ChildRouter.renderToView(req, res, {
                            listCategories,
						});
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userCreate } = req.user;
						let { 
							title, category: categoryID, slug, description, content, seo, status, type, pathImage
						} = JSON.parse(req.body.data);

                        // console.log({ req, __bodyData: JSON.parse(req.body.data), __file: req.file, fileName: req.fileName });

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage
                            });
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await POST_MODEL.insert({
							title, categoryID, slug, description, content, image, seo, status, type, userCreate
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Cập nhật post (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.UPDATE_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Update Post - HAILUU',
					code: CF_ROUTINGS_ACCOUNT.UPDATE_POST,
					inc: path.resolve(__dirname, '../../views/inc/admin/update-blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { postID } = req.query;
						let listCategories  = await CATEGORY_MODEL.getList({ status: 1 });
                        let infoPost        = await POST_MODEL.getInfo({ postID });

                        ChildRouter.renderToView(req, res, {
                            infoPost: infoPost.data,
                            listCategories: listCategories.data
                        });
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userUpdate } = req.user;
						let { 
							postID, title, category: categoryID, slug, 
							description, content, seo, status, type, pathImage
						} = JSON.parse(req.body.data);

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
								size, 
								name: req.fileName, 
								path: pathImage
							});
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await POST_MODEL.update({
							postID, title, categoryID, slug, 
							description, content, image, seo, status, type, userUpdate
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Xóa post (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.DELETE_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;

						let dataAfterRemovePost = await POST_MODEL.delete({ postID });
						res.json(dataAfterRemovePost);
					}]
                },
            },

            /**
             * Function: Chi tiết post (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.INFO_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;

						let infoPost = await POST_MODEL.getInfo({ postID });
						res.json(infoPost);
					}]
                },
            },

            /**
             * Function: LIST POST (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.LIST_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Posts - HAILUU',
					code: CF_ROUTINGS_ACCOUNT.LIST_POST,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { keyword, type, category } = req.query;

						let listPosts 		= await POST_MODEL.getList({ keyword, category });
						let countPost 		= await POST_COLL.count();
						let countPostActive = await POST_COLL.count({ status: 1 });
						let countPostLock 	= await POST_COLL.count({ status: 0 });

						if(type === 'API'){
							return res.json({
								listPosts,
								countPost,
								countPostLock,
								countPostActive
							});
						}
						ChildRouter.renderToView(req, res, {
							listPosts: listPosts.data,
							countPost,
							countPostLock,
							countPostActive,
						});
                    }]
                },
            },



             /**
             * =============================== **************** ===============================
             * =============================== QUẢN LÝ CATEGORY ================================
             * =============================== **************** ===============================
             */

             /**
             * Function: Tạo danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.ADD_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { _id: userCreate } = req.user;
						let { name, slug, description, status } = req.body;

						let dataAfterAddCategory = await CATEGORY_MODEL.insert({
							name, slug, description, status, userCreate
						});
						res.json(dataAfterAddCategory);
					}]
                },
            },

             /**
             * Function: Cập nhật danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.UPDATE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { _id: userUpdate } = req.user;
						let { categoryID, name, slug, description, status } = req.body;

						let dataAfterUpdateCategory = await CATEGORY_MODEL.update({
							categoryID, name, slug, description, status, userUpdate
						});
						res.json(dataAfterUpdateCategory);
					}]
                },
            },

             /**
             * Function: Xóa danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.DELETE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { categoryID } = req.query;

						let dataAfterRemoveCategory = await CATEGORY_MODEL.delete({ categoryID });
						res.json(dataAfterRemoveCategory);
					}]
                },
            },

            /**
             * Update order
             * Dattv
             */
             [CF_ROUTINGS_ACCOUNT.UPDATE_ORDER_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObject } = req.body;
                        let infoResult = await CATEGORY_MODEL.updateOrder({ arrayObject });
                        res.json(infoResult);
                    }]
                },
            },

             /**
             * Function: Chi tiết danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.INFO_CATEGORY]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Detail Categories - HAILUU',
					code: CF_ROUTINGS_ACCOUNT.INFO_CATEGORY,
					inc: path.resolve(__dirname, '../../views/inc/admin/detail-category.ejs'),
					view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						let { categoryID, type } = req.query;
						let infoCategory = await CATEGORY_MODEL.getInfo({ categoryID });

						if(type === 'API'){
							return res.json(infoCategory);
						}

						let listPostInCategory = await POST_COLL
							.find({ category: categoryID })
							.populate('category image userCreate')
							.lean();

						ChildRouter.renderToView(req, res, { 
							infoCategory: (infoCategory && infoCategory.data) || {},
							listPostInCategory
						});
					}]
                },
            },

             /**
             * Function: Danh sách danh mục (VIEW, API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.LIST_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Categories - HAILUU',
					code: CF_ROUTINGS_ACCOUNT.LIST_CATEGORY,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-category.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						let { keyword, type } = req.query;

						let listCategories 		= await CATEGORY_MODEL.getList({ keyword });
						let countCategory 		= await CATEGORY_COLL.count({ status: { $nin: [-1] } });
						let countCategoryActive = await CATEGORY_COLL.count({ status: 1 });
						let countCategoryLock 	= await CATEGORY_COLL.count({ status: 0 });

						if(type === 'api'){
							return res.json({
								listCategories,
								countCategory,
								countCategoryActive,
								countCategoryLock
							});
						}

						ChildRouter.renderToView(req, res, { 
							listCategories: listCategories.data,
							countCategory,
							countCategoryActive,
							countCategoryLock
						});
					}]
                },
            },

             /**
             * =============================== **************** ===============================
             * =============================== QUẢN LÝ COMMENT  ===============================
             * =============================== **************** ===============================
             */

             /**
             * Function: Tạo comment (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.ADD_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { blogID, fullname, email, content, star } = req.body;

						if(req.user){
							if(fullname !== req.user.fullname || email !== req.user.email){
								return res.json({ error: true, message: 'Không thể bình luận với tên hoặc email khác khi đã đăng nhập' });
							}
						}

						let dataAfterAdd = await COMMENT_MODEL.insert({
							blogID, fullname, email, content, star
						});
						res.json(dataAfterAdd);
					}]
                },
            },

            /**
             * Function: Cập nhật comment (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.UPDATE_COMMENT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { commentID, status } = req.body;

						let dataAfterUpdate = await COMMENT_MODEL.updateStatus({
							commentID, status
						});
						res.json(dataAfterUpdate);
					}]
                },
            },

			/**
             * Function: Danh sách comment (API)
             * Date: 03/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ACCOUNT.LIST_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID, page } = req.query;

						let listCommentByPost = await COMMENT_MODEL.getListByPostWithPage({ postID, page });
						res.json(listCommentByPost);
					}]
                },
            },

            /**
             * Function: Danh sách comment product (API)
             * Date: 03/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ACCOUNT.LIST_COMMENT_PRODUCT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { productID, page } = req.query;
						let listCommentByProduct = await COMMENT_MODEL.getListByBlogOrProduct({ productID, page });
						res.json(listCommentByProduct);
					}]
                },
            },

            /**
             * Function: Xóa comment (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ACCOUNT.DELETE_COMMENT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { commentID } = req.query;

						let dataAfterDelete = await COMMENT_MODEL.delete({ commentID });
						res.json(dataAfterDelete);
					}]
                },
            },


        }
    }
};
