"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { CF_ROUTINGS_ORDER }                         = require('../../config/constants/routings');

/**
 * MODELS, COLLECTIONS
 */
const ORDER_MODEL 				= require('../../models/order').MODEL;
const ORDER_LINE_MODEL 			= require('../../models/order_line').MODEL;
const IMAGE_MODEL				= require('../../models/image').MODEL;
const PRICE_SHIP_MODEL			= require('../../models/price_ship').MODEL;
const CUSTOMER_ADDRESS_COLL    	= require("../../package/customer/database/customer_address-coll");
const COMMON_MODEL              = require("../../config/constants/constant/common").MODEL;
const TRANSACTION_MODEL			= require('../../models/transaction').MODEL
const ORDER_COLL				= require('../../database/order-coll');
const ORDER_LINE_COLL			= require('../../database/order_line-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

			// '/url-payment': {
            //     config: {
            //         auth: [ roles.role.all.bin ],
			// 		type: 'json',
            //     },
            //     methods: {
			// 		get: [ async function(req, res){

			// 			const infoCreatePaymentUrl = await ORDER_MODEL.createRequestPayment({
			// 				orderId: uuidv4(),
			// 				orderInfo: "Pay with momo - HAILUU",
			// 				amount: "1000000",
			// 			});
			// 			res.json(infoCreatePaymentUrl);
			// 			// let endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
			// 			// let hostname = "https://test-payment.momo.vn"
			// 			// let path = "/gw_payment/transactionProcessor"
			// 		}]
            //     },
            // },

			'/url-return': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { requestId, orderId, orderInfo } = req.query;
						const ipAddr 			= req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.ip;
						const statusErrorPay 	= [1006, 11, 20, 40, 41, 42, 43];

						console.log({ __URL_RETURN_QUERY: req.query, __HEADERS: JSON.stringify(req.headers) });

						const checkStatusTransaction = await ORDER_MODEL.checkStatusTransactionMomo({ requestId, orderId });
						const infoOrder 			 = await ORDER_COLL.findOne({ orderID: orderId, payment: 2 }).lean();
						console.log({ checkStatusTransaction });

						// Thanh toán momo không thành công (cập nhật lại trạng thái cho order_lines và xoá order)
						if(checkStatusTransaction.error || statusErrorPay.includes(checkStatusTransaction.data.resultCode) && infoOrder){
							await ORDER_LINE_COLL.updateMany({ _id: { $in: infoOrder.orderLine } }, { $set: { status: 1 } });
							await ORDER_MODEL.delete({ orderID: infoOrder._id });
							return res.redirect('/gio-hang');
						}

						switch (checkStatusTransaction.data.resultCode) {
							case 0:
								// Cập nhật trạng thái đã thanh toán qua momo cho đơn hàng
								await ORDER_COLL.findOneAndUpdate({ 
									orderID: orderId, payment: 2 
								}, { 
									$set: { statusPayment: 3 } 
								});

								await TRANSACTION_MODEL.insert({
									transactionID: checkStatusTransaction.data.transId,
									orderID: infoOrder._id, 
									customerID: infoOrder.customer,
									amount: infoOrder.amount,
									message: checkStatusTransaction.data.message,
									orderInfo,
									ipAddr,
									status: 1
								});
								return res.redirect('/dat-hang-thanh-cong');
							default:
								await TRANSACTION_MODEL.insert({
									transactionID: checkStatusTransaction.data.transId,
									orderID: infoOrder._id, 
									customerID: infoOrder.customer,
									amount: infoOrder.amount,
									orderInfo,
									ipAddr,
									status: checkStatusTransaction.data.resultCode,
									message: checkStatusTransaction.data.message
								});
								return res.redirect('/don-dat-hang');
						}

						// console.log(`Status: ${res.statusCode}`);
						// console.log(`Headers: ${JSON.stringify()}`);
						// console.log({ __QUERY: req.query })
						// res.json(req.query);
					}]
                },
            },

			'/ipn-return': {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						console.log({ __IPN_RETURN_QUERY: req.query, __IPN_RETURN_body: req.body });
					}]
				}
			},

             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ ORDER ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: Create Order (API)
             * Date: 09/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.ADD_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: customerID } = req.user;
						const {
							orderLine, note, weight, amount, 
							deliveryPrice, payment, addressID, exportVAT
						} = req.body;

						const infoAfterInsert = await ORDER_MODEL.insert({
							orderLine, customerID, note, weight, amount, 
							deliveryPrice, payment, addressID, exportVAT
						});

						// Thanh toán momo
						if(!infoAfterInsert.error && +payment === 2) {
							const infoCreatePaymentUrl = await ORDER_MODEL.createRequestPayment({
								orderId: infoAfterInsert.data.orderID,
								orderInfo: "Thanh toán bằng ví momo - HAILUU",
								amount
							});
							res.json(infoCreatePaymentUrl);
						} else{
							res.json(infoAfterInsert);
						}
					}]
                },
            },

			 /**
             * Function: Update Order (API)
             * Date: 09/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.UPDATE_ORDER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userUpdate } = req.user;
						const {
							orderID, orderLine, note, weight, amount, deliveryPrice, deliveryDate, 
							payment, address, status, statusPayment
						} = req.body;

						let infoAfterUpdate = await ORDER_MODEL.update({
                            orderID, orderLine, note, weight, amount, deliveryPrice, deliveryDate, 
							payment, address, status, statusPayment, userUpdate
						});
						res.json(infoAfterUpdate);
					}]
                },
            },

			/**
             * Function: Delete Order (API)
             * Date: 09/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.DELETE_ORDER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { orderID } = req.query;

						const infoAfterDelete = await ORDER_MODEL.delete({ orderID });
						res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: Info Order (API)
             * Date: 09/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.INFO_ORDER]: {
                config: {
					auth: [ roles.role.admin.bin ],
					title: 'Info Order - HAILUU',
                    view: 'index-admin.ejs',
					type: 'view',
					code: CF_ROUTINGS_ORDER.INFO_ORDER,
					inc: path.resolve(__dirname, '../../views/inc/admin/detail-order.ejs'),
                },
                methods: {
					get: [ async function(req, res){
						const { orderID, type } = req.query;

						const infoOrder = await ORDER_MODEL.getInfo({ orderID });

						let infoAddress = null;
						if(infoOrder.data && infoOrder.data.address){
							infoAddress = await CUSTOMER_ADDRESS_COLL.findById(infoOrder.data.address._id);

							let infoProvince   = await COMMON_MODEL.getInfoProvince({ 
								provinceCode: infoAddress.city 
							});
							let infoDistrict   = await COMMON_MODEL.getInfoDistrict({ 
								districtCode: infoAddress.district 
							});
							let infoWard       = await COMMON_MODEL.getInfoWard({ 
								district: infoAddress.district, wardCode: infoAddress.ward 
							});

							infoAddress = {
								info: infoAddress,
								infoProvince: infoProvince.data, 
								infoDistrict: infoDistrict.data, 
								infoWard: infoWard.data
							}
						}

						if(type === 'API'){
							return res.json(infoOrder);
						}

						ChildRouter.renderToView(req, res, {
							infoOrder: (infoOrder && infoOrder.data) || {},
							infoAddress
						})
					}]
                },
            },

             /**
             * Function: List Order (API)
             * Date: 09/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_ORDER.LIST_ORDER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					title: 'List Order - HAILUU',
                    view: 'index-admin.ejs',
					type: 'view',
					code: CF_ROUTINGS_ORDER.LIST_ORDER,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-order.ejs'),
                },
                methods: {
					get: [ async function(req, res){
						const { keyword, status, statusPayment, type } = req.query;

						const listOrder = await ORDER_MODEL.getListByFilter({
							keyword, status, statusPayment
						});

						if(type === 'API'){
							return res.json(listOrder);
						}

						ChildRouter.renderToView(req, res, {
							listOrder: (listOrder && listOrder.data) || []
						})
					}]
                },
            },


			/**
             * =============================== ******************* ===============================
             * =============================== QUẢN LÝ ORDER-LINE ===============================
             * =============================== ******************* ===============================
             */

			 /**
             * Function: Add to cart (API)
             * Date: 02/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.ADD_TO_CART]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userCreate } = req.user;
						const { productID, variantID } = req.body;

						const infoAfterAdd = await ORDER_LINE_MODEL.addToCart({ productID, variantID, userCreate });
						res.json(infoAfterAdd);
					}]
                },
            },

			/**
             * Function: Update order-line (API)
             * Date: 08/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.UPDATE_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userUpdate } = req.user;
						const { orderLineID, variantID, files } = req.body;

						const infoAfterUpdate = await ORDER_LINE_MODEL.update({ 
							orderLineID, variantID, files, userUpdate 
						});
						res.json(infoAfterUpdate);
					}]
                },
            },

			/**
             * Function: Delete order-line (API)
             * Date: 08/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.DELETE_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { orderLineID } = req.query;

						const infoAfterDelete = await ORDER_LINE_MODEL.delete({ orderLineID });
						res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: Info order-line (API)
             * Date: 08/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.INFO_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { orderLineID } = req.query;

						const infoOrderLine = await ORDER_LINE_MODEL.getInfo({ orderLineID });
						res.json(infoOrderLine);
					}]
                },
            },

			/**
             * Function: List order-line (API)
             * Date: 11/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.LIST_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { _id: customerID } = req.user;

						const listOrderLineByCustomer = await ORDER_LINE_MODEL.getList({ customerID });
						res.json(listOrderLineByCustomer);
					}]
                },
            },

			/**
             * Function: Push file into order-line (API)
             * Date: 22/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.PUSH_FILE_TO_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userUpdate } = req.user;
						const { orderLineID, file } = req.body;

						if(file){
							const infoFile = await IMAGE_MODEL.insert({ 
								name: file.name,
								path: file.path,
								size: file.size,
								type: file.type,
								userUpdate
							});

							await ORDER_LINE_COLL.findByIdAndUpdate(orderLineID, {
								$addToSet: { files: infoFile.data._id }
							}, { new: true });

							return res.json({ error: false, fileID: infoFile.data._id });
						}

						res.json({ error: true, message: 'param_invalid' });
					}]
                },
            },

			/**
             * Function: Push file into order-line (API)
             * Date: 22/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.UPDATE_FILE_TO_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userUpdate } = req.user;
						const { orderLineID, file } = req.body;

						if(file){
							const infoFile = await IMAGE_MODEL.insert({ 
								name: file.name,
								path: file.path,
								size: file.size,
								type: file.type,
								userUpdate
							});

							await ORDER_LINE_COLL.findByIdAndUpdate(orderLineID, {
								$addToSet: { files: infoFile.data._id }
							}, { new: true });

							return res.json({ error: false, fileID: infoFile.data._id });
						}

						res.json({ error: true, message: 'param_invalid' });
					}]
                },
            },

			/**
             * Function: Delete file in order-line (API)
             * Date: 22/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.DELETE_FILE_IN_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { orderLineID, fileID } = req.body;

						const infoAfterDelete = await ORDER_LINE_COLL.findByIdAndUpdate(orderLineID, {
							$pull: { files: fileID }
						}, { new: true });

						return res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: Delete file in order-line (API)
             * Date: 22/07/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.API_DELETE_FILE_IN_ORDER_LINE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { orderLineID, fileID } = req.body;

						const infoAfterDelete = await ORDER_LINE_COLL.findByIdAndUpdate(orderLineID, {
							$pull: { files: fileID }
						}, { new: true });

						return res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: REPORT ORDER BY STATUS || CHART API
             * Date: 27/07/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_ORDER.REPORT_ORDER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { start, end } = req.query;
						const listDataReport = await ORDER_MODEL.listDataOrderReport({ start, end });
						return res.json(listDataReport);
					}]
                },
            },

			/**
             * =============================== ************************** ===============================
             * =============================== QUẢN LÝ BOART PRICE - SHIP ===============================
             * =============================== ************************** ===============================
             */

			/**
             * Function: Tạo bảng giá vận chuyển (API))
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.ADD_PRICE_SHIP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userCreate } = req.user;
						const { name, price, from, to } = req.body;

						const infoAfterInsert = await PRICE_SHIP_MODEL.insert({
							name, price, from, to, userCreate
						});
						return res.json(infoAfterInsert);
					}]
                },
            },

			/**
             * Function: Cập nhật bảng giá vận chuyển (API))
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ORDER.UPDATE_PRICE_SHIP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						const { _id: userUpdate } = req.user;
						const { priceShipID, name, price, from, to, status } = req.body;

						const infoAfterInsert = await PRICE_SHIP_MODEL.update({
							priceShipID, name, price, from, to, status, userUpdate
						});
						return res.json(infoAfterInsert);
					}]
                },
            },

			/**
             * Function: Xoá bảng giá vận chuyển (API))
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ORDER.DELETE_PRICE_SHIP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { priceShipID } = req.query;

						const infoAfterDelete = await PRICE_SHIP_MODEL.delete({ priceShipID });
						return res.json(infoAfterDelete);
					}]
                },
            },

			/**
             * Function: Thông tin bảng giá vận chuyển (API))
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_ORDER.INFO_PRICE_SHIP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						const { priceShipID } = req.query;

						const infoPriceShip = await PRICE_SHIP_MODEL.getInfo({ priceShipID });
						return res.json(infoPriceShip);
					}]
                },
            },

			/**
             * Function: Danh sách bảng giá vận chuyển (VIEW)
             * Date: 05/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_ORDER.LIST_PRICE_SHIP]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					title: 'Danh sách giá vận chuyển - HAILUU',
                    view: 'index-admin.ejs',
					type: 'view',
					code: CF_ROUTINGS_ORDER.LIST_PRICE_SHIP,
					inc: path.resolve(__dirname, '../../views/inc/admin/list-price-ship.ejs'),
                },
                methods: {
					get: [ async function(req, res){
						const listPriceShip = await PRICE_SHIP_MODEL.getList({});

						ChildRouter.renderToView(req, res, {
							listPriceShip: (listPriceShip && listPriceShip.data) || []
						})
					}]
                },
            },

        }
    }
};
