"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const path = require('path');

/**
 * INTERNAL PAKCAGE
 */
const ChildRouter                                   = require('../child_routing');
const roles                                         = require('../../config/cf_role');
const { uploadSingle }	                            = require('../../config/cf_multer');
const { CF_ROUTINGS_GALLERY_PRODUCT }               = require('../../config/constants/routings');

/**
 * COLLECTIONS
 */

//const ORDER_COLL = require('../../database/order-coll');

/**
 * MODELS
 */
const GALLERY_PRODUCT_MODEL = require('../../models/gallery_product').MODEL;
const IMAGE_MODEL = require('../../models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            [CF_ROUTINGS_GALLERY_PRODUCT.LIST_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
					inc: path.resolve(__dirname, '../../views/inc/admin/gallery_product/list_gallery_product.ejs'),
                    view: 'index-admin.ejs',
                    code: CF_ROUTINGS_GALLERY_PRODUCT.LIST_GALLERY_PRODUCT
                },
                methods: {
                    get: [ async function (req, res) {
                        let listGalleryProduct  = await GALLERY_PRODUCT_MODEL.getList({ });
                        ChildRouter.renderToView(req, res, {
                            listGalleryProduct: listGalleryProduct.data
                        });
                     }]
                },
            },

             /**
             * Function: Create Gallery Product
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_GALLERY_PRODUCT.ADD_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let  data =  JSON.parse(req.body.data)
                        let  {name, description, url, avatar} = data;
                        let infoGalleryProduct = await GALLERY_PRODUCT_MODEL.insert({name, url, image: avatar, description, userID});
                        return res.json(infoGalleryProduct);
                    }]
                },
            },

            /**
             * Function: Get info Gallery Product
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_GALLERY_PRODUCT.INFO_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { galleryID } = req.params;
                        let infoGalleryProduct  = await GALLERY_PRODUCT_MODEL.getInfo({ galleryID });
                        return res.json(infoGalleryProduct);
                    }]
                },
            },
            
            /**
             * Function: Update Gallery Product
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_GALLERY_PRODUCT.UPDATE_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id : userID } = req.user;
                        let { galleryID } = req.params;
                        let data = JSON.parse(req.body.data);
                        let { name, avatar, description, url } = data;
                        let infoGalleryProduct  = await GALLERY_PRODUCT_MODEL.update({ galleryID, name, url, image: avatar, description, userID });
                        return res.json(infoGalleryProduct);
                    }]
                },
            },

             /**
             * Function: Remove Gallery Product
             * Date: 23/07/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_GALLERY_PRODUCT.REMOVE_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
                },
                methods: {
                    get: [ async function (req, res) {
                        let  { _id : userID } = req.user;
                        let { galleryID } = req.params;
                        let infoGalleryProduct  = await GALLERY_PRODUCT_MODEL.remove({ galleryID });
                        return res.json(infoGalleryProduct);
                    }]
                },
            },

            /**
             * Update order
             * Dattv
             */
             [CF_ROUTINGS_GALLERY_PRODUCT.UPDATE_ORDER_GALLERY_PRODUCT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { arrayObject } = req.body;
                        console.log({ arrayObject });
                        let infoResult = await GALLERY_PRODUCT_MODEL.updateOrder({ arrayObject });
                        res.json(infoResult);
                    }]
                },
            },

        }
    }
};
